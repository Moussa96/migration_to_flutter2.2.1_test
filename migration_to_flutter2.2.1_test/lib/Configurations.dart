import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

//const String BASE_URL = 'http://192.168.1.131:8014/cyclos/';
// const String BASE_URL = 'http://cashless.cashcall.com.eg:48080/cashless-ict/';
// const String BASE_URL = 'https://cashless.cashcall.com.eg:40443/cashless-ict/';
// const String BASE_URL = 'https://cyclos3.cashcall.com.eg/ict/';

//########################## Testing Environment ###############################
/* const String DOMAIN = 'https://cyclos3.cashcall.com.eg/'; // Cyclos 29 Testing
 const String CONTEXT_ROOT = 'cyclos/'; // Cyclos 29 Testing*/
//########################## Staging Environment ###############################
 const String DOMAIN = 'https://cashlesstst.cashcall.com.eg/'; // 65 Staging
 const String CONTEXT_ROOT = 'cashcall/'; // 65 Staging
//########################## Production Environment ############################
/*const String DOMAIN = 'https://cashless.cashcall.com.eg/'; // 81 Production
const String CONTEXT_ROOT = 'cashless/'; // 81 Production*/
//########################## local Environment ###############################
// const String DOMAIN = 'https://clstst.cashcall.com.eg/'; // 65 Staging
// const String CONTEXT_ROOT = 'cyclos/'; // 65 Staging
//=================================================================================

const String BASE_URL = DOMAIN + CONTEXT_ROOT;
// const String BASE_URL = 'https://cashless.cashcall.com.eg/cashless/';

//########################## Payment gateway Testing Environment ###############################
 const String PAYMENT_GATEWAY_BASE_URL ='https://paygatetst.cashcall.com.eg/atcashcall/payment/';
 const String PAYMENT_GW_MERCHANT_CODE = 'CL';
 const String PAYMENT_GW_API_KEY = '1f1926a4fcc7085642cf19be7e900467de6dab2267e332c5172fae85dd7148e1';
 const String PAYMENT_GW_SECURE_HASH_KEY = '275dfa4c2bdef0d8ec80459e67dcdf9625bedcbdaa2875924aeb56de860ad50f';
//########################## Payment gateway  Production Environment ###############################
/*const String PAYMENT_GATEWAY_BASE_URL ='https://paygate.cashcall.com.eg/atcashcall/payment/';
const String PAYMENT_GW_MERCHANT_CODE = 'CL';
const String PAYMENT_GW_API_KEY = '600f813222a501f269b436c2910acef443e0102078ea93e3caa3da26f20dfc52';
const String PAYMENT_GW_SECURE_HASH_KEY = '439898a1fbc925b3295acf33ef1bda1e6d619ff0398eb6a40962b5ae2757027e';*/
//#####################################################################################
const String MFS_IMGS_BASE_URL = DOMAIN+"cashcall_services/"+CONTEXT_ROOT+"images/";

const String MFS_SCENARIO_BILL = "bill";
const String MFS_SCENARIO_CHARGE = "charge";
const String MFS_SCENARIO_CHARGE_PACKAGES = "charge_packages";
const String MFS_SCENARIO_VOUCHER = "voucher";

const String EXTERNAL_SERVICE_NAME_EN = "EXTERNAL_SERVICE_NAME_EN";
const String EXTERNAL_SERVICE_NAME_AR = "EXTERNAL_SERVICE_NAME_AR";
const String EXTERNAL_SERVICE_CODE = "EXTERNAL_SERVICE_CODE";

const String VERSION_CODE = "1.1.23+23";

const String MFS_PAY_TYPE_EXACT = "Exact";
const String MFS_PAY_TYPE_NORMAL = "Normal";
const String MFS_PAY_TYPE_DOWN_PARTIAL = "DownPartial";
const String MFS_PAY_TYPE_UP_PARTIAL = "UpPartial";

const String SOAP_USERNAME = 'ICT';
const String SOAP_PASSWORD = 'f55b5127531579e9';

const String CASH_OUT_TRANSFER_TYPE_CODE = 'CASH_OUT';
const String CASH_IN_TRANSFER_TYPE_CODE = 'CASH_IN';
const String CASH_IN_POS_TRANSFER_TYPE_CODE = 'CASH_IN_POS';
const String PURCHASE_TRANSFER_TYPE_CODE = 'PURCHASE';
const String MONEY_TRANSFER_TYPE_CODE = 'MONEY_TRANSFER';
const String AUTHORIZED_PURCHASE_TRANSFER_TYPE_CODE = 'AUTHORIZED_PURCHASE';
const String PURCHASE_CASHCALL_SERVICES_TRANSFER_TYPE_CODE = 'PURCHASE_CASHCALL_SERVICES';
const String AGENT_SETTLEMENT_TRANSFER_TYPE_CODE = 'AGENT_SETTLEMENT_COMMUNITY';
const String MERCHANT_SETTLEMENT_TRANSFER_TYPE_CODE = 'MERCHANT_SETTLEMENT_COMMUNITY';
const String LOAD_WALLET_TRANSFER_TYPE_CODE = 'LOAD_WALLET';
const String PAYMENTGATEWAY_ORDER_ID = 'PAYMENTGATEWAY_ORDER_ID';
const String MERCHANT_ORDER_ID = 'MERCHANT_ORDER_ID';
const String MFS_TO_MEMBER_ID = '97946';
// const String MFS_TO_MEMBER_ID = '90653';
/* permission codes*/
const String ADD_MEMBER_PIN = 'ADD_MEMBER_PIN';
const String ADD_MEMBER_QR = 'ADD_MEMBER_QR';
const String BALANCE_ENQUIRY_NFC = 'BALANCE_ENQUIRY_NFC';
const String BALANCE_ENQUIRY_PIN = 'BALANCE_ENQUIRY_PIN';
const String BALANCE_ENQUIRY_QR = 'BALANCE_ENQUIRY_QR';
const String BALANCE_ENQUIRY_VIP = 'BALANCE_ENQUIRY_VIP';
const String CASH_IN_NFC = 'CASH_IN_NFC';
const String CASH_IN_PIN = 'CASH_IN_PIN';
const String CASH_IN_QR = 'CASH_IN_QR';
const String CASH_IN_VIP = 'CASH_IN_VIP';
const String CASH_OUT_NFC = 'CASH_OUT_NFC';
const String CASH_OUT_PIN = 'CASH_OUT_PIN';
const String CASH_OUT_QR = 'CASH_OUT_QR';
const String CASH_OUT_VIP = 'CASH_OUT_VIP';
const String CASHCALL_SERVICES = 'CASHCALL_SERVICES';
const String LOAD_WALLET_FORM = 'LOAD_WALLET_FORM';
const String MARKET_PLACE = 'MARKET_PLACE';
const String MARKET_PLACE_PRODUCTS = 'MARKET_PLACE_PRODUCTS';
const String PAY_TROUGH_SCAN_QR = 'PAY_TROUGH_SCAN_QR';
const String PENDING_PAYMENT_REQUESTS = 'PENDING_PAYMENT_REQUESTS';
const String PROFILE = 'PROFILE';
const String PURCHASE_NFC = 'PURCHASE_NFC';
const String PURCHASE_PIN = 'PURCHASE_PIN';
const String PURCHASE_QR = 'PURCHASE_QR';
const String PURCHASE_VIP = 'PURCHASE_VIP';
const String SETTLEMEN = 'SETTLEMEN';
const String VIEW_TRANSACTION_HISTORY = 'VIEW_TRANSACTION_HISTORY';
const String UNEXPECTED_ERROR = 'UNEXPECTED_ERROR';
const String API_ERROR_CANCEL='API_ERROR_CANCEL';
const String API_ERROR_CONNECT_TIMEOUT='API_ERROR_CONNECT_TIMEOUT';
const String API_ERROR_DEFAULT='API_ERROR_DEFAULT';
const String API_ERROR_RECEIVE_TIMEOUT='API_ERROR_RECEIVE_TIMEOUT';
const String API_ERROR_SEND_TIMEOUT='API_ERROR_SEND_TIMEOUT';



const String CV_POS_RECEIPT_NUMBER = 'POS_RECEIPT_NUMBER';
const String CV_REFERENCE_TRANSACTION_ID_CASHOUT =
    'CASH_OUT_REFERENCE_TRANSACTION_ID';
const String CV_REFERENCE_TRANSACTION_ID_PURCHASE =
    'PURCHASE_REFERENCE_TRANSACTION_ID';
const String CV_USER_IDENTIFIER = 'USER_IDENTIFIER';
const String CV_USER_QR = 'USER_QR';
const String ORDER_ITEMS = 'ORDER_ITEMS';

const String STATIC_QR = 'STATIC_QR';
const String DYNAMIC_QR = 'DYNAMIC_QR';

const String GROUP_AGENT = 'AGENT';
const String GROUP_MERCHANT = 'MERCHANT';
const String GROUP_CONSUMER = 'FULL_MEMBER';
const String SERVER_ERROR ='SERVER_ERROR';
const String NETWORK_ERROR = 'NETWORK_ERROR';

const String VIP_PASSWORD = 'x`\\\$G.X836@nt';
const int API_TIMEOUT_MIN = 5;

/*UI CONFIGURATIONS*/
const TEXT_PRIMARY_COLOR = Color(0xff2d3859);
const PRIMARY_COLOR = Color(0xff222146);
const PALE_GRAY = Color(0xffe8ebf5);
const  LIGHT_GRAY_BLUE= Color(0xff9d9ace);
const  SECOND_COLOR= Color(0xff00d7f8);
const BACKGROUND_COLOR = Color(0xfff4f4f8);
const FONT_COLOR = Color(0xff4a4969);
const ACTIVE_CASHCALL_ITEMS = Color(0xff163c49);
const TEXT_COLOR = Color(0xff98a3a5);
const UNACTIVE_CASHCALL_ITEMS = Color(0xff878787);
const CASHCALL_TITLE = Color(0xff505455);
const NAVBAR_COLOR = Color(0x1a000000);
const LANGUAGE_ALERT_COLOR = Color(0xff8f8f8f);
const  BLUE_COLOR= Color(0xff00a3da);
const SECONDARY_COLOR = Color(0xff0296ce);
const GRAY_COLOR = Color(0x3d000000);
const CURRENCY_HINT_COLOR = Color(0xff5f6869);
const NOTIFICATION_BACKGROUND_COLOR = Color(0xffe5eef5);
const NOTIFICATION_TEXT_COLOR = Color(0xff222425);
const LOGOUT_COLOR = Color(0xff505552);
const DIVIDER_COLOR = Color(0xffd8d8d8);
const BLACK_COLOR = Color(0xff000000);
const HINT_COLOR = Color(0xffc8ccd8);
const BOXBORDER_COLOR = Color(0x33000000);
const GRAY_BOXBORDER_COLOR = Color(0x40000000);
const PAY_NOW_COLOR = Color(0xff6664c7);
const BORDER_COLOR = Color(0xffafb2b5);
const APPBAR_MAIN_COLOR = Color(0xff00aeef);
const MAIN_COLOR = Color(0xff5f5d91);
const APPBAR_MAJOR_COLOR = Color(0x80000000);
const SWITCH_COLOR = Color(0xff0c89b8);
const CASHCALL_BACKGROUND_COLOR = Color(0x1f000000);
const WHITE_COLOR = Color(0xffffffff);
const RED_COLOR = Color(0xffcd1717);
const LANGUAGE_COLOR = Color(0xff0a7aff);
const MAJOR_COLOR = Colors.blue;
const MAJOR_BUTTONS_TEXT_COLOR = Colors.white;

const MaterialColor MINOR_COLOR = const MaterialColor(
  0xFF1976D2,
  const <int, Color>{
    50: const Color(0xFF0070FF),
    100: const Color(0xFF40C4FF),
    200: const Color(0xFF40C4FF),
    300: const Color(0xFF40C4FF),
    400: const Color(0xFF40C4FF),
    500: const Color(0xFF40C4FF),
    600: const Color(0xFF40C4FF),
    700: const Color(0xFF40C4FF),
    800: const Color(0xFF40C4FF),
    900: const Color(0xFF40C4FF),
  },
);

const TextStyle MAJOR_TEXT_STYLE = TextStyle(
  fontSize: 20.0,
);

// NumberFormat MONEY_FORMATTER = NumberFormat("###.0#", "en_US");
NumberFormat MONEY_FORMATTER = NumberFormat("#,##0.00", "en_US");
