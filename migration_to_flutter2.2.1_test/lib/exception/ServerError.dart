import 'package:dio/dio.dart' hide Headers;

import '../Configurations.dart';

class ServerError implements Exception {
  int _statusCode = -1;
  String _errorCode = "";
  String _errorMessage = "";

  ServerError.withError({required DioError error}) {
    _handleError(error);
  }

  getErrorCode() {
    return _errorCode;
  }

  getErrorMessage() {
    return _errorMessage;
  }

  int get statusCode => _statusCode;

  set statusCode(int value) {
    _statusCode = value;
  }

  _handleError(DioError error) {
    switch (error.type) {
       case DioErrorType.cancel:
        _errorCode = API_ERROR_CANCEL;
        _errorMessage = "Request was cancelled";
        break;
      case DioErrorType.connectTimeout:
        _errorCode = API_ERROR_CONNECT_TIMEOUT;
        _errorMessage = "Connection timeout";
        break;
      case DioErrorType.other:
        _errorCode = API_ERROR_DEFAULT;
        _errorMessage =
        "Connection failed due to internet connection";
        break;
      case DioErrorType.receiveTimeout:
        _errorCode = API_ERROR_RECEIVE_TIMEOUT;
        _errorMessage = "Receive timeout in connection";
        break;
      case DioErrorType.response:
        _statusCode = error.response!.statusCode!;
        try{
          if(error.response!.data != null){
            _errorCode = error.response!.data['errorCode'];
            _errorMessage = error.response!.data['errorDetails'];
          }
        }catch(err, stacktrace) {
          _errorCode = UNEXPECTED_ERROR;
          _errorMessage =
          "Received invalid status code: ${error.response!.statusCode!} and stacktrace is $stacktrace";
        }
        break;
      case DioErrorType.sendTimeout:
        _errorCode = API_ERROR_SEND_TIMEOUT;
        _errorMessage = "Receive timeout in send request";
        break;
    }
    return _errorMessage;
  }
}