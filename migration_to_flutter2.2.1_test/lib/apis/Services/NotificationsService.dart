import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/BaseService.dart';
import 'package:cashless/apis/restAbstracted/NotificationRestClient.dart';
import 'package:cashless/exception/ServerError.dart';
import 'package:cashless/models/response/NotificationRes/NotificationRes.dart';
import 'package:cashless/util/Util.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:logger/logger.dart';

import '../RetrofitResponse.dart';

class NotificationsService extends BaseService{
  final logger = Logger();
  late Dio dio;
  late NotificationRestClient _notificationRestClient;

  NotificationsService() {
    dio = new Dio();
    _notificationRestClient = new NotificationRestClient(dio);
  }

  Future<RetrofitResponse<List<NotificationRes>>> getListNotifications() async {
    List<NotificationRes> response;
    try {
      String? authorization =
          await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      response = await _notificationRestClient
          .geNotificationsRest(authorization!)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('NotificationsService getListNotifications', [response]);
    } catch (error, stacktrace) {
      logger.e('NotificationsService getListNotifications', [error, stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }

  Future<RetrofitResponse<NotificationRes>> getNotification(int id) async {
    NotificationRes response;
    try {
      String? authorization =
          await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      response = await _notificationRestClient
          .getSelectedNotification(authorization!, id)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('NotificationsService getListNotifications', [response]);
    } catch (error, stacktrace) {
      logger.e('NotificationsService getNotification', [error, stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }
}
