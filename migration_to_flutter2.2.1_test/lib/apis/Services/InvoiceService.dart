import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/BaseService.dart';
import 'package:cashless/apis/restAbstracted/InvoiceRestClient.dart';
import 'package:cashless/exception/ServerError.dart';
import 'package:cashless/models/response/GetInvoicesResponse.dart';
import 'package:cashless/util/Util.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:logger/logger.dart';

import '../../Configurations.dart';

class InvoiceService extends BaseService{
  final logger = Logger();
  late Dio dio;
  late InvoiceRestClient restClientInvoice;

  InvoiceService() {
    dio = new Dio();
    restClientInvoice = new InvoiceRestClient(dio);
  }

  Future<RetrofitResponse<GetInvoicesResponse>> getInvoices() async {
    GetInvoicesResponse response;
    try {
      String? authorization = await Util.loadFromSharedPreferences(
          Util.SP_Authorization_Token);
      print('authorization' + authorization!);
      response = await restClientInvoice.getInvoicesRest(authorization)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('InvoiceService getInvoices', [response]);
    } catch (error, stacktrace) {
      logger.e('InvoiceService getInvoices', [error, stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }
}