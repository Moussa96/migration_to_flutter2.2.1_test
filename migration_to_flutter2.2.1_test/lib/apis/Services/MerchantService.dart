import 'dart:collection';

import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/BaseService.dart';
import 'package:cashless/apis/restAbstracted/MerchantRestClient.dart';
import 'package:cashless/exception/ServerError.dart';
import 'package:cashless/models/response/ListMerchantsResponse.dart';
import 'package:cashless/util/Util.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:logger/logger.dart';


class MerchantService extends BaseService{
  final logger = Logger();
  late Dio dio;
  late MerchantRestClient restClient;

  MerchantService() {
    dio = new Dio();
    restClient = new MerchantRestClient(dio);
  }

  Future<RetrofitResponse<ListMerchantsResponse>> listMerchants(
      {bool showImages = true,
      bool showCustomFields = true,
      int pageSize = 100,
      int currentPage = 0}) async {
    ListMerchantsResponse response;
    try {
      String? authorization =
          await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      Map<String, dynamic> queries = new HashMap<String, dynamic>();
      queries['showImages'] = showImages;
      queries['showCustomFields'] = showCustomFields;
      queries['pageSize'] = pageSize;
      queries['currentPage'] = currentPage;
      response = await restClient
          .getAllMerchants(authorization!, queries)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('MerchantUseCase listMerchants', [response]);
    } catch (error, stacktrace) {
      logger.e('MerchantUseCase listMerchants', [error, stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }
}
