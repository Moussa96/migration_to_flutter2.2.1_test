import 'dart:convert';

import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/BaseService.dart';
import 'package:cashless/apis/restAbstracted/MfsRestClient.dart';
import 'package:cashless/exception/ServerError.dart';
import 'package:cashless/models/requests/MfsServiceCalculatorRequest.dart';
import 'package:cashless/models/Mfs/MfsCashcallServices.dart';
import 'package:cashless/models/Mfs/MfsCategories.dart';
import 'package:cashless/models/response/MfsServiceCalculatorResponse.dart';
import 'package:cashless/util/Util.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:logger/logger.dart';
import '../../Configurations.dart';

class MfsRestClientImpl extends BaseService{
  final logger = Logger();
  late Dio dio;
  late MfsRestClient _mfsRestClient;

  MfsRestClientImpl() {
    dio = new Dio();
    _mfsRestClient = new MfsRestClient(dio);
  }

  Future<RetrofitResponse<MfsCashcallServices>> getMfs() async {
    late MfsCashcallServices response;
    try {
      String? authorization = await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      response = await _mfsRestClient
          .geMfsResponse(authorization!)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
     logger.i('MfsService getMfs', [response]);
    } catch (error, stacktrace) {

      logger.e('MfsService getMfs', [response,stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }

  Future<RetrofitResponse<MfsCashcallServices>> getMfsCashcallServices() async {
    MfsCashcallServices? response;
    try {
      String? authorization = await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      response = await _mfsRestClient
          .geMfsServices(authorization!)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('MfsService getMfsCashcallServices', [response]);
    } catch (error, stacktrace) {
      print(error);

      logger.e('MfsService getMfsCashcallServices', [response,stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }

  Future<RetrofitResponse<MfsServiceCalculatorResponse>> confirmBillEnquiry(
      MfsServiceCalculatorRequest billEnquiry,
      {String? authorization}) async {
    MfsServiceCalculatorResponse? response;
    try {
      authorization = authorization != null
          ? authorization
          : await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      response = await _mfsRestClient
          .confirmBillEnquiry(billEnquiry, authorization!)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('MfsService confirmBillEnquiry', [response]);
    } catch (error, stacktrace) {
      print(error);
      logger.e('MfsService confirmBillEnquiry', [response,stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }


  Future<RetrofitResponse<MfsServiceCalculatorResponse>> confirmServiceCalculator(
      MfsServiceCalculatorRequest serviceCalculator,
      {String? authorization}) async {
    MfsServiceCalculatorResponse? response;
    try {
      authorization = authorization != null
          ? authorization
          : await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      print(jsonEncode(serviceCalculator));
      response = await _mfsRestClient
          .confirmServiceCalculator(serviceCalculator, authorization!)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('MfsService confirmServiceCalculator', [response]);
    } catch (error, stacktrace) {
      print(error);
      logger.e('MfsService confirmServiceCalculator', [response,stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }

}
