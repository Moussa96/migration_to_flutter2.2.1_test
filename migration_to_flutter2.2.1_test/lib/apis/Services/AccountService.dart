import 'dart:convert';

import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/BaseService.dart';
import 'package:cashless/apis/restAbstracted/AccountRestClient.dart';
import 'package:cashless/exception/ServerError.dart';
import 'package:cashless/models/Account.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/util/Util.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:logger/logger.dart';

import '../../Configurations.dart';

class AccountService extends BaseService {
  final logger = Logger();
  late Dio dio;

  late AccountRestClient restClient;
  AccountService() {
    dio = new Dio();
    restClient = new AccountRestClient(dio);
  }
  Future<RetrofitResponse<List<Account>>> listAccounts({User? user}) async {
    List<Account> response;
    try {
      var userName = user != null ? user.username : null;
      var password = user != null ? user.password : null;
      String? authorization = user != null
          ? 'Basic ' + base64Encode(utf8.encode('$userName:$password'))
          : await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      response = await restClient
          .listAccounts(authorization!)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('AccountService listAccounts', [response]);
    } catch (error, stacktrace) {
      logger.e('AccountService listAccounts', [error, stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError( error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }

  void listAccountss({User? user, required Function processor}) async {
    RetrofitResponse<List<Account>> response = RetrofitResponse();
    try {
      var userName = user != null ? user.username : null;
      var password = user != null ? user.password : null;
      String? authorization = user != null
          ? 'Basic ' + base64Encode(utf8.encode('$userName:$password'))
          : await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      response.setData(await restClient
          .listAccounts(authorization!)
          .timeout(Duration(minutes: API_TIMEOUT_MIN)));
      logger.i('listAccounts listAccounts', [response.data]);
    } catch (error, stacktrace) {
      logger.e('AccountService listAccountss', [error, stacktrace]);
      response..setException(ServerError.withError(error: error as DioError));
      return processor(value: response);
    }
    return processor(value: response);
  }
}
