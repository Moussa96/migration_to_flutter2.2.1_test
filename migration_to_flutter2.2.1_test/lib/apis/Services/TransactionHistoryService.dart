import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/restAbstracted/TransactionHistoryRestClient.dart';
import 'package:cashless/exception/ServerError.dart';
import 'package:cashless/models/response/transactionRes/GetTransHisResCustom.dart';
import 'package:cashless/util/Util.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:logger/logger.dart';
import '../../Configurations.dart';
import 'BaseService.dart';

class TransactionHistoryService extends BaseService{
  final logger = Logger();
  late Dio dio;
  late TransactionHistoryRestClient _transactionHistoryRestClient;

  TransactionHistoryService() {
    dio = new Dio();
    _transactionHistoryRestClient = new TransactionHistoryRestClient(dio);
  }

  Future<RetrofitResponse<GetTransHisResCustom>> getTransactionHistory(
      String accountID, Map<String, dynamic> queries) async {
    GetTransHisResCustom? response;
    try {
      String? authorization =
          await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      print('authorization' + authorization!);
      print("> * the values are authorization, accountID, queries" +
          authorization +
          " " +
          accountID +
          " " +
          queries.toString());
      response = await _transactionHistoryRestClient
          .getTransactionsHistoryByCustomValue(
              authorization, accountID, queries)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('TransactionHistoryService getTransactionHistory', [response]);
    } catch (error, stacktrace) {
      logger.e('TransactionHistoryService getTransactionHistory', [response,stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }
}
