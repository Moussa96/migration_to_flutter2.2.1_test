import 'dart:io';
import 'package:cashless/Configurations.dart';
import 'package:http/http.dart' as http;

class BaseService {
  static const String LOGIN_APIS = 'rest/access/initialData';
  static const String ACCOUNTS_APIS = 'rest/accounts/info';
  static const String TRANSFERS_APIS ='rest/payments/paymentData';
  static const String LIST_CONTACTS_APIS = 'rest/contacts';
  static const String MANAGE_CONTACTS_APIS = 'rest/contacts/byMemberPrincipal/';
  static const String SEARCH_MEMBERS_APIS = 'rest/members/principal/';
  static const String MEMBER_PAYMENT_APIS = 'rest/payments/memberPayment';
  static const String CONFIRM_MEMBER_PAYMENT_APIS = 'rest/payments/confirmMemberPayment';
  static const String CONFIRM_MFS_PAYMENT_API = 'rest/mfs/confirmMfsPayment';
  static const String ENQUIRY_MEMBER_PAYMENT_APIS = 'rest/accounts/';
  static const String MEMBER_INVOICE_APIS = 'rest/invoices/toMe';
  static const String TRANSACTIONS_HISTORY_API = 'rest/accounts/';
  final String REGISTRATION_API = BASE_URL + 'services/members';
  final String RESTPASSWORD_API = BASE_URL + 'services/access';
  static const String LIST_PRODUCTS_API = 'rest/ads';
  final String LIST_MY_PRODUCTS_API = BASE_URL + 'rest/ads/mine';
  static const String LIST_MERCHANTS_API = 'rest/members';
  static const String LIST_NOTIFICATIONS_API = 'rest/messages/toMe';
  final String SELECTED_NOTIFICATION_API = 'rest/messages/';
  static const  String MFS_SERVICES_API = 'rest/mfs/services';
  static const  String MFS_BILL_ENQUIRY = 'rest/mfs/billenquiry';
  static const  String MFS_SERVICE_CALCULATOR = 'rest/mfs/servicecalculator';
  static const String MAKE_SYSTEM_TO_MEMBER_PAYMENT_APIS = 'rest/payments/systemToMemberPayment';
  static const String RESET_PASSWORD_API = 'rest/general/resetPassword/';
  static const String ACCEPT_DENY_REQUEST = 'rest/payments/authorizePayment';

  static const String CONFIRM_SYSTEM_PAYMENT_API ='rest/payments/confirmSystemPayment';
  // final String MFS_PAYMENT_API = "http://localhost:8080/cyclos/rest/payments/confirmSystemPayment";
      //BASE_URL + 'rest/mfs';
  static const String TRANSACTION_DETAILS_API = 'rest/accounts/transferData/';
  final String CHARGE_BACK_API = BASE_URL + 'services/payment';



  void setAuthentication(String authentication) {
    authentication = authentication;
  }

  HttpClient certificate() {
    HttpClient client = new HttpClient();
    client.badCertificateCallback = ((X509Certificate cert, String host, int port) {
      final isValidHost = host == "cyclos3.cashcall.com.eg";
      return isValidHost;
    });
    return   client;
  }

//  @deprecated
//  Future<Map> generateHttpHeaders() async {
//    String authorizationToken = await loadFromSharedPreferences(BaseService.SP_Authorization_Token);
//    Map<String, String> requestHeaders = {SP_Authorization_Token: authorizationToken};
//    return requestHeaders;
//  }

  log(http.Response response, String tag) {
    print('$tag Response status: ${response.statusCode}');
    print('$tag Response body: ${response.body}');
  }
}
