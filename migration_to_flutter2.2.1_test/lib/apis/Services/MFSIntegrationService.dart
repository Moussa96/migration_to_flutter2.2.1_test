import 'dart:convert';
import 'dart:io';
import 'package:cashless/models/services/SystemPayment.dart';
import 'package:cashless/util/Util.dart';
import 'BaseService.dart';
import 'package:http/http.dart' as http;

class MFSIntegrationService extends BaseService {

  Future<String> getServices() async {
    String? authorization = await Util.loadFromSharedPreferences(
        Util.SP_Authorization_Token);
    var response = await http.get(
        Uri.parse(BaseService.MFS_SERVICES_API), headers: {'Authorization': authorization!});
    utf8.decode(response.bodyBytes);
    if (response.statusCode == 200) {
      return utf8.decode(response.bodyBytes);
    } else {
      throw Exception(response);
    }
  }

}