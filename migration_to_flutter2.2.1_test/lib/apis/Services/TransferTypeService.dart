import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/restAbstracted/TransferTypeRestClient.dart';
import 'package:cashless/exception/ServerError.dart';
import 'package:cashless/models/response/TransferRes/TransferTypesResponse.dart';
import 'package:cashless/util/Util.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:logger/logger.dart';
import '../../Configurations.dart';
import 'BaseService.dart';

class TransferTypeService extends BaseService{
  final logger = Logger();
  late Dio dio;
  late TransferTypeRestClient _transferTypeRestClient;

  TransferTypeService() {
    dio = new Dio();
    _transferTypeRestClient = new TransferTypeRestClient(dio);
  }

  Future<RetrofitResponse<TransferTypesResponse>> getTransferTypes() async {
    TransferTypesResponse? response;
    try {
      String? authorization =
          await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      response = await _transferTypeRestClient
          .getTransferType(authorization!)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('TransferTypeService getTransferTypes', [response]);
    } catch (error, stacktrace) {
      logger.e('TransferTypeService getTransferTypes', [response,stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }
}
