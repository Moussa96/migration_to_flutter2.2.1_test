import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/BaseService.dart';
import 'package:cashless/apis/restAbstracted/PaymentRestClient.dart';
import 'package:cashless/exception/ServerError.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/OrderItems.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/Profile.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/models/response/MakePaymentResponse.dart';
import 'package:cashless/models/response/PaymentResponse.dart';
import 'package:cashless/models/services/SystemPayment.dart';
import 'package:cashless/pages/transaction_summary/old_transaction_summary.dart';
import 'package:cashless/util/Util.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';

import 'ContactService.dart';

class PaymentService extends BaseService{
  final logger = Logger();
  late Dio dio;
  late PaymentRestClient restClient;
  PaymentService() {
    dio = new Dio();
    restClient = new PaymentRestClient(dio);
  }
  Future<RetrofitResponse<PaymentResponse>> makeSettlement(
      SystemPayment request) async {
    PaymentResponse? response;
    try {
      String? authorization =
          await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      print('authorization' + authorization!);
      response = await restClient
          .confirmSystemPayment(request, authorization)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('PaymentService makeSettlement', [response]);
    } catch (error, stacktrace) {
      logger.e('PaymentService makeSettlement', [response,stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }

  Future<RetrofitResponse<PaymentResponse>> confirmMemberPayment(
      Payment payment,
      {String? authorization}) async {
    PaymentResponse? response;
    try {
      authorization = authorization != null
          ? authorization
          : await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      response = await restClient
          .confirmMemberPayment(payment, authorization!)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('PaymentService confirmMemberPayment', [response]);
    } catch (error, stacktrace) {
      print(error);
      logger.e('PaymentService confirmMemberPayment', [response,stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }

  Future<RetrofitResponse<PaymentResponse>> confirmMfsPayment(
      Payment payment,
      {required String? authorization}) async {
    PaymentResponse? response;
    try {
      print("///////////////////////////");
      print(jsonEncode(payment));
      authorization = authorization != null
          ? authorization
          : await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      response = await restClient
          .confirmMfsPayment(payment, authorization!)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('PaymentService confirmMfsPayment', [response]);
    } catch (error, stacktrace) {
      print(error);
      logger.e('PaymentService confirmMfsPayment', [response,stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }

  Future<RetrofitResponse<PaymentResponse>> confirmSystemPayment(
      SystemPayment payment,
      {required String? authorization}) async {
    PaymentResponse? response;
    try {
      authorization = authorization != null
          ? authorization
          : await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      response = await restClient
          .confirmSystemPayment(payment, authorization!)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('PaymentService confirmMemberPayment', [response]);
    } catch (error, stacktrace) {
      print(error);
      logger.e('PaymentService confirmMemberPayment', [response,stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }

  Future<RetrofitResponse<MakePaymentResponse>> makePayment(Payment payment,
      {bool member: true}) async {
    MakePaymentResponse? response;
    try {
      String authorization;
      if (member)
        authorization =
            (await Util.loadFromSharedPreferences(Util.SP_Authorization_Token))!;
      else
        authorization =
            'Basic ' + base64Encode(utf8.encode('paymentgw01:1234'));
      print(authorization);
      response = await restClient
          .makePayment(payment, authorization)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('PaymentService makePayment', [response]);
    } catch (error, stacktrace) {
      logger.e('PaymentService makePayment', [response,stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }
  Future<RetrofitResponse<MakePaymentResponse>> makeSystemToMemberPayment(Payment payment) async {
    MakePaymentResponse? response;
    try {
      String? authorization = await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      payment.fromSystem = true;
      response = await restClient
          .makeSystemToMemberPayment(payment, authorization!)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('PaymentService makeSystemToMemberPayment', [response]);
    } catch (error, stacktrace) {
      logger.e('PaymentService makeSystemToMemberPayment', [response,stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }

  Future<http.Response> chargeBack(
      String userName, String password, String transferId) async {
    String authorization =
        'Basic ' + base64Encode(utf8.encode('$userName:$password'));
    String body =
        '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:pay="http://payments.webservices.cashless.strohalm.nl/"> <soapenv:Header/> <soapenv:Body> <pay:chargeback> <transferId>$transferId</transferId></pay:chargeback></soapenv:Body></soapenv:Envelope>';

    var response = await http.post(Uri.parse(CHARGE_BACK_API),
        headers: {
          HttpHeaders.authorizationHeader: authorization,
          HttpHeaders.contentTypeHeader: "application/xml"
        },
        body: body);
    log(response, 'PaymentService');
    return response;
  }

  doPayment(BuildContext context, Payment payment, User user,
      {Function? paymentStatus, OrderItems? orderItems}) async {

    ContactService contactUseCase = ContactService();
    if (payment.transferTypeCode == CASH_IN_TRANSFER_TYPE_CODE) {
      payment.toMemberId = user.id;
      if (payment.posNumber != null) {
        payment.transferTypeCode = CASH_IN_POS_TRANSFER_TYPE_CODE;
        CustomValue customValue =
            CustomValue(CV_POS_RECEIPT_NUMBER, payment.posNumber);
        List<CustomValue> _customValues = [];
        payment.customValues = _customValues;
        payment.customValues!.add(customValue);
      }
    } else if (payment.transferTypeCode == CASH_OUT_TRANSFER_TYPE_CODE) {
      //payment.onbehalfOfMemberId = user.id;
      CustomValue customValue =
          CustomValue(CV_REFERENCE_TRANSACTION_ID_CASHOUT, Util.generateUUID());
      payment.customValues=[];
      payment.customValues!.add(customValue);
    } else if (payment.transferTypeCode == PURCHASE_TRANSFER_TYPE_CODE) {
      //payment.onbehalfOfMemberId = user.id;
      CustomValue customValue = CustomValue(
          CV_REFERENCE_TRANSACTION_ID_PURCHASE, Util.generateUUID());
      payment.customValues=[];
      payment.customValues!.add(customValue);
      if (orderItems != null) {
        CustomValue orderCustomValue =
            CustomValue(ORDER_ITEMS, jsonEncode(orderItems.toJson()));

        payment.customValues!.add(orderCustomValue);
      }
    }
    if (payment.paymentMethod != Payment.NFC_Payment_Method) {
      print((user.username != null ? user.username : "null")! +
          " " + (user.password != null ? user.password : "null")!);
      RetrofitResponse<Profile> res = await contactUseCase
          .login(user.username!, user.password!, savePreferences: false);
      if (res.data != null) {
        if (res.data!.member!.id != null &&
            res.data!.member!.username == user.username) {
          if (payment.transferTypeCode == CASH_IN_TRANSFER_TYPE_CODE ||
              payment.transferTypeCode == CASH_IN_POS_TRANSFER_TYPE_CODE) {
            payment.toMemberId = res.data!.member!.id.toString()!;
            paymentRequest(context, payment, paymentStatus!);
          } else {
            paymentRequest(context, payment, paymentStatus!,
                userName: user.username, password: user.password);
          }
        }
      } else {
        paymentStatus!();
        Scaffold.of(context).showSnackBar(SnackBar(
            content: Text(getTranslated(context, res.getErrorCode)),
            duration: Duration(seconds: 2)));
      }
    } else {
      paymentRequest(context, payment, paymentStatus!);
    }
  }

  void paymentRequest(
      BuildContext context, Payment payment, Function paymentDone,
      {String? userName, String? password}) async {
    Map<String, dynamic> paymentInfo = new HashMap();
    String json = jsonEncode(payment.toJson());

    print("///////////////////////  " + json);
    String? authorization = userName != null && password != null
        ? 'Basic ' + base64Encode(utf8.encode('$userName:$password'))
        : null;
    RetrofitResponse<PaymentResponse> response = await
        confirmMemberPayment(payment, authorization: authorization);
    print("///////////////////////  " +json);
    if (response.statusCode == 200) {
      paymentInfo[getTranslated(context, "from")] =
          response.data!.fromMemberName;
      paymentInfo[getTranslated(context, "to")] = response.data!.toMemberName;
      paymentInfo[getTranslated(context, "total_amount")] = payment.amount;
      //paymentDone();
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => TransactionSummary(paymentInfo)));
      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(getTranslated(context, "success_payment")),
          duration: Duration(seconds: 2)));
    } else {
      paymentDone();
      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(getTranslated(context, "error_payment")),
          duration: Duration(seconds: 2)));
    }
  }
}
