import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/BaseService.dart';
import 'package:cashless/apis/restAbstracted/ProductRestClient.dart';
import 'package:cashless/exception/ServerError.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/models/response/productRes/GetProductsResponse.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:logger/logger.dart';
import '../../Configurations.dart';

class ProductsService extends BaseService{
  final logger = Logger();
  late Dio dio;
  late ProductRestClient _productRestClient;

  ProductsService() {
    dio = new Dio();
    _productRestClient = new ProductRestClient(dio);
  }

  Future<RetrofitResponse<GetProductsResponse>> getProducts() async {
    GetProductsResponse? response;
    try {
      String? authorization =
          await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      //print('authorization' + authorization);
      print("before ");
      response = await _productRestClient
          .geProductsRest(authorization!)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('ProductsService getProducts', [response]);
    } catch (error, stacktrace) {
      logger.e('ProductsService getProducts', [response,stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }

  Future<RetrofitResponse<GetProductsResponse>> getProductById(
      Map<String, dynamic> queries) async {
    GetProductsResponse? response;
    try {
      String? authorization =
          await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      response = await _productRestClient
          .geProductsRestById(authorization!, queries)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('ProductsService getProductById', [response]);
    } catch (error, stacktrace) {
      print(error);
      logger.e('ProductsService getProductById', [response,stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }
}
