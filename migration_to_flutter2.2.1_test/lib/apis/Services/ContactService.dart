import 'dart:convert';
import 'dart:io';
import 'package:cashless/apis/Services/BaseService.dart';
import 'package:cashless/exception/ServerError.dart';
import 'package:cashless/models/Contact.dart';
import 'package:cashless/models/Member.dart';
import 'package:cashless/models/Profile.dart';
import 'package:cashless/models/ResetPasswordResponse.dart';
import 'package:cashless/models/requests/SaveContactRequest.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/util/Util.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:cashless/apis/restAbstracted/ContactRestClient.dart';
import 'package:logger/logger.dart';
import 'package:http/http.dart' as http;
import 'package:xml2json/xml2json.dart';
import '../../Configurations.dart';
import '../RetrofitResponse.dart';

import '../../Configurations.dart';

class ContactService extends BaseService {
  final logger = Logger();
  late Dio dio;
  late ContactRestClient restClient;
  ContactService() {
    dio = new Dio();
    restClient = new ContactRestClient(dio);
  }
  Future<RetrofitResponse<Profile>> login(String userName, String password,
      {bool savePreferences = true}) async {
    Profile profile;
    try {
      String authorization =
          'Basic ' + base64Encode(utf8.encode('$userName:$password'));
      profile = await restClient
          .getProfile(authorization)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('ContactService getContacts', [profile]);
      if (profile != null) {
        if (savePreferences) {
          Util.saveTOSharedPreferences(
              Util.SP_Authorization_Token, authorization);
        }
      }
    } catch (error, stacktrace) {
      logger.e('ContactService login', [error, stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(profile);
  }
  Future<RetrofitResponse<ResetPasswordResponse>> resetPassword(String username) async {
    ResetPasswordResponse resetPasswordResponse;
    try {
      resetPasswordResponse = await restClient
          .resetPassword(username)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('ContactService resetPassword', [resetPasswordResponse]);
    } catch (error, stacktrace) {
      logger.e('ContactService resetPassword', [error, stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(resetPasswordResponse);
  }

  Future<RetrofitResponse<Contact>> addToMyContacts(String contactId) async {
    Contact response;
    try {
      String? authorization =
          await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      SaveContactRequest saveContactRequest = new SaveContactRequest(notes: "");
      response = await restClient
          .saveContact(contactId, saveContactRequest, authorization!)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('ContactService addToMyContacts', [response]);
    } catch (error, stacktrace) {
      logger.e('ContactService addToMyContacts', [error, stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }

  Future<RetrofitResponse<Member>> searchContact(String contactName) async {
    Member response;
    try {
      String? authorization =
          await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      response = await restClient
          .getContact(contactName, authorization!)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('ContactService searchContact', [response]);
    } catch (error, stacktrace) {
      logger.e('ContactService searchContact', [error, stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }

  Future<RetrofitResponse<List<Member>>> getContacts() async {
    List<Member> response;
    try {
      String? authorization =
          await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      response = await restClient
          .getAllContacts(authorization!)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('ContactService getContacts', [response]);
    } catch (error, stacktrace) {
      logger.e('ContactService getContacts', [error, stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }

  Future<http.Response> register(
      String userName, String password, User user) async {
    String authorization =
        'Basic ' + base64Encode(utf8.encode('$userName:$password'));
    String? credentials = user.password;
    print("credentials : " + (credentials == null ? "null" : credentials!));
    String body =
        '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mem="http://members.webservices.cashless.strohalm.nl/"> <soapenv:Header/> <soapenv:Body> <mem:registerMember> <!--Optional:--> <params> <!--Optional:--> <groupId>25</groupId> <!--Optional:--> <username>${user.mobileNumber}</username> <!--Optional:--> <name>${user.name}</name> <!--Optional:--> <loginPassword>${credentials}</loginPassword> <forcedResetPassword>false</forcedResetPassword><!--Optional:--><!--Optional:--><!--<pin></pin>--> <!--Optional:--> <credentials>$credentials</credentials> <!--Zero or more repetitions:--> <fields> <!--Optional:--> <internalName>mobilePhone</internalName> <!--Optional:--> <fieldId>329</fieldId> <!--Optional:--> <displayName>Mobile phone</displayName> <!--Optional:--> <value>${user.mobileNumber}</value> </fields><!--Optional:-->';
    if(user.customValues != null) {
      for (int i = 0; i < user.customValues!.length; i++) {
        print(
            '$i : <internalName>${user.customValues![i]
                .internalName}</internalName> <!--Optional:--> <fieldId>${user
                .customValues![i]
                .fieldId}</fieldId> <!--Optional:--> <displayName>${user
                .customValues![i]
                .displayName}</displayName> <!--Optional:--> <value>${user
                .customValues![i].value}</value>');
        body +=
        '<fields><internalName>${user.customValues![i]
            .internalName}</internalName> <!--Optional:--> <fieldId>${user
            .customValues![i]
            .fieldId}</fieldId> <!--Optional:--> <displayName>${user
            .customValues![i]
            .displayName}</displayName> <!--Optional:--> <value>${user
            .customValues![i].value}</value></fields>';
      }
    }
    body += '</params></mem:registerMember></soapenv:Body></soapenv:Envelope>';
    print(body);
    var response = await http.post(Uri.parse(REGISTRATION_API),
        headers: {
          HttpHeaders.authorizationHeader: authorization,
          HttpHeaders.contentTypeHeader: "application/xml"
        },
        body: body);
    log(response, 'ContactsService#register');
    return response;
  }

  Future<http.Response> search(
      String userName, String password, String userIdentifier) async {
    String authorization =
        'Basic ' + base64Encode(utf8.encode('$userName:$password'));
  String  tempUserUUID = userIdentifier.trim();
    //String credentials = Util.generateString(5);
    //print("credentials : " + credentials);
    // String body =
    //     '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mem="http://members.webservices.cashless.strohalm.nl/"> <soapenv:Header/> <soapenv:Body> <mem:search>        <params>    <fields>    <internalName>${CV_USER_IDENTIFIER}</internalName>    <value>${userIdentifier}</value>    </fields>    </params>    </mem:search></soapenv:Body></soapenv:Envelope>';
    //
    String body='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:mem="http://members.webservices.cashless.strohalm.nl/"> <soapenv:Header/> <soapenv:Body><mem:search><params><fields><internalName>$CV_USER_IDENTIFIER</internalName><value>'+tempUserUUID;
    body +='</value></fields></params></mem:search></soapenv:Body></soapenv:Envelope>';
    print('/////////////////////////////');
    print('the request body is $body');
    var response = await http.post(Uri.parse(REGISTRATION_API),
        headers: {
          HttpHeaders.authorizationHeader: authorization,
          HttpHeaders.contentTypeHeader: "application/xml"
        },
        body: body);
    log(response, 'ContactsService#search');
    return response;
  }

  Future<User?> identifyUser(String userIdentifier) async {
    http.Response response =
        await search(SOAP_USERNAME, SOAP_PASSWORD, userIdentifier);
    Xml2Json xml2json = Xml2Json();
    xml2json.parse(response.body);
    var searchResult = xml2json.toParker();
    var totalCount = jsonDecode(searchResult)['soap:Envelope']['soap:Body']
        ['ns2:searchResponse']['return']['totalCount'];
    if (totalCount == '1') {
      User user = User();
      user.id = jsonDecode(searchResult)['soap:Envelope']['soap:Body']
          ['ns2:searchResponse']['return']['members']['id'];
      user.username = jsonDecode(searchResult)['soap:Envelope']['soap:Body']
          ['ns2:searchResponse']['return']['members']['username'];
      user.name = jsonDecode(searchResult)['soap:Envelope']['soap:Body']
          ['ns2:searchResponse']['return']['members']['name'];
      return user;
    }
  }

  Future<http.Response> restPassword(String userName, String password,
      String principal, String oldPassword, String newPassword) async {
    String authorization =
        'Basic ' + base64Encode(utf8.encode('$userName:$password'));
    String body =
        '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:acc="http://access.webservices.cashless.strohalm.nl/"> <soapenv:Header/> <soapenv:Body> <acc:changeCredentials> <!--Optional:--> <params> <!--Optional:--> <principalType>L</principalType> <!--Optional:--> <principal>${principal}</principal> <!--Optional:--> <oldCredentials>${oldPassword}</oldCredentials> <!--Optional:--> <newCredentials>${newPassword}</newCredentials></params></acc:changeCredentials></soapenv:Body></soapenv:Envelope>';
    print(body);
    var response = await http.post(Uri.parse(RESTPASSWORD_API),
        headers: {
          HttpHeaders.authorizationHeader: authorization,
          HttpHeaders.contentTypeHeader: "application/xml"
        },
        body: body);
    log(response, 'ContactsService#register');
    return response;
  }

  Future<Map<String, dynamic>> getContactInfo() async {
    Map<String, dynamic> userInfo = Map<String, dynamic>();
    userInfo['username'] = await Util.loadFromSharedPreferences(Util.USERNAME);
    userInfo['name'] = await Util.loadFromSharedPreferences(Util.NAME);
    userInfo['email'] = await Util.loadFromSharedPreferences(Util.EMAIL);
    userInfo['defaultAccountID'] =
        await Util.loadFromSharedPreferences(Util.ACCOUNT_ID);
    userInfo['id'] = await Util.loadFromSharedPreferences(Util.USER_ID);
    return userInfo;
  }
}
