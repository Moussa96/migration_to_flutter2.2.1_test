import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/exception/ServerError.dart';
import 'package:cashless/apis/restAbstracted/PendingPaymentsRestClient.dart';
import 'package:cashless/models/requests/PendingStatusRequest.dart';
import 'package:cashless/models/PendingStatusResponse.dart';

import 'package:cashless/models/response/transactionRes/GetTransHisResCustom.dart';
import 'package:cashless/models/response/transactionRes/GetTransHisResCustomElements.dart';
import 'package:cashless/util/Util.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:logger/logger.dart';
import '../../Configurations.dart';
import 'BaseService.dart';

class PendingPaymentsService extends BaseService {
  final logger = Logger();
  late Dio dio;
  late PendingPaymentsRestClient _PendingPaymentsRestClient;

  PendingPaymentsService() {
    dio = new Dio();
    _PendingPaymentsRestClient = new PendingPaymentsRestClient(dio);
  }

  Future<RetrofitResponse<GetTransHisResCustom>> getPendingPayment(Map<String, dynamic> queries) async {
    GetTransHisResCustom? response;
    try {
      String? authorization =
          await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      print('authorization' + authorization!);
      response = await _PendingPaymentsRestClient.getPendingPaymentsRequests(
              authorization,queries)
          .timeout(Duration(minutes: API_TIMEOUT_MIN));
      logger.i('getPendingPayments getPendingPayments', [response]);
    } catch (error, stacktrace) {
      logger.e('getPendingPaymentsRequests getPendingPaymentsRequests',
          [response, stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }


  Future<RetrofitResponse<PendingStatusResponse>> getStatusOfPendingRequest(
      String transferId, PendingStatusRequest request, {String? authorization}) async {
    PendingStatusResponse? response;
    try {
      authorization = authorization != null
          ? authorization
          : await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      // String authorization =
      //     await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
      print('authorization' + authorization!);
      print(transferId);
      print(request.toJson());
      response = await _PendingPaymentsRestClient.getStatusOfPendingRequest(
              transferId, request, authorization);
      logger
          .i('getStatusOfPendingRequest getStatusOfPendingRequest', [response]);
    } catch (error, stacktrace) {
      print(error);
      logger.e('getStatusOfPendingRequest getStatusOfPendingRequest',
          [response, stacktrace]);
      return RetrofitResponse()
        ..setException(ServerError.withError(error: error as DioError));
    }
    return RetrofitResponse()..setData(response);
  }

//  Future<RetrofitResponse<GetTransHisResCustomElements>> getPendingPaymentItem() async {
//    GetTransHisResCustomElements response;
//    try {
//      String authorization =
//      await Util.loadFromSharedPreferences(Util.SP_Authorization_Token);
//      print('authorization' + authorization);
//      response = await _PendingPaymentsRestClient.getPendingPaymentsRequests(
//          authorization)
//          .timeout(Duration(minutes: API_TIMEOUT_MIN));
//      logger.i('getPendingPayments getPendingPayments', [response]);
//    } catch (error, stacktrace) {
//      logger.e('getPendingPaymentsRequests getPendingPaymentsRequests',
//          [response, stacktrace]);
//      return RetrofitResponse()
//        ..setException(ServerError.withError(error: error));
//    }
//    return RetrofitResponse()..setData(response);
//  }

}
