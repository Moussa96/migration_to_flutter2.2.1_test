import 'package:cashless/apis/Services/BaseService.dart';
import 'package:cashless/models/requests/MfsServiceCalculatorRequest.dart';
import 'package:cashless/models/Mfs/MfsCashcallServices.dart';
import 'package:cashless/models/Mfs/MfsCategories.dart';
import 'package:cashless/models/response/MfsServiceCalculatorResponse.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:retrofit/retrofit.dart';

import '../../Configurations.dart';
part 'MfsRestClient.g.dart';



@RestApi(baseUrl: BASE_URL)
abstract class MfsRestClient {
  factory MfsRestClient(Dio dio, {String baseUrl}) = _MfsRestClient;

  @GET(BaseService.MFS_SERVICES_API)
  @Headers(<String, dynamic>{
    "Content-Type": "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<MfsCashcallServices> geMfsResponse(
      @Header("Authorization") String authorization);


  @GET(BaseService.MFS_SERVICES_API)
  @Headers(<String, dynamic>{
    "Content-Type": "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<MfsCashcallServices> geMfsServices(
      @Header("Authorization") String authorization);

  @POST(BaseService.MFS_BILL_ENQUIRY)
  @Headers(<String, dynamic>{
    "Content-Type" : "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<MfsServiceCalculatorResponse> confirmBillEnquiry(@Body() MfsServiceCalculatorRequest billEnquiryRequest ,
      @Header("Authorization") String authorization);

  @POST(BaseService.MFS_SERVICE_CALCULATOR)
  @Headers(<String, dynamic>{
    "Content-Type" : "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<MfsServiceCalculatorResponse> confirmServiceCalculator(@Body() MfsServiceCalculatorRequest serviceCalculatorRequest ,
      @Header("Authorization") String authorization);
}
