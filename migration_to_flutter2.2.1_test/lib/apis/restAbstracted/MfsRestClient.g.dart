// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MfsRestClient.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _MfsRestClient implements MfsRestClient {
  _MfsRestClient(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://cashlesstst.cashcall.com.eg/cashcall/';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<MfsCashcallServices> geMfsResponse(authorization) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<MfsCashcallServices>(Options(
                method: 'GET',
                headers: <String, dynamic>{
                  r'Content-Type': 'application/json',
                  r'Client-Version': '1.1.23+23',
                  r'Authorization': authorization
                },
                extra: _extra,
                contentType: 'application/json')
            .compose(_dio.options, 'rest/mfs/services',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = MfsCashcallServices.fromJson(_result.data!);
    return value;
  }

  @override
  Future<MfsCashcallServices> geMfsServices(authorization) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<MfsCashcallServices>(Options(
                method: 'GET',
                headers: <String, dynamic>{
                  r'Content-Type': 'application/json',
                  r'Client-Version': '1.1.23+23',
                  r'Authorization': authorization
                },
                extra: _extra,
                contentType: 'application/json')
            .compose(_dio.options, 'rest/mfs/services',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = MfsCashcallServices.fromJson(_result.data!);
    return value;
  }

  @override
  Future<MfsServiceCalculatorResponse> confirmBillEnquiry(
      billEnquiryRequest, authorization) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(billEnquiryRequest.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<MfsServiceCalculatorResponse>(Options(
                method: 'POST',
                headers: <String, dynamic>{
                  r'Content-Type': 'application/json',
                  r'Client-Version': '1.1.23+23',
                  r'Authorization': authorization
                },
                extra: _extra,
                contentType: 'application/json')
            .compose(_dio.options, 'rest/mfs/billenquiry',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = MfsServiceCalculatorResponse.fromJson(_result.data!);
    return value;
  }

  @override
  Future<MfsServiceCalculatorResponse> confirmServiceCalculator(
      serviceCalculatorRequest, authorization) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(serviceCalculatorRequest.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<MfsServiceCalculatorResponse>(Options(
                method: 'POST',
                headers: <String, dynamic>{
                  r'Content-Type': 'application/json',
                  r'Client-Version': '1.1.23+23',
                  r'Authorization': authorization
                },
                extra: _extra,
                contentType: 'application/json')
            .compose(_dio.options, 'rest/mfs/servicecalculator',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = MfsServiceCalculatorResponse.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
