import 'package:cashless/apis/Services/BaseService.dart';
import 'package:cashless/models/response/NotificationRes/NotificationRes.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:retrofit/retrofit.dart';

import '../../Configurations.dart';

part 'NotificationRestClient.g.dart';

@RestApi(baseUrl: BASE_URL)
abstract class NotificationRestClient {
  factory NotificationRestClient(Dio dio, {String baseUrl}) =
      _NotificationRestClient;

  @GET(BaseService.LIST_NOTIFICATIONS_API)
  @Headers(<String, dynamic>{
    "Content-Type": "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<List<NotificationRes>> geNotificationsRest(
      @Header("Authorization") String authorization);

  /*
  * 
  * Selected Notification
  * 
  * */
  @Headers(<String, dynamic>{
    "Content-Type": "application/json",
    "Client-Version" : VERSION_CODE
  })
  @GET("rest/messages/{id}")
  Future<NotificationRes> getSelectedNotification(
      @Header("Authorization") String authorization, @Path("id") int id);
}
