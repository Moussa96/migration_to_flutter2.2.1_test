// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'InvoiceRestClient.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _InvoiceRestClient implements InvoiceRestClient {
  _InvoiceRestClient(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://cashlesstst.cashcall.com.eg/cashcall/';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<GetInvoicesResponse> getInvoicesRest(authorization) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<GetInvoicesResponse>(Options(
                method: 'GET',
                headers: <String, dynamic>{
                  r'Content-Type': 'application/json',
                  r'Client-Version': '1.1.23+23',
                  r'Authorization': authorization
                },
                extra: _extra,
                contentType: 'application/json')
            .compose(_dio.options, 'rest/invoices/toMe',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = GetInvoicesResponse.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
