import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/response/MakePaymentResponse.dart';
import 'package:cashless/models/response/PaymentResponse.dart';
import 'package:cashless/models/services/SystemPayment.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart' hide Headers;

import '../../Configurations.dart';
import '../Services/BaseService.dart';
part 'PaymentRestClient.g.dart';

@RestApi(baseUrl: BASE_URL)
abstract class PaymentRestClient{
  factory PaymentRestClient(Dio dio, {String baseUrl}) = _PaymentRestClient;

  @POST(BaseService.CONFIRM_SYSTEM_PAYMENT_API)
  @Headers(<String, dynamic>{
    "Content-Type" : "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<PaymentResponse> confirmSystemPayment(
      @Body() SystemPayment payment
      ,@Header("Authorization") String authorization);

  @POST(BaseService.CONFIRM_MEMBER_PAYMENT_APIS)
  @Headers(<String, dynamic>{
    "Content-Type" : "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<PaymentResponse> confirmMemberPayment(@Body() Payment payment ,
      @Header("Authorization") String authorization);

  @POST(BaseService.CONFIRM_MFS_PAYMENT_API)
  @Headers(<String, dynamic>{
    "Content-Type" : "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<PaymentResponse> confirmMfsPayment(@Body() Payment payment ,
      @Header("Authorization") String authorization);

  @POST(BaseService.MEMBER_PAYMENT_APIS)
  @Headers(<String, dynamic>{
    "Content-Type" : "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<MakePaymentResponse> makePayment(@Body() Payment payment ,
      @Header("Authorization") String authorization);

  @POST(BaseService.MAKE_SYSTEM_TO_MEMBER_PAYMENT_APIS)
  @Headers(<String, dynamic>{
    "Content-Type" : "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<MakePaymentResponse> makeSystemToMemberPayment(@Body() Payment payment ,
      @Header("Authorization") String authorization);
}