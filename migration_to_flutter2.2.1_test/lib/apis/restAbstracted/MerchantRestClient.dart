import 'package:cashless/models/response/ListMerchantsResponse.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart' hide Headers;

import '../../Configurations.dart';
import '../Services/BaseService.dart';
part 'MerchantRestClient.g.dart';

@RestApi(baseUrl: BASE_URL)

abstract class MerchantRestClient{
  factory MerchantRestClient(Dio dio, {String baseUrl}) = _MerchantRestClient;

  @GET('${BaseService.LIST_MERCHANTS_API}')
  @Headers(<String, dynamic>{
    "Content-Type" : "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<ListMerchantsResponse> getAllMerchants(@Header("Authorization") String authorization,
      @Queries() Map<String, dynamic> queries);

}