// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AccountRestClient.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _AccountRestClient implements AccountRestClient {
  _AccountRestClient(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://cashlesstst.cashcall.com.eg/cashcall/';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<List<Account>> listAccounts(authorization) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<List<dynamic>>(
        _setStreamType<List<Account>>(Options(
                method: 'GET',
                headers: <String, dynamic>{
                  r'Client-Version': '1.1.23+23',
                  r'Authorization': authorization
                },
                extra: _extra)
            .compose(_dio.options, 'rest/accounts/info',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    var value = _result.data!
        .map((dynamic i) => Account.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
