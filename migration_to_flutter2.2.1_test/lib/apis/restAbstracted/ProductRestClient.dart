import 'package:cashless/apis/Services/BaseService.dart';
import 'package:cashless/models/response/productRes/GetProductsResponse.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:retrofit/retrofit.dart';

import '../../Configurations.dart';

part 'ProductRestClient.g.dart';

@RestApi(baseUrl: BASE_URL)
abstract class ProductRestClient {
  factory ProductRestClient(Dio dio, {String baseUrl}) = _ProductRestClient;

  @GET(BaseService.LIST_PRODUCTS_API)
  @Headers(<String, dynamic>{
    "Content-Type": "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<GetProductsResponse> geProductsRest(
      @Header("Authorization") String authorization);


  @GET(BaseService.LIST_PRODUCTS_API)
  @Headers(<String, dynamic>{
    "Content-Type": "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<GetProductsResponse> geProductsRestById(
      @Header("Authorization") String authorization,
      @Queries() Map<String, dynamic> queries);
}
