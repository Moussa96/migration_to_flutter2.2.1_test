
import 'package:cashless/models/Account.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart' hide Headers;

import '../../Configurations.dart';
import '../Services/BaseService.dart';
part 'AccountRestClient.g.dart';

@RestApi(baseUrl: BASE_URL)
abstract class AccountRestClient{
  factory AccountRestClient(Dio dio, {String baseUrl}) = _AccountRestClient;
  @GET(BaseService.ACCOUNTS_APIS)
  @Headers(<String, dynamic>{
    "Client-Version" : VERSION_CODE
  })
  Future<List<Account>> listAccounts(@Header("Authorization") String authorization);


}