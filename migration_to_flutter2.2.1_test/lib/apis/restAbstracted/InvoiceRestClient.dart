
import 'package:cashless/apis/Services/BaseService.dart';
import 'package:cashless/models/response/GetInvoicesResponse.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart' hide Headers;

import '../../Configurations.dart';
part 'InvoiceRestClient.g.dart';
@RestApi(baseUrl: BASE_URL)
abstract class InvoiceRestClient{
  factory InvoiceRestClient(Dio dio, {String baseUrl}) = _InvoiceRestClient;
  @GET(BaseService.MEMBER_INVOICE_APIS)
  @Headers(<String, dynamic>{
    "Content-Type": "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<GetInvoicesResponse> getInvoicesRest(
      @Header("Authorization") String authorization);

}
