import 'package:cashless/apis/Services/BaseService.dart';
import 'package:cashless/models/requests/PendingStatusRequest.dart';
import 'package:cashless/models/PendingStatusResponse.dart';
import 'package:cashless/models/response/productRes/GetProductsResponse.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:cashless/models/response/transactionRes/GetTransHisResCustom.dart';
import 'package:retrofit/retrofit.dart';
import '../../Configurations.dart';

part 'PendingPaymentsRestClient.g.dart';

@RestApi(baseUrl: BASE_URL)
abstract class PendingPaymentsRestClient {
  factory PendingPaymentsRestClient(Dio dio, {String baseUrl}) =
      _PendingPaymentsRestClient;

  @GET("/rest/payments/pendingPayments")
  @Headers(<String, dynamic>{
    "Content-Type": "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<GetTransHisResCustom> getPendingPaymentsRequests(
      @Header("Authorization") String authorization,
      @Queries() Map<String, dynamic> queries);

  /*
  *
  *
  *
  *
  * */

//  @POST('${BaseService.ACCEPT_DENY_REQUEST}/{transferId}')
  @POST("rest/payments/authorizePayment/{transferId}")
  @Headers(<String, dynamic>{
    "Content-Type": "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<PendingStatusResponse> getStatusOfPendingRequest(
      @Path("transferId") String transferId,
      @Body() PendingStatusRequest request,
      @Header("Authorization") String authorization);
}
