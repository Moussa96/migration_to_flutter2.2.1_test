import 'package:cashless/models/Profile.dart';
import 'package:cashless/models/ResetPasswordResponse.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:cashless/models/Contact.dart';
import 'package:cashless/models/Member.dart';
import 'package:cashless/models/requests/SaveContactRequest.dart';
import '../../Configurations.dart';
import '../Services/BaseService.dart';
part 'ContactRestClient.g.dart';

@RestApi(baseUrl: BASE_URL)
abstract class ContactRestClient{

  factory ContactRestClient(Dio dio, {String baseUrl}) = _ContactRestClient;
  @GET('${BaseService.LOGIN_APIS}')
  @Headers(<String, dynamic>{
    "Content-Type" : "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<Profile> getProfile(@Header("Authorization") String authorization);

  @GET('${BaseService.SEARCH_MEMBERS_APIS}/{contactName}')
  @Headers(<String, dynamic>{
    "Content-Type" : "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<Member> getContact(@Path() String contactName,@Header("Authorization") String authorization);

  @GET('${BaseService.LIST_CONTACTS_APIS}')
  @Headers(<String, dynamic>{
    "Content-Type" : "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<List<Member>> getAllContacts(@Header("Authorization") String authorization);

  @POST('${BaseService.MANAGE_CONTACTS_APIS}/{contactID}')
  @Headers(<String, dynamic>{
    "Content-Type" : "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<Contact> saveContact(@Path() String contactID,@Body()SaveContactRequest request,@Header("Authorization") String authorization);

  @GET('${BaseService.RESET_PASSWORD_API}/{username}')
  @Headers(<String, dynamic>{
    "Content-Type" : "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<ResetPasswordResponse> resetPassword(@Path() String username);


}