// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'PendingPaymentsRestClient.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _PendingPaymentsRestClient implements PendingPaymentsRestClient {
  _PendingPaymentsRestClient(this._dio, {this.baseUrl}) {
    baseUrl ??= 'https://cashlesstst.cashcall.com.eg/cashcall/';
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<GetTransHisResCustom> getPendingPaymentsRequests(
      authorization, queries) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    queryParameters.addAll(queries);
    final _data = <String, dynamic>{};
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<GetTransHisResCustom>(Options(
                method: 'GET',
                headers: <String, dynamic>{
                  r'Content-Type': 'application/json',
                  r'Client-Version': '1.1.23+23',
                  r'Authorization': authorization
                },
                extra: _extra,
                contentType: 'application/json')
            .compose(_dio.options, '/rest/payments/pendingPayments',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = GetTransHisResCustom.fromJson(_result.data!);
    return value;
  }

  @override
  Future<PendingStatusResponse> getStatusOfPendingRequest(
      transferId, request, authorization) async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(request.toJson());
    final _result = await _dio.fetch<Map<String, dynamic>>(
        _setStreamType<PendingStatusResponse>(Options(
                method: 'POST',
                headers: <String, dynamic>{
                  r'Content-Type': 'application/json',
                  r'Client-Version': '1.1.23+23',
                  r'Authorization': authorization
                },
                extra: _extra,
                contentType: 'application/json')
            .compose(_dio.options, 'rest/payments/authorizePayment/$transferId',
                queryParameters: queryParameters, data: _data)
            .copyWith(baseUrl: baseUrl ?? _dio.options.baseUrl)));
    final value = PendingStatusResponse.fromJson(_result.data!);
    return value;
  }

  RequestOptions _setStreamType<T>(RequestOptions requestOptions) {
    if (T != dynamic &&
        !(requestOptions.responseType == ResponseType.bytes ||
            requestOptions.responseType == ResponseType.stream)) {
      if (T == String) {
        requestOptions.responseType = ResponseType.plain;
      } else {
        requestOptions.responseType = ResponseType.json;
      }
    }
    return requestOptions;
  }
}
