import 'package:cashless/apis/Services/BaseService.dart';
import 'package:cashless/models/response/TransferRes/TransferTypesResponse.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart' hide Headers;

import '../../Configurations.dart';
part 'TransferTypeRestClient.g.dart';

@RestApi(baseUrl: BASE_URL)
abstract class TransferTypeRestClient {
  factory TransferTypeRestClient(Dio dio, {String baseUrl}) = _TransferTypeRestClient;
  @POST(BaseService.TRANSFERS_APIS)
  @Headers(<String, dynamic>{
    "Content-Type" : "application/json",
    "Client-Version" : VERSION_CODE
  })
  Future<TransferTypesResponse> getTransferType(
      @Header("Authorization") String authorization);
}