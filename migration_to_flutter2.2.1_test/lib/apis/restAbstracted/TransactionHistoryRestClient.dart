import 'package:cashless/apis/Services/BaseService.dart';
import 'package:cashless/models/response/transactionRes/GetTransHisResCustom.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:flutter/cupertino.dart';
import 'package:retrofit/http.dart';
import 'package:retrofit/retrofit.dart';

import '../../Configurations.dart';

part 'TransactionHistoryRestClient.g.dart';

@RestApi(baseUrl: BASE_URL)
abstract class TransactionHistoryRestClient {
  factory TransactionHistoryRestClient(Dio dio, {String baseUrl}) =
      _TransactionHistoryRestClient;

  @Headers(<String, dynamic>{
    "Content-Type": "application/json",
    "Client-Version" : VERSION_CODE
  })
  @GET('${BaseService.TRANSACTIONS_HISTORY_API}{accountID}/history')
  Future<GetTransHisResCustom> getTransactionsHistoryByCustomValue(
      @Header("Authorization") String authorization,
      @Path("accountID") String accountID,
      @Queries() Map<String, dynamic> queries);

}
