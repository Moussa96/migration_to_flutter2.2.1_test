import '../exception/ServerError.dart';

class RetrofitResponse<T> {
  ServerError? _error;
  T? data;
  late int _statusCode;


   int get statusCode {
    if(_error != null)
      return _error!.statusCode;
    else
      return this._statusCode;
   }

  set statusCode(int value) {
    _statusCode = value;
  }

  setException(ServerError error) {
    _error = error;
  }

  setData(T data) {
    this.data = data;
    this.statusCode = 200;
  }

  get getException {
    return _error;
  }
  get getErrorCode{
    return _error?.getErrorCode();
  }
  get getErrorMessage{
    return _error?.getErrorMessage();
  }
}