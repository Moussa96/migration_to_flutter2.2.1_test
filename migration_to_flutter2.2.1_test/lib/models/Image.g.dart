// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Image.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Image _$ImageFromJson(Map<String, dynamic> json) {
  return Image(
    id: json['id'] as int,
    caption: json['caption'] as String,
    thumbnailUrl: json['thumbnailUrl'] as String,
    fullUrl: json['fullUrl'] as String,
    lastModified: json['lastModified'] as String,
  );
}

Map<String, dynamic> _$ImageToJson(Image instance) => <String, dynamic>{
      'id': instance.id,
      'caption': instance.caption,
      'thumbnailUrl': instance.thumbnailUrl,
      'fullUrl': instance.fullUrl,
      'lastModified': instance.lastModified,
    };
