class Order {
  String _merchantOrderId;

  Order(this._merchantOrderId);

  String get merchantOrderId => _merchantOrderId;

  set merchantOrderId(String value) {
    _merchantOrderId = value;
  }
  Map<String, dynamic> toJson() =>
      {
        'merchantOrderId': _merchantOrderId,
      };
}

