class CustomProperty {
  String _serviceCode, _accountId;

  CustomProperty(this._serviceCode, this._accountId);
  Map<String,dynamic> toJson() =>
      {
        "serviceCode":_serviceCode,
        "accountId":_accountId
      };
}