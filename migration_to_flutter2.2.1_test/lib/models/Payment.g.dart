// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Payment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Payment _$PaymentFromJson(Map<String, dynamic> json) {
  $checkKeys(json, disallowNullValues: const [
    'transferTypeId',
    'transferTypeCode',
    'toMemberId',
    'onbehalfOfMemberId',
    'amount',
    'fromSystem',
    'description',
    'products',
    'customValues'
  ]);
  return Payment(
    transferTypeId: json['transferTypeId'] as String,
    transferTypeCode: json['transferTypeCode'] as String,
    toMemberId: json['toMemberId'] as String,
    onbehalfOfMemberId: json['onbehalfOfMemberId'] as String,
    amount: (json['amount'] as num).toDouble(),
    fromSystem: json['fromSystem'] as bool,
    description: json['description'] as String,
    products: (json['products'] as List<dynamic>)
        .map((e) => Product.fromJson(e as Map<String, dynamic>))
        .toList(),
    customValues: (json['customValues'] as List<dynamic>)
        .map((e) => CustomValue.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$PaymentToJson(Payment instance) => <String, dynamic>{
      'transferTypeId': instance.transferTypeId,
      'transferTypeCode': instance.transferTypeCode,
      'toMemberId': instance.toMemberId,
      'onbehalfOfMemberId': instance.onbehalfOfMemberId,
      'amount': instance.amount,
      'fromSystem': instance.fromSystem,
      'description': instance.description,
      'products': instance.products,
      'customValues': instance.customValues,
    };
