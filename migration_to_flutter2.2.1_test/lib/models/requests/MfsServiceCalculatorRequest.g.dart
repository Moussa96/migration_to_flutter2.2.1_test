// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MfsServiceCalculatorRequest.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MfsServiceCalculatorRequest _$MfsServiceCalculatorRequestFromJson(
    Map<String, dynamic> json) {
  return MfsServiceCalculatorRequest(
    serviceProviderCode: json['serviceProviderCode'] as String,
    serviceCode: json['serviceCode'] as String,
    transactionValue: json['transactionValue'] as String,
    inParameters: json['inParameters'] as String,
    externalTxnId: json['externalTxnId'] as String,
    externalRefNumber: json['externalRefNumber'] as String,
    userBAccountId: json['userBAccountId'] as String,
  );
}

Map<String, dynamic> _$MfsServiceCalculatorRequestToJson(
        MfsServiceCalculatorRequest instance) =>
    <String, dynamic>{
      'serviceProviderCode': instance.serviceProviderCode,
      'serviceCode': instance.serviceCode,
      'transactionValue': instance.transactionValue,
      'userBAccountId': instance.userBAccountId,
      'externalTxnId': instance.externalTxnId,
      'externalRefNumber': instance.externalRefNumber,
      'inParameters': instance.inParameters,
    };
