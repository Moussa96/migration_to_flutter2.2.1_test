import 'package:json_annotation/json_annotation.dart';
part 'SaveContactRequest.g.dart';

@JsonSerializable()
class SaveContactRequest{
  @JsonKey(name: 'notes')
  String? notes;

  SaveContactRequest({this.notes});

  factory SaveContactRequest.fromJson(Map<String, dynamic> json) => _$SaveContactRequestFromJson(json);
  Map<String, dynamic> toJson() => _$SaveContactRequestToJson(this);
}