import 'package:json_annotation/json_annotation.dart';

part 'PendingStatusRequest.g.dart';

@JsonSerializable()
class PendingStatusRequest {

  @JsonKey(name: 'comments')
  String comments;

  @JsonKey(name: 'isAcceptedPayment')
  bool isAcceptedPayment;

  PendingStatusRequest(this.comments,
      this.isAcceptedPayment); //  PendingStatusRequest({this.notes});



  factory PendingStatusRequest.fromJson(Map<String, dynamic> json) =>
      _$PendingStatusRequestFromJson(json);

  Map<String, dynamic> toJson() => _$PendingStatusRequestToJson(this);
}
