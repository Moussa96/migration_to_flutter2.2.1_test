import '../CustomProperty.dart';
import '../Order.dart';


class CreateOrderRequest{
  late String _currency;
  late String _amount;
  late String _paymentMethod;
  late String _language;
  late Order _order;
  List<CustomProperty>? _customProperties = [];
  late String _signature;


  List<CustomProperty>? get customProperties => _customProperties;

  String get currency => _currency;

  set currency(String value) {
    _currency = value;
  }


  CreateOrderRequest(this._amount){
    _currency = "EGP";
    _paymentMethod = "";
    _language = "ar-eg";
  }

  Map<String, dynamic> toJson() =>
      {
        'currency': _currency,
        'amount': _amount,
        'paymentMethod' : _paymentMethod,
        'language' : _language,
        'order' : _order,
        'customProperties' : _customProperties,
        'signature' : _signature
      };

  String get amount => _amount;

  String get signature => _signature;

  set signature(String value) {
    _signature = value;
  }


  Order get order => _order;

  set order(Order value) {
    _order = value;
  }

  String get language => _language;

  set language(String value) {
    _language = value;
  }

  String get paymentMethod => _paymentMethod;

  set paymentMethod(String value) {
    _paymentMethod = value;
  }

  set amount(String value) {
    _amount = value;
  }
}