// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'PendingStatusRequest.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PendingStatusRequest _$PendingStatusRequestFromJson(Map<String, dynamic> json) {
  return PendingStatusRequest(
    json['comments'] as String,
    json['isAcceptedPayment'] as bool,
  );
}

Map<String, dynamic> _$PendingStatusRequestToJson(
        PendingStatusRequest instance) =>
    <String, dynamic>{
      'comments': instance.comments,
      'isAcceptedPayment': instance.isAcceptedPayment,
    };
