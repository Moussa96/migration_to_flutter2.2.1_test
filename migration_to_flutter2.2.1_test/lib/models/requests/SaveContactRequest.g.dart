// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'SaveContactRequest.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SaveContactRequest _$SaveContactRequestFromJson(Map<String, dynamic> json) {
  return SaveContactRequest(
    notes: json['notes'] as String,
  );
}

Map<String, dynamic> _$SaveContactRequestToJson(SaveContactRequest instance) =>
    <String, dynamic>{
      'notes': instance.notes,
    };
