import 'package:json_annotation/json_annotation.dart';

part 'MfsServiceCalculatorRequest.g.dart';

@JsonSerializable()
class MfsServiceCalculatorRequest {
  @JsonKey(name: 'serviceProviderCode')
  String? serviceProviderCode;
  @JsonKey(name: 'serviceCode')
  String? serviceCode;
  @JsonKey(name: 'transactionValue')
  String? transactionValue;
  @JsonKey(name: 'userBAccountId')
  String? userBAccountId;
  @JsonKey(name: 'externalTxnId')
  String? externalTxnId;
  @JsonKey(name: 'externalRefNumber')
  String? externalRefNumber;
  @JsonKey(name: 'inParameters')
  String? inParameters;


  MfsServiceCalculatorRequest({
    this.serviceProviderCode,
    this.serviceCode,
    this.transactionValue,
    this.inParameters,
    this.externalTxnId,
    this.externalRefNumber,
    this.userBAccountId
  }); //  PendingStatusRequest({this.notes});



  factory MfsServiceCalculatorRequest.fromJson(Map<String, dynamic> json) =>
      _$MfsServiceCalculatorRequestFromJson(json);

  Map<String, dynamic> toJson() => _$MfsServiceCalculatorRequestToJson(this);
}
