import 'package:json_annotation/json_annotation.dart';

part 'PendingStatusResponse.g.dart';

@JsonSerializable()
class PendingStatusResponse {
  @JsonKey(name: 'id')
  int id;

  @JsonKey(name: 'pending')
  bool pending;

  @JsonKey(name: 'fromMemberName')
  String fromMemberName;

  @JsonKey(name: 'toMemberName')
  String toMemberName;

  PendingStatusResponse(
      this.id, this.pending, this.fromMemberName, this.toMemberName);

  factory PendingStatusResponse.fromJson(Map<String, dynamic> json) =>
      _$PendingStatusResponseFromJson(json);

  Map<String, dynamic> toJson() => _$PendingStatusResponseToJson(this);
}
