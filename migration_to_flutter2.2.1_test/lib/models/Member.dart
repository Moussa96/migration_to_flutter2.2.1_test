import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/Image.dart';
import 'package:json_annotation/json_annotation.dart';
part 'Member.g.dart';

@JsonSerializable()
class Member{
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'username')
  String? username;
  @JsonKey(name: 'email')
  String? email;
  @JsonKey(name: 'groupName')
  String? groupName;
  @JsonKey(name: 'groupCode')
  String? groupCode;
  @JsonKey(name: 'groupNature')
  String? groupNature;
  @JsonKey(name: 'forceChangePassword')
  bool? forceChangePassword;
  @JsonKey(name: 'customValues')
  List<CustomValue>? customValues;
  @JsonKey(name: 'images')
  List<Image>? images;


  Member({
      this.id,
      this.name,
      this.username,
      this.email,
      this.groupName,
      this.groupCode,
      this.groupNature,
      this.forceChangePassword,
      this.customValues,
      this.images});

  factory Member.fromJson(Map<String, dynamic> json) => _$MemberFromJson(json);
  Map<String, dynamic> toJson() => _$MemberToJson(this);
}