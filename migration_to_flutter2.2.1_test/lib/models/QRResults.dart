class QRResults {
  int status = 0;
  dynamic content;

  QRResults({dynamic content}) {
    if (content != null) {
      this.status = 1;
      this.content = content;
    }
  }
}