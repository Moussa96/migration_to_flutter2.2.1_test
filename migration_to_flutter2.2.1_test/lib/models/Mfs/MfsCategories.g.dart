// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MfsCategories.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MfsCategories _$MfsCategoriesFromJson(Map<String, dynamic> json) {
  return MfsCategories(
    category_code: json['category_code'] as String,
    category_name: json['category_name'] as String,
    category_arabic_name: json['category_arabic_name'] as String,
    sub_category: (json['sub_category'] as List<dynamic>)
        .map((e) => MfsSubCategory.fromJson(e as Map<String, dynamic>))
        .toList(),
    services: (json['services'] as List<dynamic>)
        .map((e) => MfsServices.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$MfsCategoriesToJson(MfsCategories instance) =>
    <String, dynamic>{
      'category_code': instance.category_code,
      'category_name': instance.category_name,
      'category_arabic_name': instance.category_arabic_name,
      'sub_category': instance.sub_category,
      'services': instance.services,
    };
