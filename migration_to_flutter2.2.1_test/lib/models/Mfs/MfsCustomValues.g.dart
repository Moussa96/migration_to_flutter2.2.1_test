// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MfsCustomValues.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MfsCustomValues _$MfsCustomValuesFromJson(Map<String, dynamic> json) {
  return MfsCustomValues(
    access_token: json['access_token'] as String,
    mfs_transcation_request: json['mfs_transcation_request'] as String,
    mfs_transcation_response: json['mfs_transcation_response'] as String,
  );
}

Map<String, dynamic> _$MfsCustomValuesToJson(MfsCustomValues instance) =>
    <String, dynamic>{
      'mfs_transcation_request': instance.mfs_transcation_request,
      'mfs_transcation_response': instance.mfs_transcation_response,
      'access_token': instance.access_token,
    };
