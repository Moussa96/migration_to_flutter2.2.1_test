import 'package:json_annotation/json_annotation.dart';
part 'MfsTrxParams.g.dart';

@JsonSerializable()
class MfsTrxParams {
  @JsonKey(name: 'label')
  String? label;
  @JsonKey(name: 'label_en')
  String? label_en;
  @JsonKey(name: 'json_id')
  String? json_id;
  @JsonKey(name: 'type')
  String? type;
  @JsonKey(name: 'regex')
  String? regex;
  @JsonKey(name: 'max')
  double? max;
  @JsonKey(name: 'min')
  double? min;
  @JsonKey(name: 'helper')
  String? helper;
  @JsonKey(name: 'helper_en')
  String? helper_en;
  @JsonKey(name: 'err')
  String? err;
  @JsonKey(name: 'err_en')
  String? err_en;

  MfsTrxParams({this.label, this.label_en, this.type, this.min, this.max, this.err_en, this.err, this.helper, this.helper_en,this.json_id});

  factory MfsTrxParams.fromJson(Map<String, dynamic> json) =>
      _$MfsTrxParamsFromJson(json);

  Map<String, dynamic> toJson() => _$MfsTrxParamsToJson(this);
}
