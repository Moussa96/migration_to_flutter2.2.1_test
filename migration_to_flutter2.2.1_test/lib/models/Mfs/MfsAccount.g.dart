// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MfsAccount.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MfsAccount _$MfsAccountFromJson(Map<String, dynamic> json) {
  return MfsAccount(
    label: json['label'] as String,
    label_en: json['label_en'] as String,
    regex: json['regex'] as String,
    err: json['err'] as String,
    err_en: json['err_en'] as String,
  );
}

Map<String, dynamic> _$MfsAccountToJson(MfsAccount instance) =>
    <String, dynamic>{
      'label': instance.label,
      'label_en': instance.label_en,
      'regex': instance.regex,
      'err': instance.err,
      'err_en': instance.err_en,
    };
