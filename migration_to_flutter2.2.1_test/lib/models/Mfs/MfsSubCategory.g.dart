// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MfsSubCategory.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MfsSubCategory _$MfsSubCategoryFromJson(Map<String, dynamic> json) {
  return MfsSubCategory(
    sub_category_code: json['sub_category_code'] as String,
    sub_category_name: json['sub_category_name'] as String,
    sub_category_arabic_name: json['sub_category_arabic_name'] as String,
    services: (json['services'] as List<dynamic>)
        .map((e) => MfsServices.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$MfsSubCategoryToJson(MfsSubCategory instance) =>
    <String, dynamic>{
      'sub_category_code': instance.sub_category_code,
      'sub_category_name': instance.sub_category_name,
      'sub_category_arabic_name': instance.sub_category_arabic_name,
      'services': instance.services,
    };
