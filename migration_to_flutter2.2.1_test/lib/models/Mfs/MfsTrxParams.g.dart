// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MfsTrxParams.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MfsTrxParams _$MfsTrxParamsFromJson(Map<String, dynamic> json) {
  return MfsTrxParams(
    label: json['label'] as String,
    label_en: json['label_en'] as String,
    type: json['type'] as String,
    min: (json['min'] as num).toDouble(),
    max: (json['max'] as num).toDouble(),
    err_en: json['err_en'] as String,
    err: json['err'] as String,
    helper: json['helper'] as String,
    helper_en: json['helper_en'] as String,
    json_id: json['json_id'] as String,
  )..regex = json['regex'] as String;
}

Map<String, dynamic> _$MfsTrxParamsToJson(MfsTrxParams instance) =>
    <String, dynamic>{
      'label': instance.label,
      'label_en': instance.label_en,
      'json_id': instance.json_id,
      'type': instance.type,
      'regex': instance.regex,
      'max': instance.max,
      'min': instance.min,
      'helper': instance.helper,
      'helper_en': instance.helper_en,
      'err': instance.err,
      'err_en': instance.err_en,
    };
