import 'package:cashless/models/Mfs/MfsServices.dart';
import 'package:json_annotation/json_annotation.dart';
part 'MfsSubCategory.g.dart';

@JsonSerializable()
class MfsSubCategory {
  @JsonKey(name: 'sub_category_code')
  String? sub_category_code;
  @JsonKey(name: 'sub_category_name')
  String? sub_category_name;
  @JsonKey(name: 'sub_category_arabic_name')
  String? sub_category_arabic_name;
  @JsonKey(name: 'services')
  List<MfsServices>? services;

  MfsSubCategory({this.sub_category_code, this.sub_category_name, this.sub_category_arabic_name, this.services});

  factory MfsSubCategory.fromJson(Map<String, dynamic> json) =>
      _$MfsSubCategoryFromJson(json);

  Map<String, dynamic> toJson() => _$MfsSubCategoryToJson(this);
}