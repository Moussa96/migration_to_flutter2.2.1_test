// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MfsScenarioConfiguration.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MfsScenarioConfiguration _$MfsScenarioConfigurationFromJson(
    Map<String, dynamic> json) {
  return MfsScenarioConfiguration(
    scenario_id: json['scenario_id'] as String,
    account: MfsAccount.fromJson(json['account'] as Map<String, dynamic>),
    trx_params: (json['trx_params'] as List<dynamic>)
        .map((e) => MfsTrxParams.fromJson(e as Map<String, dynamic>))
        .toList(),
  )..pay_type = json['pay_type'] as String;
}

Map<String, dynamic> _$MfsScenarioConfigurationToJson(
        MfsScenarioConfiguration instance) =>
    <String, dynamic>{
      'scenario_id': instance.scenario_id,
      'pay_type': instance.pay_type,
      'account': instance.account,
      'trx_params': instance.trx_params,
    };
