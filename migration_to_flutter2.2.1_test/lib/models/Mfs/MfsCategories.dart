import 'package:cashless/models/Mfs/MfsServices.dart';
import 'package:cashless/models/Mfs/MfsSubCategory.dart';
import 'package:json_annotation/json_annotation.dart';
part 'MfsCategories.g.dart';

@JsonSerializable()
class MfsCategories {
  @JsonKey(name: 'category_code')
  String? category_code;
  @JsonKey(name: 'category_name')
  String? category_name;
  @JsonKey(name: 'category_arabic_name')
  String? category_arabic_name;
  @JsonKey(name: 'sub_category')
  List<MfsSubCategory>? sub_category;
  @JsonKey(name: 'services')
    List<MfsServices>? services;

  MfsCategories({this.category_code, this.category_name, this.category_arabic_name, this.sub_category, this.services});

  factory MfsCategories.fromJson(Map<String, dynamic> json) =>
      _$MfsCategoriesFromJson(json);

  Map<String, dynamic> toJson() => _$MfsCategoriesToJson(this);
}
