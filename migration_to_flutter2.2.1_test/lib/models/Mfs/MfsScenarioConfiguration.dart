import 'package:cashless/models/Mfs/MfsAccount.dart';
import 'package:cashless/models/Mfs/MfsTrxParams.dart';
import 'package:json_annotation/json_annotation.dart';
part 'MfsScenarioConfiguration.g.dart';

@JsonSerializable()
class MfsScenarioConfiguration {
  @JsonKey(name: 'scenario_id')
  String? scenario_id;
  @JsonKey(name: 'pay_type')
  String? pay_type;
  @JsonKey(name: 'account')
  MfsAccount? account;
  @JsonKey(name: 'trx_params')
  List<MfsTrxParams>? trx_params;

  MfsScenarioConfiguration({this.scenario_id, this.account, this.trx_params});

  factory MfsScenarioConfiguration.fromJson(Map<String, dynamic> json) =>
      _$MfsScenarioConfigurationFromJson(json);

  Map<String, dynamic> toJson() => _$MfsScenarioConfigurationToJson(this);
}
