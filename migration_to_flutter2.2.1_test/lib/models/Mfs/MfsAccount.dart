import 'package:json_annotation/json_annotation.dart';
part 'MfsAccount.g.dart';

@JsonSerializable()
class MfsAccount {
  @JsonKey(name: 'label')
  String? label;
  @JsonKey(name: 'label_en')
  String? label_en;
  @JsonKey(name: 'regex')
  String? regex;
  @JsonKey(name: 'err')
    String? err;
  @JsonKey(name: 'err_en')
    String? err_en;

  MfsAccount({this.label, this.label_en, this.regex, this.err, this.err_en});

  factory MfsAccount.fromJson(Map<String, dynamic> json) =>
      _$MfsAccountFromJson(json);

  Map<String, dynamic> toJson() => _$MfsAccountToJson(this);
}
