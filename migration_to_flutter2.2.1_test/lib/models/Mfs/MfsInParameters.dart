import 'package:json_annotation/json_annotation.dart';
part 'MfsInParameters.g.dart';

@JsonSerializable()
class MfsInParameters {
  @JsonKey(name: 'in_param_1')
  String? in_param_1;
  @JsonKey(name: 'in_param_2')
  String? in_param_2;
  @JsonKey(name: 'in_param_3')
  String? in_param_3;
  @JsonKey(name: 'in_param_4')
  String? in_param_4;
  @JsonKey(name: 'in_param_5')
  String? in_param_5;

  MfsInParameters({this.in_param_1, this.in_param_2, this.in_param_3, this.in_param_4, this.in_param_5});

  factory MfsInParameters.fromJson(Map<String, dynamic> json) =>
      _$MfsInParametersFromJson(json);

  Map<String, dynamic> toJson() => _$MfsInParametersToJson(this);
}
