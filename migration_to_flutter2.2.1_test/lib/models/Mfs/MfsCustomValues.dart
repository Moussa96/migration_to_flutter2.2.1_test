import 'package:json_annotation/json_annotation.dart';
part 'MfsCustomValues.g.dart';

@JsonSerializable()
class MfsCustomValues {
  @JsonKey(name: 'mfs_transcation_request')
  String? mfs_transcation_request;
  @JsonKey(name: 'mfs_transcation_response')
  String? mfs_transcation_response;
  @JsonKey(name: 'access_token')
  String? access_token;

  MfsCustomValues({this.access_token, this.mfs_transcation_request, this.mfs_transcation_response});

  factory MfsCustomValues.fromJson(Map<String, dynamic> json) =>
      _$MfsCustomValuesFromJson(json);

  Map<String, dynamic> toJson() => _$MfsCustomValuesToJson(this);
}
