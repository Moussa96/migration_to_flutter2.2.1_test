// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MfsServices.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MfsServices _$MfsServicesFromJson(Map<String, dynamic> json) {
  return MfsServices(
    service_code: json['service_code'] as String,
    service_name: json['service_name'] as String,
    service_arabic_name: json['service_arabic_name'] as String,
    sub_category_code: json['sub_category_code'] as String,
    sub_category_name: json['sub_category_name'] as String,
    sub_category_arabic_name: json['sub_category_arabic_name'] as String,
    description: json['description'] as String,
    scenario_configuration: json['scenario_conf'] as String,
    service_provider_name: json['service_provider_name'] as String,
    service_provider_arabic_name:
        json['service_provider_arabic_name'] as String,
    service_provider_code: json['service_provider_code'] as String,
    denomination_type: json['denomination_type'] as String,
    prereq_service_code: json['prereq_service_code'] as String,
    prereq_service_param_map: json['prereq_service_param_map'] as String,
    Packages: json['Packages'] as String,
    service_category: json['service_category'] as String,
    service_transaction_type: json['service_transaction_type'] as String,
    service_type: json['service_type'] as String,
    short_code: json['short_code'] as String,
    wlService_code: json['wl_service_code'] as String,
    wl_user_id: json['wl_user_id'] as String,
  );
}

Map<String, dynamic> _$MfsServicesToJson(MfsServices instance) =>
    <String, dynamic>{
      'service_code': instance.service_code,
      'service_name': instance.service_name,
      'service_arabic_name': instance.service_arabic_name,
      'description': instance.description,
      'service_provider_code': instance.service_provider_code,
      'service_provider_name': instance.service_provider_name,
      'service_provider_arabic_name': instance.service_provider_arabic_name,
      'service_type': instance.service_type,
      'service_transaction_type': instance.service_transaction_type,
      'denomination_type': instance.denomination_type,
      'prereq_service_code': instance.prereq_service_code,
      'prereq_service_param_map': instance.prereq_service_param_map,
      'scenario_conf': instance.scenario_configuration,
      'short_code': instance.short_code,
      'service_category': instance.service_category,
      'wl_service_code': instance.wlService_code,
      'wl_user_id': instance.wl_user_id,
      'sub_category_code': instance.sub_category_code,
      'sub_category_name': instance.sub_category_name,
      'sub_category_arabic_name': instance.sub_category_arabic_name,
      'Packages': instance.Packages,
    };
