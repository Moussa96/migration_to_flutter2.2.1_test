// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MfsInParameters.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MfsInParameters _$MfsInParametersFromJson(Map<String, dynamic> json) {
  return MfsInParameters(
    in_param_1: json['in_param_1'] as String,
    in_param_2: json['in_param_2'] as String,
    in_param_3: json['in_param_3'] as String,
    in_param_4: json['in_param_4'] as String,
    in_param_5: json['in_param_5'] as String,
  );
}

Map<String, dynamic> _$MfsInParametersToJson(MfsInParameters instance) =>
    <String, dynamic>{
      'in_param_1': instance.in_param_1,
      'in_param_2': instance.in_param_2,
      'in_param_3': instance.in_param_3,
      'in_param_4': instance.in_param_4,
      'in_param_5': instance.in_param_5,
    };
