import 'package:cashless/models/Mfs/MfsCategories.dart';
import 'package:json_annotation/json_annotation.dart';
part 'MfsCashcallServices.g.dart';

@JsonSerializable()
class MfsCashcallServices {
  @JsonKey(name: 'cashcall_services')
  List<MfsCategories>? cashcall_services;

  MfsCashcallServices({this.cashcall_services});

  factory MfsCashcallServices.fromJson(Map<String, dynamic> json) =>
      _$MfsCashcallServicesFromJson(json);

  Map<String, dynamic> toJson() => _$MfsCashcallServicesToJson(this);
}
