// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MfsCashcallServices.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MfsCashcallServices _$MfsCashcallServicesFromJson(Map<String, dynamic> json) {
  return MfsCashcallServices(
    cashcall_services: (json['cashcall_services'] as List<dynamic>)
        .map((e) => MfsCategories.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$MfsCashcallServicesToJson(
        MfsCashcallServices instance) =>
    <String, dynamic>{
      'cashcall_services': instance.cashcall_services,
    };
