import 'package:json_annotation/json_annotation.dart';
part 'MfsServices.g.dart';

@JsonSerializable()
class MfsServices {

  @JsonKey(name: 'service_code')
  String? service_code;
  @JsonKey(name: 'service_name')
  String? service_name;
  @JsonKey(name: 'service_arabic_name')
  String? service_arabic_name;
  @JsonKey(name: 'description')
  String? description;
  @JsonKey(name: 'service_provider_code')
  String? service_provider_code;
  @JsonKey(name: 'service_provider_name')
  String? service_provider_name;
  @JsonKey(name: 'service_provider_arabic_name')
  String? service_provider_arabic_name;
  @JsonKey(name: 'service_type')
  String? service_type;
  @JsonKey(name: 'service_transaction_type')
  String? service_transaction_type;
  @JsonKey(name: 'denomination_type')
  String? denomination_type;
  @JsonKey(name: 'prereq_service_code')
  String? prereq_service_code;
  @JsonKey(name: 'prereq_service_param_map')
  String? prereq_service_param_map;
  @JsonKey(name: 'scenario_conf')
  String? scenario_configuration;
  @JsonKey(name: 'short_code')
  String? short_code;
  @JsonKey(name: 'service_category')
  String? service_category;
  @JsonKey(name: 'wl_service_code')
  String? wlService_code;
  @JsonKey(name: 'wl_user_id')
  String? wl_user_id;
  @JsonKey(name: 'sub_category_code')
  String? sub_category_code;
  @JsonKey(name: 'sub_category_name')
  String? sub_category_name;
  @JsonKey(name: 'sub_category_arabic_name')
  String? sub_category_arabic_name;
  @JsonKey(name: 'Packages')
  String? Packages;

  MfsServices({this.service_code, this.service_name, this.service_arabic_name, this.sub_category_code, this.sub_category_name, this.sub_category_arabic_name,
    this.description, this.scenario_configuration, this.service_provider_name, this.service_provider_arabic_name, this.service_provider_code, this.denomination_type,
    this.prereq_service_code, this.prereq_service_param_map, this.Packages, this.service_category, this.service_transaction_type, this.service_type,
    this.short_code, this.wlService_code, this.wl_user_id});

  factory MfsServices.fromJson(Map<String, dynamic> json) =>
      _$MfsServicesFromJson(json);

  Map<String, dynamic> toJson() => _$MfsServicesToJson(this);
}
