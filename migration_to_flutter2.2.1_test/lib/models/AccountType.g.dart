// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AccountType.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccountType _$AccountTypeFromJson(Map<String, dynamic> json) {
  return AccountType(
    id: json['id'] as int,
    name: json['name'] as String,
    currency: Currency.fromJson(json['currency'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AccountTypeToJson(AccountType instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'currency': instance.currency,
    };
