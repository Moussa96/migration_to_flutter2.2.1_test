// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'User.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  $checkKeys(json, disallowNullValues: const [
    'id',
    'nationalId',
    'groupCode',
    'name',
    'username',
    'email',
    'password',
    'mobileNumber',
    'accounts',
    'customValues',
    'requireTransactionPassword',
    'canMakeMemberPayments',
    'canMakeSystemPayments',
    'decimalSeparator',
    'decimalCount'
  ]);
  return User(
    id: json['id'] as String,
    mobileNumber: json['mobileNumber'] as String,
    password: json['password'] as String,
    name: json['name'] as String,
    username: json['username'] as String,
    email: json['email'] as String,
    nationalId: json['nationalId'] as String,
    groupCode: json['groupCode'] as String,
    requireTransactionPassword: json['requireTransactionPassword'] as bool,
    accounts: (json['accounts'] as List<dynamic>)
        .map((e) => Account.fromJson(e as Map<String, dynamic>))
        .toList(),
    canMakeMemberPayments: json['canMakeMemberPayments'] as bool,
    canMakeSystemPayments: json['canMakeSystemPayments'] as bool,
    decimalCount: json['decimalCount'] as bool,
    decimalSeparator: json['decimalSeparator'] as bool,
    customValues: (json['customValues'] as List<dynamic>)
        .map((e) => CustomValue.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'nationalId': instance.nationalId,
      'groupCode': instance.groupCode,
      'name': instance.name,
      'username': instance.username,
      'email': instance.email,
      'password': instance.password,
      'mobileNumber': instance.mobileNumber,
      'accounts': instance.accounts,
      'customValues': instance.customValues,
      'requireTransactionPassword': instance.requireTransactionPassword,
      'canMakeMemberPayments': instance.canMakeMemberPayments,
      'canMakeSystemPayments': instance.canMakeSystemPayments,
      'decimalSeparator': instance.decimalSeparator,
      'decimalCount': instance.decimalCount,
    };
