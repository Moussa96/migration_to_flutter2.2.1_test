// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GetListNotificationsResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetListNotificationsResponse _$GetListNotificationsResponseFromJson(
    Map<String, dynamic> json) {
  return GetListNotificationsResponse(
    notificationRes: (json['notificationRes'] as List<dynamic>)
        .map((e) => NotificationRes.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$GetListNotificationsResponseToJson(
        GetListNotificationsResponse instance) =>
    <String, dynamic>{
      'notificationRes': instance.notificationRes,
    };
