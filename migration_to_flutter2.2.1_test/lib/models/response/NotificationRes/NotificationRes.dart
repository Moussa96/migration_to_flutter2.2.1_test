import 'package:cashless/models/Member.dart';
import 'package:json_annotation/json_annotation.dart';

part 'NotificationRes.g.dart';

@JsonSerializable()
class NotificationRes {
  int? id;
  DateTime? date;
  Member? toMember;
  String? direction;
  String? subject;
  String? body;
  String? type;
  bool? read;
  bool? replied;
  bool? html;
  bool? emailSent;

  NotificationRes({
      this.id,
      this.date,
      this.toMember,
      this.direction,
      this.subject,
      this.body,
      this.type,
      this.read,
      this.replied,
      this.html,
      this.emailSent});

  factory NotificationRes.fromJson(Map<String, dynamic> json) =>
      _$NotificationResFromJson(json);

  Map<String, dynamic> toJson() => _$NotificationResToJson(this);
}
