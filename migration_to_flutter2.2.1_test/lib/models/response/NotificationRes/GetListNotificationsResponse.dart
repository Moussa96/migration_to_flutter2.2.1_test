import 'package:cashless/models/response/NotificationRes/NotificationRes.dart';
import 'package:json_annotation/json_annotation.dart';

part 'GetListNotificationsResponse.g.dart';

@JsonSerializable()
class GetListNotificationsResponse {
  List<NotificationRes>? notificationRes;

  GetListNotificationsResponse({this.notificationRes});

  factory GetListNotificationsResponse.fromJson(Map<String, dynamic> json) =>
      _$GetListNotificationsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetListNotificationsResponseToJson(this);
}
