// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'NotificationRes.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationRes _$NotificationResFromJson(Map<String, dynamic> json) {
  return NotificationRes(
    id: json['id'] as int,
    date: DateTime.parse(json['date'] as String),
    toMember: Member.fromJson(json['toMember'] as Map<String, dynamic>),
    direction: json['direction'] as String,
    subject: json['subject'] as String,
    body: json['body'] as String,
    type: json['type'] as String,
    read: json['read'] as bool,
    replied: json['replied'] as bool,
    html: json['html'] as bool,
    emailSent: json['emailSent'] as bool,
  );
}

Map<String, dynamic> _$NotificationResToJson(NotificationRes instance) =>
    <String, dynamic>{
      'id': instance.id,
      'date': instance.date!.toIso8601String(),
      'toMember': instance.toMember,
      'direction': instance.direction,
      'subject': instance.subject,
      'body': instance.body,
      'type': instance.type,
      'read': instance.read,
      'replied': instance.replied,
      'html': instance.html,
      'emailSent': instance.emailSent,
    };
