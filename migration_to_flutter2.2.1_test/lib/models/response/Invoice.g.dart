// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Invoice.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Invoice _$InvoiceFromJson(Map<String, dynamic> json) {
  return Invoice(
    id: json['id'] as int,
    description: json['description'] as String,
    amount: (json['amount'] as num).toDouble(),
    date: DateTime.parse(json['date'] as String),
    from: Member.fromJson(json['from'] as Map<String, dynamic>),
    to: Member.fromJson(json['to'] as Map<String, dynamic>),
    status: json['status'] as String,
  );
}

Map<String, dynamic> _$InvoiceToJson(Invoice instance) => <String, dynamic>{
      'id': instance.id,
      'description': instance.description,
      'amount': instance.amount,
      'date': instance.date!.toIso8601String(),
      'from': instance.from,
      'to': instance.to,
      'status': instance.status,
    };
