// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ListMerchantsResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListMerchantsResponse _$ListMerchantsResponseFromJson(
    Map<String, dynamic> json) {
  return ListMerchantsResponse(
    currentPage: json['currentPage'] as int,
    pageSize: json['pageSize'] as int,
    totalCount: json['totalCount'] as int,
    elements: (json['elements'] as List<dynamic>)
        .map((e) => Member.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ListMerchantsResponseToJson(
        ListMerchantsResponse instance) =>
    <String, dynamic>{
      'currentPage': instance.currentPage,
      'pageSize': instance.pageSize,
      'totalCount': instance.totalCount,
      'elements': instance.elements,
    };
