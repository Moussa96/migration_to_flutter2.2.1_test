import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/Mfs/MfsCustomValues.dart';
import 'package:json_annotation/json_annotation.dart';
part 'PaymentResponse.g.dart';

@JsonSerializable()
class PaymentResponse{
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'pending')
  bool? pending;
  @JsonKey(name: 'fromMemberName')
  String? fromMemberName;
  @JsonKey(name: 'toMemberName')
  String? toMemberName;
  @JsonKey(name: 'transactionNumber')
  String? transactionNumber;
  @JsonKey(name: 'customValues')
  MfsCustomValues? customValues;


  PaymentResponse(
  {this.id, this.pending, this.fromMemberName, this.toMemberName, this.customValues});

  factory PaymentResponse.fromJson(Map<String, dynamic> json) => _$PaymentResponseFromJson(json);
  Map<String, dynamic> toJson() => _$PaymentResponseToJson(this);

}
