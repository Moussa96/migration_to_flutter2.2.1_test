import 'package:cashless/models/Member.dart';
import 'package:json_annotation/json_annotation.dart';

part 'ListMerchantsResponse.g.dart';

@JsonSerializable()
class ListMerchantsResponse{
  @JsonKey(name: 'currentPage')
  int? currentPage;
  @JsonKey(name: 'pageSize')
  int? pageSize;
  @JsonKey(name: 'totalCount')
  int? totalCount;
  @JsonKey(name: 'elements')
  List<Member>? elements;

  ListMerchantsResponse(
  {this.currentPage, this.pageSize, this.totalCount, this.elements});

  factory ListMerchantsResponse.fromJson(Map<String, dynamic> json) => _$ListMerchantsResponseFromJson(json);
  Map<String, dynamic> toJson() => _$ListMerchantsResponseToJson(this);

}