import 'package:cashless/models/Fees.dart';
import 'package:json_annotation/json_annotation.dart';

import '../Member.dart';

part 'MakePaymentResponse.g.dart';

@JsonSerializable()
class MakePaymentResponse{
  @JsonKey(name: 'wouldRequireAuthorization')
  bool? wouldRequireAuthorization;
  @JsonKey(name: 'from')
  Member? fromMember;
  @JsonKey(name: 'to')
  Member? toMember;
  @JsonKey(name: 'finalAmount')
  double? finalAmount;
  @JsonKey(name: 'formattedFinalAmount')
  String? formattedFinalAmount;
  @JsonKey(name: 'fees')
  List<Fees>? fees;

  MakePaymentResponse({this.wouldRequireAuthorization, this.fromMember,
      this.toMember, this.finalAmount, this.formattedFinalAmount, this.fees});
  factory MakePaymentResponse.fromJson(Map<String, dynamic> json) => _$MakePaymentResponseFromJson(json);
  Map<String, dynamic> toJson() => _$MakePaymentResponseToJson(this);
}