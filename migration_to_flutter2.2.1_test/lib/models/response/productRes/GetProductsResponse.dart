import 'package:cashless/models/response/productRes/ProductElements.dart';
import 'package:json_annotation/json_annotation.dart';

part 'GetProductsResponse.g.dart';

@JsonSerializable()
class GetProductsResponse {
  @JsonKey(name: 'currentPage')
  int? currentPage;
  @JsonKey(name: 'pageSize')
  int? pageSize;
  @JsonKey(name: 'totalCount')
  int? totalCount;
  @JsonKey(name: 'elements')
  List<ProductElements>? elements;

  GetProductsResponse({
      this.currentPage, this.pageSize, this.totalCount, this.elements});

  factory GetProductsResponse.fromJson(Map<String, dynamic> json) =>
      _$GetProductsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GetProductsResponseToJson(this);
}
