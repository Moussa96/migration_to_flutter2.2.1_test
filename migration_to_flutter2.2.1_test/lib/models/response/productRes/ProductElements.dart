import 'package:cashless/models/Currency.dart';
import 'package:cashless/models/Image.dart';
import 'package:cashless/models/Member.dart';
import 'package:cashless/models/response/productRes/Category.dart';
import 'package:json_annotation/json_annotation.dart';

part 'ProductElements.g.dart';

@JsonSerializable()
class ProductElements {
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'category')
  Category? category;
  @JsonKey(name: 'title')
  String? title;
  @JsonKey(name: 'description')
  String? description;
  @JsonKey(name: 'currency')
  Currency? currency;
  @JsonKey(name: 'price')
  double? price;
  @JsonKey(name: 'formattedPrice')
  String? formattedPrice;
  @JsonKey(name: 'owner')
  Member? owner;
  @JsonKey(name: 'images')
  List<Image>? images;
  @JsonKey(name: 'searching')
  bool? searching;
  @JsonKey(name: 'html')
  bool? html;


  ProductElements({
      this.id,
      this.title,
      this.description,
      this.currency,
      this.price,
      this.formattedPrice,
      this.owner,
      this.images,
      this.searching,
      this.html});

  factory ProductElements.fromJson(Map<String, dynamic> json) =>
      _$ProductElementsFromJson(json);

  Map<String, dynamic> toJson() => _$ProductElementsToJson(this);
}
