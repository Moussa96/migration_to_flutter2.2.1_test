// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ProductElements.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductElements _$ProductElementsFromJson(Map<String, dynamic> json) {
  return ProductElements(
    id: json['id'] as int,
    title: json['title'] as String,
    description: json['description'] as String,
    currency: Currency.fromJson(json['currency'] as Map<String, dynamic>),
    price: (json['price'] as num).toDouble(),
    formattedPrice: json['formattedPrice'] as String,
    owner: Member.fromJson(json['owner'] as Map<String, dynamic>),
    images: (json['images'] as List<dynamic>)
        .map((e) => Image.fromJson(e as Map<String, dynamic>))
        .toList(),
    searching: json['searching'] as bool,
    html: json['html'] as bool,
  )..category = Category.fromJson(json['category'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ProductElementsToJson(ProductElements instance) =>
    <String, dynamic>{
      'id': instance.id,
      'category': instance.category,
      'title': instance.title,
      'description': instance.description,
      'currency': instance.currency,
      'price': instance.price,
      'formattedPrice': instance.formattedPrice,
      'owner': instance.owner,
      'images': instance.images,
      'searching': instance.searching,
      'html': instance.html,
    };
