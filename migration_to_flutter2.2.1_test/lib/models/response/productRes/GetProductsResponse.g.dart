// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GetProductsResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetProductsResponse _$GetProductsResponseFromJson(Map<String, dynamic> json) {
  return GetProductsResponse(
    currentPage: json['currentPage'] as int,
    pageSize: json['pageSize'] as int,
    totalCount: json['totalCount'] as int,
    elements: (json['elements'] as List<dynamic>)
        .map((e) => ProductElements.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$GetProductsResponseToJson(
        GetProductsResponse instance) =>
    <String, dynamic>{
      'currentPage': instance.currentPage,
      'pageSize': instance.pageSize,
      'totalCount': instance.totalCount,
      'elements': instance.elements,
    };
