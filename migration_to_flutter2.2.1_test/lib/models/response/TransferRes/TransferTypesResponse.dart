import 'package:cashless/models/response/transactionRes/TransferTypeRes.dart';
import 'package:json_annotation/json_annotation.dart';

import '../../AccountStatus.dart';

part 'TransferTypesResponse.g.dart';

@JsonSerializable()
class TransferTypesResponse {
  @JsonKey( name: 'transferTypeRes')
  List<TransferTypeRes>? transferTypeRes;

  @JsonKey(name: 'accountsStatus')
  AccountStatus? accountsStatus;

  TransferTypesResponse({this.transferTypeRes, this.accountsStatus});

  factory TransferTypesResponse.fromJson(Map<String, dynamic> json) =>
      _$TransferTypesResponseFromJson(json);

  Map<String, dynamic> toJson() => _$TransferTypesResponseToJson(this);
}
