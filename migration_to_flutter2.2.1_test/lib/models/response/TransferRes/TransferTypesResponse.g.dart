// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TransferTypesResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransferTypesResponse _$TransferTypesResponseFromJson(
    Map<String, dynamic> json) {
  return TransferTypesResponse(
    transferTypeRes: (json['transferTypeRes'] as List<dynamic>)
        .map((e) => TransferTypeRes.fromJson(e as Map<String, dynamic>))
        .toList(),
    accountsStatus:
        AccountStatus.fromJson(json['accountsStatus'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$TransferTypesResponseToJson(
        TransferTypesResponse instance) =>
    <String, dynamic>{
      'transferTypeRes': instance.transferTypeRes,
      'accountsStatus': instance.accountsStatus,
    };
