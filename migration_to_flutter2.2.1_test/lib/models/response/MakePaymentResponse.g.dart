// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MakePaymentResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MakePaymentResponse _$MakePaymentResponseFromJson(Map<String, dynamic> json) {
  return MakePaymentResponse(
    wouldRequireAuthorization: json['wouldRequireAuthorization'] as bool,
    fromMember: Member.fromJson(json['from'] as Map<String, dynamic>),
    toMember: Member.fromJson(json['to'] as Map<String, dynamic>),
    finalAmount: (json['finalAmount'] as num).toDouble(),
    formattedFinalAmount: json['formattedFinalAmount'] as String,
    fees: (json['fees'] as List<dynamic>)
        .map((e) => Fees.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$MakePaymentResponseToJson(
        MakePaymentResponse instance) =>
    <String, dynamic>{
      'wouldRequireAuthorization': instance.wouldRequireAuthorization,
      'from': instance.fromMember,
      'to': instance.toMember,
      'finalAmount': instance.finalAmount,
      'formattedFinalAmount': instance.formattedFinalAmount,
      'fees': instance.fees,
    };
