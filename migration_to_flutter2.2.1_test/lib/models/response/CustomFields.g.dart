// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CustomFields.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomFields _$CustomFieldsFromJson(Map<String, dynamic> json) {
  return CustomFields(
    json['id'] as int,
    json['displayName'] as String,
    json['internalName'] as String,
    json['required'] as bool,
    json['type'] as String,
    json['control'] as String,
    json['minLength'] as int,
    json['maxLength'] as int,
  );
}

Map<String, dynamic> _$CustomFieldsToJson(CustomFields instance) =>
    <String, dynamic>{
      'id': instance.id,
      'displayName': instance.displayName,
      'internalName': instance.internalName,
      'required': instance.required,
      'type': instance.type,
      'control': instance.control,
      'minLength': instance.minLength,
      'maxLength': instance.maxLength,
    };
