import 'package:json_annotation/json_annotation.dart';

part 'MfsServiceCalculatorResponse.g.dart';

@JsonSerializable()
class MfsServiceCalculatorResponse {

  @JsonKey(name: 'receipt_Due_Amount_from_consumer')
  String? receipt_Due_Amount_from_consumer;
  @JsonKey(name: 'receipt_Deducted_Amount')
  String? receipt_Deducted_Amount;
  @JsonKey(name: 'input_transaction_value')
  String? input_transaction_value;
  @JsonKey(name: 'out_parameters')
  String? out_parameters;
  @JsonKey(name: 'outParameters')
  String? outParameters;
  @JsonKey(name: 'user_a_msg')
  String? user_a_msg;
  @JsonKey(name: 'sys_to_sys_msg')
  String? sys_to_sys_msg;
  @JsonKey(name: 'status_code')
  String? status_code;



  MfsServiceCalculatorResponse({
      this.receipt_Due_Amount_from_consumer,
      this.out_parameters,
      this.status_code,
      this.sys_to_sys_msg,
      this.user_a_msg
      }); //  PendingStatusRequest({this.notes});



  factory MfsServiceCalculatorResponse.fromJson(Map<String, dynamic> json) =>
      _$MfsServiceCalculatorResponseFromJson(json);

  Map<String, dynamic> toJson() => _$MfsServiceCalculatorResponseToJson(this);
}
