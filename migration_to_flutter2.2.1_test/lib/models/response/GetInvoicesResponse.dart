import 'package:cashless/models/response/Invoice.dart';
import 'package:json_annotation/json_annotation.dart';


part 'GetInvoicesResponse.g.dart';

@JsonSerializable()
class GetInvoicesResponse {
  List<Invoice>? invoices;

  GetInvoicesResponse({this.invoices});

  factory GetInvoicesResponse.fromJson(Map<String, dynamic> json) => _$GetInvoicesResponseFromJson(json);
  Map<String, dynamic> toJson() => _$GetInvoicesResponseToJson(this);

}