import 'package:cashless/models/response/transactionRes/TransferTypeRes.dart';
import 'package:json_annotation/json_annotation.dart';


part 'CustomFields.g.dart';

@JsonSerializable()
class CustomFields {
  int id;
  String displayName;
  String internalName;
  bool required;
  String type;
  String control;
  int minLength;
  int maxLength;

  CustomFields(this.id, this.displayName, this.internalName, this.required,
      this.type, this.control, this.minLength, this.maxLength);

  factory CustomFields.fromJson(Map<String, dynamic> json) =>
      _$CustomFieldsFromJson(json);

  Map<String, dynamic> toJson() => _$CustomFieldsToJson(this);
}
