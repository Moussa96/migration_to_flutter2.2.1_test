// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'PaymentResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaymentResponse _$PaymentResponseFromJson(Map<String, dynamic> json) {
  return PaymentResponse(
    id: json['id'] as int,
    pending: json['pending'] as bool,
    fromMemberName: json['fromMemberName'] as String,
    toMemberName: json['toMemberName'] as String,
    customValues:
        MfsCustomValues.fromJson(json['customValues'] as Map<String, dynamic>),
  )..transactionNumber = json['transactionNumber'] as String;
}

Map<String, dynamic> _$PaymentResponseToJson(PaymentResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'pending': instance.pending,
      'fromMemberName': instance.fromMemberName,
      'toMemberName': instance.toMemberName,
      'transactionNumber': instance.transactionNumber,
      'customValues': instance.customValues,
    };
