// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GetInvoicesResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetInvoicesResponse _$GetInvoicesResponseFromJson(Map<String, dynamic> json) {
  return GetInvoicesResponse(
    invoices: (json['invoices'] as List<dynamic>)
        .map((e) => Invoice.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$GetInvoicesResponseToJson(
        GetInvoicesResponse instance) =>
    <String, dynamic>{
      'invoices': instance.invoices,
    };
