import 'package:cashless/models/Member.dart';
import 'package:json_annotation/json_annotation.dart';
part 'Invoice.g.dart';

@JsonSerializable()
class Invoice {
  @JsonKey( name: 'id')
  int? id;
  @JsonKey( name: 'description')
  String? description;
  @JsonKey( name: 'amount')
  double? amount;
  @JsonKey( name: 'date')
  DateTime? date;
  @JsonKey( name: 'from')
  Member? from;
  @JsonKey( name: 'to')
  Member? to;
  @JsonKey( name: 'status')
  String? status;

  Invoice({this.id, this.description, this.amount, this.date,
      this.from, this.to, this.status});

  factory Invoice.fromJson(Map<String, dynamic> json) => _$InvoiceFromJson(json);
  Map<String, dynamic> toJson() => _$InvoiceToJson(this);

}
