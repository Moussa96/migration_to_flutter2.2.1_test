// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MfsServiceCalculatorResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MfsServiceCalculatorResponse _$MfsServiceCalculatorResponseFromJson(
    Map<String, dynamic> json) {
  return MfsServiceCalculatorResponse(
    receipt_Due_Amount_from_consumer:
        json['receipt_Due_Amount_from_consumer'] as String,
    out_parameters: json['out_parameters'] as String,
    status_code: json['status_code'] as String,
    sys_to_sys_msg: json['sys_to_sys_msg'] as String,
    user_a_msg: json['user_a_msg'] as String,
  )
    ..receipt_Deducted_Amount = json['receipt_Deducted_Amount'] as String
    ..input_transaction_value = json['input_transaction_value'] as String
    ..outParameters = json['outParameters'] as String;
}

Map<String, dynamic> _$MfsServiceCalculatorResponseToJson(
        MfsServiceCalculatorResponse instance) =>
    <String, dynamic>{
      'receipt_Due_Amount_from_consumer':
          instance.receipt_Due_Amount_from_consumer,
      'receipt_Deducted_Amount': instance.receipt_Deducted_Amount,
      'input_transaction_value': instance.input_transaction_value,
      'out_parameters': instance.out_parameters,
      'outParameters': instance.outParameters,
      'user_a_msg': instance.user_a_msg,
      'sys_to_sys_msg': instance.sys_to_sys_msg,
      'status_code': instance.status_code,
    };
