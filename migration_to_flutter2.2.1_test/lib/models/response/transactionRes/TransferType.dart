
import 'package:cashless/models/Currency.dart';
import 'package:json_annotation/json_annotation.dart';

part 'TransferType.g.dart';

@JsonSerializable()
class TransferType {
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "name")
  String? name;
  @JsonKey(name: "currency")
  Currency? currency;


  TransferType({this.id, this.name, this.currency});

  factory TransferType.fromJson(Map<String, dynamic> json) =>
      _$TransferTypeFromJson(json);

  Map<String, dynamic> toJson() => _$TransferTypeToJson(this);
}
