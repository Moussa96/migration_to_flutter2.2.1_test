// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TransactionTypeResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransactionTypeResponse _$TransactionTypeResponseFromJson(
    Map<String, dynamic> json) {
  return TransactionTypeResponse(
    id: json['id'] as int,
    name: json['name'] as String,
    currency: Currency.fromJson(json['currency'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$TransactionTypeResponseToJson(
        TransactionTypeResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'currency': instance.currency,
    };
