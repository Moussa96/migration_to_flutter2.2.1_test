// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GetTransactionHistoryResponses.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetTransactionHistoryResponses _$GetTransactionHistoryResponsesFromJson(
    Map<String, dynamic> json) {
  return GetTransactionHistoryResponses(
    transactionTypeResponse: (json['transactionTypeResponse'] as List<dynamic>)
        .map((e) =>
            TransactionHistoryResponse.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$GetTransactionHistoryResponsesToJson(
        GetTransactionHistoryResponses instance) =>
    <String, dynamic>{
      'transactionTypeResponse': instance.transactionTypeResponse,
    };
