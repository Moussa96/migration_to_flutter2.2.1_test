// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GetTransHisResCustom.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetTransHisResCustom _$GetTransHisResCustomFromJson(Map<String, dynamic> json) {
  return GetTransHisResCustom(
    currentPage: json['currentPage'] as int,
    pageSize: json['pageSize'] as int,
    totalCount: json['totalCount'] as int,
    elements: (json['elements'] as List<dynamic>)
        .map((e) =>
            GetTransHisResCustomElements.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$GetTransHisResCustomToJson(
        GetTransHisResCustom instance) =>
    <String, dynamic>{
      'currentPage': instance.currentPage,
      'pageSize': instance.pageSize,
      'totalCount': instance.totalCount,
      'elements': instance.elements,
    };
