// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TransferTypeRes.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransferTypeRes _$TransferTypeResFromJson(Map<String, dynamic> json) {
  return TransferTypeRes(
    id: json['id'] as int,
    name: json['name'] as String,
    code: json['code'] as String,
    from: TransferType.fromJson(json['from'] as Map<String, dynamic>),
    to: TransferType.fromJson(json['to'] as Map<String, dynamic>),
    customFields: (json['customFields'] as List<dynamic>)
        .map((e) => CustomFields.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$TransferTypeResToJson(TransferTypeRes instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'code': instance.code,
      'from': instance.from,
      'to': instance.to,
      'customFields': instance.customFields,
    };
