// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'GetTransHisResCustomElements.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetTransHisResCustomElements _$GetTransHisResCustomElementsFromJson(
    Map<String, dynamic> json) {
  return GetTransHisResCustomElements(
    json['id'] as int,
    DateTime.parse(json['date'] as String),
    json['formattedDate'] as String,
    DateTime.parse(json['processDate'] as String),
    json['formattedProcessDate'] as String,
    (json['amount'] as num).toDouble(),
    json['formattedAmount'] as String,
    TransferTypeRes.fromJson(json['transferType'] as Map<String, dynamic>),
    json['description'] as String,
    Member.fromJson(json['member'] as Map<String, dynamic>),
    json['chargedBack'] as bool,
    json['transactionNumber'] as String,
    (json['customValues'] as List<dynamic>)
        .map((e) => CustomValue.fromJson(e as Map<String, dynamic>))
        .toList(),
  )..chargedBackOfTransferID = json['chargedBackOfTransferID'] as int;
}

Map<String, dynamic> _$GetTransHisResCustomElementsToJson(
        GetTransHisResCustomElements instance) =>
    <String, dynamic>{
      'id': instance.id,
      'date': instance.date!.toIso8601String(),
      'formattedDate': instance.formattedDate,
      'processDate': instance.processDate!.toIso8601String(),
      'formattedProcessDate': instance.formattedProcessDate,
      'amount': instance.amount,
      'formattedAmount': instance.formattedAmount,
      'transferType': instance.transferType,
      'description': instance.description,
      'member': instance.member,
      'chargedBack': instance.chargedBack,
      'transactionNumber': instance.transactionNumber,
      'chargedBackOfTransferID': instance.chargedBackOfTransferID,
      'customValues': instance.customValues,
    };
