// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TransferType.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransferType _$TransferTypeFromJson(Map<String, dynamic> json) {
  return TransferType(
    id: json['id'] as int,
    name: json['name'] as String,
    currency: Currency.fromJson(json['currency'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$TransferTypeToJson(TransferType instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'currency': instance.currency,
    };
