import 'package:cashless/models/Currency.dart';
import 'package:json_annotation/json_annotation.dart';

part 'TransactionTypeResponse.g.dart';

@JsonSerializable()
class TransactionTypeResponse {
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "name")
  String? name;
  @JsonKey(name: "currency")
  Currency? currency;


  TransactionTypeResponse({this.id, this.name, this.currency});

  factory TransactionTypeResponse.fromJson(Map<String, dynamic> json) =>
      _$TransactionTypeResponseFromJson(json);

  Map<String, dynamic> toJson() => _$TransactionTypeResponseToJson(this);
}





