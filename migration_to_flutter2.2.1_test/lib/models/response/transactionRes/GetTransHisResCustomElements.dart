import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/Member.dart';
import 'package:cashless/models/response/CustomFields.dart';
import 'package:cashless/models/response/transactionRes/TransferTypeRes.dart';
import 'package:json_annotation/json_annotation.dart';

part 'GetTransHisResCustomElements.g.dart';

@JsonSerializable()
class GetTransHisResCustomElements {
  int? id;
  DateTime? date;
  String? formattedDate;
  DateTime? processDate;
  String?  formattedProcessDate;
  double? amount;
  String? formattedAmount;
  TransferTypeRes? transferType;
  String? description;
  Member? member;
  bool? chargedBack;
  String? transactionNumber;
  int? chargedBackOfTransferID;

  List<CustomValue> customValues;

  GetTransHisResCustomElements(
      this.id,
      this.date,
      this.formattedDate,
      this.processDate,
      this.formattedProcessDate,
      this.amount,
      this.formattedAmount,
      this.transferType,
      this.description,
      this.member,
      this.chargedBack,
      this.transactionNumber,
      this.customValues);

  factory GetTransHisResCustomElements.fromJson(Map<String, dynamic> json) =>
      _$GetTransHisResCustomElementsFromJson(json);

  Map<String, dynamic> toJson() => _$GetTransHisResCustomElementsToJson(this);
}
