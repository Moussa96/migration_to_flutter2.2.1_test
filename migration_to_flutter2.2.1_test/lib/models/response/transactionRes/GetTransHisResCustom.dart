import 'package:cashless/models/response/transactionRes/GetTransHisResCustomElements.dart';
import 'package:json_annotation/json_annotation.dart';

part 'GetTransHisResCustom.g.dart';

@JsonSerializable()
class GetTransHisResCustom {
  @JsonKey(name: 'currentPage')
  int? currentPage;
  @JsonKey(name: 'pageSize')
  int? pageSize;
  @JsonKey(name: 'totalCount')
  int? totalCount;
  @JsonKey(name: 'elements')
  List<GetTransHisResCustomElements>? elements;

  GetTransHisResCustom({
      this.currentPage, this.pageSize, this.totalCount, this.elements});

  factory GetTransHisResCustom.fromJson(Map<String, dynamic> json) =>
      _$GetTransHisResCustomFromJson(json);

  Map<String, dynamic> toJson() => _$GetTransHisResCustomToJson(this);
}
