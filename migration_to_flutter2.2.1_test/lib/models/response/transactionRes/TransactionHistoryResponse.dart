import 'package:cashless/models/response/transactionRes/TransactionTypeResponse.dart';
import 'package:json_annotation/json_annotation.dart';

part 'TransactionHistoryResponse.g.dart';

@JsonSerializable()
class TransactionHistoryResponse {
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "type")
  TransactionTypeResponse? type;
  @JsonKey(name: "default")
  String? defaultVal;


  TransactionHistoryResponse({this.id, this.type, this.defaultVal});

  factory TransactionHistoryResponse.fromJson(Map<String, dynamic> json) =>
      _$TransactionHistoryResponseFromJson(json);

  Map<String, dynamic> toJson() => _$TransactionHistoryResponseToJson(this);
}
