// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'TransactionHistoryResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TransactionHistoryResponse _$TransactionHistoryResponseFromJson(
    Map<String, dynamic> json) {
  return TransactionHistoryResponse(
    id: json['id'] as int,
    type:
        TransactionTypeResponse.fromJson(json['type'] as Map<String, dynamic>),
    defaultVal: json['default'] as String,
  );
}

Map<String, dynamic> _$TransactionHistoryResponseToJson(
        TransactionHistoryResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'default': instance.defaultVal,
    };
