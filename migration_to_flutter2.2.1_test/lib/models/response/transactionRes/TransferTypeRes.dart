import 'package:cashless/models/response/CustomFields.dart';
import 'package:cashless/models/response/transactionRes/TransferType.dart';
import 'package:json_annotation/json_annotation.dart';

part 'TransferTypeRes.g.dart';

@JsonSerializable()
class TransferTypeRes {
  @JsonKey(name: "id")
  int? id;
  @JsonKey(name: "name")
  String? name;
  @JsonKey(name: "code")
  String? code;
  @JsonKey(name: "from")
  TransferType? from;
  @JsonKey(name: "to")
  TransferType? to;
  @JsonKey(name: "customFields")
  List<CustomFields>? customFields;

  TransferTypeRes({
      this.id, this.name, this.code, this.from, this.to, this.customFields});

  factory TransferTypeRes.fromJson(Map<String, dynamic> json) =>
      _$TransferTypeResFromJson(json);

  Map<String, dynamic> toJson() => _$TransferTypeResToJson(this);
}
