import 'package:cashless/models/response/transactionRes/TransactionHistoryResponse.dart';
import 'package:json_annotation/json_annotation.dart';

part 'GetTransactionHistoryResponses.g.dart';

@JsonSerializable()
class GetTransactionHistoryResponses {

  List<TransactionHistoryResponse>? transactionTypeResponse;

  GetTransactionHistoryResponses({this.transactionTypeResponse});

  factory GetTransactionHistoryResponses.fromJson(Map<String, dynamic> json) => _$GetTransactionHistoryResponsesFromJson(json);
  Map<String, dynamic> toJson() => _$GetTransactionHistoryResponsesToJson(this);

}