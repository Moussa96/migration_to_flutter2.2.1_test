import 'package:json_annotation/json_annotation.dart';


part 'Fees.g.dart';

@JsonSerializable()
class Fees{
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'amount')
  double? amount;
  @JsonKey(name: 'formattedAmount')
  String? formattedAmount;

  Fees({this.name, this.amount, this.formattedAmount});
  factory Fees.fromJson(Map<String, dynamic> json) => _$FeesFromJson(json);
  Map<String, dynamic> toJson() => _$FeesToJson(this);
}