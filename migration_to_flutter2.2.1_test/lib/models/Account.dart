import 'package:json_annotation/json_annotation.dart';

import 'AccountInfo.dart';
import 'AccountStatus.dart';

part 'Account.g.dart';

@JsonSerializable()
class Account{
  @JsonKey(name: 'account')
  AccountInfo? accountInfo;
  @JsonKey(name: 'status')
  AccountStatus? accountStatus;

  Account({this.accountInfo, this.accountStatus});

  factory Account.fromJson(Map<String, dynamic> json) => _$AccountFromJson(json);
  Map<String, dynamic> toJson() => _$AccountToJson(this);
}
