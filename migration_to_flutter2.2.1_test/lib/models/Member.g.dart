// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Member.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Member _$MemberFromJson(Map<String, dynamic> json) {
  return Member(
    id: json['id'] as int,
    name: json['name'] as String,
    username: json['username'] as String,
    email: json['email'] as String,
    groupName: json['groupName'] as String,
    groupCode: json['groupCode'] as String,
    groupNature: json['groupNature'] as String,
    forceChangePassword: json['forceChangePassword'] as bool,
    customValues: (json['customValues'] as List<dynamic>)
        .map((e) => CustomValue.fromJson(e as Map<String, dynamic>))
        .toList(),
    images: (json['images'] as List<dynamic>)
        .map((e) => Image.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$MemberToJson(Member instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'username': instance.username,
      'email': instance.email,
      'groupName': instance.groupName,
      'groupCode': instance.groupCode,
      'groupNature': instance.groupNature,
      'forceChangePassword': instance.forceChangePassword,
      'customValues': instance.customValues,
      'images': instance.images,
    };
