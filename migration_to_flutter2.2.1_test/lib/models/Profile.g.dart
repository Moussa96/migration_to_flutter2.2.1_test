// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Profile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Profile _$ProfileFromJson(Map<String, dynamic> json) {
  return Profile(
    member: Member.fromJson(json['profile'] as Map<String, dynamic>),
    requireTransactionPassword: json['requireTransactionPassword'] as bool,
    accounts: (json['accounts'] as List<dynamic>)
        .map((e) => AccountInfo.fromJson(e as Map<String, dynamic>))
        .toList(),
    permissions:
        (json['permissions'] as List<dynamic>).map((e) => e as String).toList(),
    canMakeMemberPayments: json['canMakeMemberPayments'] as bool,
    canMakeSystemPayments: json['canMakeSystemPayments'] as bool,
    decimalCount: json['decimalCount'] as int,
    decimalSeparator: json['decimalSeparator'] as String,
    errorCode: json['errorCode'] as String,
  );
}

Map<String, dynamic> _$ProfileToJson(Profile instance) => <String, dynamic>{
      'profile': instance.member,
      'requireTransactionPassword': instance.requireTransactionPassword,
      'accounts': instance.accounts,
      'permissions': instance.permissions,
      'canMakeMemberPayments': instance.canMakeMemberPayments,
      'canMakeSystemPayments': instance.canMakeSystemPayments,
      'decimalCount': instance.decimalCount,
      'decimalSeparator': instance.decimalSeparator,
      'errorCode': instance.errorCode,
    };
