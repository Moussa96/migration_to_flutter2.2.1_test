import 'package:json_annotation/json_annotation.dart';
part 'ResetPasswordResponse.g.dart';

@JsonSerializable()
class ResetPasswordResponse{
  @JsonKey(name: 'status')
  String? status;
  @JsonKey(name: 'message')
  String? message;

  ResetPasswordResponse({this.status, this.message});

  factory ResetPasswordResponse.fromJson(Map<String, dynamic> json) => _$ResetPasswordResponseFromJson(json);
  Map<String, dynamic> toJson() => _$ResetPasswordResponseToJson(this);
}