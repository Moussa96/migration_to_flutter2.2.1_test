import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/Product.dart';
import 'package:json_annotation/json_annotation.dart';
part 'Payment.g.dart';

@JsonSerializable()
class Payment{
  @JsonKey(disallowNullValue: true, name: 'transferTypeId')
  String? transferTypeId;
  @JsonKey(disallowNullValue: true, name: 'transferTypeCode')
  String? transferTypeCode;
  @JsonKey(disallowNullValue: true, name: 'toMemberId')
  String? toMemberId;
  @JsonKey(disallowNullValue: true, name: 'onbehalfOfMemberId')
  String? onbehalfOfMemberId;
  @JsonKey(disallowNullValue: true, name: 'amount')
  double? amount;
  @JsonKey(disallowNullValue: true, name: 'fromSystem')
  bool? fromSystem;
  @JsonKey(disallowNullValue: true, name: 'description')
  String? description;
  @JsonKey(ignore: true)
  String? fromMemberId;
  @JsonKey(ignore: true)
  String? paymentMethod;
  @JsonKey(ignore: true)
  String? posNumber;
  @JsonKey(disallowNullValue: true, name: 'products')
  @JsonKey(name: 'products',ignore: true)
  List<Product>? products = [];
  @JsonKey(disallowNullValue: true, name: 'customValues')
  List<CustomValue>? customValues = [];
  static String   QR_Payment_Method = "qr";
  static String   PIN_Payment_Method ="pin";
  static String   NFC_Payment_Method ="nfc";
  static String   VIP_Payment_Method ="vip";


  Payment({
    this.transferTypeId,
    this.transferTypeCode,
    this.toMemberId,
    this.onbehalfOfMemberId,
    this.amount,
    this.fromSystem,
    this.description,
    this.fromMemberId,
    this.paymentMethod,
    this.posNumber,
    this.products,
    this.customValues});

  factory Payment.fromJson(Map<String, dynamic> json) => _$PaymentFromJson(json);
  Map<String, dynamic> toJson() => _$PaymentToJson(this);
}
