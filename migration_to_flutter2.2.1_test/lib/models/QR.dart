import 'package:cashless/models/User.dart';
import 'package:cashless/models/Payment.dart';
import 'package:json_annotation/json_annotation.dart';
part 'QR.g.dart';

@JsonSerializable()
class QR{
  @JsonKey(disallowNullValue: true, name: 'qrType')
  String? qrType;
  @JsonKey(disallowNullValue: true, name: 'payment')
  Payment? payment;
  @JsonKey(disallowNullValue: true, name: 'user')
  User? user;


  QR({
    this.qrType,
    this.payment,
    this.user
  });

  factory QR.fromJson(Map<String, dynamic> json) => _$QRFromJson(json);
  Map<String, dynamic> toJson() => _$QRToJson(this);
}