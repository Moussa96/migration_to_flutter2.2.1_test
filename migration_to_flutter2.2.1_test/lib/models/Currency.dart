import 'package:json_annotation/json_annotation.dart';
part 'Currency.g.dart';

@JsonSerializable()
class Currency{
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'symbol')
  String? symbol;

  Currency({this.id, this.name, this.symbol});
  factory Currency.fromJson(Map<String, dynamic> json) => _$CurrencyFromJson(json);
  Map<String, dynamic> toJson() => _$CurrencyToJson(this);
}