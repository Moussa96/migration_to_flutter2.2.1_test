import 'package:json_annotation/json_annotation.dart';
part 'Product.g.dart';

@JsonSerializable()
class Product{
  @JsonKey(disallowNullValue: true, name: 'title')
  String? title;
  @JsonKey(disallowNullValue: true, name: 'categoryName')
  String? categoryName;
  @JsonKey(disallowNullValue: true, name: 'categoryId')
  int? categoryId;
  @JsonKey(disallowNullValue: true, name: 'imageUrl')
  String? imageUrl;
  @JsonKey(disallowNullValue: true, name: 'description')
  String? description;
  @JsonKey(disallowNullValue: true, name: 'orderedAmount')
  int? orderedAmount;
  @JsonKey(disallowNullValue: true, name: 'price')
  double? price;
  @JsonKey(disallowNullValue: true, name: 'totalPrice')
  double? totalPrice;
  @JsonKey(disallowNullValue: true, name: 'productId')
  int? productId;

  @JsonKey(disallowNullValue: true, name: 'merchantId')
  String? merchantId;


  Product({
      this.title,
      this.categoryName,
      this.categoryId,
      this.imageUrl,
      this.description,
      this.orderedAmount = 0,
      this.price,
      this.productId,
      this.merchantId,
      this.totalPrice
  });

  factory Product.fromJson(Map<String, dynamic> json) => _$ProductFromJson(json);
  Map<String, dynamic> toJson() => _$ProductToJson(this);
}


// class Product{
//   String _title;
//   String _categoryName;
//   int _categoryId;
//   String _imageUrl;
//   String _description;
//   int _orderedAmount = 0;
//   double _price;
//   int _productId;
//   String _merchantId;
//   int _quantity;
//
//   int get productId => _productId;
//
//   set productId(int value) {
//     _productId = value;
//   }
//
//   double _totalPrice;
//
//
//   String get title => _title;
//
//   set title(String value) {
//     _title = value;
//   }
//
//   String get categoryName => _categoryName;
//
//   set categoryName(String value) {
//     _categoryName = value;
//   }
//
//   int get categoryId => _categoryId;
//
//   set categoryId(int value) {
//     _categoryId = value;
//   }
//
//   String get imageUrl => _imageUrl;
//
//   set imageUrl(String value) {
//     _imageUrl = value;
//   }
//
//   String get description => _description;
//
//   set description(String value) {
//     _description = value;
//   }
//
//   int get orderedAmount => _orderedAmount;
//
//   set orderedAmount(int value) {
//     _orderedAmount = value;
//   }
//
//   double get price => _price;
//
//   set price(double value) {
//     _price = value;
//   }
//
//   String get merchantId => _merchantId;
//
//   set merchantId(String value) {
//     _merchantId = value;
//   }
//
//   int get quantity => _quantity;
//
//   set quantity(int value) {
//     _quantity = value;
//   }
//
//   double get totalPrice => _totalPrice;
//
//   set totalPrice(double value) {
//     _totalPrice = value;
//   }
//
//   Map<String, dynamic> toJson() =>
//       {
//         "title": title,
//         "productId": productId,
//         "merchantId": merchantId,
//         "categoryId": categoryId,
//         "price": price,
//         "totalPrice": totalPrice,
//         "quantity": quantity,
//       };
//
//
// }