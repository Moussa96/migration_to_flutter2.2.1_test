import 'package:json_annotation/json_annotation.dart';

import 'Currency.dart';
part 'AccountType.g.dart';

@JsonSerializable()
class AccountType{
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'currency')
  Currency? currency;

  AccountType({this.id, this.name, this.currency});
  factory AccountType.fromJson(Map<String, dynamic> json) => _$AccountTypeFromJson(json);
  Map<String, dynamic> toJson() => _$AccountTypeToJson(this);
}