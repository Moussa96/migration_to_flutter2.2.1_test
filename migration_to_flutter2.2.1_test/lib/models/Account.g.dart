// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Account.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Account _$AccountFromJson(Map<String, dynamic> json) {
  return Account(
    accountInfo: AccountInfo.fromJson(json['account'] as Map<String, dynamic>),
    accountStatus:
        AccountStatus.fromJson(json['status'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$AccountToJson(Account instance) => <String, dynamic>{
      'account': instance.accountInfo,
      'status': instance.accountStatus,
    };
