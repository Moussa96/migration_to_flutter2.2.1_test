// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'QR.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

QR _$QRFromJson(Map<String, dynamic> json) {
  $checkKeys(json, disallowNullValues: const ['qrType', 'payment', 'user']);
  return QR(
    qrType: json['qrType'] as String,
    payment: Payment.fromJson(json['payment'] as Map<String, dynamic>),
    user: User.fromJson(json['user'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$QRToJson(QR instance) => <String, dynamic>{
      'qrType': instance.qrType,
      'payment': instance.payment,
      'user': instance.user,
    };
