// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AccountInfo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccountInfo _$AccountInfoFromJson(Map<String, dynamic> json) {
  return AccountInfo(
    id: json['id'] as int,
    accountType: AccountType.fromJson(json['type'] as Map<String, dynamic>),
    isDefault: json['default'] as bool,
  );
}

Map<String, dynamic> _$AccountInfoToJson(AccountInfo instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.accountType,
      'default': instance.isDefault,
    };
