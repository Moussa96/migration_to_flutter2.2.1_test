// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'AccountStatus.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccountStatus _$AccountStatusFromJson(Map<String, dynamic> json) {
  return AccountStatus(
    balance: (json['balance'] as num).toDouble(),
    formattedBalance: json['formattedBalance'] as String,
    availableBalance: (json['availableBalance'] as num).toDouble(),
    formattedAvailableBalance: json['formattedAvailableBalance'] as String,
    reservedAmount: (json['reservedAmount'] as num).toDouble(),
    formattedReservedAmount: json['formattedReservedAmount'] as String,
    creditLimit: (json['creditLimit'] as num).toDouble(),
    formattedCreditLimit: json['formattedCreditLimit'] as String,
  );
}

Map<String, dynamic> _$AccountStatusToJson(AccountStatus instance) =>
    <String, dynamic>{
      'balance': instance.balance,
      'formattedBalance': instance.formattedBalance,
      'availableBalance': instance.availableBalance,
      'formattedAvailableBalance': instance.formattedAvailableBalance,
      'reservedAmount': instance.reservedAmount,
      'formattedReservedAmount': instance.formattedReservedAmount,
      'creditLimit': instance.creditLimit,
      'formattedCreditLimit': instance.formattedCreditLimit,
    };
