// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) {
  $checkKeys(json, disallowNullValues: const [
    'title',
    'categoryName',
    'categoryId',
    'imageUrl',
    'description',
    'orderedAmount',
    'price',
    'totalPrice',
    'productId',
    'merchantId'
  ]);
  return Product(
    title: json['title'] as String,
    categoryName: json['categoryName'] as String,
    categoryId: json['categoryId'] as int,
    imageUrl: json['imageUrl'] as String,
    description: json['description'] as String,
    orderedAmount: json['orderedAmount'] as int,
    price: (json['price'] as num).toDouble(),
    productId: json['productId'] as int,
    merchantId: json['merchantId'] as String,
    totalPrice: (json['totalPrice'] as num).toDouble(),
  );
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'title': instance.title,
      'categoryName': instance.categoryName,
      'categoryId': instance.categoryId,
      'imageUrl': instance.imageUrl,
      'description': instance.description,
      'orderedAmount': instance.orderedAmount,
      'price': instance.price,
      'totalPrice': instance.totalPrice,
      'productId': instance.productId,
      'merchantId': instance.merchantId,
    };
