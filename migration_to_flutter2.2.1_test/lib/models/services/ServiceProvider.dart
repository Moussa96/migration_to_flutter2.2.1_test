import 'package:cashless/models/services/ChannelScenario.dart';
import 'package:cashless/models/services/MFS_Service.dart';

class ServiceProvider{
  String serviceProviderCode;
  String serviceProviderName;
  String serviceProviderArabicName;
  String description;
  int rank;
  ChannelScenario channelScenario;
  List<MFS_Service> services;

  ServiceProvider(
      this.serviceProviderCode,
      this.serviceProviderName,
      this.serviceProviderArabicName,
      this.description,
      this.rank,
      this.channelScenario,
      this.services);

  factory ServiceProvider.fromJson(dynamic json) {

    var tagObjsJson = json['services'] as List;
    List<MFS_Service> serviceObjs = tagObjsJson.map(
            (packageJson) => MFS_Service.fromJson(packageJson)).toList();
    return ServiceProvider(json['service_provider_code'] as String, json['service_provider_name'] as String,
        json['service_provider_arabic_name'] as String, json['description'] as String,
        json['rank'] as int, ChannelScenario.fromJson(json['channel_scenario']),
        serviceObjs);
  }
}