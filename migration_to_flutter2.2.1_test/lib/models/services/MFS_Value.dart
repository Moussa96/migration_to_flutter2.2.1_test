import 'dart:collection';

class MFS_Value {

  String? _serviceProviderCode;
  String? _serviceCode;
  String? _userB_AccountId;
  String? _externalTxnId;
  String? _externalRefNumber;
  String? _externalTxnDateTime;
  String? _transactionValue;
  Map<String,String> _inParameters = HashMap();


  //ServiceFormCustomValues(this._serviceProviderCode, this._serviceCode);

  String? get serviceCode => _serviceCode;

  set serviceCode(String? value) {
    _serviceCode = value;
  }

  String? get transactionValue => _transactionValue;

  set transactionValue(String? value) {
    _transactionValue = value;
  }

  String? get externalRefNumber => _externalRefNumber;

  String? get serviceProviderCode => _serviceProviderCode;

  set serviceProviderCode(String? value) {
    _serviceProviderCode = value;
  }

  set externalRefNumber(String? value) {
    _externalRefNumber = value;
  }

  String? get externalTxnDateTime => _externalTxnDateTime;

  set externalTxnDateTime(String? value) {
    _externalTxnDateTime = value;
  }

  String? get userB_AccountId => _userB_AccountId;

  Map<String, String> get inParameters => _inParameters;

  set inParameters(Map<String, String> value) {
    _inParameters = value;
  }

  String? get externalTxnId => _externalTxnId;

  set externalTxnId(String? value) {
    _externalTxnId = value;
  }

  set userB_AccountId(String? value) {
    _userB_AccountId = value;
  }

  Map<String, dynamic> toJson() =>
      {
        "service_provider_code": _serviceProviderCode,
        "service_code": _serviceCode,
        "transaction_value": _transactionValue,
        "user_b_account_id": _userB_AccountId,
        "external_txn_id": _externalTxnId,
        "external_ref_number": _externalRefNumber,
        "external_txn_date_time": _externalTxnDateTime,
        "in_parameters": _inParameters
      };
}
