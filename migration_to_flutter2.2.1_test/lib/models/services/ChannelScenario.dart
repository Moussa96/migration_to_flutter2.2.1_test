class ChannelScenario{
  String category;
  String parent;

  ChannelScenario(this.category, this.parent);
  factory ChannelScenario.fromJson(dynamic json) {
    return ChannelScenario(json['category'] as String, json['parent'] as String);
  }
}