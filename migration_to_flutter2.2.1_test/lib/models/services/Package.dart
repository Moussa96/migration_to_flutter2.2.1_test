class Package{
  String packageCode;
  String serviceCode;
  String packageName;
  String packageArabicName;
  String packageTransactionValue;
  String allowMultiplePurchase;
  String evdSelector;
  String hostedStockCardValue;
  String destinationAccountId;
  String status;
  int rank;

  Package(
      this.packageCode,
      this.serviceCode,
      this.packageName,
      this.packageArabicName,
      this.packageTransactionValue,
      this.allowMultiplePurchase,
      this.evdSelector,
      this.hostedStockCardValue,
      this.destinationAccountId,
      this.status,
      this.rank);
  factory Package.fromJson(dynamic json) {
    return Package(json['package_code'] as String, json['service_code'] as String,
      json['package_name'] as String,json['package_arabic_name'] as String,
      json['package_transaction_value'] as String,json['allow_multiple_purchase'] as String,
      json['evd_selector'] as String,json['hosted_stock_card_value'] as String,
      json['destination_account_id'] as String,json['status'] as String,json['rank'] as int);
  }
}