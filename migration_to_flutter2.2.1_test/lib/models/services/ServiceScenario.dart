import 'dart:convert';

import 'package:cashless/models/services/ReceiptConfig.dart';
import 'package:cashless/models/services/ScenarioAccount.dart';
import 'package:cashless/models/services/ScenarioParameter.dart';

class ServiceScenario {
  static final String BILL = 'bill';
  static final String CHARGE_PACKAGES = 'charge_packages';
  static final String CHARGE = 'charge';
  static final String VOUCHER = 'voucher';

  String scenarioID;
  ScenarioAccount scenarioAccount;
  String paymentType;
  List<ScenarioParameter>? transactionParam;
  ReceiptConfig receiptConfig;

  ServiceScenario(this.scenarioID, this.scenarioAccount, this.paymentType,
      this.transactionParam, this.receiptConfig);

  factory ServiceScenario.fromJson(dynamic json) {
    print("///////////////////hhhhhhhhhhhh");
    print(json.toString());
    List<ScenarioParameter>? scenarioParameterObjs;
    var tagObjsJson = json['trx_params'] as List;
    if(tagObjsJson !=null) {
      scenarioParameterObjs = tagObjsJson.map(
              (scenarioParameterJson) =>
              ScenarioParameter.fromJson(scenarioParameterJson)).toList();
    }
    if(json['receipt_config']!=null) {
      return ServiceScenario(json['scenario_id'] as String,
          ScenarioAccount.fromJson(json['account']),
          json['pay_type'] as String, scenarioParameterObjs,
          ReceiptConfig.fromJson(json['receipt_config']));
    }else{
      return ServiceScenario(json['scenario_id'] as String,
          ScenarioAccount.fromJson(json['account']),
          json['pay_type'] as String, scenarioParameterObjs, json['receipt_config']);
    }
  }
}