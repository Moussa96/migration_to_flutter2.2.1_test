class ReceiptConfig {
  late String label;
  bool? printProviderName;

  ReceiptConfig(this.label);
  factory ReceiptConfig.fromJson(dynamic json) {
    print("//////////////");
    print(json.toString());
    if(json['label']==null){
      return ReceiptConfig("");
    }
    return ReceiptConfig(json['label'] as String);
  }
}
