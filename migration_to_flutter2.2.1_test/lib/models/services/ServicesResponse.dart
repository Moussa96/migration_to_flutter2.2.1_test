import 'package:cashless/models/services/ServiceProvider.dart';

class ServicesResponse{
  List<ServiceProvider> serviceProviders = [] ;

  ServicesResponse(this.serviceProviders);

  factory ServicesResponse.fromJson(dynamic json) {
    //print("/////////"+json['serviceProvider']);
    var tagObjsJson = json['serviceProvider'] as List;
    List<ServiceProvider> serviceProviderObjs = tagObjsJson.map(
            (tagJson) => ServiceProvider.fromJson(tagJson)).toList();
    return ServicesResponse(serviceProviderObjs);
  }
}