class ScenarioAccount {
  String label;
  String labelEn;
  String regex;
  String error;
  String errorEn;

  getLabel() {
    return labelEn;
  }

  ScenarioAccount(this.label, this.labelEn, this.regex, this.error, this.errorEn);

  factory ScenarioAccount.fromJson(dynamic json) {
    if(json != null)
      return ScenarioAccount(json['label'] as String,json['label_en'] as String,
        json['regex'] as String,json['err'] as String,
        json['err_en'] as String);
    else
      return json;
  }
}
