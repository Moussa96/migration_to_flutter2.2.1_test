// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'SystemPayment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SystemPayment _$SystemPaymentFromJson(Map<String, dynamic> json) {
  return SystemPayment(
    json['transferTypeCode'] as String,
    (json['amount'] as num).toDouble(),
    (json['customValues'] as List<dynamic>)
        .map((e) => CustomValue.fromJson(e as Map<String, dynamic>))
        .toList(),
  )..transferTypeId = json['transferTypeId'] as String;
}

Map<String, dynamic> _$SystemPaymentToJson(SystemPayment instance) =>
    <String, dynamic>{
      'transferTypeId': instance.transferTypeId,
      'transferTypeCode': instance.transferTypeCode,
      'amount': instance.amount,
      'customValues': instance.customValues,
    };
