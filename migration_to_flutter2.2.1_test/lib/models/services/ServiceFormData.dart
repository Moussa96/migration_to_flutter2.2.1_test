import 'package:cashless/models/CustomValue.dart';

class ServiceFormData {
  String? transferTypeId;
  double? amount;
  List<CustomValue>? customValues = [];
  Map<String, dynamic> toJson() =>
      {
        'transferTypeId': transferTypeId,
        'amount': amount,
        'customValues' : customValues
      };

//factory Payment.fromJson(Map<String, dynamic> json) => _$PaymentFromJson(json);

/// `toJson` is the convention for a class to declare support for serialization
/// to JSON. The implementation simply calls the private, generated
/// helper method `_$UserToJson`.
//Map<String, dynamic> toJson() => _$PaymentToJson(this);
}
