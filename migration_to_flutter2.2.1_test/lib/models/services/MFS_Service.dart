import 'package:cashless/models/services/Package.dart';
import 'package:cashless/models/services/ServiceScenario.dart';

class MFS_Service{
  String serviceName;
  String serviceCode;
  String serviceArabicName;
  String description;
  String serviceProviderCode;
  String serviceProviderName;
  String serviceProviderArabicName;
  String serviceType;
  String serviceTransactionType;
  String denominationType;
  String prereqServiceCode;
  String shortCode;
  String serviceCategory;
  ServiceScenario serviceScenario;
  List<Package> packages;

  MFS_Service(
      this.serviceName,
      this.serviceCode,
      this.serviceArabicName,
      this.description,
      this.serviceProviderCode,
      this.serviceProviderName,
      this.serviceProviderArabicName,
      this.serviceType,
      this.serviceTransactionType,
      this.denominationType,
      this.prereqServiceCode,
      this.shortCode,
      this.serviceCategory,
      this.serviceScenario,
      this.packages);

  factory MFS_Service.fromJson(dynamic json) {
    print('//////////////////////');
    print(json.toString());
    var tagObjsJson = json['Packages'] as List;
    List<Package> packageObjs = tagObjsJson!=null?tagObjsJson.map(
            (packageJson) => Package.fromJson(packageJson)).toList() :[];
    return MFS_Service(json['service_name'] as String, json['service_code'] as String,
        json['service_arabic_name'] as String, json['description'] as String,
      json['service_provider_code'] as String, json['service_provider_name'] as String,
      json['service_provider_arabic_name'] as String, json['service_type'] as String,
      json['service_transaction_type'] as String, json['denomination_type'] as String,
      json['prereq_service_code'] as String, json['short_code'] as String,
      json['service_category'] as String, ServiceScenario.fromJson(json['scenario_conf']), packageObjs);
  }

}