import 'package:json_annotation/json_annotation.dart';
import 'package:cashless/models/CustomValue.dart';
part 'SystemPayment.g.dart';

@JsonSerializable(includeIfNull: false)
class SystemPayment {
  String? transferTypeId;
  late String transferTypeCode;
  late double amount;
  late List<CustomValue> customValues = [];


  SystemPayment.empty();

  SystemPayment(this.transferTypeCode, this.amount,
      this.customValues);

  factory SystemPayment.fromJson(Map<String, dynamic> json) => _$SystemPaymentFromJson(json);
  Map<String, dynamic> toJson() => _$SystemPaymentToJson(this);
}
