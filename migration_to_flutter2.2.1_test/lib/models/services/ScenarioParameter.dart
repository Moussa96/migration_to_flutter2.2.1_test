import 'package:cashless/models/services/ScenarioAccount.dart';

class ScenarioParameter extends ScenarioAccount {
  String jsonId;
  String type;
  String helper;
  String helperEn;
  int min;
  int max;

  ScenarioParameter(String label,String label_en,String regex,String err,String err_en,
      this.jsonId, this.type, this.helper, this.helperEn, this.min, this.max) : super(label, label_en , regex, err, err_en);

  factory ScenarioParameter.fromJson(dynamic json) {
    return ScenarioParameter(json['label'] as String,json['label_en'] as String,
        json['regex'] as String,json['err'] as String,
        json['err_en'] as String,json['json_id'] as String,
        json['type'] as String,json['helper'] as String,
        json['helper_en'] as String, json['min'] as int, json['max'] as int);
  }

}
