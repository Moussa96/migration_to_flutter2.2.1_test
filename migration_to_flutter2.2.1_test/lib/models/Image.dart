import 'package:json_annotation/json_annotation.dart';
part 'Image.g.dart';

@JsonSerializable()
class Image{
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'caption')
  String? caption;
  @JsonKey(name: 'thumbnailUrl')
  String? thumbnailUrl;
  @JsonKey(name: 'fullUrl')
  String? fullUrl;
  @JsonKey(name: 'lastModified')
  String? lastModified;

  Image({this.id, this.caption, this.thumbnailUrl, this.fullUrl,
      this.lastModified});
  factory Image.fromJson(Map<String, dynamic> json) => _$ImageFromJson(json);
  Map<String, dynamic> toJson() => _$ImageToJson(this);
}