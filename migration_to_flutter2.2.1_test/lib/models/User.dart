import 'Account.dart';
import 'CustomValue.dart';
import 'package:cashless/models/CustomValue.dart';
import 'package:json_annotation/json_annotation.dart';
part 'User.g.dart';

@JsonSerializable()
class User{
  @JsonKey(disallowNullValue:true,name: 'id')
  String? id;
  @JsonKey(disallowNullValue: true, name: 'nationalId')
  String? nationalId;
  @JsonKey(disallowNullValue: true, name: 'groupCode')
  String? groupCode;
  @JsonKey(disallowNullValue:true, name: 'name')
  String? name;
  @JsonKey(disallowNullValue:true, name: 'username')
  String? username;
  @JsonKey(disallowNullValue:true, name: 'email')
  String? email;
  @JsonKey(disallowNullValue:true, name: 'password')
  String? password;
  @JsonKey(disallowNullValue:true, name: 'mobileNumber')
  String? mobileNumber;
  @JsonKey(disallowNullValue:true, name: 'accounts')
  List<Account>? accounts;
  @JsonKey(disallowNullValue:true, name: 'customValues')
  List<CustomValue>? customValues = [];
  @JsonKey(disallowNullValue:true, name: 'requireTransactionPassword')
  bool? requireTransactionPassword;
  @JsonKey(disallowNullValue:true, name: 'canMakeMemberPayments')
  bool? canMakeMemberPayments;
  @JsonKey(disallowNullValue:true, name: 'canMakeSystemPayments')
  bool? canMakeSystemPayments;
  @JsonKey(disallowNullValue:true, name: 'decimalSeparator')
  bool? decimalSeparator;
  @JsonKey(disallowNullValue:true, name: 'decimalCount')
  bool? decimalCount;



  User({
    this.id,
    this.mobileNumber,
    this.password,
    this.name,
    this.username,
    this.email,
    this.nationalId,
    this.groupCode,
    this.requireTransactionPassword,
    this.accounts,
    this.canMakeMemberPayments,
    this.canMakeSystemPayments,
    this.decimalCount,
    this.decimalSeparator,
    this.customValues
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}

