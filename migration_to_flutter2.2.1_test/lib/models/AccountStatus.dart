import 'package:json_annotation/json_annotation.dart';


part 'AccountStatus.g.dart';

@JsonSerializable()
class AccountStatus{
  @JsonKey(name: 'balance')
  double? balance;
  @JsonKey(name: 'formattedBalance')
  String? formattedBalance;
  @JsonKey(name: 'availableBalance')
  double? availableBalance;
  @JsonKey(name: 'formattedAvailableBalance')
  String? formattedAvailableBalance;
  @JsonKey(name: 'reservedAmount')
  double? reservedAmount;
  @JsonKey(name: 'formattedReservedAmount')
  String? formattedReservedAmount;
  @JsonKey(name: 'creditLimit')
  double? creditLimit;
  @JsonKey(name: 'formattedCreditLimit')
  String? formattedCreditLimit;

  AccountStatus({
      this.balance,
      this.formattedBalance,
      this.availableBalance,
      this.formattedAvailableBalance,
      this.reservedAmount,
      this.formattedReservedAmount,
      this.creditLimit,
      this.formattedCreditLimit});
  factory AccountStatus.fromJson(Map<String, dynamic> json) => _$AccountStatusFromJson(json);
  Map<String, dynamic> toJson() => _$AccountStatusToJson(this);
}