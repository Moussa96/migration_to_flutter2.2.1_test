// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'PendingStatusResponse.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PendingStatusResponse _$PendingStatusResponseFromJson(
    Map<String, dynamic> json) {
  return PendingStatusResponse(
    json['id'] as int,
    json['pending'] as bool,
    json['fromMemberName'] as String,
    json['toMemberName'] as String,
  );
}

Map<String, dynamic> _$PendingStatusResponseToJson(
        PendingStatusResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'pending': instance.pending,
      'fromMemberName': instance.fromMemberName,
      'toMemberName': instance.toMemberName,
    };
