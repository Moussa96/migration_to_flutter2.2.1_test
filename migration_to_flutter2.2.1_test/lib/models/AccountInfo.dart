import 'package:json_annotation/json_annotation.dart';

import 'AccountType.dart';
part 'AccountInfo.g.dart';

@JsonSerializable()
class AccountInfo{
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'type')
  AccountType? accountType;
  @JsonKey(name: 'default')
  bool? isDefault;

  AccountInfo({this.id, this.accountType, this.isDefault});
  factory AccountInfo.fromJson(Map<String, dynamic> json) => _$AccountInfoFromJson(json);
  Map<String, dynamic> toJson() => _$AccountInfoToJson(this);


}
