import 'package:cashless/models/AccountInfo.dart';
import 'package:json_annotation/json_annotation.dart';
import 'Member.dart';
part 'Profile.g.dart';

@JsonSerializable()
class Profile{
  @JsonKey(name: 'profile')
  Member? member;
  @JsonKey(name: 'requireTransactionPassword')
  bool? requireTransactionPassword;
  @JsonKey(name: 'accounts')
  List<AccountInfo>? accounts;
  @JsonKey(name: 'permissions')
  List<String>? permissions;
  @JsonKey(name: 'canMakeMemberPayments')
  bool? canMakeMemberPayments;
  @JsonKey(name: 'canMakeSystemPayments')
  bool? canMakeSystemPayments;
  @JsonKey(name: 'decimalCount')
  int? decimalCount;
  @JsonKey(name: 'decimalSeparator')
  String? decimalSeparator;
  @JsonKey(name: 'errorCode')
  String? errorCode;

  Profile({
      this.member,
      this.requireTransactionPassword,
      this.accounts,
      this.permissions,
      this.canMakeMemberPayments,
      this.canMakeSystemPayments,
      this.decimalCount,
      this.decimalSeparator,
      this.errorCode});

  factory Profile.fromJson(Map<String, dynamic> json) => _$ProfileFromJson(json);
  Map<String, dynamic> toJson() => _$ProfileToJson(this);
}