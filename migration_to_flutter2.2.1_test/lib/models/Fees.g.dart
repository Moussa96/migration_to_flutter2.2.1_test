// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Fees.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Fees _$FeesFromJson(Map<String, dynamic> json) {
  return Fees(
    name: json['name'] as String,
    amount: (json['amount'] as num).toDouble(),
    formattedAmount: json['formattedAmount'] as String,
  );
}

Map<String, dynamic> _$FeesToJson(Fees instance) => <String, dynamic>{
      'name': instance.name,
      'amount': instance.amount,
      'formattedAmount': instance.formattedAmount,
    };
