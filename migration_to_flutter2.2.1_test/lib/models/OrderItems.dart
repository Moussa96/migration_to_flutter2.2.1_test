import 'package:cashless/models/Product.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/models/Payment.dart';
import 'package:json_annotation/json_annotation.dart';
part 'OrderItems.g.dart';

@JsonSerializable()
class OrderItems{
  @JsonKey(name: 'items')
  List<Product>? items = [];


  OrderItems({
    this.items
  });

  factory OrderItems.fromJson(Map<String, dynamic> json) => _$OrderItemsFromJson(json);
  Map<String, dynamic> toJson() => _$OrderItemsToJson(this);
}
// class OrderItems {
//   List<Map<String, dynamic>> _items = List();
//
//   List<Map<String, dynamic>> get items => _items;
//
//   set items(List<Map<String, dynamic>> value) {
//     _items = value;
//   }
//
//   Map<String, dynamic> toJson() =>
//     {
//       "items": items,
//     };
// }
