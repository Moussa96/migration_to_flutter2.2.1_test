import 'package:json_annotation/json_annotation.dart';
part 'Contact.g.dart';

@JsonSerializable()
class Contact{
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'notes')
  String? notes;


  Contact({this.id, this.notes});

  factory Contact.fromJson(Map<String, dynamic> json) => _$ContactFromJson(json);
  Map<String, dynamic> toJson() => _$ContactToJson(this);
}