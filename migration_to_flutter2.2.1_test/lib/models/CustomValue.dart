import 'package:json_annotation/json_annotation.dart';

part 'CustomValue.g.dart';

@JsonSerializable()
class CustomValue {
  int? fieldId;
  String? internalName;
  String? displayName;
  String? value;

  CustomValue(this.internalName, this.value);

  factory CustomValue.fromJson(Map<String, dynamic> json) => _$CustomValueFromJson(json);
  Map<String, dynamic> toJson() => _$CustomValueToJson(this);
}
