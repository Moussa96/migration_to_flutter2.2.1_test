// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CustomValue.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomValue _$CustomValueFromJson(Map<String, dynamic> json) {
  return CustomValue(
    json['internalName'] as String,
    json['value'] as String,
  )
    ..fieldId = json['fieldId'] as int
    ..displayName = json['displayName'] as String;
}

Map<String, dynamic> _$CustomValueToJson(CustomValue instance) =>
    <String, dynamic>{
      'fieldId': instance.fieldId,
      'internalName': instance.internalName,
      'displayName': instance.displayName,
      'value': instance.value,
    };
