import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/MerchantService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Member.dart';
import 'package:cashless/models/response/ListMerchantsResponse.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/pages/marketplace/old_market_place_products.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class MarketPlace extends KFDrawerContent {
  MarketPlace({Key? key}) : super();

  @override
  _OldMarketPlaceState createState() => _OldMarketPlaceState();
}

class _OldMarketPlaceState extends State<MarketPlace> {
  MerchantService merchantUseCase = MerchantService();
  List<Member> _merchants = [], _filteredMerchants = [];
  int? _merchantsCount;

  loadMerchants() async {
    RetrofitResponse<ListMerchantsResponse> result =
        await merchantUseCase.listMerchants();
    return result;
  }

  @override
  void initState() {
    super.initState();
    loadMerchants().then((result) {
      if (result != null && result.statusCode == 200 && result.data != null) {

        setState(() {
          _merchants.addAll(result.data.elements
              .where((Member val) =>
                  val.groupCode == GROUP_MERCHANT &&
                  val.id.toString() != MFS_TO_MEMBER_ID)
              .toList());
          _merchantsCount = _merchants.length;
          for(int i = 0; i < _merchantsCount!; i++){
            if(_merchants[i].images !=null){
              print(_merchants[i].images![0].fullUrl);

              String fullUrl = _merchants[i].images![0].fullUrl!;
              _merchants[i].images![0].fullUrl = BASE_URL + fullUrl.substring(fullUrl.indexOf("image?id="));
              print(_merchants[i].images![0].fullUrl);
            }
          }
          _filteredMerchants.addAll(_merchants);
        });
      }
    });
  }

  Widget _buildEmptyCard() {
    return Card(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Text(
          getTranslated(context, 'marketplace_nomerchants'),
          style: TextStyle(fontSize: 28.0),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  _searchBar() {
    return Padding(
        padding: const EdgeInsets.all(5),
        child: TextField(
          decoration: InputDecoration(
            hintText: getTranslated(context, 'marketplace_search'),
            suffixIcon: Icon(Icons.search, color: Colors.blue[700]),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.blue[700]!, width: 2)),
            enabledBorder: OutlineInputBorder(borderSide: BorderSide(width: 0)),
          ),
          onChanged: (input) {
            setState(() {
              _filteredMerchants.clear();
              if (input == "") {
                _filteredMerchants.addAll(_merchants);
              } else {
                for (int i = 0; i < _merchants.length; i++) {
                  if (_merchants[i]
                      .name!
                      .toLowerCase()
                      .contains(input.toLowerCase())) {
                    _filteredMerchants.add(_merchants[i]);
                  }
                }
                print(_filteredMerchants);
              }
              _merchantsCount = _filteredMerchants.length;
            });
          },
        ));
  }

  _gridView() {
    return Expanded(
        child: GridView.count(
            crossAxisCount: 2,
            children: List.generate(_merchantsCount!, (index) {
              return Card(
                  color: Colors.white,
                  margin: EdgeInsets.all(5),
                  child: InkWell(
                      highlightColor: Colors.white60,
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MarketPlaceProducts(
                                    merchantId: _filteredMerchants[index].id!)));
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(80.0),
                            child: _filteredMerchants[index].images != null
                                ? Image.network(
                                    _filteredMerchants[index].images![0].fullUrl!,
                                    height: 120)
                                : Image.asset(
                                    'lib/assets/profile.png',
                                     height: 120,
                                  ),
                          ),
                          Padding(
                              padding: EdgeInsets.fromLTRB(0, 10, 0, 5),
                              child: Text(_filteredMerchants[index].name!,
                                  style:
                                      TextStyle(fontWeight: FontWeight.w500))),
                        ],
                      )));
            })));
  }

  Widget _buildMerchantsGrid(BuildContext context) {
    return _merchantsCount == 0
        ? Column(children: [_searchBar(), _buildEmptyCard()])
        : _merchantsCount == null
            ? CustomWidgetBuilder.buildSpinner(context)
            : Column(children: [_searchBar(), _gridView()]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white70.withOpacity(0.9),
        appBar: ActionBar(context,
            titleText: getTranslated(context, 'marketplace_title'),
            icon: Icons.arrow_back_ios,
            pressFunc: () => Navigator.pop(context)),
        body: _buildMerchantsGrid(context));
  }
}
