import 'dart:collection';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/ProductsService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Member.dart';
import 'package:cashless/models/OrderItems.dart';
import 'package:cashless/pages/marketplace/checkout.dart';
import 'package:cashless/pages/product_details/old_products_details.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:cashless/widgets/custom_widgets/CustomAppBar.dart';
import 'package:cashless/widgets/custom_widgets/CustomButton.dart';
import 'package:cashless/widgets/custom_widgets/CustomText.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:cashless/models/Product.dart';
import 'package:cashless/pages/error_page.dart';

class MarketPlaceProducts extends StatefulWidget {
  Member? merchant;

  MarketPlaceProducts({Key? key, this.merchant}) : super();

  @override
  _MarketPlaceProductsState createState() => _MarketPlaceProductsState();
}

class _MarketPlaceProductsState extends State<MarketPlaceProducts> {
  int? _productCount;
  var loggedMerchantId;
  double _total = 0;
  double checkoutTotal = 0.0;
  bool enableCheckoutBtn = false;
  ProductsService _productService = ProductsService();
  List<Product> products = [];
  List<Product> selectedProducts = [];
  List<int> _counter = [];

  @override
  void initState() {
    super.initState();
    Util.loadFromSharedPreferences(Util.USER_ID).then((value) => loggedMerchantId = value);
    new Future.delayed(Duration.zero, () {
      loadProducts(widget.merchant!.id!).then((result) {
        if (result != null && result.statusCode == 200) {
          setState(() {
            _productCount = result.data.totalCount;

            for (int i = 0; i < _productCount!; i++) {
              _counter.add(0);
              Product _product = Product();
              _product.title = result.data.elements[i].title;
              _product.productId = result.data.elements[i].id;
              _product.categoryId = result.data.elements[i].category.id;
              _product.categoryName = result.data.elements[i].category.name;
              _product.description = result.data.elements[i].description;
              if(result.data.elements[i].images !=null){
               String fullUrl = result.data.elements[i].images[0].fullUrl;
               _product.imageUrl = BASE_URL + fullUrl.substring(fullUrl.indexOf("image?id="));
              }
              else{
                _product.imageUrl = null;
              }

              _product.price = result.data.elements[i].price;
              products.add(_product);
            }
          });
        }else if (Util.isConnectionError(result.getErrorCode)) {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ErrorPage(errorType: NETWORK_ERROR)));
        } else if (Util.isServerError(result.getErrorCode)) {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ErrorPage(errorType: SERVER_ERROR)));
        }
      });
    });
  }

  loadProducts(int? _id) async {
    if (_id == null) {
      _id = int.parse(loggedMerchantId);
    }
    Map<String, dynamic> params = new HashMap<String, dynamic>();
    params.putIfAbsent("memberId", () => _id);
    return await _productService.getProductById(params);
  }

  _incrementCounter(int i) {
    setState(() {
      products[i].orderedAmount = products[i].orderedAmount! +  1;
      checkoutTotal+= products[i].price!;
      enableCheckoutBtn = true;
    });
  }

  _decrementCounter(int i) {
    setState(() {
      if (products[i].orderedAmount! <= 0) {
        products[i].orderedAmount = 0;
      } else {
        products[i].orderedAmount= products[i].orderedAmount! - 1;
        checkoutTotal-= products[i].price!;
      }
    });
    selectedProducts =
        products.where((product) => product.orderedAmount! > 0).toList();
    if (selectedProducts.length == 0) {
      setState(() {
        enableCheckoutBtn = false;
      });
    }
  }

  void confirmProducts(BuildContext context) {
    OrderItems orderItems = OrderItems();
    selectedProducts = products.where((product) => product.orderedAmount! > 0).toList();
    _total = 0;
    List<Product> itemsList = [];
    for (var s in selectedProducts) {
      _total += s.price! * s.orderedAmount!;
      Product item = Product(
        totalPrice: s.price! * s.orderedAmount!,
        orderedAmount: s.orderedAmount,
        price: s.price,
        merchantId: loggedMerchantId,
        productId: s.productId,
        categoryId: s.categoryId,
        title: s.title
      );
      itemsList.add(item);
    }
    orderItems.items = itemsList;
    for (var it in orderItems.items!)
    print(it.toJson());
    if (widget.merchant!.id != null) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
              Checkout(
                amount: _total,
                orderItems: orderItems,
                merchant: widget.merchant
              )
        )
      );
    }
    else {
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>
              ProductsDetails(
                  amount: _total,
                  orderItems: orderItems
              )
          )
      );
    }
  }

  Widget _buildEmptyCard() {
    return Card(
      color: Colors.white60,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            getTranslated(context,'no_products'),
            style: TextStyle(fontSize: 32.0),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white.withOpacity(0.9),
        resizeToAvoidBottomInset: false,
        appBar: CustomAppBar(
            title: widget.merchant!.name??
                getTranslated(context, 'products_title')
        ),
        body: Container(
            child: _productCount == null ?
            CustomWidgetBuilder.buildSpinner(context)
              : _productCount == 0 ? _buildEmptyCard()
              : Column(children: [
                  Expanded(
                      child: ListView.builder(
                          itemCount: products.length,
                          itemBuilder: (context, i) =>
                              Container(
                              color: Colors.white,
                              margin: EdgeInsets.only(bottom: 1),
                              padding: EdgeInsets.all(12),
                              child: Row(
                                children: [
                                  Container(
                                    decoration:BoxDecoration(
                                      border: Border.all(color: Colors.grey.shade300),
                                      borderRadius: BorderRadius.circular(50)
                                    ),
                                    child: ClipRRect(
                                      child: products[i].imageUrl != null ?
                                      Image.network(
                                        products[i].imageUrl!,
                                        height: MediaQuery.of(context).size.width*0.25,
                                        width: MediaQuery.of(context).size.width*0.25,
                                        fit: BoxFit.cover
                                      )
                                      :Image.asset(
                                        'lib/assets/profile.png',
                                        height: MediaQuery.of(context).size.width*0.25,
                                        width: MediaQuery.of(context).size.width*0.25,
                                        fit: BoxFit.cover
                                      ),
                                      borderRadius: BorderRadius.circular(50),
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                  Container(
                                    width: MediaQuery.of(context).size.width *0.65,
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            products[i].title!,
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight:
                                                    FontWeight.bold),
                                          ),
                                          Html(
                                            data: products[i].description,
                                            style: {
                                              "html": Style.fromTextStyle(TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.black.withOpacity(0.6)))},
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text(getTranslated(context, "products_details_price")),
                                                  Text(
                                                      products[i]
                                                              .price
                                                              .toString() +
                                                          " " +
                                                          getTranslated(
                                                              context, "units"),
                                                      style: TextStyle(
                                                          fontSize: 16.0,
                                                          fontWeight:
                                                              FontWeight.bold)),
                                                ],
                                              ),
                                              Container(
                                                width: MediaQuery.of(context).size.width*0.35,
                                                child: products[i]
                                                    .orderedAmount!>0?Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    GestureDetector(
                                                      onTap: () async {
                                                        _decrementCounter(i);
                                                      },
                                                      child: Container(
                                                        width: 40,
                                                        height: 40,
                                                        decoration: BoxDecoration(
                                                          border: Border.all(color: Colors.blue[900]!),
                                                          borderRadius: BorderRadius.circular(5)
                                                        ),
                                                        child: Icon(Icons.remove, color: Colors.blue[900]),
                                                      ),
                                                    ),
                                                    Text(
                                                        products[i]
                                                          .orderedAmount
                                                          .toString(),
                                                        style: TextStyle(
                                                          fontSize: 20,
                                                          fontWeight:
                                                            FontWeight
                                                              .bold
                                                        )
                                                    ),
                                                    GestureDetector(
                                                      onTap: () async {
                                                        _incrementCounter(i);
                                                      },
                                                      child: Container(
                                                        width: 40,
                                                        height: 40,
                                                        decoration: BoxDecoration(
                                                          border: Border.all(color: Colors.blue[900]!),
                                                          borderRadius: BorderRadius.circular(5)
                                                        ),
                                                        child: Icon(Icons.add, color: Colors.blue[900]),
                                                      ),
                                                    ),
                                                  ])
                                                :CustomButton(
                                                  text: getTranslated(context,
                                                      "checkout_add_to_cart"),
                                                  padding: 10,
                                                  color: Color(0xffe8ebf5),
                                                  textColor: Color(0xff153fbe),
                                                  onPressed: () async {
                                                    _incrementCounter(i);
                                                  },
                                                ),
                                              )
                                            ],
                                          ),
                                        ]),
                                  ),
                                ],
                              )
                          )
                      )
                  ),
                  enableCheckoutBtn?
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Container(
                          height: 80,
                          color: PRIMARY_COLOR,
                          padding: EdgeInsets.all(10),
                          child: Row(
                            children: [
                              Image.asset("lib/assets/cart_with_bg.png",width: 50),
                              SizedBox(width: 10),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  CustomText(
                                    text:getTranslated(context,'total'),
                                    color: Colors.grey.shade300,
                                    fontSize: 12,
                                  ),
                                  CustomText(text: checkoutTotal.toString()+ " " +
                                      getTranslated(context, "units"),
                                      color: Colors.white),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          height: 80,
                          child: CustomButton(
                            text:getTranslated(context, 'products_btn_confirm'),
                            onPressed: (){confirmProducts(context);},
                            borderRadius: 0,
                            color: Color(0xff1e1d3d),
                            // color: SECONDARY_COLOR,
                          ),
                        ),
                      )
                    ],
                  ):Container()

                ]
            )
        )
    );
  }
}