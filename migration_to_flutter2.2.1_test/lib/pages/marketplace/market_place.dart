import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/MerchantService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Member.dart';
import 'package:cashless/models/response/ListMerchantsResponse.dart';
import 'package:cashless/pages/fragments/navigation_bar.dart';
import 'package:cashless/pages/marketplace/market_place_products.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cashless/pages/error_page.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';

class MarketPlace extends StatefulWidget {
  BuildContext mContext;
  MarketPlace({Key? key,required this.mContext}) : super();

  @override
  _MarketPlaceState createState() => _MarketPlaceState(mContext);
}

class _MarketPlaceState extends State<MarketPlace> {
  MerchantService? merchantUseCase = MerchantService();
  List<Member>? _merchants = [], _filteredMerchants = [];
  int? _merchantsCount;
  bool search = false, no_products = false;

  loadMerchants() async {
    RetrofitResponse<ListMerchantsResponse> result =
        await merchantUseCase!.listMerchants();
    return result;
  }

  late SearchBar searchBar;

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: PRIMARY_COLOR,
        title: Image.asset('lib/assets/navbarMainLogo.png'),
        actions: [searchBar.getSearchAction(context)]);
  }

  _MarketPlaceState(BuildContext mContext) {
    searchBar = new SearchBar(
      hintText: getTranslated(mContext, 'search'),
        onChanged: (input) {
          setState(() {
            search = true;
            _filteredMerchants!.clear();
            if (input == "") {
              _filteredMerchants!.addAll(_merchants!);
            } else {
              for (int i = 0; i < _merchants!.length; i++) {
                if (_merchants![i]
                    .name!
                    .toLowerCase()
                    .contains(input.toLowerCase())) {
                  _filteredMerchants!.add(_merchants![i]);
                }
              }
              print(_filteredMerchants);
            }
            _merchantsCount = _filteredMerchants!.length;
          });
        },
        onCleared: () {
          setState(() {
            search = false;
            _filteredMerchants!.clear();
            _filteredMerchants!.addAll(_merchants!);
            _merchantsCount = _filteredMerchants!.length;
          });
        },
        onClosed: () {
          setState(() {
            search = false;
          });
        },
        setState: setState,
        onSubmitted: print,
        buildDefaultAppBar: buildAppBar,
        inBar: false);
  }

  @override
  void initState() {
    super.initState();
    loadMerchants().then((result) {
      if (result != null && result.statusCode == 200 && result.data != null) {
        setState(() {
          _merchants!.addAll(result.data.elements
              .where((Member val) =>
                  val.groupCode == GROUP_MERCHANT &&
                  val.id.toString() != MFS_TO_MEMBER_ID)
              .toList());
          _merchantsCount = _merchants!.length;
          for (int i = 0; i < _merchantsCount!; i++) {
            if (_merchants![i].images != null) {
              print(_merchants![i].images![0].fullUrl);

              String? fullUrl = _merchants![i].images![0].fullUrl;
              _merchants![i].images![0].fullUrl =
                  BASE_URL + fullUrl!.substring(fullUrl.indexOf("image?id="));
              print(_merchants![i].images![0].fullUrl);
            }
          }
          _filteredMerchants!.addAll(_merchants!);
        });
      } else if (Util.isConnectionError(result.getErrorCode)) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ErrorPage(errorType: NETWORK_ERROR)));
      } else if (Util.isServerError(result.getErrorCode)) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ErrorPage(errorType: SERVER_ERROR)));
      } else {
        setState(() {
          no_products = true;
        });
      }
    });
  }

  Widget _buildEmptyCard() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset('lib/assets/stores_empty_graphics.png'),
        SizedBox(
          height: 15,
        ),
        Text(getTranslated(context, 'stores_list_empty'),
            style: GoogleFonts.lato(
                color: PRIMARY_COLOR,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
                fontSize: 12.9),
            textAlign: TextAlign.center),
        SizedBox(
          height: 3,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Text(getTranslated(context, 'stores_list_empty_message'),
              style: GoogleFonts.lato(
                  color: MAIN_COLOR,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  fontSize: 14.9),
              textAlign: TextAlign.center),
        )
      ],
    );
  }

  Widget _buildEmptySearchCard() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset('lib/assets/search_no_result.png'),
        SizedBox(
          height: 10,
        ),
        Center(
          child: Text(
            getTranslated(context, 'no_result_found'),
            style: GoogleFonts.lato(
                color: CASHCALL_TITLE,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                fontSize: 14.6),
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 60),
          child: Text(
            getTranslated(context, 'no_result_found_message'),
            style: GoogleFonts.lato(
                color: CASHCALL_TITLE,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
                fontSize: 15),
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }

  _merchantListView() {
    return Expanded(
        child: ListView(
            children: List.generate(_merchantsCount!, (index) {
      return Card(
        child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => MarketPlaceProducts(
                          merchant: _filteredMerchants![index],
                        )));
          },
          child: Padding(
            padding: EdgeInsets.all(9),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Card(
                        elevation: 1,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: _filteredMerchants![index].images != null
                              ? Image.network(
                                  _filteredMerchants![index].images![0].fullUrl!,
                                  height: 60,
                                )
                              : Image.asset(
                                  'lib/assets/profile.png',
                                  height: 60,
                                ),
                        )),
                    Padding(
                      padding: const EdgeInsets.all(13),
                      child: Text(_filteredMerchants![index].name!,
                          style: TextStyle(
                              color: TEXT_PRIMARY_COLOR,
                              fontWeight: FontWeight.bold,
                              fontSize: 16)),
                    )
                  ],
                ),
                Localization.of(context).locale.languageCode == 'en'
                    ? Image.asset("lib/assets/services_arrow.png")
                    : RotatedBox(
                        child: Image.asset("lib/assets/services_arrow.png"),
                        quarterTurns: 90,
                      ),
              ],
            ),
          ),
        ),
      );
    })));
  }

  Widget _buildMerchantsList(BuildContext context) {
    return _merchantsCount == 0 && search!
        ? _buildEmptySearchCard()
        : _merchantsCount == 0
            ? Column(children: [_buildEmptyCard()])
            : no_products!
                ? _buildEmptyCard()
                : _merchantsCount == null
                    ? CustomWidgetBuilder.buildSpinner(context)
                    : Column(children: [_merchantListView()]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: BACKGROUND_COLOR,
      appBar: searchBar.build(context),
      body: Padding(
        padding: const EdgeInsets.all(12),
        child: _buildMerchantsList(context),
      ),
      bottomNavigationBar: NavigationBar(activeItem: 2),
    );
  }
}
