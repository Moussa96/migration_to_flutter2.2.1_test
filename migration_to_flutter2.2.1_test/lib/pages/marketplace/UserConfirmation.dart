import 'dart:async';
import 'dart:collection';
import 'dart:convert';

import 'package:cashless/apis/Services/ContactService.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/OrderItems.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/pages/transaction_summary/old_transaction_summary.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../Configurations.dart';
import '../DrawerMain.dart';
import '../pendingRequest.dart';

class UserConfirmation extends StatefulWidget {
  User? user;
  String merchantID;
  double amount;



  // List<Product> products;
  OrderItems orderItems = OrderItems();
  var transactionId;

  UserConfirmation(this.user,this.merchantID,this.amount, this.orderItems,this.transactionId,{Key? key,}) : super(key: key);


  @override
  State<StatefulWidget> createState() => _UserConfirmation ();
}

class _UserConfirmation extends State<UserConfirmation> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
late String username;
late String name;
  
   @override
   void initState() {
  
  username=widget.user!.username!;
  name= widget.user!.name!;
   }
 
  void showAlertDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(backgroundColor: Colors.blue),
          Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: Text(getTranslated(context, "loading"))),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  void gotoHome(BuildContext context){
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (c) => DrawerMain()), (route) => false);
  }


  Future<void> confirmPayment(BuildContext context) async {

    showAlertDialog(context);


    Payment payment = Payment();
    payment.onbehalfOfMemberId=widget.user!.id;
    // payment.fromMemberId=widget.user.id;
    payment.toMemberId = await Util.loadFromSharedPreferences(Util.USER_ID);
    payment.amount = widget.amount;
    payment.transferTypeCode=PURCHASE_TRANSFER_TYPE_CODE;
    List<CustomValue> customValues = [];
    CustomValue idCustomValue =
    CustomValue(CV_REFERENCE_TRANSACTION_ID_PURCHASE, Util.generateUUID());
    CustomValue orderCustomValue =
    CustomValue(ORDER_ITEMS, jsonEncode(widget.orderItems.toJson()));

    if (payment.customValues == null) {
      customValues.add(idCustomValue);
      customValues.add(orderCustomValue);
      payment.customValues = customValues;
    } else {
      payment.customValues!.add(idCustomValue);
      payment.customValues!.add(orderCustomValue);
    }

    print(jsonEncode(payment));
    PaymentService paymentService = PaymentService();
    paymentService.confirmMemberPayment(payment).then((value) {
      if (value.statusCode == 200) {
        Map<String, dynamic> paymentInfo = new HashMap();
        paymentInfo[getTranslated(context, "from")] = value.data!.fromMemberName;
        paymentInfo[getTranslated(context, "to")] = value.data!.toMemberName;
        paymentInfo[getTranslated(context, "total_amount")] = payment.amount;
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => TransactionSummary(paymentInfo)));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(getTranslated(context, "error_payment")),
            duration: Duration(seconds: 2)));
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(

        backgroundColor: Colors.grey[300],
        resizeToAvoidBottomInset: false,
        appBar: ActionBar(context,
            icon: Icons.arrow_back_ios,
            pressFunc: () => Navigator.pop(context)
        ),
        // drawer: DrawerFragment(),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Card(
            elevation: 1,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Column(
              children: <Widget>[
                CustomWidgetBuilder.buildText("Please Confirm the user to complete payment",fontSize: 22),
                // CustomWidgetBuilder.buildText("Please Confirm the user to complete payment",fontSize: 16, textColor: Colors.grey),
                 Container(
                    padding: EdgeInsets.all(5),
                    width: MediaQuery.of(context).size.width*0.85,
                    height: MediaQuery.of(context).size.width*0.85,
                    child:CustomWidgetBuilder.buildText("the username : $username ",fontSize: 15),
                   // Padding: CustomWidgetBuilder.buildRaisedButton(context, onPressedTarget, param, buttonLabel);
                ),
                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Column (children: [
                    CustomWidgetBuilder.buildRaisedButton(context, confirmPayment, null, getTranslated(context, "products_details_btn_confirm"), size: MainAxisSize.min),
                    CustomWidgetBuilder.buildRaisedButton(context, gotoHome, null, getTranslated(context, "transaction_summary_btn"), size: MainAxisSize.min,icon: Icons.home),
                  ],
                  )
                    //CustomWidgetBuilder.buildRaisedButton(context, gotoHome, null, getTranslated(context, "transaction_summary_btn"), size: MainAxisSize.max,icon: Icons.home),
                )
              ],
            ),
          ),
        )
      ),
    );
  }
}