import 'dart:async';
import 'dart:convert';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/Member.dart';
import 'package:cashless/models/OrderItems.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/pages/home/home.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/custom_widgets/CustomAppBar.dart';
import 'package:cashless/widgets/custom_widgets/CustomButton.dart';
import 'package:cashless/widgets/custom_widgets/CustomText.dart';
import 'package:cashless/widgets/custom_widgets/CustomTextFormField.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';

class Checkout extends StatefulWidget {
  double? amount;
  bool? merchantFlow;

  OrderItems? orderItems = OrderItems();
  Member? merchant;
  var transactionId;
  Payment? dynamicQRPayment;

  Checkout(
      {Key? key,
        this.amount,
        this.orderItems,
        this.merchant,
      })
      : super();

  @override
  _CheckoutState createState() => _CheckoutState();
}

class _CheckoutState extends State<Checkout> {
  bool confirmBtnEnabled = true;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController note = TextEditingController();


  void _showLoadingSheet(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isDismissible: false,
      builder: (context) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 30),
        child: Row(
          children: [
            Image.asset('lib/assets/loading_spinner'),
            Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                child: Text(getTranslated(context, "loading"))),
          ],
        ),
      ),
    );
  }

  void _showRequestSentSheet(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isDismissible: false,
      enableDrag: false,
      builder: (context) => Padding(
        padding: const EdgeInsets.all(12),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                Image.asset('lib/assets/stores_request_sent.png', height: 180),
                SizedBox(width: 13),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomText(
                        text: getTranslated(context,'request_confirmation'),
                        fontWeight: FontWeight.bold,
                              fontSize: 16.0, color: TEXT_PRIMARY_COLOR
                      ),
                      SizedBox(height: 8),
                      CustomText(
                       text:getTranslated(context, "checkout_request_confirmation_part1")+
                           widget.amount.toString()+ " " +
                           getTranslated(context, "units")+
                           getTranslated(context, "checkout_request_confirmation_part2")+
                           widget.merchant!.name!+
                           getTranslated(context, "checkout_request_confirmation_part3"),
                        color: Color(0xff505455),
                        fontSize: 14,
                      ),
                      SizedBox(height: 10),
                      CustomText(text: getTranslated(context,'transaction_summary_tanks_message'), fontSize: 14)
                    ],
                  ),
                )
              ],
            ),
            SizedBox(height: 24),
            CustomButton(
              onPressed: (){
                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute
                  (builder: (c) => ConsumerHome()), (route) => false);
              },
              text: getTranslated(context, "OK"),
              color: SECONDARY_COLOR,
              padding: 15,
            )
          ],
        ),
      ),
    );
  }

  void confirmPayment(BuildContext context) async {
    setState(() {
      confirmBtnEnabled = false;
    });
    _showLoadingSheet(context);
    Payment payment = Payment();
    payment.transferTypeCode = AUTHORIZED_PURCHASE_TRANSFER_TYPE_CODE;
    payment.toMemberId = widget.merchant!.id.toString();
    payment.amount = widget.amount;
    payment.description = note.text;
    List<CustomValue> customValues = [];
    CustomValue idCustomValue =
    CustomValue(CV_REFERENCE_TRANSACTION_ID_PURCHASE, Util.generateUUID());
    CustomValue orderCustomValue =
    CustomValue(ORDER_ITEMS, jsonEncode(widget.orderItems!.toJson()));

    if (payment.customValues == null) {
      customValues.add(idCustomValue);
      customValues.add(orderCustomValue);
      payment.customValues = customValues;
    } else {
      payment.customValues!.add(idCustomValue);
      payment.customValues!.add(orderCustomValue);
    }

    PaymentService paymentService = PaymentService();
    print(payment.toJson());
    paymentService.confirmMemberPayment(payment).then((value) {
      if (value.statusCode == 200 && value.data!.pending!) {
        Navigator.pop(context);
        _showRequestSentSheet(context);
      } else {
        Timer(Duration(seconds: 1), () {
          Navigator.pop(context);
          setState(() {
            confirmBtnEnabled = true;
          });
        });
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(getTranslated(context, "error_payment")),
            duration: Duration(seconds: 2)));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor:BACKGROUND_COLOR,
        resizeToAvoidBottomInset: true,
        appBar: CustomAppBar(
          title: getTranslated(context, 'products_details_title'),
        ),
        body: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 12),
                  child: Column(
                    children: [
                      Card(
                        child: Padding(
                          padding: const EdgeInsets.all(12),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Card(
                                      elevation: 1,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.circular(5),
                                        child: widget.merchant!.images != null
                                            ?Image.network(
                                          widget.merchant!.images![0].fullUrl!,
                                          height: 35,
                                        )
                                            :Image.asset(
                                          'lib/assets/profile.png',
                                          height: 35,
                                        ),
                                      )),
                                  Padding(
                                    padding: const EdgeInsets.all(13),
                                    child: Text(widget.merchant!.name!,
                                        style: TextStyle(
                                            color: TEXT_PRIMARY_COLOR,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16
                                        )
                                    ),
                                  )
                                ],
                              ),
                              CustomText(
                                text: getTranslated(context,
                                    "checkout_confirmation_msg_part1")+
                                    widget.merchant!.name!+
                                    getTranslated(context,
                                        "checkout_confirmation_msg_part2"),
                                fontSize: 14,
                                color: Colors.grey.shade700,
                              )
                            ],
                          ),
                        ),
                      ),
                      Card(
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(12),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  CustomText(
                                    text: getTranslated(context, "products_details_name"),
                                    fontSize: 12,
                                    color: Colors.grey.shade700,
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width*0.3,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        CustomText(
                                          text: getTranslated(context, "products_details_qnt"),
                                          fontSize: 12,
                                          color: Colors.grey.shade700,
                                        ),
                                        CustomText(
                                          text: getTranslated(context, "products_details_price"),
                                          fontSize: 12,
                                          color: Colors.grey.shade700,
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              height: MediaQuery.of(context).size.height * 0.3,
                              child: ListView.builder(
                                itemCount: widget.orderItems!.items!.length,
                                itemBuilder: (context, i) =>
                                Container(
                                  color: i%2==0?Colors.white:Colors.grey.shade200,
                                  padding: EdgeInsets.symmetric(vertical: 15, horizontal: 12),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                    CustomText(
                                      text: widget.orderItems!.items![i].title!,
                                      fontSize: 14
                                    ),
                                      Container(
                                        width: MediaQuery.of(context).size.width*0.3,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            CustomText(
                                              text: widget.orderItems!.items![i].orderedAmount.toString(),
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                            ),
                                            CustomText(
                                              text: widget.orderItems!.items![i].price.toString() +
                                                  " " +
                                                  getTranslated(context, "units"),
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ],
                                        ),
                                      )
                                  ]),
                                )
                              ),
                            ),
                            SizedBox(height: 60),
                            DottedLine(dashLength: 5, dashGapLength: 2),
                            Container(
                            margin: EdgeInsets.all(12),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                  CustomText(text:
                                      getTranslated(context,
                                      "products_details_total"),
                                      color: Colors.grey,
                                      fontSize: 14
                                    ),
                                    CustomText(text: widget.amount.toString()
                                        + " " +
                                        getTranslated(context, "units"),
                                    fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          DottedLine(dashLength: 5, dashGapLength: 2),
                          Container(
                            padding: EdgeInsets.all(12),
                            child: Row(
                                children: [
                                  Image.asset('lib/assets/info_ic.png'),
                                  SizedBox(width: 12),
                                  Flexible(
                                    child: CustomText(
                                      text: getTranslated(context, "checkout_note"),
                                      fontSize: 14,
                                      color: Colors.grey
                                    ),
                                  ),
                                ],
                              ),
                          )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            buildAdditionalNote(context),
            CustomButton(
              text: getTranslated(context, "products_details_btn_confirm"),
              borderRadius: 0,
              isEnabled: confirmBtnEnabled,
              onPressed:(){confirmPayment(context);},
            )
          ],
        )
    );
  }
  buildAdditionalNote(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12),
      child: CustomTextFormField(label:getTranslated(context,'additional_information') ,controller:note,
        inputType: TextInputType.multiline,maxLine: null,),
    );
  }
}

