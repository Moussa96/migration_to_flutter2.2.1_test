import 'dart:collection';
import 'dart:convert';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/ProductsService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/OrderItems.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/product_details/old_products_details.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:cashless/models/Product.dart';

class MarketPlaceProducts extends KFDrawerContent {
  int? merchantId;

  MarketPlaceProducts({Key? key, this.merchantId}) : super();

  @override
  _OldMarketPlaceProductsState createState() => _OldMarketPlaceProductsState();
}

class _OldMarketPlaceProductsState extends State<MarketPlaceProducts> {
  int? _productCount;
  var loggedMerchantId;
  double _total = 0;
  bool enableConfirmBtn = false;
  ProductsService _productService = ProductsService();
  List<Product> products = [];
  List<Product> selectedProducts = [];
  List<int> _counter = [];

  @override
  void initState() {
    super.initState();
    Util.loadFromSharedPreferences(Util.USER_ID).then((value) => loggedMerchantId = value);
    new Future.delayed(Duration.zero, () {
      loadProducts(widget.merchantId!).then((result) {
        if (result != null && result.statusCode == 200) {
          setState(() {
            _productCount = result.data.totalCount;
            for (int i = 0; i < _productCount!; i++) {
              _counter.add(0);
            }
            for (int i = 0; i < _productCount!; i++) {
              Product _product = Product();
              _product.title = result.data.elements[i].title;
              _product.productId = result.data.elements[i].id;
              _product.categoryId = result.data.elements[i].category.id;
              _product.categoryName = result.data.elements[i].category.name;
              _product.description = result.data.elements[i].description;
              if(result.data.elements[i].images !=null){
                String fullUrl = result.data.elements[i].images[0].fullUrl;
                _product.imageUrl = BASE_URL + fullUrl.substring(fullUrl.indexOf("image?id="));
              }
              else{
                _product.imageUrl = null;
              }
              // _product.imageUrl = result.data.elements[i].images != null
              //     ? result.data.elements[i].images[0].fullUrl : null;

              _product.price = result.data.elements[i].price;
              products.add(_product);
            }
          });
        }
      });
    });
  }

  loadProducts(int? _id) async {
    if (_id == null) {
      _id = int.parse(loggedMerchantId);
    }
    Map<String, dynamic> params = new HashMap<String, dynamic>();
    params.putIfAbsent("memberId", () => _id);
    return await _productService.getProductById(params);
    // else {
    //   return await _productService.getProducts();
    // }
  }

  _incrementCounter(int i) {
    setState(() {
      products[i].orderedAmount = products[i].orderedAmount! + 1;
      enableConfirmBtn = true;
    });
  }

  _decrementCounter(int i) {
    setState(() {
      if (products[i].orderedAmount! <= 0) {
        products[i].orderedAmount = 0;
      } else {
        products[i].orderedAmount = products[i].orderedAmount! - 1;
      }
    });
    selectedProducts =
        products.where((product) => product.orderedAmount! > 0).toList();
    if (selectedProducts.length == 0) {
      setState(() {
        enableConfirmBtn = false;
      });
    }
  }

  void confirmProducts(BuildContext context) {
    OrderItems orderItems = OrderItems();
    selectedProducts = products.where((product) => product.orderedAmount! > 0).toList();
    _total = 0;
    List<Product> itemsList = [];
    for (var s in selectedProducts) {
      _total += s.price! * s.orderedAmount!;
      Product item = Product(
          totalPrice: s.price! * s.orderedAmount!,
          orderedAmount: s.orderedAmount,
          price: s.price,
          merchantId: loggedMerchantId,
          productId: s.productId,
          categoryId: s.categoryId,
          title: s.title
      );
      itemsList.add(item);
    }
    orderItems.items = itemsList;
    for (var it in orderItems.items!)
      print(it.toJson());
    if (widget.merchantId != null) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  ProductsDetails(
                      amount: _total,
                      orderItems: orderItems,
                      merchantId: widget.merchantId
                  )
          )
      );
    }
    else {
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>
              ProductsDetails(
                  amount: _total,
                  orderItems: orderItems
              )
          )
      );
    }
  }

  Widget _buildEmptyCard() {
    return Card(
      color: Colors.white60,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'No products to display!',
            style: TextStyle(fontSize: 32.0),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white.withOpacity(0.9),
        resizeToAvoidBottomInset: false,
        appBar: ActionBar(context,
            titleText: getTranslated(context, 'products_title'),
            icon: Icons.arrow_back_ios,
            pressFunc: () => Navigator.pop(context)),
        body: Container(
            margin: EdgeInsets.symmetric(horizontal: 5),
            child: _productCount == null ?
            CustomWidgetBuilder.buildSpinner(context)
                : _productCount == 0 ? _buildEmptyCard()
                : Column(children: [
              SizedBox(height: 5),
              Expanded(
                  child: ListView.builder(
                      itemCount: products.length,
                      itemBuilder: (context, i) => Card(
                          clipBehavior: Clip.antiAlias,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.circular(10)),
                          margin: EdgeInsets.symmetric(
                              vertical: 5, horizontal: 5),
                          child: Column(children: [
                            Row(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                products[i].imageUrl != null
                                    ? Image.network(
                                    products[i].imageUrl!,
                                    height: 120,
                                    width: 120,
                                    fit: BoxFit.cover)
                                    : Image.asset('lib/assets/profile.png'  ,   height: 120,
                                    width: 120,
                                    fit: BoxFit.cover),
                                Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Column(
                                      mainAxisAlignment:
                                      MainAxisAlignment.start,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          products[i].title!,
                                          style: TextStyle(
                                              fontSize: 24.0,
                                              fontWeight:
                                              FontWeight.bold),
                                        ),
                                        Container(
                                          width:
                                          MediaQuery.of(context)
                                              .size
                                              .width -
                                              150,
                                          child: Html(
                                            data: products[i]
                                                .description,
                                            style: {
                                              "html": Style.fromTextStyle(TextStyle(
                                                  color: Colors.black.withOpacity(0.6)))},
                                          ),
                                        ),
                                        Text(
                                            products[i]
                                                .price
                                                .toString() +
                                                " " +
                                                getTranslated(
                                                    context, "units"),
                                            style: TextStyle(
                                                fontSize: 16.0,
                                                fontWeight:
                                                FontWeight.normal,
                                                color: Colors.green)),
                                      ]),
                                ),
                              ],
                            ),
                            Row(
                                mainAxisAlignment:
                                MainAxisAlignment.center,
                                children: [
                                  MaterialButton(
                                    shape: CircleBorder(),
                                    color: Colors.blue,
                                    child: Icon(Icons.remove,
                                        color: Colors.white),
                                    onPressed: () async {
                                      _decrementCounter(i);
                                    },
                                  ),
                                  Text(
                                      products[i]
                                          .orderedAmount
                                          .toString(),
                                      style: TextStyle(fontSize: 24)),
                                  MaterialButton(
                                      shape: CircleBorder(),
                                      color: Colors.blue,
                                      child: Icon(Icons.add,
                                          color: Colors.white),
                                      onPressed: () async {
                                        _incrementCounter(i);
                                      })
                                ])
                          ])
                      )
                  )
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.9,
                margin: EdgeInsets.all(10),
                child: CustomWidgetBuilder.buildRaisedButton(
                    context,
                    confirmProducts,
                    null,
                    getTranslated(context, 'products_btn_confirm'),
                    isEnabled: enableConfirmBtn,
                    size: MainAxisSize.max,
                    icon: Icons.arrow_forward),
              )
            ]
            )
        )
    );
  }
}