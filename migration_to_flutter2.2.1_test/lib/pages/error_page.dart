import 'package:flutter/material.dart';
import 'package:cashless/pages/fragments/error_app_bar.dart';
import 'package:cashless/Configurations.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:cashless/localization/localization_utility.dart';
class ErrorPage extends StatelessWidget{
  String errorType;
  String imageName = '';
  String title = '';
  String message = '';

  ErrorPage({Key key,@required this.errorType}) : super();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: BACKGROUND_COLOR,
      appBar: ErrorAppBar(context),
      body: buildErrorPageBody(context),
    );
  }

  buildErrorPageBody(BuildContext context) {
    initBodyParameters(context);
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Image.asset('lib/assets/$imageName.png'),
          SizedBox(height: 10,),
          CustomWidgetBuilder.buildText(title,
              color: PRIMARY_COLOR,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              fontSize: 13.6),
          SizedBox(height: 5,),
          CustomWidgetBuilder.buildText(message,
              color: PRIMARY_COLOR,
              fontWeight: FontWeight.w700,
              fontStyle: FontStyle.normal,
              fontSize: 13.6),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 64,vertical: 21),
            child: CustomWidgetBuilder.buildRaisedButton(context, tryAgain, null,
                getTranslated(context,'try_again'),fontColor: SECONDARY_COLOR,
                fontWeight: FontWeight.w400,
                fontStyle:  FontStyle.normal,
                fontSize: 13.6,
              color: Colors.white
            ),
          )
        ],
      ),
    );
  }

  void initBodyParameters(BuildContext context) {
    if (errorType == NETWORK_ERROR) {
      imageName = 'internet_connection_error_graphics';
      title = getTranslated(context,'error_page_title_internet_connection');
      message = getTranslated(context,'error_page_message_internet_connection');
    } else {
      imageName = 'server_error_graphics';
      title = getTranslated(context,'error_page_title_server_error');
      message = getTranslated(context,'error_page_message_server_error');
    }
  }
  void tryAgain(BuildContext context){
    Navigator.pop(context);
  }
}
