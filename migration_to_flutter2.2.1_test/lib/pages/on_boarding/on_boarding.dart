import 'package:cashless/Configurations.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/pages/login/login_form.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
class OnBoarding extends StatefulWidget {
  OnBoarding({Key? key}) : super();

  @override
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  int currentPosition = 0;
  List<OnBoard> onBoard = [];
  late int onBoardSize;
  bool update = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    onBoard.add(OnBoard(
        'lib/assets/onboarding_top_graphics.png', 'title1', 'message1'));
    onBoard.add(
        OnBoard('lib/assets/onboarding_2_graphics.png', 'title2', 'message2'));
    onBoard.add(
        OnBoard('lib/assets/onboarding_3_graphics.png', 'title3', 'message3'));
    onBoardSize = onBoard.length;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              child: Image.asset(
                onBoard[currentPosition].imagePath,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 22, vertical: 1),
              child: Text(
                  getTranslated(context, 'number${currentPosition + 1}') +
                      "/" +
                      getTranslated(context, 'number${onBoardSize}'),
                  style: GoogleFonts.lato(
                      color: PRIMARY_COLOR,
                      fontWeight: FontWeight.w300,
                      fontStyle: FontStyle.normal,
                      fontSize: 11.9)),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 21,right: 21),
              child: Row(
                children: List.generate(onBoardSize, (index) {
                  return Container(
                    padding: EdgeInsets.all(1),
                    child: Image.asset(index == currentPosition
                        ? 'lib/assets/onboarding_active_dot.png'
                        : 'lib/assets/onboarding_not_active_dot.png'),
                  );
                }),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 22,
                top: 20,
                right: 20
              ),
              child: Text(
                  getTranslated(context, onBoard[currentPosition].title),
                  style: GoogleFonts.lato(
                      color: PRIMARY_COLOR,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                      fontSize: 15.3)),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 7.6, horizontal: 22),
              child: Text(
                  getTranslated(context, onBoard[currentPosition].message),
                  style: GoogleFonts.lato(
                      color: FONT_COLOR,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      fontSize: 13.6)),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 21),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    child: Container(
                      child: Image.asset(
                          Localization.of(context).locale.languageCode == 'en'?
                          'lib/assets/onboarding_previous_arrow_with_blue_bg.png':
                          'lib/assets/onboarding_next_arrowwith_blue_bg.png'),
                    ),
                    onTap: () {
                      setState(() {
                        currentPosition <= 0
                            ? currentPosition = 0
                            : currentPosition--;
                      });
                    },
                  ),
                  InkWell(
                    child: Container(
                      child: Image.asset(
                          Localization.of(context).locale.languageCode == 'en'?
                          'lib/assets/onboarding_next_arrowwith_blue_bg.png':
                          'lib/assets/onboarding_previous_arrow_with_blue_bg.png'
                      ),
                    ),
                    onTap: () {
                      setState(() {
                        currentPosition >= onBoardSize - 1
                            ? Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => LoginForm()))
                            : currentPosition++;
                      });
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: InkWell(
        onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => LoginForm()));
        },
        child: Container(
          width: size.width,
          height: size.height * 0.07,
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: GRAY_COLOR,
                offset: Offset(0, -1),
                blurRadius: 2,
                spreadRadius: 0)
          ], color: SECONDARY_COLOR),
          child: Center(
            child: CustomWidgetBuilder.buildText(
                getTranslated(context, 'get_started_btn'),
                color: Colors.white,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
                fontSize: 18),
          ),
        ),
      ),
    );
  }
}

class OnBoard {
  String imagePath;
  String title;
  String message;
  OnBoard(this.imagePath, this.title, this.message);
}
