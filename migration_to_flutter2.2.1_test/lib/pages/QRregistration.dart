import 'dart:convert';
import 'dart:io';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/ContactService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/QRResults.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/util/NFCUtil.dart';
import 'package:cashless/util/QRGenerator.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart';
import 'package:xml2json/xml2json.dart';

class QRRegistration extends KFDrawerContent {
  bool useQR;
  QRRegistration({Key key, this.useQR = true}) : super();

  @override
  _QRRegistrationState createState() => _QRRegistrationState();
}

class _QRRegistrationState extends State<QRRegistration> {
  final _formKey = GlobalKey<FormState>();
  User user = User();
  bool success = false;
  bool _nfcEnabled = true;
  final _mobileNumberController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();
  String identifier;
  final contact = ContactService();
  final Xml2Json xml2Json = Xml2Json();
  GlobalKey globalKey = new GlobalKey();
  dynamic _userInfo;
  var data;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.useQR) {
      scanQR(context, false);
    }
  }

  void register(BuildContext context) async {
    // Validate returns true if the form is valid, or false
    // otherwise.
    if (widget.useQR && _userInfo == null) {
      CustomWidgetBuilder.buildAlertDialog(context, scanQR, true,
          getTranslated(context, "QRregistration_scanErrorMessage"),
          getTranslated(context, "QRregistration_scanErrorMessageconfirm"));
    }
    if (_formKey.currentState.validate()) {
      // If the form is valid, display a Snackbar.
      // String jsonIdentifier = jsonEncode(_userInfo);
      if (widget.useQR) {
        identifier = await Util.encrypt(_userInfo);
        print(identifier);
        CustomValue user_identifier = CustomValue(
            CV_USER_IDENTIFIER, identifier);
        user_identifier.displayName = 'User Identifier';
        user_identifier.fieldId = 56905;
        CustomValue user_QR = CustomValue(CV_USER_QR, _userInfo);
        user_QR.displayName = 'User QR';
        user_QR.fieldId = 56939;
        user.customValues = [];
        user.customValues.add(user_identifier);
        user.customValues.add(user_QR);
      }

      _formKey.currentState.save();
      user.name = _mobileNumberController.text;
      user.username = _mobileNumberController.text;
      user.mobileNumber = _mobileNumberController.text;
      user.password = _passwordController.text;
      Response response = await contact.register(
          SOAP_USERNAME, SOAP_PASSWORD, user);
      xml2Json.parse(response.body);
      var jsonString = xml2Json.toParker();
      data = jsonDecode(jsonString);
      print('///////////////////////////' + data.toString());
      if (response.statusCode == 200) {
        setState(() {
          success = true;
        });
        Scaffold.of(context).showSnackBar(SnackBar(content: Text(
            'Member register successfully with mobile number ${data['soap:Envelope']['soap:Body']['ns2:registerMemberResponse']['return']['username']}')));
      } else {
        Scaffold.of(context).showSnackBar(SnackBar(content: Text(
            data['soap:Envelope']['soap:Body']['soap:Fault']['faultstring'])));
      }
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(getTranslated(context, "registeration_snack_error"))));
    }
  }

  void onDone(BuildContext context) {
    print('onDone function');
    // Show dialog on Android (iOS has it's own one)
    if (Platform.isAndroid) {
      showDialog(
        context: context,
        builder: (context) =>
            AlertDialog(
              title: const Text("Scan the tag you want to write to"),
              actions: <Widget>[
                FlatButton(
                  child: const Text("OK"),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
        barrierDismissible: false,
      );
    }
  }

  void writeDone(){
    Navigator.pop(context);
    setState(() {
      _nfcEnabled = false;
    });

  }

  void initializeNFC(BuildContext context) async {
    print('initializeNFC function');
    dynamic nfcData =await NFCUtil.writeNFC(context, _userInfo, onDone, writeDone: writeDone);
    print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.');
    print(nfcData);
  }

  void scanQR(BuildContext context, bool closedAlter) async {
    if (closedAlter)
      Navigator.pop(context);
    print('Scan QR');
    QRResults result;
    QRGenerator.scan(encrypt: false).then((value) {
      result = value as QRResults;
      //_userInfo = jsonDecode(result.content);
      _userInfo = result.content;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // drawer: DrawerFragment(),
        appBar: ActionBar(
            context, titleText: getTranslated(context, 'registeration_title'),
            icon: Icons.arrow_back_ios,
            pressFunc: () => Navigator.pop(context)),
        body: ListView(
          children: <Widget>[
            Builder(
              builder: (BuildContext context) {
                return Container(
                  margin: EdgeInsets.all(15.0),
                  padding: EdgeInsets.all(10.0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          //crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Flexible(child: CustomWidgetBuilder
                                .buildFormInputText(
                                controller: _mobileNumberController,
                                hint: getTranslated(
                                    context, 'registeration_mobile'),
                                errorMessage: getTranslated(
                                    context, 'registeration_mobile_required'),
                                inputType: TextInputType.number,
                                maxLength: 11,
                                isEnabled: !success)),
                          ],
                        ),
                        TextFormField(
                            controller: _passwordController,
                            decoration: InputDecoration(hintText: getTranslated(
                                context, 'registeration_password')),
                            keyboardType: TextInputType.number,
                            enabled: !success,
                            maxLength: 4,
                            obscureText: true,
                            validator: (val) {
                              if (val.isEmpty || val.length != 4)
                                return getTranslated(
                                    context, 'registeration_length_err');
                              return null;
                            }
                        ),
                        TextFormField(
                            controller: _confirmPasswordController,
                            decoration: InputDecoration(hintText: getTranslated(
                                context, 'registeration_confirm_password')),
                            keyboardType: TextInputType.number,
                            enabled: !success,
                            maxLength: 4,
                            obscureText: true,
                            validator: (val) {
                              if (val.isEmpty || val.length != 4)
                                return getTranslated(
                                    context, 'registeration_length_err');
                              if (val != _passwordController.text)
                                return getTranslated(
                                    context, "registeration_password_match");
                              return null;
                            }
                        ),
                        // CustomWidgetBuilder.buildFormInputText(controller: _passwordController, hint: getTranslated(context, 'registeration_password'), errorMessage: getTranslated(context, 'registeration_password_required'), inputType: TextInputType.number, maxLength: 4,isEnabled: !success),
                        // CustomWidgetBuilder.buildFormInputText(controller: _confirmPasswordController, hint: getTranslated(context, 'registeration_confirm_password'), errorMessage: getTranslated(context, 'registeration_password_required'), inputType: TextInputType.number,maxLength: 4,isEnabled: !success),

                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: CustomWidgetBuilder.buildRaisedButton(
                              context, register, null, getTranslated(context,
                              'registeration_btn_register'),
                              size: MainAxisSize.max, isEnabled: !success),
                        ),

                        success && widget.useQR ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: CustomWidgetBuilder.buildRaisedButton(
                                  context, initializeNFC, null, getTranslated(
                                  context, 'registeration_btn_initializeNFC'),
                                  size: MainAxisSize.max, isEnabled: _nfcEnabled),
                            )
                          ],
                        ) : Container(),
                      ],
                    ),
                  ),
                );
              },
            ),
          ],
        )
    );
  }
}