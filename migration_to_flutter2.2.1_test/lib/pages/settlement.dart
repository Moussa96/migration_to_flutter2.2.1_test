import 'dart:async';
import 'package:cashless/apis/Services/AccountService.dart';
import 'package:cashless/apis/Services/PaymentService.dart';

import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Balance.dart';
import 'package:cashless/models/services/SystemPayment.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import '../Configurations.dart';
import 'fragments/KF_Drawer.dart';
import 'fragments/action_bar.dart';

class Settlement extends KFDrawerContent {
  @override
  _SettlementState createState() => _SettlementState();

}

class _SettlementState extends State<Settlement> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String _name = '',userCategory='';
  AccountService _accountUseCase = AccountService();
  Balance _balance = Balance();
  SystemPayment payment = SystemPayment.empty();
  PaymentService paymentService = PaymentService();
  bool isEnabled = true;
  var data;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Util.loadFromSharedPreferences(Util.NAME).then((result) {
      updateBalance();
      setState(() {
        _name = result;
      });
    });
    Util.loadFromSharedPreferences(Util.GROUP).then((value) => {
      userCategory = value
    });

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      resizeToAvoidBottomInset: false,
      key: _scaffoldKey,
      appBar: ActionBar(context, icon: Icons.arrow_back_ios,pressFunc:  () => Navigator.pop(context)),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView(
          children: <Widget>[
            CustomWidgetBuilder.buildText(getTranslated(context, 'profile_hello') + _name),
            Card(
                color: Colors.lightGreen,
                child: CustomWidgetBuilder.buildText(getTranslated(context, 'profile_available')+ MONEY_FORMATTER.format(_balance.availableBalance) + " " + _balance.currency,fontWeight:FontWeight.normal,textColor: Colors.white)
            ),

            Padding(padding: const EdgeInsets.all(15.0),
                child: CustomWidgetBuilder.buildRaisedButton(context, SettlementBalance , null,getTranslated(context, 'Settlement_btn_makeSettlement'),size: MainAxisSize.max,isEnabled: isEnabled)),
          ],

        ),
      ),
    );
  }
  void SettlementBalance(BuildContext context)  {
    CustomWidgetBuilder.buildAlertDialog(context, makeSettlement, null, getTranslated(context,'Settlement_confirm_message'), getTranslated(context, 'transactionHistory_chargeBackConfirm'),cancelButton: true,labelCancelButton: getTranslated(context, 'transactionHistory_cancelChargeBack'));
  }
  void makeSettlement(BuildContext context){
    setState(() {
      isEnabled = false;
    });
    showAlertDialog(context);
    print('SettlementBalance function');
    print(userCategory);
    if(userCategory == GROUP_AGENT)
      payment.transferTypeCode = AGENT_SETTLEMENT_TRANSFER_TYPE_CODE;
    else if(userCategory == GROUP_MERCHANT)
      payment.transferTypeCode = MERCHANT_SETTLEMENT_TRANSFER_TYPE_CODE;
    payment.amount = 1;
    //payment.amount = 2;
    paymentService.makeSettlement(payment).then((value) => {
      print('request : ' + payment.toJson().toString()),
      print(value.getException.toString()),
      data = value,
      if(value.data != null){
        _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(getTranslated(context, 'Settlement_successful_transaction')))),
        updateBalance(),
      }else{
        setState(() {
          isEnabled = true;
        }),
        _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(getTranslated(context, 'Settlement_failure_transaction')))),
      },
      paymentDone()
    }
    );
  }
  void updateBalance(){
    _accountUseCase.listAccounts().then((value) => {
      setState(() {
        if(value.data != null)
          _balance = Util.getBalanceInfo(value.data);
      })
    });
  }
  void paymentDone(){
    Timer(Duration(seconds: 1), () {
      Navigator.pop(context);
      Navigator.pop(context);
    });
  }
  void showAlertDialog(BuildContext context){
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(backgroundColor: Colors.blue),
          Container(margin: EdgeInsets.symmetric(horizontal: 10),child:Text(getTranslated(context, "loading"))),
        ],),
    );
    showDialog(barrierDismissible: false,
      context:context,
      builder:(BuildContext context){
        return alert;
      },
    );
  }
}
