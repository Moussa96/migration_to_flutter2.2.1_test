import 'dart:collection';
import 'dart:convert';

import 'package:cashless/apis/Services/TransactionHistoryService.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/models/response/transactionRes/GetTransactionHistoryResponses.dart';
import 'package:cashless/pages/transaction_summary/old_transaction_summary.dart';
import 'package:cashless/util/QRGenerator.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:uuid/uuid.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'fragments/action_bar.dart';

class DynamicQR extends StatelessWidget {
  Map<String, dynamic> cashOutInfo;
  QrImage _qrImage;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  DynamicQR(this.cashOutInfo, {Key key}) : super();
  String userId;
  GetTransactionHistoryResponses response;

//  TransactionsHistoryService history = new TransactionsHistoryService();
  TransactionHistoryService history = new TransactionHistoryService();
  BuildContext context;

  void Enquiry(BuildContext context) async {
    Map<dynamic, dynamic> summary = new HashMap();
    User user = new User();
    var uuid = Uuid();
    String customValue;

    await Util.loadFromSharedPreferences(Util.ACCOUNT_ID).then((result) {
      userId = result;
    });
    await QRGenerator.generate(cashOutInfo).then((value) => {_qrImage = value});

    if (cashOutInfo['QRCodeType'] == "purchase") {
      customValue = "referenceTransactionId";
    } else {
      customValue = "ReferenceTransactionId";
    }

    Map<String, dynamic> params = new HashMap<String, dynamic>();
    params.putIfAbsent("customValue.$customValue", () => customValue);
    params.putIfAbsent(
        "currentPage", () => cashOutInfo['ReferenceTransactionId']);

//    Map<dynamic, dynamic> rpTranscation;
    await history
        .getTransactionHistory(userId, params)
        .then((value) => {
              if (value.data != null &&
                  value.data.elements != null &&
                  value.data.elements[0].customValues[0].value ==
                      cashOutInfo['ReferenceTransactionId'])
                {
                  summary['Transaction Number'] =
                      value.data.elements[0].transactionNumber,
                  summary['From'] = value.data.elements[0].member.username,
                  summary['Total Amount'] =
                      value.data.elements[0].formattedAmount,
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => TransactionSummary(summary))),
                }
              else
                {
                  Scaffold.of(context).showSnackBar(SnackBar(
                      content: Text('Payment was not performed yet.'))),
                }
            })
        .catchError(() => {
              Scaffold.of(context).showSnackBar(
                  SnackBar(content: Text('Payment not performed.')))
            });

//    response = await history.getTransactionHistory(userId,
//        customValueName: customValue,
//        value: cashOutInfo['ReferenceTransactionId']);

    print(response);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: ActionBar(context,
          icon: Icons.arrow_back_ios, pressFunc: () => Navigator.pop(context)),
      // drawer: DrawerFragment(),
      body: Builder(builder: (BuildContext context) {
        return Padding(
          padding: const EdgeInsets.all(15.0),
          child: ListView(
            children: <Widget>[
              CustomWidgetBuilder.buildText('Your QR code'),
              Center(
                child: _qrImage,
              ),
              CustomWidgetBuilder.buildRaisedButton(
                  context, Enquiry, null, 'Enquiry',
                  icon: Icons.remove_red_eye, size: MainAxisSize.max),
            ],
          ),
        );
      }),
    );
  }
}
