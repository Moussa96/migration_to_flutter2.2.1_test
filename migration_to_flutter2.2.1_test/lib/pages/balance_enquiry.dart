import 'dart:io';

import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/AccountService.dart';
import 'package:cashless/apis/Services/ContactService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Account.dart';
import 'package:cashless/models/Balance.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/QRResults.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/util/NFCUtil.dart';
import 'package:cashless/util/QRGenerator.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';

class BalanceEnquiry extends KFDrawerContent {
  String enquiryType;
  BalanceEnquiry({Key key, this.enquiryType}) : super();

  @override
  _BalanceEnquiryState createState() => _BalanceEnquiryState();
}

class _BalanceEnquiryState extends State<BalanceEnquiry> {
  User user = User();
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  AccountService _accountUseCase = AccountService();
  Balance _balance = Balance();
  ContactService _contactsService = ContactService();
  TextEditingController _mobileNumController = TextEditingController();
  TextEditingController _pinController = TextEditingController();
  bool _showScanBtn = true;
  bool _showBalance = false;
  bool _userIdentified = false;

  @override
  void initState() {
    super.initState();
    if(widget.enquiryType == 'pin'){
      _showScanBtn = false;
      _userIdentified = true;
    }
  }

  void scanQR(BuildContext context) async {
    QRResults result ;
    QRGenerator.scan().then((value) async{
      result = value as QRResults;
      String userIdentifier = result.content;
      print("User identifier to search");
      user = await _contactsService.identifyUser(userIdentifier);
      if(user != null){
        setState(() {
          _userIdentified = true;
        });
      }else{
        _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(getTranslated(context,"USER_NOT_FOUND")), duration: Duration(seconds: 2)));
      }
    });
  }

  void readNFC(BuildContext context) async {
    if (Platform.isAndroid) {
      showDialog(context: context,
          builder: (context) => AlertDialog(
              title: Text(getTranslated(context, "SCAN_CARD")),
              actions: <Widget>[
                FlatButton(
                  child: Text(getTranslated(context, "OK")),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                )]
          )
      );
    }
    await NFCUtil.startScanning(context, getNFCData);
  }

  void getNFCData(String userIdentifier) async {
    user = await _contactsService.identifyUser(userIdentifier);
    if(user != null){
      setState(() {
        _userIdentified = true;
        Navigator.of(context, rootNavigator: true).pop('dialog');
      });
    }else{
      _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(getTranslated(context, "USER_NOT_FOUND")), duration: Duration(seconds: 2)));
    }
  }

  void processor ({RetrofitResponse< List<Account>> value}) {
    if(value.statusCode == 200 && value.data != null){
      setState(() {
        _balance = Util.getBalanceInfo(value.data);
        _showBalance = true;
        _userIdentified = false;
        _showScanBtn = false;
      });
    }else
    {
      _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(getTranslated(context, value.getErrorCode/*"INVALID_CREDENTIALS"*/)), duration: Duration(seconds: 2)));
    }
  }

  void validatePin(BuildContext context) async {
    if (_formKey.currentState.validate() ||
        widget.enquiryType == Payment.NFC_Payment_Method) {
      _formKey.currentState.save();
      user.password = _pinController.text;
      if (widget.enquiryType == Payment.PIN_Payment_Method) {
        user.username = _mobileNumController.text;
      }
      else if (widget.enquiryType == Payment.NFC_Payment_Method ||
          widget.enquiryType == Payment.VIP_Payment_Method) {
        user.password = VIP_PASSWORD;
      }
      /*_accountUseCase.listAccounts(user: user).then((value) =>
      {
        if(value.data != null){
          setState(() {
            _balance = Util.getBalanceInfo(value.data);
            _showBalance = true;
            _userIdentified = false;
            _showScanBtn = false;
          })
        }else
          {
            _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(getTranslated(context, "INVALID_CREDENTIALS")), duration: Duration(seconds: 2)))
          }
      });*/
      _accountUseCase.listAccountss(user: user, processor: processor);
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[300],
      resizeToAvoidBottomInset: false,
      appBar: ActionBar(context, icon: Icons.arrow_back_ios,pressFunc:  () => Navigator.pop(context)),
      body: Column(
        children: [
          Form(
            key: _formKey,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  _showBalance==false?CustomWidgetBuilder.buildText(getTranslated(context, "balance_enquiry_title"), fontSize: 30, fontWeight: FontWeight.normal,textColor: Colors.black54):Container(),
                  _showScanBtn?Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20),
                    child: widget.enquiryType == 'qr'||widget.enquiryType == 'vip'?
                    CustomWidgetBuilder.buildRaisedButton(context, scanQR, null, getTranslated(context, "cashin_btn_scanqr"), size:MainAxisSize.max)
                    :CustomWidgetBuilder.buildRaisedButton(
                        context,readNFC , null, getTranslated(context, "cashin_btn_scan_nfc"), size: MainAxisSize.max)
                  ):Container(),
                  _userIdentified? Column(
                    children: [
                      widget.enquiryType=='qr'?CustomWidgetBuilder.buildText(getTranslated(context, "balance_enquiry_enter_pin") + user.name, fontWeight: FontWeight.normal):Container(),
                      widget.enquiryType=='nfc'||widget.enquiryType=='vip'?CustomWidgetBuilder.buildText(getTranslated(context, "balance_enquiry_scanned_user") + user.name, fontWeight: FontWeight.normal):Container(),
                      widget.enquiryType=='pin'?CustomWidgetBuilder.buildFormInputText(
                          controller: _mobileNumController, hint: getTranslated(context, "cashin_mobile_num"),maxLength: 11,inputType: TextInputType.number,
                          errorMessage: getTranslated(context, "cashin_mobile_num_required")):Container(),
                      widget.enquiryType=='pin'||widget.enquiryType=='qr'?CustomWidgetBuilder.buildFormInputText(
                          controller: _pinController, hint: getTranslated(context, "cashin_pin"), maxLength: 4, inputType: TextInputType.number,
                          errorMessage: getTranslated(context, "cashin_pin_required"),isPassword: true):Container(),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 30.0, horizontal: 0),
                        child: CustomWidgetBuilder.buildRaisedButton(
                            context, validatePin, null, getTranslated(context, "confirm_payment_btn"), size: MainAxisSize.max),
                      ),
                    ],
                  ):Container()
                ],
              ),
            ),
          ),
          _showBalance ? Expanded(
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: ListView(
                children: <Widget>[
                  CustomWidgetBuilder.buildText(getTranslated(context, 'profile_hello') + user.username,fontSize: 25, textColor: Colors.black54),
                  Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                          gradient: LinearGradient(begin: Alignment.topRight, end: Alignment.bottomLeft,
                              stops: [0.1, 0.5, 0.7, 0.9],
                              colors: [Colors.lightGreen[700], Colors.lightGreen[600], Colors.lightGreen[500], Colors.lightGreen[400]])
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomWidgetBuilder.buildText(getTranslated(context, 'profile_balance'),align: TextAlign.left, fontWeight:FontWeight.normal,textColor: Colors.white),
                          Center(child: CustomWidgetBuilder.buildText(MONEY_FORMATTER.format(_balance.balance) + " " + _balance.currency,fontWeight:FontWeight.bold, fontSize:22, textColor: Colors.white)),
                          CustomWidgetBuilder.buildText(getTranslated(context, 'profile_available'), fontWeight:FontWeight.normal,textColor: Colors.white),
                          Center(child: CustomWidgetBuilder.buildText(MONEY_FORMATTER.format(_balance.availableBalance) + " " + _balance.currency,fontWeight:FontWeight.bold, fontSize:22, textColor: Colors.white))
                        ],
                      )
                  ),
                ],
              ),
            ),
          ):Container()
        ],
      )
    );
  }
}
