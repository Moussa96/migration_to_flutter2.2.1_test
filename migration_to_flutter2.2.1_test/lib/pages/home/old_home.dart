import 'dart:async';
import 'dart:collection';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Sse.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/pages/MfsCashcallServices.dart';
import 'package:cashless/pages/profile/old_profile.dart';
import 'package:cashless/pages/qr_payment/old_qr_payment.dart';
import 'package:cashless/pages/balance_enquiry.dart';
import 'package:cashless/pages/balance_transfer.dart';
import 'package:cashless/pages/cash_out.dart';
import 'package:cashless/pages/marketplace/old_market_place.dart';
import 'package:cashless/pages/marketplace/old_market_place_products.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/pages/PendingPaymentRequests.dart';
import 'package:cashless/pages/load_wallet/old_load_wallet_form.dart';
import 'package:cashless/pages/settlement.dart';
import 'package:cashless/pages/static_purchase.dart';
import 'package:cashless/pages/transaction_history/old_transaction_history.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:w3c_event_source/event_source.dart';
import '../QRregistration.dart';

class Home extends KFDrawerContent {
  StreamSubscription<MessageEvent>? subscribtion;

  Home({Key? key}) : super() {
    // final events = EventSource(Uri.parse('https://magento.cashcall.com.eg/sse/'));
    // this.subscribtion = events.events.listen((MessageEvent message) {
    //print('${message.data}');
    //});
  }

  cancelSub() {
    Timer(Duration(seconds: 1), () {
      // Canceling the subscription closes the connection.
      this.subscribtion!.cancel();
    });
  }

  @override
  _OldHomeState createState() => _OldHomeState.cancelSSE(cancelSub);
}

// Full members
class _OldHomeState extends State<Home> {
  static late Stream<dynamic> myStream;
  final int numberOfColumns = 3;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  String? name = 'CashCall', imageUrl = '';
  List<Widget> _activeMenu = [];
  late List<String>? permissions;
  Function _cancelSSE;

  _OldHomeState.cancelSSE(this._cancelSSE);

  //_HomeState.cancelSSE(this._cancelSSE);
  // var h = new Home();
  // Void x = h.cancelSub();

  @override
  void initState() {
    super.initState();
    setState(() {
      Util.loadFromSharedPreferences(Util.IMAGE_URL)
          .then((value) => {imageUrl = value!});
      Util.loadFromSharedPreferences(Util.NAME).then((result) {
        setState(() {
          name = result!;
        });
      });
      Util.loadPermissionsFromSharedPreferences(Util.PERMISSIONS).then((value) {
        permissions = value!;
        initMenu(this.context);
      });
      Util.loadFromSharedPreferences(Util.USERNAME).then((username) {
        subscribeToNotificationServer(context, username!);
      });
    });
  }

  void subscribeToNotificationServer(BuildContext context, String username) {
    myStream = Sse.connect(
      uri: Uri.parse('$DOMAIN/notificationserver/subscribe/$username'),
      closeOnError: false,
      withCredentials: false,
    ).stream;
    myStream.listen((event) {
      print(
          '/////////////////////////////////////////////////////////////////');
      print('Received:' +
          DateTime.now().millisecondsSinceEpoch.toString() +
          ' : ' +
          event.toString());

      showSimpleNotification(
          Text(getTranslated(context, "notification_message")),
          background: Colors.white,
          slideDismiss: true);
      setState(() {
        ActionBar.counter++;
      });
    });
  }

  void initMenu(BuildContext context) {
    _activeMenu.clear();
    Widget loadWallet = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_loadwallet'),
        icon: Icons.account_balance_wallet,
        route: LoadWallet());
    Widget payThroughScanQR = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_payment'),
        icon: Icons.payment,
        route: QRPayment());
    Widget transactionHistory = CustomWidgetBuilder.buildHomeMenu(
        context, onTap,
        title: getTranslated(context, 'home_history'),
        icon: Icons.history,
        route: TransactionHistory());
    Widget marketplace = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_marketplace'),
        icon: Icons.store,
        route: MarketPlace());
    Widget marketplaceProducts = CustomWidgetBuilder.buildHomeMenu(
        context, onTap,
        title: getTranslated(context, 'home_marketplace'),
        icon: Icons.store,
        route: MarketPlaceProducts());
    Widget pendingPaymentRequest = CustomWidgetBuilder.buildHomeMenu(
        context, onTap,
        title: getTranslated(context, 'home_pending_payment_requests'),
        icon: Icons.store,
        route: PendingPaymentRequests());

    Widget addMemberQR = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_addQR_member'),
        icon: Icons.person_add,
        route: QRRegistration());

    Widget addMemberPIN = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_addPIN_member'),
        icon: Icons.person_add,
        route: QRRegistration(useQR: false));
    Widget cashInQR = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_cash_in_qr'),
        icon: Icons.account_balance_wallet,
        route: BalanceTransfer(cashInType: 'qr'));
    Widget cashInNFC = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_cash_in_nfc'),
        icon: Icons.account_balance_wallet,
        route: BalanceTransfer(cashInType: 'nfc'));
    Widget cashInPIN = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_cash_in_pin'),
        icon: Icons.account_balance_wallet,
        route: BalanceTransfer(cashInType: 'pin'));
    Widget vipCashIn = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_cash_in_vip'),
        icon: Icons.stars,
        route: BalanceTransfer(cashInType: 'vip'));
    Widget vipCashOut = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_cash_out_vip'),
        icon: Icons.stars,
        route: CashOut(cashoutType: 'vip'));
    Widget vipBalanceEnquiry = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_balance_enquiry_vip'),
        icon: Icons.stars,
        route: BalanceEnquiry(enquiryType: 'vip'));
    Widget vipPurchase = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_purchase_vip'),
        icon: Icons.stars,
        route: Purchase(purchaseType: 'vip'));
    Widget balanceEnquiryQR = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_balance_enquiry_qr'),
        icon: Icons.live_help,
        titleFontSize: 13,
        route: BalanceEnquiry(enquiryType: 'qr'));
    Widget balanceEnquiryNFC = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_balance_enquiry_nfc'),
        icon: Icons.live_help,
        titleFontSize: 13,
        route: BalanceEnquiry(enquiryType: 'nfc'));
    Widget balanceEnquiryPIN = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_balance_enquiry_pin'),
        icon: Icons.live_help,
        titleFontSize: 13,
        route: BalanceEnquiry(enquiryType: 'pin'));
    Widget cashOutQR = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_cash_out_qr'),
        icon: Icons.payment,
        route: CashOut(cashoutType: 'qr'));
    Widget cashOutNFC = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_cash_out_nfc'),
        icon: Icons.payment,
        route: CashOut(cashoutType: 'nfc'));
    Widget cashOutPIN = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_cash_out_pin'),
        icon: Icons.payment,
        route: CashOut(cashoutType: 'pin'));
    Widget profile = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_profile'),
        icon: Icons.person_outline,
        route: Profile());
    Widget settlement = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_settlement'),
        icon: Icons.monetization_on,
        route: Settlement());
    Widget purchaseQR = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_purchase_qr'),
        icon: Icons.add_shopping_cart,
        route: Purchase(purchaseType: 'qr'));
    Widget purchaseNFC = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_purchase_nfc'),
        icon: Icons.add_shopping_cart,
        route: Purchase(purchaseType: 'nfc'));
    Widget purchasePIN = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_purchase_pin'),
        icon: Icons.add_shopping_cart,
        route: Purchase(purchaseType: 'pin'));
    Widget cashcallServices = CustomWidgetBuilder.buildHomeMenu(context, onTap,
        title: getTranslated(context, 'home_CashcallServices'),
        icon: Icons.playlist_add_check,
        route: CashcallServices());
    Map<String, Widget> allMenus = new HashMap<String, Widget>();
    allMenus[ADD_MEMBER_PIN] = addMemberPIN;
    allMenus[ADD_MEMBER_QR] = addMemberQR;
    allMenus[BALANCE_ENQUIRY_NFC] = balanceEnquiryNFC;
    allMenus[BALANCE_ENQUIRY_PIN] = balanceEnquiryPIN;
    allMenus[BALANCE_ENQUIRY_QR] = balanceEnquiryQR;
    allMenus[BALANCE_ENQUIRY_VIP] = vipBalanceEnquiry;
    allMenus[CASH_IN_NFC] = cashInNFC;
    allMenus[CASH_IN_PIN] = cashInPIN;
    allMenus[CASH_IN_QR] = cashInQR;
    allMenus[CASH_IN_VIP] = vipCashIn;
    allMenus[CASH_OUT_NFC] = cashOutNFC;
    allMenus[CASH_OUT_PIN] = cashOutPIN;
    allMenus[CASH_OUT_QR] = cashOutQR;
    allMenus[CASH_OUT_VIP] = vipCashOut;
    allMenus[CASHCALL_SERVICES] = cashcallServices;
    allMenus[LOAD_WALLET_FORM] = loadWallet;
    allMenus[MARKET_PLACE] = marketplace;
    allMenus[MARKET_PLACE_PRODUCTS] = marketplaceProducts;
    allMenus[PAY_TROUGH_SCAN_QR] = payThroughScanQR;
    allMenus[PENDING_PAYMENT_REQUESTS] = pendingPaymentRequest;
    allMenus[PROFILE] = profile;
    allMenus[PURCHASE_NFC] = purchaseNFC;
    allMenus[PURCHASE_PIN] = purchasePIN;
    allMenus[PURCHASE_QR] = purchaseQR;
    allMenus[PURCHASE_VIP] = vipPurchase;
    allMenus[SETTLEMEN] = settlement;
    allMenus[MARKET_PLACE] = marketplace;
    allMenus[MARKET_PLACE_PRODUCTS] = marketplaceProducts;
    allMenus[VIEW_TRANSACTION_HISTORY] = transactionHistory;
    if (permissions != null) {
      for (int i = 0; i < permissions!.length; i++) {
        if (allMenus.containsKey(permissions![i]))
          _activeMenu.add(allMenus[permissions![i]]!);
      }
    }
  }

  void logoutAction() {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text(getTranslated(context, "home_logout_title")),
              content: Text(getTranslated(context, "home_logout_content")),
              actions: <Widget>[
                FlatButton(
                  child: Text(getTranslated(context, "home_logout_cancel_btn")),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child:
                      Text(getTranslated(context, "home_logout_confirm_btn")),
                  onPressed: () {
                    ActionBar.counter = 0;
                    Util.clear();
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        '/login', (Route<dynamic> route) => false);
                  },
                ),
              ]);
        });
  }

  void setLang(BuildContext context) {
    setState(() {
      initMenu(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Colors.grey[300],
        resizeToAvoidBottomInset: false,
        key: scaffoldKey,
        appBar: ActionBar(
          context,
          icon: Icons.menu,
          pressFunc: widget.onMenuPressed,
          icon3: Icons.exit_to_app,
          pressFunc3: logoutAction,
          setLang: setLang,
          sLang: true,
        ),
        body: Column(
          children: [
            Container(
              color: MINOR_COLOR,
              child: Padding(
                padding: EdgeInsets.only(bottom: 20, top: 40),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(color: Colors.white, width: 2),
                          ),
                          child: CircleAvatar(
                            backgroundColor: Colors.white,
                            backgroundImage: imageUrl != '' && imageUrl != null
                                ? NetworkImage(imageUrl!) as ImageProvider
                                : AssetImage(
                                    'lib/assets/profile.png',
                                  ),
                            radius: 55.0,
                          ),
                        ),
                        Container(
                          child: Text(
                              getTranslated(context, 'home_welcome') + name!,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontSize: 16)),
                          padding: EdgeInsets.all(20.0),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: GridView.count(
                crossAxisCount: numberOfColumns,
                children: List.generate(_activeMenu.length, (index) {
                  return _activeMenu[index];
                }),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void onTap(BuildContext context, String? pageURL) {
    if (pageURL != null) Navigator.pushNamed(context, pageURL);
  }
}
