import 'dart:collection';
import 'dart:convert';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/AccountService.dart';
import 'package:cashless/apis/Services/MfsRestClientImpl.dart';
import 'package:cashless/apis/Sse.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Balance.dart';
import 'package:cashless/models/Mfs/MfsCashcallServices.dart';
import 'package:cashless/models/Mfs/MfsCategories.dart';
import 'package:cashless/models/Mfs/MfsScenarioConfiguration.dart';
import 'package:cashless/models/Mfs/MfsServices.dart';
import 'package:cashless/models/Mfs/MfsSubCategory.dart';
import 'package:cashless/models/Mfs/MfsTrxParams.dart';
import 'package:cashless/main.dart';
import 'package:cashless/models/requests/MfsServiceCalculatorRequest.dart';
import 'package:cashless/pages/services_list/cashcall_services_selected_package_voucher.dart';
import 'package:cashless/pages/fragments/home_action_bar.dart';
import 'package:cashless/pages/fragments/navigation_bar.dart';
import 'package:cashless/pages/services_list/cashcall_services_confirm_bill.dart';
import 'package:cashless/pages/services_list/cashcall_services_confirm_voucher.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/custom_widgets/CustomTextFormField.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:cashless/widgets/custom_widgets/HomeCustomWidget.dart';
import 'package:cashless/widgets/custom_widgets/CashCallCustomWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cashless/pages/error_page.dart';
class ConsumerHome extends StatefulWidget{
  bool? changeLanguage = false;
  ConsumerHome({Key? key, this.changeLanguage}) : super();

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<ConsumerHome> {
  MfsRestClientImpl _mfsService = MfsRestClientImpl();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  MfsCashcallServices cashcall_services = MfsCashcallServices();
  MfsCategories categories = MfsCategories();
  List<MfsServices>? services = [];
  List<MfsSubCategory>? subCategories = [];
  Color selectedColor = Colors.blue[400]!;
  List<bool> aciveCards = [];
  int activeService = 0;
  int lastIndex = 0;
  late Balance balance;
  late Size size;
  static late Stream<dynamic> myStream;
  AccountService _accountUseCase = AccountService();
  final _formKey = GlobalKey<FormState>();
  bool _loading = false;

  TextEditingController accountController = TextEditingController();
  List<TextEditingController> _trxParamsControllers = [];
  @override
  void initState() {
    restoreLang();
    getBalance();
    getUsername();
    getCashCallServices();
    super.initState();
  }

  restoreLang() async {
    if(widget.changeLanguage != null && widget.changeLanguage!) {
      String? value = await Util.loadFromSharedPreferences(Util.LANGUAGE);
      if (value != null && value == 'ar') {
        MyApp.setLocale(context, Locale('ar', 'EG'));
      }
    }
  }

  getUsername() {
    Util.loadFromSharedPreferences(Util.USERNAME).then((username) {
      subscribeToNotificationServer(context, username!);
    });
  }

  getBalance() {
    balance = Balance();
    _accountUseCase.listAccounts().then((value) => {
          setState(() {
            if (value.data != null) balance = Util.getBalanceInfo(value.data!);
          })
        });
  }

  getCashCallServices() {
    loadMfsCategories().then((result) {
      if (result.statusCode == 200 && result.data.cashcall_services != null) {
        setState(() {
          cashcall_services = result.data;
          services = cashcall_services.cashcall_services![0].services;
          subCategories = cashcall_services.cashcall_services![0].sub_category;
          aciveCards = List.generate(cashcall_services.cashcall_services!.length,
              (i) => i == 0 ? true : false);
        });
      } else if (Util.isConnectionError(result.getErrorCode)) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ErrorPage(errorType: NETWORK_ERROR)));
      } else if (Util.isServerError(result.getErrorCode)) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ErrorPage(errorType: SERVER_ERROR)));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content:
                Text(getTranslated(context, result.getErrorCode.toString()))));
      }
    });
  }

  void subscribeToNotificationServer(BuildContext context, String username) {
    myStream = Sse.connect(
      uri: Uri.parse('$DOMAIN/notificationserver/subscribe/$username'),
      closeOnError: false,
      withCredentials: false,
    ).stream;
    myStream.listen((event) {
      print(
          '/////////////////////////////////////////////////////////////////');
      print('Received:' +
          DateTime.now().millisecondsSinceEpoch.toString() +
          ' : ' +
          event.toString());

/*      showSimpleNotification(Text(getTranslated(context, "notification_message")),
          background: Colors.white,slideDismiss: true);*/
      setState(() {
        HomeActionBar.counter++;
      });
    });
  }

  loadMfsCategories() async {
    return await _mfsService.getMfs();
  }

  selectCategory(int index) {
    setState(() {
      services = cashcall_services.cashcall_services![index].services;
      subCategories = cashcall_services.cashcall_services![index].sub_category;
      aciveCards[lastIndex] = false;
      aciveCards[index] = true;
      lastIndex = index;
    });
  }

  void clear() {
    accountController.clear();
    _trxParamsControllers.clear();
  }

  void buildServicePopUpForm(BuildContext context,
      {MfsScenarioConfiguration? scenarioConfiguration,  MfsServices? service}) {
    Function? buildForm;
    scenarioConfiguration!.scenario_id!.toLowerCase() == MFS_SCENARIO_BILL
        ? buildForm = _buildBillView
        : scenarioConfiguration!.scenario_id!.toLowerCase() == MFS_SCENARIO_CHARGE
            ? buildForm = _buildChargeView
            : buildForm = null;
    CashCallCustomWidget.buildServicePopUpForm(context, buildForm,
        service: service!, scenarioConfiguration: scenarioConfiguration);
  }

  Widget _buildChargeView(BuildContext context,
      {MfsScenarioConfiguration? scenarioConfiguration, MfsServices? service}) {
    List<MfsTrxParams>? trxParameters;
    trxParameters = scenarioConfiguration!.trx_params;
    if (trxParameters != null) {
      for (int i = 0; i < trxParameters.length; i++) {
        _trxParamsControllers.add(TextEditingController());
      }
    }
    return Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                Localization.of(context).locale.languageCode == 'en'
                    ? service!.service_name!
                    : service!.service_arabic_name!,
                style: GoogleFonts.lato(
                    color: PRIMARY_COLOR,
                    fontWeight: FontWeight.w700,
                    fontStyle: FontStyle.normal,
                    fontSize: 13.6),
              ),
              InkWell(
                  onTap: () {
                    if (!_loading) Navigator.pop(context);
                  },
                  child: Row(
                    children: [
                      Image.asset('lib/assets/services_list_cancel_ic.png'),
                      SizedBox(
                        width: 3,
                      ),
                      Text(
                        getTranslated(context, 'home_cancel_btn'),
                        style: GoogleFonts.lato(
                            color: CASHCALL_TITLE,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            fontSize: 11.9),
                      ),
                    ],
                  )),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomTextFormField(
                    label: Localization.of(context).locale.languageCode == 'en'
                      ? scenarioConfiguration.account!.label_en!
                      : scenarioConfiguration.account!.label!,
                    labelFontSize: 11.9,
                    addContactButton: true,
                    expression: scenarioConfiguration.account!.regex != null
                        ? RegExp(scenarioConfiguration.account!.regex!)
                        : RegExp(""),
                    expErrorMessage:
                        Localization.of(context).locale.languageCode == 'en'
                            ? scenarioConfiguration.account!.err_en!
                            : scenarioConfiguration.account!.err!,
                    errorMessage:
                        Localization.of(context).locale.languageCode == 'en'
                            ? scenarioConfiguration.account!.err_en!
                            : scenarioConfiguration.account!.err!,
                   inputType: TextInputType.text,
                    controller: accountController),
                SizedBox(
                  height: 10,
                ),
                trxParameters != null
                    ? SingleChildScrollView(
                        child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: trxParameters.length,
                        itemBuilder: (BuildContext context, int i) => Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                                Localization.of(context).locale.languageCode ==
                                        'en'
                                    ? trxParameters![i].label_en!
                                    : trxParameters![i].label!,
                                style: GoogleFonts.lato(
                                    color: TEXT_PRIMARY_COLOR,
                                    fontWeight: FontWeight.w700,
                                    fontStyle: FontStyle.normal,
                                    fontSize: 11.9)),
                            SizedBox(
                              height: 10,
                            ),
                            HomeCustomWidget.buildFormInputText(
                              controller: _trxParamsControllers[i],
                              expression: trxParameters[i].regex != null
                                  ? RegExp(trxParameters[i].regex!): null,

                              expErrorMessage: Localization.of(context)
                                          .locale
                                          .languageCode ==
                                      'en'
                                  ? trxParameters[i]!.err_en!
                                  : trxParameters[i]!.err!,
                              errorMessage: Localization.of(context)
                                          .locale
                                          .languageCode ==
                                      'en'
                                  ? trxParameters[i]!.err_en!
                                  : trxParameters[i]!.err!,
                              maxValue: trxParameters[i]!.max!,
                              minValue: trxParameters[i]!.min!,
                              inputType: TextInputType.text
                            ),
                            trxParameters[i].helper_en != null
                                ? Align(
                                    alignment: Alignment.topRight,
                                    child: Text(Localization.of(context)
                                                .locale
                                                .languageCode ==
                                            'en'
                                        ? trxParameters[i]!.helper_en!
                                        : trxParameters[i]!.helper!))
                                : Container(),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      ))
                    : Container()
              ],
            ),
          ),
          Row(children: [
            Expanded(
              child: InkWell(
                  onTap: () {
                    chargeServiceCalc(
                        context,
                        accountController,
                        service,
                        _trxParamsControllers,
                        trxParameters!,
                        scenarioConfiguration);
                  },
                  child: Container(
                    height: size.height * 0.07,
                    width: size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(3.408)),
                        boxShadow: [
                          BoxShadow(
                              color: GRAY_COLOR,
                              offset: Offset(0, 1),
                              blurRadius: 2,
                              spreadRadius: 0)
                        ],
                        color: SECONDARY_COLOR),
                    child: Center(
                      child: Text(
                        getTranslated(context, 'continue'),
                        style: GoogleFonts.lato(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                            fontSize: 13.6),
                      ),
                    ),
                  )),
            ),
            _loading
                ? Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: CircularProgressIndicator(
                        valueColor: new AlwaysStoppedAnimation<Color>(
                            Colors.blue[700]!)),
                  )
                : Container()
          ]),
        ]));
  }

  Widget _buildBillView(BuildContext context,
      {MfsScenarioConfiguration? scenarioConfiguration, MfsServices? service}) {
    List<MfsTrxParams>? trxParameters;
    trxParameters = scenarioConfiguration!.trx_params;
    if (trxParameters != null) {
      for (int i = 0; i < trxParameters.length; i++) {
        _trxParamsControllers.add(TextEditingController());
      }
    }
    return Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    flex: 8,
                    child: Text(
                      Localization.of(context).locale.languageCode == 'en'
                          ? service!.service_name!
                          : service!.service_arabic_name!,
                      style: GoogleFonts.lato(
                          color: PRIMARY_COLOR,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                          fontSize: 13.6),
                      overflow: TextOverflow.clip,
                    ),
                  ),
                  Spacer(),
                  Flexible(
                    flex: 2,
                    child: InkWell(
                        onTap: () {
                          if (!_loading) Navigator.pop(context);
                        },
                        child: Row(
                          children: [
                            Image.asset(
                                'lib/assets/services_list_cancel_ic.png'),
                            SizedBox(
                              width: 3,
                            ),
                            Text(
                              getTranslated(context, 'home_cancel_btn'),
                              style: GoogleFonts.lato(
                                  color: CASHCALL_TITLE,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  fontSize: 11.9),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ],
                        )),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        CustomTextFormField(
                            label: Localization.of(context).locale.languageCode == 'en'
                                ? scenarioConfiguration!.account!.label_en!
                                : scenarioConfiguration!.account!.label!,
                            addContactButton: true,
                            expression:
                                scenarioConfiguration.account!.regex != null
                                    ? RegExp(scenarioConfiguration.account!.regex!)
                                    : null,
                            expErrorMessage:
                                Localization.of(context).locale.languageCode ==
                                        'en'
                                    ? scenarioConfiguration.account!.err_en!
                                    : scenarioConfiguration.account!.err!,
                            errorMessage:
                                Localization.of(context).locale.languageCode ==
                                        'en'
                                    ? scenarioConfiguration.account!.err_en!
                                    : scenarioConfiguration.account!.err!,
                            inputType: TextInputType.text,
                            controller: accountController,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    trxParameters != null
                        ? ListView.builder(
                            shrinkWrap: true,
                            itemCount: trxParameters.length,
                            itemBuilder: (BuildContext context, int i) =>
                                trxParameters![i].label_en != "Amount"
                                    ? Column(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                            CustomTextFormField(
                                                label: Localization.of(context)
                                                    .locale!
                                                    .languageCode! ==
                                                    'en'
                                                    ? trxParameters[i]!.label_en!
                                                    : trxParameters[i]!.label!,
                                                expression: trxParameters[i].regex !=
                                                    null
                                                    ? RegExp(
                                                    trxParameters![i].regex!)
                                                    : null,
                                                expErrorMessage:
                                                Localization.of(context)
                                                    .locale
                                                    .languageCode ==
                                                    'en'
                                                    ? trxParameters![i]
                                                    .err_en
                                                    : trxParameters![i].err,
                                                errorMessage:
                                                Localization.of(context)
                                                    .locale
                                                    .languageCode ==
                                                    'en'
                                                    ? trxParameters![i]
                                                    .err_en
                                                    : trxParameters![i].err,
                                                maxValue: trxParameters![i].max,
                                                minValue: trxParameters![i].min,
                                                inputType: TextInputType.text,
                                                controller:
                                                _trxParamsControllers[i]
                                            ),
                                            trxParameters[i].helper_en != null
                                                ? Align(
                                                    alignment:
                                                        Alignment.topRight,
                                                    child: Text(Localization.of(context)
                                                                .locale
                                                                .languageCode ==
                                                            'en'
                                                        ? trxParameters![i]!
                                                            .helper_en!
                                                        : trxParameters![i]!
                                                            .helper!))
                                                : Container()
                                          ])
                                    : Container(),
                          )
                        : Container()
                  ],
                ),
              ),
              SizedBox(
                height: 17,
              ),
              Row(mainAxisSize: MainAxisSize.min, children: [
                Expanded(
                  child: InkWell(
                      onTap: () {
                        if (!_loading)
                          billEnquireAndServiceCalculation(
                              context,
                              accountController,
                              service,
                              _trxParamsControllers,
                              trxParameters,
                              scenarioConfiguration);
                      },
                      child: Container(
                        height: size.height * 0.07,
                        width: size.width,
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(3.408)),
                            boxShadow: [
                              BoxShadow(
                                  color: GRAY_COLOR,
                                  offset: Offset(0, 1),
                                  blurRadius: 2,
                                  spreadRadius: 0)
                            ],
                            color: SECONDARY_COLOR),
                        child: Center(
                          child: Text(
                            getTranslated(context, 'continue'),
                            style: GoogleFonts.lato(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontStyle: FontStyle.normal,
                                fontSize: 13.6),
                          ),
                        ),
                      )),
                ),
                _loading
                    ? Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation<Color>(
                                SECONDARY_COLOR)),
                      )
                    : Container()
              ])
            ]));
  }

  void billEnquireAndServiceCalculation(
      BuildContext context,
      TextEditingController accountController,
      MfsServices service,
      List<TextEditingController> _trxParamsControllers,
      List<MfsTrxParams>? trxParameters,
      MfsScenarioConfiguration scenarioConfiguration) {
    if (_formKey.currentState!.validate()) {
      var account = accountController.text;
      //Navigator.pop(context);
      CustomWidgetBuilder.showAlertDialog(context);
      setState(() {
        _loading = true;
      });
      MfsServiceCalculatorRequest billEnqRequest = MfsServiceCalculatorRequest(
          serviceProviderCode: service.service_provider_code,
          serviceCode: service.prereq_service_code,
          externalRefNumber: Util.generateUUID(),
          externalTxnId: Util.generateUUID(),
          userBAccountId: account,
          transactionValue: "0",
          inParameters: "{}");
      print("bill enq request");
      print(billEnqRequest.toJson());
      _mfsService.confirmBillEnquiry(billEnqRequest).then((billEnqResponse) {
        if (billEnqResponse.statusCode == 200) {
          MfsServiceCalculatorRequest serviceCalcRequest = billEnqRequest;
          print("bill enq response");
          print(billEnqResponse.data!.toJson());

          Map<dynamic, dynamic>? billEnquiryOutParams =
              jsonDecode(billEnqResponse!.data!.out_parameters!);
          print("out parama");
          print(billEnquiryOutParams);

          if (billEnqResponse.data!.status_code == '0') {
            print("billEnquiryOutParams");
            print(billEnquiryOutParams!['OUT_PARAMETER_1']);
            if (billEnquiryOutParams!['OUT_PARAMETER_1'] == "0" ||
                billEnquiryOutParams!['OUT_PARAMETER_1'].toString().isEmpty) {
              Navigator.pop(context);
              Navigator.pop(context);
              CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
                  getTranslated(context, 'no_bill_message'));
              setState(() {
                _loading = false;
              });
            } else {
              serviceCalcRequest.transactionValue = billEnquiryOutParams != null
                  ? billEnquiryOutParams['OUT_PARAMETER_1']
                  : null;

              if (trxParameters != null) {
                for (int i = 0; i < trxParameters.length; i++) {
                  billEnquiryOutParams![trxParameters[i].json_id] =
                      _trxParamsControllers[i].text;
                }
              }

              serviceCalcRequest.inParameters =
                  jsonEncode(billEnquiryOutParams);

              serviceCalcRequest.serviceCode = service.service_code;
              print("service calc request");

              print(serviceCalcRequest.toJson());

              _mfsService
                  .confirmServiceCalculator(serviceCalcRequest)
                  .then((serviceCalcResponse) {
                if (serviceCalcResponse.statusCode == 200) {
                  print("service calc response");
                  print(serviceCalcResponse.data!.toJson());

                  Map<dynamic, dynamic>? serviceCalcOutParams =
                      jsonDecode(serviceCalcResponse.data!.out_parameters!);

                  if (serviceCalcResponse.data!.status_code == '0') {
                    setState(() {
                      _loading = false;
                    });
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ConfirmBillCashCallService(
                                  response: serviceCalcResponse.data!,
                                  service: service,
                                  accountValue: accountController.text,
                                  serviceCalcRequest: serviceCalcRequest,
                                )));
                  } else {
                    Navigator.pop(context);
                    Navigator.pop(context);
                    CustomWidgetBuilder.buildSnackBar(
                        scaffoldKey,
                        context,
                        serviceCalcOutParams != null &&
                                serviceCalcOutParams['Message'] != null
                            ? serviceCalcOutParams['Message']
                            : serviceCalcResponse.data!.user_a_msg);
                    setState(() {
                      _loading = false;
                    });
                  }
                } else {
                  Navigator.pop(context);
                  Navigator.pop(context);
                  CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
                      getTranslated(context, serviceCalcResponse.getErrorCode));
                  setState(() {
                    _loading = false;
                  });
                }
              });
            }
          } else {
            Navigator.pop(context);
            Navigator.pop(context);
            CustomWidgetBuilder.buildSnackBar(
                scaffoldKey,
                context,
                billEnquiryOutParams != null &&
                        billEnquiryOutParams['Message'] != null
                    ? billEnquiryOutParams['Message']
                    : billEnqResponse.data!.user_a_msg);
            setState(() {
              _loading = false;
            });
          }
        } else {
          Navigator.pop(context);
          Navigator.pop(context);
          CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
              getTranslated(context, billEnqResponse.getErrorCode));
          setState(() {
            _loading = false;
          });
        }
      });
    }
  }

  void chargeServiceCalc(
      BuildContext context,
      TextEditingController accountController,
      MfsServices service,
      List<TextEditingController> _trxParamsControllers,
      List<MfsTrxParams> trxParameters,
      MfsScenarioConfiguration scenarioConfiguration) {
    if (_formKey.currentState!.validate()) {
      CustomWidgetBuilder.showAlertDialog(context);
      setState(() {
        _loading = true;
      });
      var account = accountController.text;
      var amount;
      Map<String, String> inParams = new HashMap();
      for (int i = 0; i < trxParameters.length; i++) {
        if (trxParameters[i].json_id == "transaction_value") {
          amount = _trxParamsControllers[i].text;
        } else {
          inParams[trxParameters[i].json_id!] = _trxParamsControllers[i].text;
        }
      }
      print(inParams);
      MfsServiceCalculatorRequest serviceCalcRequest =
          MfsServiceCalculatorRequest(
              serviceProviderCode: service.service_provider_code,
              serviceCode: service.service_code,
              externalRefNumber: Util.generateUUID(),
              externalTxnId: Util.generateUUID(),
              userBAccountId: accountController.text,
              transactionValue: amount,
              inParameters: jsonEncode(inParams));
      print(serviceCalcRequest.toJson());
      _mfsService
          .confirmServiceCalculator(serviceCalcRequest)
          .then((serviceCalcResponse) {
        if (serviceCalcResponse.statusCode == 200) {
          print(serviceCalcResponse.data!.toJson());

          Map<dynamic, dynamic>? serviceCalcOutParams =
              jsonDecode(serviceCalcResponse.data!.out_parameters!);

          if (serviceCalcResponse.data!.status_code == "0") {
            setState(() {
              _loading = false;
            });
            Navigator.pop(context);
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ConfirmBillCashCallService(
                          response: serviceCalcResponse.data!,
                          service: service,
                          accountValue: accountController.text,
                          serviceCalcRequest: serviceCalcRequest,
                        )));
          } else {
            Navigator.pop(context);
            Navigator.pop(context);
            CustomWidgetBuilder.buildSnackBar(
                scaffoldKey,
                context,
                serviceCalcOutParams != null &&
                        serviceCalcOutParams['Message'] != null
                    ? serviceCalcOutParams['Message']
                    : serviceCalcResponse.data!.user_a_msg);
            setState(() {
              _loading = false;
            });
          }
        } else {
          Navigator.pop(context);
          Navigator.pop(context);
          CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
              getTranslated(context, serviceCalcResponse.getErrorCode));
          setState(() {
            _loading = false;
          });
        }
      });
    }
  }

  void goToConfirmPayment(BuildContext context, MfsServices service, package,
      MfsScenarioConfiguration scenarioConfiguration) {
    if (scenarioConfiguration.scenario_id!.toLowerCase() ==
        MFS_SCENARIO_VOUCHER) {
      EVoucherServiceCalc(context, service, package);
    } else {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => SelectedService(
                    package: package,
                    service: service,
                  )));
    }
  }

  void EVoucherServiceCalc(
    BuildContext context,
    MfsServices service,
    package,
  ) {
    //Navigator.pop(context);
    CustomWidgetBuilder.showAlertDialog(context);
    setState(() {
      _loading = true;
    });
    Map<String, String> inParams = new HashMap();

    inParams["in_param_1"] = package['package_code'];
    inParams["in_param_2"] = package['evd_selector'];
    inParams["in_param_3"] = "1";
    print(inParams);

    MfsServiceCalculatorRequest serviceCalcRequest =
        MfsServiceCalculatorRequest(
            serviceProviderCode: service.service_provider_code,
            serviceCode: service.service_code,
            externalRefNumber: Util.generateUUID(),
            externalTxnId: Util.generateUUID(),
            userBAccountId: service.service_code,
            transactionValue: package['package_transaction_value'].toString(),
            inParameters: jsonEncode(inParams));
    _mfsService
        .confirmServiceCalculator(serviceCalcRequest)
        .then((serviceCalcResponse) {
      if (serviceCalcResponse.statusCode == 200) {
        print(serviceCalcResponse.data!.toJson());
        Map<dynamic, dynamic>? serviceCalcOutParams =
            jsonDecode(serviceCalcResponse.data!.out_parameters!);
        if (serviceCalcResponse.data!.status_code == '0') {
          setState(() {
            _loading = false;
          });
          Navigator.pop(context);
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ConfirmVoucher(
                        service: service,
                        package: package,
                        response: serviceCalcResponse.data!,
                        serviceCalcRequest: serviceCalcRequest,
                      )));
        } else {
          Navigator.pop(context);
          Navigator.pop(context);
          CustomWidgetBuilder.buildSnackBar(
              scaffoldKey,
              context,
              serviceCalcOutParams != null &&
                      serviceCalcOutParams['Message'] != null
                  ? serviceCalcOutParams['Message']
                  : serviceCalcResponse.data!.user_a_msg);
          setState(() {
            _loading = false;
          });
        }
      } else {
        Navigator.pop(context);
        Navigator.pop(context);
        CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
            getTranslated(context, serviceCalcResponse.getErrorCode));
        setState(() {
          _loading = false;
        });
      }
    });
  }

  void gotoSelectedSubcategoryService(int subCategoryIndex, int serviceIndex) {
    dynamic scenarioConfigString = subCategories![subCategoryIndex]
        .services![serviceIndex]
        .scenario_configuration;
    if (scenarioConfigString != null &&
        jsonDecode(scenarioConfigString) != null) {
      MfsScenarioConfiguration scenarioConfiguration =
          MfsScenarioConfiguration.fromJson(jsonDecode(scenarioConfigString));
      List<dynamic>? packages = Util.stringToList(
          subCategories![subCategoryIndex].services![serviceIndex].Packages!);
      if (packages != null && packages.length > 0) {
        // show package pop up
        packages
          ..sort((a, b) => a['package_transaction_value']
              .compareTo(b['package_transaction_value']));
        CashCallCustomWidget.buildPackagePopUp(context, goToConfirmPayment,
            height: size.height * 0.6,
            service: subCategories![subCategoryIndex].services![serviceIndex],
            packages: packages,
            scenarioConfiguration: scenarioConfiguration);
      } else {
        clear();
        buildServicePopUpForm(context,
            service: subCategories![subCategoryIndex].services![serviceIndex],
            scenarioConfiguration: scenarioConfiguration);
      }
    } else {
      CustomWidgetBuilder.buildSnackBar(
          scaffoldKey, context, getTranslated(context, "service_unavailable"));
    }
  }

  void gotoSelectedService(int serviceIndex) {
    dynamic scenarioConfigString =
        services![serviceIndex].scenario_configuration;
    if (scenarioConfigString != null &&
        jsonDecode(scenarioConfigString) != null) {
      MfsScenarioConfiguration scenarioConfiguration =
          MfsScenarioConfiguration.fromJson(jsonDecode(scenarioConfigString));
      List<dynamic>? packages =
          Util.stringToList(services![serviceIndex].Packages!);
      if (packages != null && packages.length > 0) {
        // show package pop up
        packages
          ..sort((a, b) => a['package_transaction_value']
              .compareTo(b['package_transaction_value']));
        CashCallCustomWidget.buildPackagePopUp(context, goToConfirmPayment,
            height: size.height * 0.6,
            service: services![serviceIndex],
            scenarioConfiguration: scenarioConfiguration,
            packages: packages);
      } else {
        clear();
        buildServicePopUpForm(context,
            service: services![serviceIndex],
            scenarioConfiguration: scenarioConfiguration);
      }
    } else {
      CustomWidgetBuilder.buildSnackBar(
          scaffoldKey, context, getTranslated(context, "service_unavailable"));
    }
  }

  Widget _buildCahCallServices(BuildContext context) {
    return cashcall_services.cashcall_services != null
        ? Container(
            child: SingleChildScrollView(
              child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                Container(
                  color: Colors.white,
                  padding: EdgeInsets.fromLTRB(12, 11, 0, 0),
                  child: CustomWidgetBuilder.buildCategoryCards(
                      cashcall_services.cashcall_services!,
                      Localization.of(context).locale.languageCode == 'en'
                          ? 'category_name'
                          : 'category_arabic_name',
                      selectCategory,
                      active: aciveCards),
                ),
                SizedBox(
                  height: 20,
                ),
                subCategories!.length > 0 &&
                        subCategories![0].sub_category_code != null
                    ? Column(
                        children: List.generate(subCategories!.length, (index) {
                          return Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10),
                                child: Row(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(20.0),
                                      child: Container(
                                        color: Colors.blue,
                                        child: FadeInImage.assetNetwork(
                                            height: 40,
                                            width: 40,
                                            placeholder:
                                                'lib/assets/historyAccountIcWithBg.png',
                                            image: MFS_IMGS_BASE_URL +
                                                "categories/" +
                                                subCategories![index]
                                                    .sub_category_code! +
                                                ".png"),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 5),
                                      child: Text(
                                          Localization.of(context)
                                                      .locale
                                                      .languageCode ==
                                                  'en'
                                              ? subCategories![index]
                                                  .sub_category_name!
                                              : subCategories![index]
                                                  .sub_category_arabic_name!,
                                          style: const TextStyle(
                                              color: CASHCALL_TITLE,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                              fontSize: 10.2)),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 10),
                                child: Column(
                                  children: List.generate(
                                      subCategories![index].services!.length,
                                      (serviceIndex) {
                                    return buildServiceCard(
                                        subCategoryIndex: index,
                                        serviceIndex: serviceIndex,
                                        arabicName: subCategories![index]
                                            .services![serviceIndex]
                                            .service_arabic_name!,
                                        englishName: subCategories![index]
                                            .services![serviceIndex]
                                            .service_name!);
                                  }),
                                ),
                              ),
                            ],
                          );
                        }),
                      )
                    : Container(),
                services != null && services!.length > 0
                    ? Column(
                        children: List.generate(services!.length, (index) {
                          return buildServiceCard(
                              serviceIndex: index,
                              arabicName: services![index].service_arabic_name!,
                              englishName: services![index].service_name!);
                        }),
                      )
                    : Container()
              ]),
            ),
          )
        : CustomWidgetBuilder.buildSpinner(context);
  }

  Widget _buildHomeBody(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(10, 30, 10, 10),
              child: Text(getTranslated(context, 'home_cashcall_services'),
                  style: const TextStyle(
                      color: CASHCALL_TITLE,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      fontSize: 11.9)),
            ),
/*            InkWell(
              onTap: () {
                viewAllAction();
              },
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 30, 10, 10),
                child: Text(
                  getTranslated(context, 'home_view_all'),
                  style: const TextStyle(
                      color: PRIMARY_COLOR,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      fontSize: 11.9),
                ),
              ),
            )*/
          ],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: null,
      child: Scaffold(
        key: scaffoldKey,
        backgroundColor: BACKGROUND_COLOR,
        appBar: HomeActionBar(context),
        body: ListView(
          children: [
            HomeCustomWidget.buildHomeTopCard(context, balance),
            Container(
                width: size.width,
                color: BACKGROUND_COLOR,
                child: Column(
                  children: [
                    _buildHomeBody(context),
                    Container(
                        decoration: BoxDecoration(boxShadow: [
                          BoxShadow(
                              color: const Color(0x1f000000),
                              offset: Offset(0, 2),
                              blurRadius: 3,
                              spreadRadius: 0)
                        ], color: const Color(0xfffafafa)),
                        child: _buildCahCallServices(context)),
                  ],
                ))
          ],
        ),
        bottomNavigationBar: NavigationBar(
          activeItem: 1,
        ),
      ),
    );
  }

  viewAllAction() {
/*    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                ViewAllServices(cashcall_services: this.cashcall_services)));*/
  }

  Widget buildServiceCard(
      {int subCategoryIndex = -1,
      int? serviceIndex,
      String? arabicName,
      String? englishName}) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10, bottom: 6),
      child: InkWell(
        onTap: () {
          if (subCategoryIndex == -1)
            gotoSelectedService(serviceIndex!);
          else
            gotoSelectedSubcategoryService(subCategoryIndex, serviceIndex!);
        },
        child: Container(
          padding: EdgeInsets.only(left: 7, top: 10, bottom: 11),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(1.704)),
              boxShadow: [
                BoxShadow(
                    color: CASHCALL_BACKGROUND_COLOR,
                    offset: Offset(0, 1),
                    blurRadius: 3,
                    spreadRadius: 0)
              ],
              color: WHITE_COLOR),
          child: Row(
            children: [
              SizedBox(
                width: 10,
              ),
              FadeInImage.assetNetwork(
                  height: 40,
                  width: 40,
                  fit: BoxFit.fill,
                  placeholder: 'lib/assets/historyAccountIcWithBg.png',
                  image: MFS_IMGS_BASE_URL +
                      "services/" +
                      (subCategoryIndex == -1
                          ? services![serviceIndex!].service_code!
                          : subCategories![subCategoryIndex]
                              .services![serviceIndex!].service_code!) +
                      ".png"),
              SizedBox(width: 8),
              Expanded(
                flex: 22,
                child: Text(
                  Localization.of(context).locale.languageCode == 'en'
                      ? englishName!
                      : arabicName!,
                  style: const TextStyle(
                      color: PRIMARY_COLOR,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                      fontSize: 11.9),
                  overflow: TextOverflow.clip,
                ),
              ),
              new Spacer(),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: Localization.of(context).locale.languageCode == 'en'
                    ? Image.asset("lib/assets/services_arrow.png")
                    : RotatedBox(
                        child: Image.asset("lib/assets/services_arrow.png"),
                        quarterTurns: 90,
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
