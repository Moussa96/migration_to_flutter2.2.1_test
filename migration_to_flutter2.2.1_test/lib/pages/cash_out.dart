import 'dart:async';
import 'dart:collection';
import 'dart:io';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/ContactService.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/QRResults.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/pages/confirm_payment.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/util/NFCUtil.dart';
import 'package:cashless/util/QRGenerator.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'dynamicQR.dart';

class CashOut extends KFDrawerContent {
  String cashoutType;

  CashOut({this.cashoutType, Key key}) : super();

  @override
  _CashOutState createState() => _CashOutState();
}

class _CashOutState extends State<CashOut> {
  final _formKey = GlobalKey<FormState>();
  final utility = QRGenerator();
  TextEditingController _mobNumController = TextEditingController();
  TextEditingController _amountController = TextEditingController();
  Map<dynamic, dynamic> cashOutInfo = new HashMap();
  Payment payment = Payment();
  PaymentService paymentService = PaymentService();
  ContactService contactsService = ContactService();
  User user = User();
  bool _enableMobileNumber = true;
  String onBehalfOfMemberId;

  @override
  void initState() {
    super.initState();
    payment.transferTypeCode = CASH_OUT_TRANSFER_TYPE_CODE;
    payment.paymentMethod = widget.cashoutType;
    setPINState();
  }

  void setPINState() {
    if (widget.cashoutType != 'pin') {
      setState(() {
        _enableMobileNumber = false;
      });
    }
  }

  void scanQR(BuildContext context) async {
    QRResults result;
    QRGenerator.scan().then((value) async {
      result = value as QRResults;
      String userIdentifier = result.content;
      user = await contactsService.identifyUser(userIdentifier);
      if (user != null) {
        setState(() {
          _mobNumController.text = user.username;
          _enableMobileNumber = false;
          //onBehalfOfMemberId = user.id;
        });
      } else {
        Scaffold.of(context).showSnackBar(SnackBar(
            content: Text(getTranslated(context, "USER_NOT_FOUND")),
            duration: Duration(seconds: 2)));
      }
    });
  }

  void generateQR(BuildContext context) async {
    print('generate QR');
    Map<String, dynamic> _cashoutinfo = {};
    if (_formKey.currentState.validate()) {
      await Util.loadFromSharedPreferences(Util.USER_ID).then((result) {
        _cashoutinfo['userId'] = result;
      });
      _cashoutinfo['memberId'] = _mobNumController.text;
      _cashoutinfo['amount'] = _amountController.text;
      _cashoutinfo['createdTime'] =
          new DateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss")
              .format(DateTime.now());
      _cashoutinfo['transferTypeId'] = "8064";
      _cashoutinfo['ReferenceTransactionId'] = Util.generateUUID();
      _cashoutinfo['QRCodeType'] = "cashOut";
      print(_cashoutinfo['createdTime']);
      print(_cashoutinfo);
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => DynamicQR(_cashoutinfo)));
    }
  }

  void readNFC(BuildContext context) async {
    if (Platform.isAndroid) {
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                  title: Text(getTranslated(context, "SCAN_CARD")),
                  actions: <Widget>[
                    FlatButton(
                      child: Text(getTranslated(context, "OK")),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    )
                  ]));
    }
    await NFCUtil.startScanning(context, getNFCData);
  }

  void getNFCData(String userIdentifier) async {
    user = await contactsService.identifyUser(userIdentifier);
    if (user != null) {
      setState(() {
        payment.onbehalfOfMemberId = user.id;
        _mobNumController.text = user.username;
        Navigator.of(context, rootNavigator: true).pop('dialog');
      });
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(getTranslated(context, "USER_NOT_FOUND")),
          duration: Duration(seconds: 2)));
    }
  }

  showAlertDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
        content: new Row(children: [
      CircularProgressIndicator(backgroundColor: Colors.blue),
      Container(
          margin: EdgeInsets.only(left: 5),
          child: Text(getTranslated(context, "loading"))),
    ]));
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void validatePayment(BuildContext context) {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      Util.loadFromSharedPreferences(Util.USER_ID).then((result) {
        setState(() {
          payment.toMemberId = result;
        });
      });
      payment.amount = double.parse(_amountController.text);
      user.username = _mobNumController.text;
      if (payment.paymentMethod == Payment.QR_Payment_Method ||
          payment.paymentMethod == Payment.PIN_Payment_Method) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    ConfirmPayment(payment: payment, user: user)));
      } else if (payment.paymentMethod == Payment.VIP_Payment_Method) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    ConfirmPayment(payment: payment, user: user, isVIP: true)));
      } else {
        showAlertDialog(context);
        paymentService.doPayment(context, payment, user,
            paymentStatus: paymentDone);
      }
    }
  }

  void paymentDone() {
    Timer(Duration(seconds: 1), () {
      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ActionBar(context,
          icon: Icons.arrow_back_ios, pressFunc: () => Navigator.pop(context)),
      body: Builder(
        builder: (BuildContext context) {
          return Container(
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20.0),
                      child: Center(
                          child: Text(
                        getTranslated(context, 'cashout_title') +
                            " " +
                            widget.cashoutType.toUpperCase(),
                        style: TextStyle(
                          fontSize: 30,
                          color: Colors.black54,
                        ),
                      )),
                    ),
                    widget.cashoutType == 'qr' || widget.cashoutType == 'vip'
                        ? Container(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 16.0, horizontal: 20),
                              child: CustomWidgetBuilder.buildRaisedButton(
                                  context,
                                  scanQR,
                                  null,
                                  getTranslated(context, 'cashout_btn_scan_qr'),
                                  size: MainAxisSize.max),
                            ),
                          )
                        : widget.cashoutType == 'nfc'
                            ? Container(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 16.0, horizontal: 20),
                                  child: CustomWidgetBuilder.buildRaisedButton(
                                      context,
                                      readNFC,
                                      null,
                                      getTranslated(
                                          context, 'cashout_btn_scan_nfc'),
                                      size: MainAxisSize.max),
                                ),
                              )
                            : Container(),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      child: CustomWidgetBuilder.buildFormInputText(
                          controller: _mobNumController,
                          hint: getTranslated(context, 'cashout_mobile_num'),
                          maxLength: 11,
                          inputType: TextInputType.number,
                          errorMessage: getTranslated(
                              context, 'cashout_mobile_num_required'),
                          isEnabled: _enableMobileNumber),
                    ),
                    Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                        child: CustomWidgetBuilder.buildFormInputText(
                          controller: _amountController,
                          hint: getTranslated(context, 'cashout_amount'),
                          errorMessage:
                              getTranslated(context, 'cashout_amount_required'),
                           inputType: TextInputType.numberWithOptions(
                            decimal: true),
                        )),

                    // Container(
                    //   child: Padding(
                    //     padding: const EdgeInsets.symmetric(
                    //         vertical: 16.0, horizontal: 20),
                    //     child: CustomWidgetBuilder.buildRaisedButton(
                    //         context, generateQR, null, getTranslated(context, 'cashout_btn_generateqr'),
                    //         size: MainAxisSize.max),
                    //   ),
                    // ),
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 16.0, horizontal: 20),
                        child: CustomWidgetBuilder.buildRaisedButton(
                            context,
                            validatePayment,
                            null,
                            getTranslated(context, "cashout_btn_submit"),
                            size: MainAxisSize.max),
                      ),
                    ),
                    // _isLoading ? Padding(
                    //   padding: const EdgeInsets.symmetric(horizontal: 25),
                    //   child: LinearProgressIndicator(backgroundColor: Colors.blue),
                    // ):Container()
                  ],
                ),
              ), // Child
            ),
          );
        },
      ),
    );
  }
}
