import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/home/old_home.dart';
import 'package:cashless/pages/profile/old_profile.dart';
import 'package:cashless/pages/transaction_history/old_transaction_history.dart';
import 'package:cashless/util/Util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:cashless/util/class_builder.dart';

class DrawerMain extends KFDrawerContent {
  DrawerMain({Key key, this.title}) : super();
  final String title;

  @override
  _DrawerMainState createState() => _DrawerMainState();
}

class _DrawerMainState extends State<DrawerMain> with TickerProviderStateMixin {
  KFDrawerController _drawerController;
  String name = 'Cashcall';
  @override
  void initState() {
    super.initState();
    // _drawerController =  buildDrawer();
    setState(() {
      Util.loadFromSharedPreferences(Util.NAME).then((result) {
        setState(() {
          name = result;
        });
      });
    });
  }

  buildDrawer() {
    return KFDrawerController(
      initialPage: ClassBuilder.fromString('Home'),
      items: [
        KFDrawerItem.initWithPage(
          text: Text(getTranslated(context, 'kfdrawer_item_profile'),
              style: TextStyle(color: Colors.white)),
          icon: Icon(Icons.person, color: Colors.white),
          page: Home(),
          route: Profile(),
        ),
        KFDrawerItem.initWithPage(
          text: Text(getTranslated(context, 'kfdrawer_item_history'),
              style: TextStyle(color: Colors.white)),
          icon: Icon(Icons.history, color: Colors.white),
          page: Home(),
          route: TransactionHistory(),
        ),
        // KFDrawerItem.initWithPage(
        //   text: Text(getTranslated(context, 'kfdrawer_item_language'), style: TextStyle(color: Colors.white)),
        //   icon: Icon(Icons.language, color: Colors.white),
        //   page: Home(),
        //   route: ChangeLanguage(),
        // ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: KFDrawer(
        controller: buildDrawer(),
        header: Align(
          alignment: Localization.of(context).locale.languageCode == 'ar'
              ? Alignment.centerRight
              : Alignment.centerLeft,
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              width: MediaQuery.of(context).size.width * 0.6,
              child: Column(
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        foregroundColor: Colors.black,
                        backgroundColor: Colors.white,
                        child: Text(name[0].toUpperCase()),
                      ),
                      Text(" " + name,
                          style: TextStyle(color: Colors.white, fontSize: 18))
                    ],
                  ),
                ],
              )),
        ),
        footer: Container(
            decoration: BoxDecoration(
                border: Border(top: BorderSide(color: Colors.grey))),
            child: KFDrawerItem(
                text: Text(getTranslated(context, 'kfdrawer_item_signout'),
                    style: TextStyle(color: Colors.white)),
                icon: Icon(
                  Icons.exit_to_app,
                  color: Colors.white,
                ),
                onPressed: () {
                  showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                            title: Text(
                                getTranslated(context, "home_logout_title")),
                            content: Text(
                                getTranslated(context, "home_logout_content")),
                            actions: <Widget>[
                              FlatButton(
                                child: Text(getTranslated(
                                    context, "home_logout_cancel_btn")),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                              FlatButton(
                                child: Text(getTranslated(
                                    context, "home_logout_confirm_btn")),
                                onPressed: () {
                                  Util.clear();
                                  Navigator.of(context).pushNamedAndRemoveUntil(
                                      '/login',
                                      (Route<dynamic> route) => false);
                                },
                              ),
                            ]);
                      });
                })),
        decoration: BoxDecoration(color: Colors.grey[800]),
      ),
    );
  }
}
