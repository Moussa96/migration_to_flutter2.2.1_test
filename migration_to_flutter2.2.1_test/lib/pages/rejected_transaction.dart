import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/AccountService.dart';
import 'package:cashless/models/Balance.dart';
import 'package:cashless/pages/home/home.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/custom_widgets/CashCallCustomWidget.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cashless/localization/localization_utility.dart';
class RejectedTransaction extends StatefulWidget{
  Map<dynamic, dynamic> transcation ;
  String errorMessage;
  bool hideBalance = true;
  RejectedTransaction({
    Key key,
    this.transcation,
    @required this.errorMessage,
    @required this.hideBalance
  }) : super();
  @override
  _RejectedTransactionState createState() => _RejectedTransactionState();

}
class _RejectedTransactionState extends State<RejectedTransaction>{
  Size size;
  Balance balance = Balance();
  void tryAgain(BuildContext context){
    Navigator.pop(context);
  }
  List<Widget> buildTransactionSummaryBody() {
    List<Widget> widgets = new List();
    for (var entry in widget.transcation.entries) {
      widgets.add(Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: CashCallCustomWidget.buildDataConfirmCashCallServiceCard(label:entry.key ,
            data: entry.value.toString(),size: Size(size.width,size.height*0.07),icon: entry.key == getTranslated(context, 'confirm_service_reference')),
      )
      );
      widgets.add(SizedBox(height: 7,));
    }
    return widgets;
  }
  void initState() {
    super.initState();
    getBalance();
  }
  void getBalance(){
    AccountService _accountUseCase = AccountService();
    balance = Balance();
    _accountUseCase.listAccounts().then((value) => {
      setState(() {
        if (value.data != null) balance = Util.getBalanceInfo(value.data);
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          backgroundColor: BACKGROUND_COLOR,
          body: SingleChildScrollView(
            child: buildBodyWidget()
          )
      ),
    );

  }
  Widget buildBodyWidget(){
    return Column(
      children: <Widget>[
        Container(child: Image.asset('lib/assets/rejected_transaction.png')),
        SizedBox(height: 16,),
        CustomWidgetBuilder.buildText(
            getTranslated(context, "rejected_transaction_title"),
            color:  PRIMARY_COLOR,
            fontWeight: FontWeight.w700,
            fontStyle:  FontStyle.normal,
            fontSize: 20.4
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 25,vertical: 10),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: CustomWidgetBuilder.buildText(widget.errorMessage,color: CASHCALL_TITLE
                  ,fontSize: 14,fontWeight: FontWeight.w400,fontStyle: FontStyle.normal,
                ),
              ),
              SizedBox(height: 20,),
              Column(
                children: buildTransactionSummaryBody(),
              ),
              SizedBox(height: 13,),
              !widget.hideBalance? buildBalanceWidget():Container(),
              SizedBox(height: 9.7,),
              CustomWidgetBuilder.buildText(getTranslated(context, 'transaction_summary_tanks_message'),
                  fontWeight: FontWeight.w400,
                  fontStyle:  FontStyle.normal,
                  fontSize: 13.6,color: PRIMARY_COLOR
              ),
              SizedBox(height: 52,),
              buildTryAgainBtn(),
              SizedBox(height: 15,),
              buildDivider(),
              buildCancelBtn(context),
            ],
          ),
        ),
      ],
    );
  }
  Widget buildTryAgainBtn(){
    return CustomWidgetBuilder.buildRaisedButton(context, tryAgain, null, getTranslated(context, 'try_again'), color: SECONDARY_COLOR,fontWeight: FontWeight.w400,
        fontStyle:  FontStyle.normal,
        fontSize: 13.6);
  }
  Widget buildBalanceWidget(){
    return CustomWidgetBuilder.buildText('${getTranslated(context, 'transaction_summary_available_balance')} "${balance.availableBalance} ${balance.currency}"',
        fontWeight: FontWeight.w400,
        fontStyle:  FontStyle.normal,
        fontSize: 13.6,
        color: CASHCALL_TITLE
    );
  }
  Widget buildDivider(){
    return Container(
      width: size.width*0.7,
      height: 1,
      decoration: BoxDecoration(
          color: DIVIDER_COLOR
      ),
    );
  }
  Widget buildCancelBtn(BuildContext context){
    return InkWell(
      onTap: (){
        // back to home
        gotoHome(context);
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20,horizontal: 30),
        child:CustomWidgetBuilder.buildText
          (getTranslated(context, 'home_logout_cancel_btn'),
            fontWeight: FontWeight.w700,
            fontStyle:  FontStyle.normal,
            fontSize: 13.6,
            color: CASHCALL_TITLE
        ),
      ),
    );
  }
  void gotoHome(BuildContext context){
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (c) => ConsumerHome()), (route) => false);
  }
}