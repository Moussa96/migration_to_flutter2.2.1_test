import 'dart:core';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/AccountService.dart';
import 'package:cashless/apis/Services/ContactService.dart';
import 'package:cashless/models/Balance.dart';
import 'package:cashless/models/QR.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/util/QRGenerator.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/custom_widgets/CustomButton.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';

class Profile extends StatefulWidget {
  Profile({Key? key}) : super();

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  late String profileQR;
  String _name = '';
  Map<String, dynamic> _userInfo = {};
  AccountService _accountUseCase = AccountService();
  Balance _balance = Balance();
  ContactService _contactService = ContactService();
  late QrImage _qrImage;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Util.loadFromSharedPreferences(Util.NAME).then((result) {
      setState(() {
        _name = result!;
      });
    });
    //Load user data that will be used as a QR
    _contactService.getContactInfo().then((result) {
      setState(() {
        _userInfo = result;
        generateStaticQR();
        // QRGenerator.generate(_userInfo).then((value) => {
        //       setState(() {
        //         _qrImage = value;
        //       })
        //     });
      });
    });
    _accountUseCase.listAccounts().then((value) => {
          setState(() {
            if (value.data != null) _balance = Util.getBalanceInfo(value.data!);
          })
        });
  }

  void generateStaticQR() async{
    final String? loggedId = await Util.loadFromSharedPreferences(Util.USER_ID);
    final String? groupCode = await Util.loadFromSharedPreferences(Util.GROUP);
    User user = User(
      id: loggedId,
      name: _name,
      groupCode: groupCode
    );
    QR qr = QR(qrType: STATIC_QR, user: user);
    print(qr.user!.toJson());
    QRGenerator.generate(qr.toJson(),size: 300).then((value) {
      setState(() {
        _qrImage = value as QrImage;
      });
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: BACKGROUND_COLOR,
      resizeToAvoidBottomInset: false,
      // appBar: ActionBar(context,
      //     icon: Icons.arrow_back_ios, pressFunc: () => Navigator.pop(context)),
      body: Column(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage("lib/assets/myQrCodeBg.png"), fit: BoxFit.cover),
              ),
              child: Center(
                  child: Card(child: _qrImage)
              ),
            ),
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.all(12),
            child: Column(
              children: [
                Row(
                  children: [
                    Image.asset('lib/assets/my_qr_code_graphics.png'),
                    SizedBox(width: 13),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                              getTranslated(context,'my_qr_code'),
                              style: GoogleFonts.lato(fontWeight: FontWeight.bold,
                                  fontSize: 16.0, color: TEXT_PRIMARY_COLOR)
                          ),
                          SizedBox(height: 8),
                          Text(getTranslated(context,'profile_my_code_message'),
                            style: GoogleFonts.lato(
                              color: CASHCALL_TITLE,
                            ),
                          )
                        ],
                      ),
                    )

                  ],
                ),
                SizedBox(height: 24),
                CustomButton(
                  onPressed: (){Navigator.pop(context);},
                  text: getTranslated(context,'home_done_btn'),
                  color: SECONDARY_COLOR,
                  padding: 15,
                )
              ],
            ),
          )
        ],
      )
    );
  }
}
