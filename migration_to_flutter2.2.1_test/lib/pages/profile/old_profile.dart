import 'dart:core';

import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/AccountService.dart';
import 'package:cashless/apis/Services/ContactService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Balance.dart';
import 'package:cashless/models/QR.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/pages/reset_password/old_resetpassword.dart';
import 'package:cashless/util/QRGenerator.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class Profile extends KFDrawerContent {
  Profile({Key? key}) : super();

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  late String profileQR;
  String _name = '';
  Map<String, dynamic> _userInfo = {};
  AccountService _accountUseCase = AccountService();
  Balance _balance = Balance();
  ContactService _contactService = ContactService();
  late QrImage _qrImage;

  void reset(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ResetPassword()),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Util.loadFromSharedPreferences(Util.NAME).then((result) {
      setState(() {
        _name = result!;
      });
    });
    //Load user data that will be used as a QR
    _contactService.getContactInfo().then((result) {
      setState(() {
        _userInfo = result;
        generateStaticQR();
        // QRGenerator.generate(_userInfo).then((value) => {
        //       setState(() {
        //         _qrImage = value;
        //       })
        //     });
      });
    });
    _accountUseCase.listAccounts().then((value) => {
      setState(() {
        if (value.data != null) _balance = Util.getBalanceInfo(value.data!);
      })
    });
  }

  void generateStaticQR() async{
    final String? loggedId = await Util.loadFromSharedPreferences(Util.USER_ID);
    final String? groupCode = await Util.loadFromSharedPreferences(Util.GROUP);
    User user = User(
        id: loggedId,
        name: _name,
        groupCode: groupCode
    );
    QR qr = QR(qrType: STATIC_QR, user: user);
    print(qr.user!.toJson());
    QRGenerator.generate(qr.toJson()).then((value) {
      setState(() {
        _qrImage = value as QrImage;
      });
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      resizeToAvoidBottomInset: false,
      appBar: ActionBar(context,
          icon: Icons.arrow_back_ios, pressFunc: () => Navigator.pop(context)),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView(
          children: <Widget>[
            CustomWidgetBuilder.buildText(
                getTranslated(context, 'profile_hello') + _name),
            Card(
                color: Colors.lightGreen,
                child: CustomWidgetBuilder.buildText(
                    getTranslated(context, 'profile_balance') +
                        MONEY_FORMATTER.format(_balance.balance) +
                        " " +
                        _balance.currency,
                    fontWeight: FontWeight.normal,
                    textColor: Colors.white)),
            Card(
                color: Colors.lightGreen,
                child: CustomWidgetBuilder.buildText(
                    getTranslated(context, 'profile_available') +
                        MONEY_FORMATTER.format(_balance.availableBalance) +
                        " " +
                        _balance.currency,
                    fontWeight: FontWeight.normal,
                    textColor: Colors.white)),
            CustomWidgetBuilder.buildText(getTranslated(context, 'profile_QR')),
            Center(
              child: Card(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: _qrImage,
                  )),
            ),
            Padding(
                padding: const EdgeInsets.all(15.0),
                child: CustomWidgetBuilder.buildRaisedButton(context, reset,
                    null, getTranslated(context, 'profile_btn_resetpass'),
                    size: MainAxisSize.min)),
          ],
        ),
      ),
    );
  }
}
