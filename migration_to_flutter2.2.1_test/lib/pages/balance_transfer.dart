import 'dart:async';
import 'dart:collection';
import 'dart:io';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/ContactService.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/QRResults.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/pages/confirm_payment.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/util/NFCUtil.dart';
import 'package:cashless/util/QRGenerator.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:xml2json/xml2json.dart';

class BalanceTransfer extends KFDrawerContent {
  String cashInType;

  BalanceTransfer({this.cashInType, Key key}) : super();

  @override
  _BalanceTransferState createState() => _BalanceTransferState();
}

class _BalanceTransferState extends State<BalanceTransfer> {
  Map<String, dynamic> cashInInfo = new HashMap();
  final utility = QRGenerator();
  TextEditingController _pinController = TextEditingController();
  TextEditingController _amountController = TextEditingController();
  TextEditingController _posController = TextEditingController();
  TextEditingController _mobileNumController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  Payment payment = Payment();
  PaymentService paymentService = PaymentService();
  ContactService contactsService = ContactService();
  User user = User();
  final Xml2Json xml2Json = Xml2Json();
  var isIdentified = "";
  bool _enableMobileNumber = true;
  bool _checked = false;
  bool _isLoading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    payment.transferTypeCode = CASH_IN_TRANSFER_TYPE_CODE;
    payment.paymentMethod = widget.cashInType;
    setPINState();
  }

  void setPINState() {
    if (widget.cashInType != 'pin') {
      setState(() {
        _enableMobileNumber = false;
      });
    }
  }

  void scanQR(BuildContext context) async {
    QRResults result;
    QRGenerator.scan().then((value) async {
      result = value as QRResults;
      //Map _userInfo = jsonDecode(result.content);
      String userIdentifier = result.content;
      user = await contactsService.identifyUser(userIdentifier);
      if (user != null) {
        setState(() {
          _mobileNumController.text = user.username;
          _enableMobileNumber = false;
          isIdentified = "true";
        });
      } else {
        setState(() {
          isIdentified = "false";
        });
        Scaffold.of(context).showSnackBar(SnackBar(
            content: Text(getTranslated(context, "USER_NOT_FOUND")),
            duration: Duration(seconds: 2)));
      }
    });
  }

  void readNFC(BuildContext context) async {
    if (Platform.isAndroid) {
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                  title: Text(getTranslated(context, "SCAN_CARD")),
                  actions: <Widget>[
                    FlatButton(
                      child: Text(getTranslated(context, "OK")),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    )
                  ]));
    }
    await NFCUtil.startScanning(context, getNFCData);
  }

  void getNFCData(String userIdentifier) async {
    user = await contactsService.identifyUser(userIdentifier);
    if (user != null) {
      setState(() {
        isIdentified = "true";
        _mobileNumController.text = user.name;
        Navigator.of(context, rootNavigator: true).pop('dialog');
      });
    } else {
      isIdentified = "false";
      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(getTranslated(context, "USER_NOT_FOUND")),
          duration: Duration(seconds: 2)));
    }
  }

  showAlertDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
        content: new Row(children: [
          CircularProgressIndicator(backgroundColor: Colors.blue),
          Container(
            margin: EdgeInsets.only(left: 5),
            child: Text(getTranslated(context, "loading"))),
      ])
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void gotoRegisteration(BuildContext context) {
    Navigator.pushNamed(context, '/qrregisteration');
  }

  void validatePayment(BuildContext context) {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      payment.amount = double.parse(_amountController.text);
      if (_checked) {
        payment.posNumber = _posController.text;
      }
      user.username = _mobileNumController.text;
      user.password = _pinController.text;
      if (payment.paymentMethod == Payment.QR_Payment_Method ||
          payment.paymentMethod == Payment.PIN_Payment_Method) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    ConfirmPayment(payment: payment, user: user)));
      } else if (payment.paymentMethod == Payment.VIP_Payment_Method) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    ConfirmPayment(payment: payment, user: user, isVIP: true)));
      } else {
        _isLoading = false;
        showAlertDialog(context);
        paymentService.doPayment(context, payment, user,
            paymentStatus: paymentDone);
      }
    }
  }

  void paymentDone() {
    Timer(Duration(seconds: 1), () {
      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ActionBar(context,
          icon: Icons.arrow_back_ios, pressFunc: () => Navigator.pop(context)),
      body: Builder(
        builder: (BuildContext context) {
          return Container(
            child: SingleChildScrollView(
              child: (Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20.0),
                        child: Center(
                            child: Text(
                          getTranslated(context, "cashin_title") +
                              " " +
                              widget.cashInType.toUpperCase(),
                          style: TextStyle(
                            fontSize: 30,
                            color: Colors.black54,
                          ),
                        )),
                      ),
                      Container(
                        child: widget.cashInType == 'qr' ||
                                widget.cashInType == 'vip'
                            ? Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 16.0, horizontal: 20),
                                child: CustomWidgetBuilder.buildRaisedButton(
                                    context,
                                    scanQR,
                                    null,
                                    getTranslated(context, "cashin_btn_scanqr"),
                                    size: MainAxisSize.max),
                              )
                            : widget.cashInType == 'nfc'
                                ? Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 16.0, horizontal: 20),
                                    child:
                                        CustomWidgetBuilder.buildRaisedButton(
                                            context,
                                            readNFC,
                                            null,
                                            getTranslated(
                                                context, "cashin_btn_scan_nfc"),
                                            size: MainAxisSize.max))
                                : Container(),
                      ),
                      isIdentified == "true" || widget.cashInType == "pin"
                          ? Column(children: [
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                child: CustomWidgetBuilder.buildFormInputText(
                                    controller: _mobileNumController,
                                    hint: getTranslated(
                                        context, "cashin_mobile_num"),
                                    maxLength: 11,
                                    inputType: TextInputType.number,
                                    errorMessage: getTranslated(
                                        context, "cashin_mobile_num_required"),
                                    isEnabled: _enableMobileNumber),
                              ),
                              Padding(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 10),
                                  child: CustomWidgetBuilder.buildFormInputText(
                                    controller: _amountController,
                                    hint:
                                        getTranslated(context, "cashin_amount"),
                                    errorMessage: getTranslated(
                                        context, "cashin_amount_required"),
                                    inputType: TextInputType.numberWithOptions(
                                        decimal: true),
                                  )),
                              // widget.cashInType == 'pin'||widget.cashInType == 'qr'?
                              // Padding(
                              //   padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                              //   child: CustomWidgetBuilder.buildFormInputText(
                              //       controller: _pinController, hint: getTranslated(context, "cashin_pin"), inputType: TextInputType.number,
                              //       errorMessage: getTranslated(context, "cashin_pin_required"),isPassword: true),
                              // ):Container(),
                              CheckboxListTile(
                                  title: Row(children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8.0),
                                      child: Image.asset(
                                          "lib/assets/visaLogo.png",
                                          height: 35),
                                    ),
                                    Text(getTranslated(
                                        context, "cashin_text_box")),
                                  ]),
                                  activeColor: Colors.blue,
                                  value: _checked,
                                  onChanged: (val) {
                                    setState(() {
                                      _checked = val;
                                    });
                                  }),
                              _checked
                                  ? Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 10, horizontal: 10),
                                      child: CustomWidgetBuilder
                                          .buildFormInputText(
                                              hint: getTranslated(context,
                                                  "cashin_pos_receipt"),
                                              controller: _posController,
                                              errorMessage: getTranslated(
                                                  context,
                                                  "cashin_pos_receipt_required"),
                                              inputType: TextInputType.number))
                                  : Container(),
                              Container(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 16.0, horizontal: 20),
                                  child: CustomWidgetBuilder.buildRaisedButton(
                                      context,
                                      validatePayment,
                                      null,
                                      getTranslated(
                                          context, "cashin_btn_submit"),
                                      size: MainAxisSize.max,
                                      isEnabled: _isLoading),
                                ),
                              )
                            ])
                          : isIdentified == "false" &&
                                  widget.cashInType != 'vip'
                              ? Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 16.0, horizontal: 20),
                                  child: CustomWidgetBuilder.buildRaisedButton(
                                      context,
                                      gotoRegisteration,
                                      null,
                                      getTranslated(context,
                                          'registeration_btn_register'),
                                      size: MainAxisSize.max),
                                )
                              : Container(),
                      // _isLoading ? Padding(
                      //   padding: const EdgeInsets.symmetric(horizontal: 25),
                      //   child: LinearProgressIndicator(backgroundColor: Colors.blue),
                      // ):Container()
                    ],
                  ))),
            ), // Child
          );
        },
      ),
    );
  }
}
