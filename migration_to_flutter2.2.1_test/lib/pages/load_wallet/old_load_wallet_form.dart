import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/response/MakePaymentResponse.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../Configurations.dart';
import 'Old_Load_Wallet_Bank_form.dart';

class LoadWallet extends KFDrawerContent {
  @override
  _LoadWalletFormState createState() => _LoadWalletFormState();
}

class _LoadWalletFormState extends State<LoadWallet> {
  final _formKey = GlobalKey<FormState>();
  final paymentService = PaymentService();
  final _receivingAmountController = TextEditingController();
  final _payingAmountController = TextEditingController();
  bool validAmount = true;
  bool _confirm = false;

  Future<RetrofitResponse<MakePaymentResponse>> getLoadWalletFees(double amount) async {
    // get load wallet information
    Payment payment = Payment();
    payment.amount = amount;
    payment.transferTypeCode = LOAD_WALLET_TRANSFER_TYPE_CODE;
    payment.customValues = [];
    payment.customValues!.add(new CustomValue(PAYMENTGATEWAY_ORDER_ID, 'U'));
    payment.customValues!.add(new CustomValue(MERCHANT_ORDER_ID, 'U'));
    return await paymentService.makeSystemToMemberPayment(payment);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ActionBar(context,
          titleText: getTranslated(context, 'loadwallet_title'),
          icon: Icons.arrow_back_ios,
          pressFunc: () => Navigator.pop(context)),
      body: ListView(
        children: [
          Builder(
            builder: (BuildContext context) {
              return Container(
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 25),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10.0),
                                    child: Text(getTranslated(
                                        context, 'loadwallet_payingamount')),
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(5),
                                    child: buildBoxInputText(context,
                                        _payingAmountController, 'PAY'),
                                  )
                                ],
                              ),
                              flex: 2,
                            ),
                            Expanded(
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    height: 40,
                                  ),
                                  Icon(
                                    Icons.arrow_forward,
                                    color: MAJOR_COLOR,
                                    size: 20,
                                  ),
                                  Icon(
                                    Icons.arrow_back,
                                    color: MAJOR_COLOR,
                                    size: 20,
                                  ),
                                ],
                              ),
                              flex: 1,
                            ),
                            Expanded(
                              child: Column(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10.0),
                                    child: Text(getTranslated(
                                        context, "loadwallet_receivingamount")),
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(5),
                                    child: buildBoxInputText(context,
                                        _receivingAmountController, 'RCV',
                                        errorMessage:
                                        'First name is required.',enabled: false),
                                  )
                                ],
                              ),
                              flex: 2,
                            ),

                          ],
                        ),
                      ),
                      !validAmount
                          ? Text(
                              getTranslated(context,
                                  "loadwallet_notvalid_amount_message"),
                              style: TextStyle(color: Colors.red),
                            )
                          : Container(),
                      Container(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        child: CustomWidgetBuilder.buildRaisedButton(
                            context,
                            loadWallet,
                            null,
                            getTranslated(context, "loadwallet_btn_confirm"),
                            size: MainAxisSize.min,isEnabled: _confirm),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }

  void loadWallet(BuildContext context) async {
    bool valid;
    if (_receivingAmountController.text.isEmpty || double.parse(_receivingAmountController.text) <= 0) {
      valid = false;
    } else {
      valid = true;
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => LoadWalletBankForm(
                  double.parse(_payingAmountController.text))));
    }
    setState(() {
      validAmount = valid;
    });
  }

  Widget buildBoxInputText(BuildContext context , TextEditingController controller, String type,
      {String hint = '',
      String? errorMessage,
      String? expErrorMessage,
      int? maxLength,
      bool enabled=true}) {
    return TextFormField(
      controller: controller,
      maxLength: maxLength,
      style: MAJOR_TEXT_STYLE,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      enabled: enabled,
      decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: MAJOR_COLOR, width: 1),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey, width: 1),
          ),
          hintText: hint),
      validator: (String? value) {
        if (errorMessage != null) {
          if (value == null || value.isEmpty) return errorMessage;
        }
        return null;
      },
      onChanged: (text) {
        if (text.isEmpty) {
          setState(() {
            _receivingAmountController.text = '';
            _payingAmountController.text = '';
          });
        }
        if (type == 'PAY') {
          setState(() {
            _confirm = false;
          });
          getLoadWalletFees(double.parse(_payingAmountController.text)).then((value) {
            if(value !=null && value.data != null && value.statusCode == 200){
              setState(() {
                _receivingAmountController.text =
                    value.data!.finalAmount.toString();
                validAmount = true;
                _confirm = true;
              });
            }
            else{
              if(value.getErrorCode != null && value.getErrorCode == 'INVALID_ARGUMENT'){
                setState(() {
                  _receivingAmountController.text ='';
                  validAmount =false;
                });
              }else{
                Scaffold.of(context).showSnackBar(
                    SnackBar(content: Text(getTranslated(context, value.getErrorCode))));
              }

            }
          });
        }
      },
    );
  }
}
