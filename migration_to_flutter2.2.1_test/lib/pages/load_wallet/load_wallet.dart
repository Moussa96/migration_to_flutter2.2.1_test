import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/response/MakePaymentResponse.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../Configurations.dart';
import 'load_wallet_bank_form.dart';

class LoadWallet extends StatefulWidget {
  @override
  _LoadWalletState createState() => _LoadWalletState();
}

class _LoadWalletState extends State<LoadWallet> {
  final _formKey = GlobalKey<FormState>();
  final paymentService = PaymentService();
  final _totalAmountController = TextEditingController();
  final _receivingAmountController = TextEditingController();
  late Size size ;
  bool validAmount = true;
  bool _confirm = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  Future<RetrofitResponse<MakePaymentResponse>> getLoadWalletFees(double? amount) async {
    // get load wallet information
    Payment payment = Payment();
    payment.amount = amount;
    payment.transferTypeCode = LOAD_WALLET_TRANSFER_TYPE_CODE;
    payment.customValues = [];
    payment.customValues!.add(new CustomValue(PAYMENTGATEWAY_ORDER_ID, 'U'));
    payment.customValues!.add(new CustomValue(MERCHANT_ORDER_ID, 'U'));
    return await paymentService.makeSystemToMemberPayment(payment);
  }
  _buildAppBar(BuildContext context){
   return AppBar(
     title: Text(getTranslated(context, 'loadwallet_title'),style: GoogleFonts.lato(),),
     centerTitle: true,
     backgroundColor: PRIMARY_COLOR,
   );
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: _buildAppBar(context),
      body: Container(
        width: size.width,
        decoration: BoxDecoration(
        gradient: LinearGradient(
        begin: Alignment(0.5, 0.5386387563848061),
        end: Alignment(0.5, 2.962704824565857),
        colors: [PRIMARY_COLOR, BLUE_COLOR])
        ),
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 25,horizontal: 10.7),
              child: Text(
                  getTranslated(context, 'loadWallet_paragraph'),
                  style: GoogleFonts.lato(
                      color:  Colors.white,
                      fontWeight: FontWeight.w300,
                      fontStyle:  FontStyle.normal,
                      fontSize: 15
                  ),
                  textAlign: TextAlign.center
              ),
            ),
            Builder(
              builder: (BuildContext context) {
                return Container(
                  child: Form(
                    key: _formKey,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 13),
                              width: size.width,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(3.408)
                                  ),
                                  boxShadow: [BoxShadow(
                                      color: const Color(0x3d000000),
                                      offset: Offset(0,2),
                                      blurRadius: 3,
                                      spreadRadius: 0
                                  )] ,
                                  color: Colors.white
                              ),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal:10.0),
                                  child: BuildLoadWalletInput(controller: _totalAmountController,titleKey: 'loadWallet_total_amount',type: 'TOTAL'),
                                ),
                                //SizedBox(width: size.width*0.15,),
                                Container(
                                    width: 1,
                                    height: size.height*0.07,
                                    decoration: BoxDecoration(
                                        color: TEXT_COLOR
                                    )
                                ),
                                //SizedBox(width: size.width*0.15,),
                                BuildLoadWalletInput(controller: _receivingAmountController,titleKey: 'loadWallet_receiving_amount',enabled: false,type: 'RCV'),
                              ],
                            ),

                          ),
                          !validAmount
                              ? Text(
                            getTranslated(context,
                                "loadwallet_notvalid_amount_message"),
                            style: TextStyle(color: Colors.red),
                          )
                              : Container(),
                          SizedBox(height: 90,),
                          InkWell(
                            onTap: (){
                              if(_confirm)
                                loadWallet();
                            },
                            child: Container(
                                width: size.width,
                                height: size.height*0.08,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(4)
                                    ),
                                    boxShadow: [BoxShadow(
                                        color: GRAY_COLOR,
                                        offset: Offset(0,1),
                                        blurRadius: 2,
                                        spreadRadius: 0
                                    )] ,
                                    color: SECONDARY_COLOR,
                                ),
                              child: // Pay Now
                              Center(
                                child: Text(
                                    getTranslated(context,'loadWallet_pay_now'),
                                    style: GoogleFonts.lato(
                                        color:  Colors.white,
                                        fontWeight: FontWeight.w700,
                                        fontStyle:  FontStyle.normal,
                                        fontSize: 16.0
                                    ),

                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ],
        ),

      ),

    );
  }
  Widget BuildLoadWalletInput({TextEditingController? controller,String? titleKey,bool enabled=true,String? type}){
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
            getTranslated(context, titleKey!),
            style: GoogleFonts.lato(
                color:  TEXT_COLOR,
                fontWeight: FontWeight.w400,
                fontStyle:  FontStyle.normal,
                fontSize: 11.9
            )
        ),
        Container(
          width: size.width*0.3,
          child: buildFormInputTextLoadWallet(context,
            isEnabled: enabled,
              hint: "00.00",
              controller: controller,
            type: type
          ),
        )
      ],
    );
  }
   Widget buildFormInputTextLoadWallet(BuildContext context,{
    TextEditingController? controller,
    String hint = '',
    String? errorMessage,
    RegExp? expression,
    String? expErrorMessage,
    bool isEnabled = true,
    double? maxValue,
    double? minValue,
    String? type
  }) {
    return TextFormField(
        style:GoogleFonts.lato(
            color:  PRIMARY_COLOR,
            fontWeight: FontWeight.w700,
            fontStyle:  FontStyle.normal,
            fontSize: 32.0
        ) ,
        controller: controller,
        enabled: isEnabled,
        keyboardType: TextInputType.numberWithOptions(decimal: true),
        decoration: new InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
          hintStyle:GoogleFonts.lato(
              color:  PRIMARY_COLOR,
              fontWeight: FontWeight.w700,
              fontStyle:  FontStyle.normal,
              fontSize: 32.0
          ) ,
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          hintText: hint,

        ),
        validator: (String? value) {
          if (errorMessage != null) {
            if (value == null || value!.isEmpty) {
              return errorMessage!;
            }
          }
          if (value != null && value.isNotEmpty &&
              expression != null &&
              !expression.hasMatch(value)) {
            return expErrorMessage;
          }
          if(maxValue!=null && minValue !=null) {
            if (double.parse(value!) > maxValue ||
                double.parse(value!) < minValue) {
              return errorMessage;
            }
          }
          return null;
        }
       , onChanged: (text) {
      if (text.isEmpty) {
        setState(() {
          _totalAmountController.text = '';
          _receivingAmountController.text = '';
        });
      }
      if (type == 'TOTAL') {
        setState(() {
          _confirm = false;
        });
        getLoadWalletFees(double.parse(_totalAmountController.text)).then((value) {
          if(value !=null && value.data != null && value.statusCode == 200){
            setState(() {
              _receivingAmountController.text =
                  value.data!.finalAmount.toString();
              validAmount = true;
              _confirm = true;
            });
          }
          else{
            if(value.getErrorCode != null && value.getErrorCode == 'INVALID_ARGUMENT'){
              setState(() {
                _receivingAmountController.text ='';
                validAmount =false;
              });
            }else{
              Scaffold.of(context).showSnackBar(
                  SnackBar(content: Text(getTranslated(context, value.getErrorCode))));
            }

          }
        });
      }
    },
    );
  }
  void loadWallet() async {
    bool valid;
    if (_receivingAmountController.text.isEmpty || double.parse(_receivingAmountController.text) <= 0) {
      valid = false;
    } else {
      valid = true;
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => LoadWalletBankForm(
                  double.parse(_totalAmountController.text))));
    }
    setState(() {
      validAmount = valid;
    });
  }


}
