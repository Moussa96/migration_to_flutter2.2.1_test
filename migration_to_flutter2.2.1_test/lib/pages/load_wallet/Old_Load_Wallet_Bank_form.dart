import 'dart:collection';
import 'dart:convert';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/CustomProperty.dart';
import 'package:cashless/models/Order.dart';
import 'package:cashless/models/requests/CreateOrderRequest.dart';
import 'package:cashless/util/Util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import '../../Configurations.dart';
import '../fragments/action_bar.dart';

class LoadWalletBankForm extends StatefulWidget {
  double amount;

  LoadWalletBankForm(@required this.amount, {Key? key}) : super();

  @override
  _LoadWalletBankFormState createState() => _LoadWalletBankFormState();
}

class _LoadWalletBankFormState extends State<LoadWalletBankForm> {
  Map<String, dynamic> orderResponse = new HashMap();
  PaymentService paymentService = PaymentService();
  late CreateOrderRequest request;
  late Future<String> requestEncoded;
  late Map<String, String> headers;

  @override
  void initState() {
    // TODO: implement initState
/*    setState(() {
      requestEncoded = createOrderRequest();
    });*/
    requestEncoded = createOrderRequest();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: null,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: ActionBar(
          context,
          titleText: getTranslated(context, 'load_wallet_bank_form_title'),
          icon: Icons.arrow_back_ios,
          pressFunc: () => Navigator.pop(context),
          icon4: Icons.home,
          pressFunc4: backToHome,
          sLang: false,
        ),

        //floatingActionButton:  CustomWidgetBuilder.buildFloationActionButton(context, Icons.home, backToHome),
        body: Center(
          child: FutureBuilder<String>(
              future: requestEncoded,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return payPluginFuture(context, snapshot.data!);
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }
                return CircularProgressIndicator();
              }),
        ),
      ),
    );
  }

  Widget payPluginFuture(BuildContext context, String requestEncoded) {
    print(
        '$PAYMENT_GATEWAY_BASE_URL$PAYMENT_GW_MERCHANT_CODE/$PAYMENT_GW_API_KEY/$requestEncoded');
    return WebviewScaffold(
     /* persistentFooterButtons: [
        CustomWidgetBuilder.buildFloationActionButton(
            context, Icons.home, backToHome)
      ],*/
      scrollBar: false,
      clearCookies: true,
      withJavascript: true,
      url:
          '$PAYMENT_GATEWAY_BASE_URL$PAYMENT_GW_MERCHANT_CODE/$PAYMENT_GW_API_KEY/$requestEncoded',
      headers: headers,
      clearCache: true,
      debuggingEnabled: true,
      withZoom: false,
      displayZoomControls: false,
    );
  }

  Future<String> createOrderRequest() async {
    request = CreateOrderRequest(widget.amount.toString());
    String merchantOrderId = Util.generateUUID();
    request.signature = Util.makeSignature(
        widget.amount, request.paymentMethod, merchantOrderId);
    request.order = Order(merchantOrderId);
    String? username = await Util.loadFromSharedPreferences(Util.USERNAME);
    request.customProperties!
        .add((CustomProperty(LOAD_WALLET_TRANSFER_TYPE_CODE, username!)));
    print(jsonEncode(request.toJson()));
    return Util.encode64(jsonEncode(request.toJson()));
  }

  void backToHome(BuildContext context) async {
    Navigator.of(context).pop(null);
    Navigator.pop(context);
  }
}
