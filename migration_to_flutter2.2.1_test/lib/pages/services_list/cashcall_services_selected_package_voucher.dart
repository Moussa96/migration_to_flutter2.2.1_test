import 'dart:collection';
import 'dart:convert';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/MfsRestClientImpl.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Mfs/MfsScenarioConfiguration.dart';
import 'package:cashless/models/Mfs/MfsTrxParams.dart';
import 'package:cashless/models/requests/MfsServiceCalculatorRequest.dart';
import 'package:cashless/models/Mfs/MfsServices.dart';
import 'package:cashless/pages/services_list/cashcall_services_confirm_package_voucher.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:cashless/widgets/custom_widgets/CashCallCustomWidget.dart';
import 'package:cashless/widgets/custom_widgets/HomeCustomWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:cashless/widgets/custom_widgets/CustomAppBar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cashless/widgets/custom_widgets/CustomTextFormField.dart';

class SelectedService extends  StatefulWidget{
  dynamic package;
  MfsServices service;
  SelectedService({
    Key key,
    this.package,
    this.service,
  })
      : super();

  @override
  _SelectedServiceState createState() => _SelectedServiceState();
}

class _SelectedServiceState extends State<SelectedService> {
  Size size;
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController _accountController = TextEditingController();
  List<MfsTrxParams> trxParameters;
  bool _loading = false;
  List<TextEditingController> _trxParamsControllers =  List();
  MfsRestClientImpl _mfsRestService = MfsRestClientImpl();
  List<dynamic> packages = [];
  dynamic package;
  MfsScenarioConfiguration scenarioConfiguration;

  @override
  void initState() {
    super.initState();
    scenarioConfiguration = MfsScenarioConfiguration.fromJson(
        jsonDecode(widget.service.scenario_configuration));
    trxParameters = scenarioConfiguration.trx_params;
    if(trxParameters != null) {
      for (int i = 0; i < trxParameters.length; i++) {
        _trxParamsControllers.add(TextEditingController());
      }
    }
    package = widget.package;
    packages = Util.stringToList(widget.service.Packages);
    packages
      ..sort((a, b) => a['package_transaction_value']
          .compareTo(b['package_transaction_value']));
  }
  void chargePackagesServiceCalc(BuildContext context) {
    if (_formKey.currentState.validate()) {
      if (package != null) {
        var account = _accountController.text;
        var amount =
        package['package_transaction_value'].toString();

        Map<String, String> inParams = new HashMap();
        if (trxParameters != null) {
          for (int i = 0; i < trxParameters.length; i++) {
            inParams[trxParameters[i].json_id] = _trxParamsControllers[i].text;
          }
        }
        inParams["in_param_1"] = package['package_code'];
        inParams["in_param_2"] = package['evd_selector'];
        inParams["in_param_3"] = "1";
        print(inParams);

        setState(() {
          _loading = true;
        });

        MfsServiceCalculatorRequest serviceCalcRequest =
        MfsServiceCalculatorRequest(
            serviceProviderCode: widget.service.service_provider_code,
            serviceCode: widget.service.service_code,
            externalRefNumber: Util.generateUUID(),
            externalTxnId: Util.generateUUID(),
            userBAccountId: _accountController.text,
            transactionValue: amount,
            inParameters: jsonEncode(inParams));
        print(serviceCalcRequest.toJson());
        _mfsRestService
            .confirmServiceCalculator(serviceCalcRequest)
            .then((serviceCalcResponse) {
          if (serviceCalcResponse.statusCode == 200) {
            print(serviceCalcResponse.data.toJson());
            Map<dynamic, dynamic> serviceCalcOutParams =
            jsonDecode(serviceCalcResponse.data.out_parameters);
            if (serviceCalcResponse.data.status_code == "0") {
              setState(() {
                _loading = false;
                _accountController.clear();
              });
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ConfirmPackageVoucher(
                        response: serviceCalcResponse.data,
                        service: widget.service,
                        serviceCalcRequest: serviceCalcRequest,
                        package: package,
                        accountValue: account,
                      )));
            } else {
              CustomWidgetBuilder.buildSnackBar(
                  scaffoldKey,
                  context,
                  serviceCalcOutParams != null &&
                      serviceCalcOutParams['Message'] != null
                      ? serviceCalcOutParams['Message']
                      : serviceCalcResponse.data.user_a_msg);
              setState(() {
                _loading = false;
              });
            }
          } else {
            CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
                getTranslated(context, serviceCalcResponse.getErrorCode));
            setState(() {
              _loading = false;
            });
          }
        });
      } else {
        CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
            getTranslated(context, 'mfs_selected_service_choose_package'));
      }
    }
  }
  Widget _buildChargeView(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomTextFormField(
                    label:Localization.of(context).locale.languageCode == 'en'
                        ? scenarioConfiguration.account.label_en
                        : scenarioConfiguration.account.label,
                    addContactButton: true,
                    expression: scenarioConfiguration.account.regex != null ?
                    RegExp(scenarioConfiguration.account.regex) : RegExp(""),
                    expErrorMessage: Localization.of(context).locale.languageCode=='en'?
                    scenarioConfiguration.account.err_en:scenarioConfiguration.account.err,
                    errorMessage: Localization.of(context).locale.languageCode=='en'?
                    scenarioConfiguration.account.err_en:scenarioConfiguration.account.err,
                    inputType: TextInputType.text,
                    controller: _accountController
              ),
            trxParameters != null
                ? SingleChildScrollView(
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: trxParameters.length,
                  itemBuilder: (BuildContext context, int i) =>
                      Column(
                        children: [
                          Text(
                              Localization.of(context)
                                  .locale
                                  .languageCode ==
                                  'en'
                                  ? trxParameters[i].label_en
                                  : trxParameters[i].label,
                              style: GoogleFonts.lato(
                                  color: TEXT_PRIMARY_COLOR,
                                  fontWeight: FontWeight.w700,
                                  fontStyle: FontStyle.normal,
                                  fontSize: 11.9)),
                          SizedBox(
                            height: 12,
                          ),
                          HomeCustomWidget.buildFormInputText(
                            controller: _trxParamsControllers[i],
                            expression: trxParameters[i].regex !=null?
                            RegExp(trxParameters[i].regex) :null,
                            expErrorMessage: Localization.of(context).locale.languageCode=='en'?
                            trxParameters[i].err_en:trxParameters[i].err,
                            errorMessage: Localization.of(context).locale.languageCode=='en'?
                            trxParameters[i].err_en:trxParameters[i].err,
                            maxValue: trxParameters[i].max,
                            minValue: trxParameters[i].min,
                            inputType: TextInputType.text,
                          ),
                          trxParameters[i].helper_en!=null?
                          Align(
                              alignment:Alignment.topRight,
                              child: Text(
                                  Localization.of(context).locale.languageCode == 'en'?
                                  trxParameters[i].helper_en:
                                  trxParameters[i].helper
                              )
                          ):Container()
                        ],
                      ),
                ))
                : Container()
          ],
        ),
      ),
    ]);
  }
  Widget _buildConfirmVoucherBody(BuildContext context) {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 11),
      children: [
        CashCallCustomWidget.buildTopConfirmCashCallServiceCard(
          size: Size(size.width, size.height * 0.12),
          serviceCode: widget.service.service_code,
          serviceName: Localization.of(context).locale.languageCode == 'en'
              ? widget.service.service_provider_name
              : widget.service.service_provider_arabic_name,
          messagePartOne: getTranslated(
              context, 'cashcall_confirm_payment_message_part_one'),
          messagePartTwo: getTranslated(
              context, 'cashcall_confirm_payment_message_part_two'),
        ),
        SizedBox(
          height: 20,
        ),
        CashCallCustomWidget.buildChangePackageCard(context, buildPackagePopUp,
            label: getTranslated(context, 'selected_package'),
            value: package['package_transaction_value'].toString(),
            btnLabel: getTranslated(context, 'change_btn')),
        SizedBox(
          height: 20,
        ),
        _buildChargeView(context),
      ],
    );
  }
  void buildPackagePopUp(BuildContext context) {
    CashCallCustomWidget.buildPackagePopUp(context, changePackageValue,
        packages: packages,
        service: widget.service,
        height: size.height * 0.6);
  }
  void changePackageValue(BuildContext context, MfsServices service, newPackage) {
    Navigator.pop(context);
    setState(() {
      package = newPackage;
    });
  }


  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
        key: scaffoldKey,
        backgroundColor: BACKGROUND_COLOR,
        resizeToAvoidBottomInset: false,
        appBar: CustomAppBar(title:getTranslated(context, 'home_CashcallServices')),
        body:
            scenarioConfiguration.scenario_id.toLowerCase() == MFS_SCENARIO_CHARGE_PACKAGES ?
            _buildConfirmVoucherBody(context)
            : Center(child: Text(getTranslated(context, "service_unavailable"), style: TextStyle(fontSize: 30))),
      bottomNavigationBar:
      Container(
        height: size.height*0.09,
        child: Row(
            children: [
          Expanded(
              child: CustomWidgetBuilder.buildRaisedButton(
                  context,
                  chargePackagesServiceCalc,
                  null,
                  getTranslated(context, "continue"),
                  size: MainAxisSize.max,
                  isEnabled: !_loading
              )
          ),
          _loading ?
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue[700])),
          ): Container()
        ]),
      ),
    );
  }
}
