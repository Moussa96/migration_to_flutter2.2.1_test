import 'dart:collection';
import 'dart:convert';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/Mfs/MfsScenarioConfiguration.dart';
import 'package:cashless/models/Mfs/MfsTrxParams.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/requests/MfsServiceCalculatorRequest.dart';
import 'package:cashless/models/Mfs/MfsServices.dart';
import 'package:cashless/models/response/MfsServiceCalculatorResponse.dart';
import 'package:cashless/widgets/custom_widgets/CustomAppBar.dart';
import 'package:cashless/pages/transaction_summary/transaction_summary.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:cashless/widgets/custom_widgets/CashCallCustomWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:cashless/apis/Services/MfsRestClientImpl.dart';
import 'package:intl/intl.dart';

class ConfirmPackageVoucher extends StatefulWidget {
  MfsServices service;
  MfsServiceCalculatorResponse response;
  dynamic package;
  MfsServiceCalculatorRequest serviceCalcRequest =
      MfsServiceCalculatorRequest();
  String accountValue;

  ConfirmPackageVoucher(
      {Key key,
      this.service,
      this.response,
      this.package,
      this.serviceCalcRequest,
      this.accountValue})
      : super();

  @override
  _ConfirmPackageVoucherState createState() => _ConfirmPackageVoucherState();
}

class _ConfirmPackageVoucherState extends State<ConfirmPackageVoucher> {
  Size size;
  var rechargeAmount;
  var dueAmount;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  String paymentType;
  MfsScenarioConfiguration scenarioConfiguration;
  List<MfsTrxParams> trxParams = List();
  MfsRestClientImpl _mfsRestService = MfsRestClientImpl();
  @override
  void initState() {
    initAmount();
  }

  void initAmount() {
    scenarioConfiguration = MfsScenarioConfiguration.fromJson(
        jsonDecode(widget.service.scenario_configuration));
    rechargeAmount = widget.response.input_transaction_value;
    dueAmount = widget.response.receipt_Deducted_Amount;
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: BACKGROUND_COLOR,
      resizeToAvoidBottomInset: false,
      appBar: CustomAppBar(
          title: getTranslated(context, 'home_CashcallServices')),
      body: _buildConfirmVoucherBody(context),
      bottomNavigationBar: Container(
          width: size.width,
          height: size.height * 0.09,
          child: CustomWidgetBuilder.buildRaisedButton(context, confirmPayment,
              null, getTranslated(context, "mfs_confirm_service_confirm_btn"),
              color: PRIMARY_COLOR)),
    );
  }

  Widget _buildConfirmVoucherBody(BuildContext context) {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 11),
      children: [
        CashCallCustomWidget.buildTopConfirmCashCallServiceCard(
          size: Size(size.width, size.height * 0.12),
          serviceCode: widget.service.service_code,
          serviceName: Localization.of(context).locale.languageCode == 'en'
              ? widget.service.service_name
              : widget.service.service_arabic_name,
          messagePartOne: getTranslated(
              context, 'cashcall_confirm_payment_message_part_one'),
          messagePartTwo: getTranslated(
              context, 'cashcall_confirm_payment_message_part_two'),
        ),
        SizedBox(
          height: 20,
        ),
        CashCallCustomWidget.buildDataConfirmCashCallServiceCard(
            size: Size(size.width, size.height * 0.07),
            label: getTranslated(context, "selected_package"),
            data: Localization.of(context).locale.languageCode == 'en'
                ? widget.package['package_name']
                : widget.package['package_arabic_name']),
        SizedBox(
          height: 10,
        ),
        CashCallCustomWidget.buildDataConfirmCashCallServiceCard(
            size: Size(size.width, size.height * 0.06),
            label: Localization.of(context).locale.languageCode == 'en'
                ? scenarioConfiguration.account.label_en
                : scenarioConfiguration.account.label,
            data: widget.accountValue),
        SizedBox(
          height: 10,
        ),
        CashCallCustomWidget.buildDataConfirmCashCallServiceCard(
            size: Size(size.width, size.height * 0.06),
            label: getTranslated(context, "mfs_confirm_service_due_amount"),
            data: rechargeAmount),
        SizedBox(
          height: 20,
        ),
        CashCallCustomWidget.buildDataConfirmCashCallServiceCard(
            size: Size(size.width, size.height * 0.075),
            label: getTranslated(context, "total_paid_amount"),
            data: dueAmount,
            color: PRIMARY_COLOR,
            dataFontColor: Colors.white,
            labelFontColor: Colors.white,
            dataFontSize: 17,
            labelFontSize: 13),
      ],
    );
  }

  confirmPayment(BuildContext context) {
    CustomWidgetBuilder.showAlertDialog(context);
    MfsServiceCalculatorRequest transactionRequest = widget.serviceCalcRequest;
    print('transaction request');
    print(transactionRequest.toJson());
    PaymentService paymentService = PaymentService();
    CustomValue transactionRequestCV =
        CustomValue("mfs_transcation_request", jsonEncode(transactionRequest));
    CustomValue transactionResponseCV =
        CustomValue("mfs_transcation_response", "mfs_transcation_response");
    CustomValue transactionIdCV =
        CustomValue("mfs_transcation_id", "mfs_transcation_id");
    CustomValue serviceNameEnCV =
        CustomValue(EXTERNAL_SERVICE_NAME_EN, widget.service.service_name);
    CustomValue serviceNameArCV = CustomValue(
        EXTERNAL_SERVICE_NAME_AR, widget.service.service_arabic_name);
    List<CustomValue> customValues = List<CustomValue>();
    customValues.add(transactionRequestCV);
    customValues.add(transactionResponseCV);
    customValues.add(transactionIdCV);
    customValues.add(serviceNameEnCV);
    customValues.add(serviceNameArCV);
    Payment payment = Payment(
      transferTypeCode: PURCHASE_CASHCALL_SERVICES_TRANSFER_TYPE_CODE,
      amount: double.parse(dueAmount),
      customValues: customValues,
    );
    print('payment request');
    print(payment.toJson());
    paymentService.confirmMfsPayment(payment).then((res) {
      if (res.statusCode == 200) {
        Map<dynamic, dynamic> paymentRes =
            jsonDecode(res.data.customValues.mfs_transcation_response);
        print('payment response');
        print(res.data.toJson());
        print(res.data.customValues.toJson());
        print('mfs payment response');
        print(res.data.customValues.mfs_transcation_response);
        String state = jsonDecode(
            res.data.customValues.mfs_transcation_response)['statusCode'];
        if (!res.data.pending) {
          Map<String, dynamic> summary = new HashMap();
          if (state == '0') {
            String formattedDate = DateFormat('dd-MM-yyyy kk:mm:ss a').format(DateTime.now());
            String serviceName = Localization.of(context).locale.languageCode == 'en'
                ? widget.service.service_name
                : widget.service.service_arabic_name;
            summary['$serviceName  ${getTranslated(context, 'for')}'] = widget.accountValue;
            summary[getTranslated(context, "date")]=formattedDate;
            summary[getTranslated(context, "confirm_service_reference")] =
                res.data.transactionNumber;
            summary[getTranslated(context, "confirm_service_amount")] =
                dueAmount.toString();
            summary[getTranslated(context, "confirm_service_name")] =
             serviceName;
            summary[getTranslated(context, "amount")]= rechargeAmount.toString();



            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        TransactionSummary(transcationResponse: summary)));
          } else {
            Navigator.pop(context);
            CustomWidgetBuilder.buildSnackBar(
                scaffoldKey,
                context,
                paymentRes['outParameters'] != null &&
                        paymentRes['outParameters']['Message'] != null &&
                        paymentRes['outParameters']['Message'] != " "
                    ? paymentRes['outParameters']['Message']
                    : paymentRes['userAMsg']);
          }
        }
      } else {
        CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
            getTranslated(context, res.getErrorCode.toString()));
        Navigator.pop(context);
      }
    });
  }
}
