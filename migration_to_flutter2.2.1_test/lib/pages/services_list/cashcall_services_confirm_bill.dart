import 'dart:collection';
import 'dart:convert';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/MfsRestClientImpl.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/Mfs/MfsScenarioConfiguration.dart';
import 'package:cashless/models/Mfs/MfsTrxParams.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/requests/MfsServiceCalculatorRequest.dart';
import 'package:cashless/models/Mfs/MfsServices.dart';
import 'package:cashless/models/response/MfsServiceCalculatorResponse.dart';
import 'package:cashless/widgets/custom_widgets/CustomAppBar.dart';
import 'package:cashless/pages/transaction_summary/transaction_summary.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:cashless/widgets/custom_widgets/CashCallCustomWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class ConfirmBillCashCallService extends StatefulWidget {
  MfsServices service;
  MfsServiceCalculatorResponse response;
  MfsServiceCalculatorRequest serviceCalcRequest =
      MfsServiceCalculatorRequest();
  String accountValue;

  ConfirmBillCashCallService(
      {Key? key,
      this.service,
      this.response,
      this.serviceCalcRequest,
      this.accountValue})
      : super();

  @override
  _ConfirmBillCashCallServiceState createState() =>
      _ConfirmBillCashCallServiceState();
}

class _ConfirmBillCashCallServiceState
    extends State<ConfirmBillCashCallService> {
  TextEditingController _amountController = TextEditingController();
  Size size;
  var rechargeAmount;
  var dueAmount;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  bool _checked = true;
  String paymentType;
  int _amountIndex = 0;
  MfsRestClientImpl _mfsRestService = MfsRestClientImpl();
  MfsScenarioConfiguration scenarioConfiguration;
  List<MfsTrxParams> trxParams = List();
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    initAmount();
  }

  void initAmount() {
    scenarioConfiguration = MfsScenarioConfiguration.fromJson(
        jsonDecode(widget.service.scenario_configuration));
    paymentType = scenarioConfiguration.pay_type != null
        ? scenarioConfiguration.pay_type
        : MFS_PAY_TYPE_EXACT;
    rechargeAmount = widget.response.input_transaction_value;
    dueAmount = widget.response.receipt_Deducted_Amount;
    trxParams = scenarioConfiguration.trx_params;
    if (trxParams != null) {
      for (int i = 0; i < trxParams.length; i++) {
        if (trxParams[i].json_id == 'transaction_value') {
          _amountIndex = 0;
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: BACKGROUND_COLOR,
      resizeToAvoidBottomInset: false,
      appBar: CustomAppBar(
          title: getTranslated(context, 'home_CashcallServices')),
      body: _buildConfirmBillCahCallServiceBody(context),
      bottomNavigationBar: Container(
          width: size.width,
          height: size.height * 0.1,
          child: CustomWidgetBuilder.buildRaisedButton(context, confirmPayment,
              null, getTranslated(context, "mfs_confirm_service_confirm_btn"),
              color: PRIMARY_COLOR)),
    );
  }

  Widget _buildConfirmBillCahCallServiceBody(BuildContext context) {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 11),
      children: [
        CashCallCustomWidget.buildTopConfirmCashCallServiceCard(
          serviceCode: widget.service.service_code,
          serviceName: Localization.of(context).locale.languageCode == 'en'
              ? widget.service.service_provider_name
              : widget.service.service_provider_arabic_name,
          messagePartOne: getTranslated(
              context, 'cashcall_confirm_payment_message_part_one'),
          messagePartTwo: getTranslated(
              context, 'cashcall_confirm_payment_message_part_two'),
        ),
        SizedBox(
          height: 20,
        ),
        CashCallCustomWidget.buildDataConfirmCashCallServiceCard(
            label: Localization.of(context).locale.languageCode == 'en'
                ? scenarioConfiguration.account.label_en
                : scenarioConfiguration.account.label,
            data: widget.accountValue),
        SizedBox(
          height: 10,
        ),
        CashCallCustomWidget.buildDataConfirmCashCallServiceCard(
            size: Size(size.width, size.height * 0.06),
            label: getTranslated(context, "mfs_confirm_service_due_amount"),
            data: rechargeAmount),
        SizedBox(
          height: 10,
        ),
        paymentType != MFS_PAY_TYPE_EXACT
            ? Container(
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                width: size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(3.408)),
                    boxShadow: [
                      BoxShadow(
                          color: const Color(0x3d000000),
                          offset: Offset(0, 1),
                          blurRadius: 2,
                          spreadRadius: 0)
                    ],
                    color: Colors.white),
                child: Column(
                  children: [
                    CashCallCustomWidget.buildSwitch(onSwitch,
                        title: getTranslated(context, 'add_extra_amount'),
                        checked: _checked),
                    SizedBox(
                      height: 20,
                    ),
                    _checked
                        ? Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: Form(
                                        key: _formKey,
                                        child: CashCallCustomWidget.buildInputField(
                                            _amountController,
                                            hint: Localization.of(context)
                                                        .locale
                                                        .languageCode ==
                                                    'en'
                                                ? trxParams[_amountIndex]
                                                    .label_en
                                                : trxParams[_amountIndex].label,
                                            error: Localization.of(context)
                                                        .locale
                                                        .languageCode ==
                                                    'en'
                                                ? trxParams[_amountIndex].err_en
                                                : trxParams[_amountIndex].err,
                                            min: double.parse(
                                                trxParams[_amountIndex]
                                                    .min
                                                    .toString()),
                                            max: double.parse(
                                                trxParams[_amountIndex].max.toString())),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    CustomWidgetBuilder.buildRaisedButton(
                                        context,
                                        changePaidAmount,
                                        null,
                                        getTranslated(context,
                                            "mfs_confirm_service_Add_btn"),
                                        size: MainAxisSize.min),
                                  ],
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                Text(
                                    Localization.of(context)
                                                .locale
                                                .languageCode ==
                                            'en'
                                        ? trxParams[_amountIndex].helper_en
                                        : trxParams[_amountIndex].helper,
                                    style: GoogleFonts.lato(
                                      color: TEXT_COLOR,
                                      fontSize: 11,
                                      fontWeight: FontWeight.w400,
                                      fontStyle: FontStyle.normal,
                                    )),
                              ],
                            ),
                          )
                        : Container(),
                  ],
                ),
              )
            : Container(),
        SizedBox(
          height: 20,
        ),
        CashCallCustomWidget.buildDataConfirmCashCallServiceCard(
            size: Size(size.width, size.height * 0.075),
            label: getTranslated(context, "total_paid_amount"),
            data: dueAmount,
            color: PRIMARY_COLOR,
            dataFontColor: Colors.white,
            labelFontColor: Colors.white,
            dataFontSize: 17,
            labelFontSize: 13),
      ],
    );
  }

  void changePaidAmount(BuildContext context) {
    if (_formKey.currentState.validate()) {
      if (paymentType == MFS_PAY_TYPE_DOWN_PARTIAL &&
          (double.parse(_amountController.text) >=
              double.parse(widget.response.receipt_Deducted_Amount))) {
        CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
            getTranslated(context, 'mfs_confirm_service_err_less'));
      } else if (paymentType == MFS_PAY_TYPE_UP_PARTIAL &&
          (double.parse(_amountController.text) <=
              double.parse(widget.response.receipt_Deducted_Amount))) {
        CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
            getTranslated(context, 'mfs_confirm_service_err_more'));
      } else {
        CustomWidgetBuilder.showAlertDialog(context);
        MfsServiceCalculatorRequest newAmountServiceCalcRequest =
            widget.serviceCalcRequest;
        newAmountServiceCalcRequest.transactionValue = _amountController.text;
        _mfsRestService
            .confirmServiceCalculator(newAmountServiceCalcRequest)
            .then((res) {
          if (res.statusCode == 200) {
            Map<dynamic, dynamic> serviceCalcOutParams =
                jsonDecode(res.data.out_parameters);
            var outParams = jsonDecode(res.data.out_parameters);
            if (outParams != null &&
                outParams['OUT_PARAMETER_1'] != 0 &&
                outParams['OUT_PARAMETER_1'] != null &&
                outParams['OUT_PARAMETER_1'] != "" &&
                outParams['OUT_PARAMETER_1'] != " " &&
                outParams['OUT_PARAMETER_1'] != "0" &&
                outParams['OUT_PARAMETER_1'] != "0.0") {
              CustomWidgetBuilder.buildSnackBar(
                  scaffoldKey, context, "There is no due bill");
            } else if (res.data.status_code == '0') {
              print(res.data.toJson());
              setState(() {
                dueAmount = res.data.receipt_Deducted_Amount;
                rechargeAmount = res.data.input_transaction_value;
              });
              Navigator.pop(context);
              _amountController.clear();
            } else {
              Navigator.pop(context);
              CustomWidgetBuilder.buildSnackBar(
                  scaffoldKey,
                  context,
                  serviceCalcOutParams != null &&
                          serviceCalcOutParams['Message'] != null &&
                          serviceCalcOutParams['Message'] != " "
                      ? serviceCalcOutParams['Message']
                      : res.data.user_a_msg);
            }
          } else {
            Navigator.pop(context);
            CustomWidgetBuilder.buildSnackBar(
                scaffoldKey, context, getTranslated(context, res.getErrorCode));
          }
        });
      }
    } else {
      CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
          getTranslated(context, "mfs_confirm_service_err_invalid"));
    }
  }

  confirmPayment(BuildContext context) {
    CustomWidgetBuilder.showAlertDialog(context);
    MfsServiceCalculatorRequest transactionRequest = widget.serviceCalcRequest;
    print('transaction request');
    print(transactionRequest.toJson());
    PaymentService paymentService = PaymentService();
    CustomValue transactionRequestCV =
        CustomValue("mfs_transcation_request", jsonEncode(transactionRequest));
    CustomValue transactionResponseCV =
        CustomValue("mfs_transcation_response", "mfs_transcation_response");
    CustomValue transactionIdCV =
        CustomValue("mfs_transcation_id", "mfs_transcation_id");
    CustomValue serviceNameEnCV =
        CustomValue(EXTERNAL_SERVICE_NAME_EN, widget.service.service_name);
    CustomValue serviceNameArCV = CustomValue(
        EXTERNAL_SERVICE_NAME_AR, widget.service.service_arabic_name);
    List<CustomValue> customValues = List<CustomValue>();
    customValues.add(transactionRequestCV);
    customValues.add(transactionResponseCV);
    customValues.add(transactionIdCV);
    customValues.add(serviceNameEnCV);
    customValues.add(serviceNameArCV);
    Payment payment = Payment(
      transferTypeCode: PURCHASE_CASHCALL_SERVICES_TRANSFER_TYPE_CODE,
      amount: double.parse(dueAmount),
      customValues: customValues,
    );
    print('payment request');
    print(payment.toJson());
    paymentService.confirmMfsPayment(payment).then((res) {
      if (res.statusCode == 200) {
        Map<dynamic, dynamic> paymentRes =
            jsonDecode(res.data.customValues.mfs_transcation_response);
        print('payment response');
        print(res.data.toJson());
        print(res.data.customValues.toJson());
        print('mfs payment response');
        print(res.data.customValues.mfs_transcation_response);
        String state = jsonDecode(
            res.data.customValues.mfs_transcation_response)['statusCode'];
        if (!res.data.pending) {
          Map<String, dynamic> summary = new HashMap();
          if (state == '0') {
            String formattedDate = DateFormat('dd-MM-yyyy kk:mm:ss a').format(DateTime.now());
            String serviceName = Localization.of(context).locale.languageCode == 'en'
                ? widget.service.service_name
                : widget.service.service_arabic_name;
            summary[getTranslated(context, "confirm_service_reference")] =
                res.data.transactionNumber;
            summary[getTranslated(context, "confirm_service_amount")] =
                dueAmount.toString();
            summary[getTranslated(context, "confirm_service_name")] = serviceName;
            summary['$serviceName  ${getTranslated(context, 'for')}'] = widget.accountValue;
            summary[getTranslated(context, "date")]=formattedDate;
            summary[getTranslated(context, "amount")]= rechargeAmount.toString();
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        TransactionSummary(transcationResponse: summary)));
          } else {
            Navigator.pop(context);
            CustomWidgetBuilder.buildSnackBar(
                scaffoldKey,
                context,
                paymentRes['outParameters'] != null &&
                        paymentRes['outParameters']['Message'] != null &&
                        paymentRes['outParameters']['Message'] != " "
                    ? paymentRes['outParameters']['Message']
                    : paymentRes['userAMsg']);
          }
        }
      } else {
        CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
            getTranslated(context, res.getErrorCode.toString()));
        Navigator.pop(context);
      }
    });
  }

  void onSwitch(bool val) {
    setState(() {
      _checked = val;
    });
  }
}
