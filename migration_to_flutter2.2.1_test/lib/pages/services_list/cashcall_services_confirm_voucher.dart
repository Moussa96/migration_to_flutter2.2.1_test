import 'dart:collection';
import 'dart:convert';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/Mfs/MfsScenarioConfiguration.dart';
import 'package:cashless/models/Mfs/MfsTrxParams.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/requests/MfsServiceCalculatorRequest.dart';
import 'package:cashless/models/Mfs/MfsServices.dart';
import 'package:cashless/models/response/MfsServiceCalculatorResponse.dart';
import 'package:cashless/widgets/custom_widgets/CustomAppBar.dart';
import 'package:cashless/pages/transaction_summary/transaction_summary.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:cashless/widgets/custom_widgets/CashCallCustomWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cashless/apis/Services/MfsRestClientImpl.dart';
import 'package:cashless/util/Util.dart';
import 'package:intl/intl.dart';

class ConfirmVoucher extends StatefulWidget {
  MfsServices service;
  MfsServiceCalculatorResponse response;
  dynamic package;
  MfsServiceCalculatorRequest serviceCalcRequest =
      MfsServiceCalculatorRequest();

  ConfirmVoucher({
    Key key,
    this.service,
    this.response,
    this.package,
    this.serviceCalcRequest,
  }) : super();

  @override
  _ConfirmVoucherState createState() => _ConfirmVoucherState();
}

class _ConfirmVoucherState extends State<ConfirmVoucher> {
  Size size;
  var rechargeAmount;
  var dueAmount;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  String paymentType;
  MfsScenarioConfiguration scenarioConfiguration;
  List<MfsTrxParams> trxParams = List();
  MfsRestClientImpl _mfsRestService = MfsRestClientImpl();
  List<dynamic> packages = [];
  @override
  void initState() {
    initAmount();
  }

  void initAmount() {
    scenarioConfiguration = MfsScenarioConfiguration.fromJson(
        jsonDecode(widget.service.scenario_configuration));
    rechargeAmount = widget.response.input_transaction_value;
    dueAmount = widget.response.receipt_Deducted_Amount;
    packages = Util.stringToList(widget.service.Packages);
    packages
      ..sort((a, b) => a['package_transaction_value']
          .compareTo(b['package_transaction_value']));
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: BACKGROUND_COLOR,
      resizeToAvoidBottomInset: false,
      appBar: CustomAppBar(
          title: getTranslated(context, 'home_CashcallServices')),
      body: _buildConfirmVoucherBody(context),
      bottomNavigationBar: Container(
          width: size.width,
          height: size.height * 0.09,
          child: CustomWidgetBuilder.buildRaisedButton(context, confirmPayment,
              null, getTranslated(context, "mfs_confirm_service_confirm_btn"),
              color: PRIMARY_COLOR)),
    );
  }

  Widget _buildConfirmVoucherBody(BuildContext context) {
    return ListView(
      padding: EdgeInsets.symmetric(horizontal: 11),
      children: [
        CashCallCustomWidget.buildTopConfirmCashCallServiceCard(
          size: Size(size.width, size.height * 0.12),
          serviceCode: widget.service.service_code,
          serviceName: Localization.of(context).locale.languageCode == 'en'
              ? widget.service.service_name
              : widget.service.service_arabic_name,
          messagePartOne: getTranslated(
              context, 'cashcall_confirm_payment_message_part_one'),
          messagePartTwo: getTranslated(
              context, 'cashcall_confirm_payment_message_part_two'),
        ),
        SizedBox(
          height: 20,
        ),
        CashCallCustomWidget.buildChangePackageCard(context, buildPackagePopUp,
            label: getTranslated(context, 'selected_package'),
            value: widget.serviceCalcRequest.transactionValue,
            btnLabel: getTranslated(context, 'change_btn')),
        SizedBox(
          height: 20,
        ),
        CashCallCustomWidget.buildDataConfirmCashCallServiceCard(
            size: Size(size.width, size.height * 0.06),
            label: getTranslated(context, "mfs_confirm_service_due_amount"),
            data: rechargeAmount),
        SizedBox(
          height: 20,
        ),
        CashCallCustomWidget.buildDataConfirmCashCallServiceCard(
            size: Size(size.width, size.height * 0.085),
            label: getTranslated(context, "total_paid_amount"),
            data: dueAmount,
            color: PRIMARY_COLOR,
            dataFontColor: Colors.white,
            labelFontColor: Colors.white,
            dataFontSize: 17,
            labelFontSize: 13),
      ],
    );
  }

  void buildPackagePopUp(BuildContext context) {
    CashCallCustomWidget.buildPackagePopUp(context, changePackageValue,
        packages: packages, service: widget.service, height: size.height * 0.6);
  }

  void changePackageValue(BuildContext context, MfsServices service, package) {
    Map<String, String> inParams = new HashMap();

    inParams["in_param_1"] = package['package_code'];
    inParams["in_param_2"] = package['evd_selector'];
    inParams["in_param_3"] = "1";
    print(inParams);
    CustomWidgetBuilder.showAlertDialog(context);
    MfsServiceCalculatorRequest newAmountServiceCalcRequest =
        widget.serviceCalcRequest;
    newAmountServiceCalcRequest.transactionValue =
        package['package_transaction_value'].toString();
    newAmountServiceCalcRequest.inParameters = jsonEncode(inParams);
    _mfsRestService
        .confirmServiceCalculator(newAmountServiceCalcRequest)
        .then((res) {
      if (res.statusCode == 200) {
        Map<dynamic, dynamic> serviceCalcOutParams =
            jsonDecode(res.data.out_parameters);
        var outParams = jsonDecode(res.data.out_parameters);

        if (res.data.status_code == '0') {
          Navigator.pop(context);
          print(res.data.toJson());
          setState(() {
            dueAmount = res.data.receipt_Deducted_Amount;
            rechargeAmount = res.data.input_transaction_value;
            Navigator.pop(context);
          });
        } else {
          Navigator.pop(context);
          Navigator.pop(context);
          CustomWidgetBuilder.buildSnackBar(
              scaffoldKey,
              context,
              serviceCalcOutParams != null &&
                      serviceCalcOutParams['Message'] != null &&
                      serviceCalcOutParams['Message'] != " "
                  ? serviceCalcOutParams['Message']
                  : res.data.user_a_msg);
        }
      } else {
        Navigator.pop(context);
        Navigator.pop(context);
        CustomWidgetBuilder.buildSnackBar(
            scaffoldKey, context, getTranslated(context, res.getErrorCode));
      }
    });
  }

  confirmPayment(BuildContext context) {
    CustomWidgetBuilder.showAlertDialog(context);
    MfsServiceCalculatorRequest transactionRequest = widget.serviceCalcRequest;
    print('transaction request');
    print(transactionRequest.toJson());
    PaymentService paymentService = PaymentService();
    CustomValue transactionRequestCV =
        CustomValue("mfs_transcation_request", jsonEncode(transactionRequest));
    CustomValue transactionResponseCV =
        CustomValue("mfs_transcation_response", "mfs_transcation_response");
    CustomValue transactionIdCV =
        CustomValue("mfs_transcation_id", "mfs_transcation_id");
    CustomValue serviceNameEnCV =
        CustomValue(EXTERNAL_SERVICE_NAME_EN, widget.service.service_name);
    CustomValue serviceNameArCV = CustomValue(
        EXTERNAL_SERVICE_NAME_AR, widget.service.service_arabic_name);
    List<CustomValue> customValues = List<CustomValue>();
    customValues.add(transactionRequestCV);
    customValues.add(transactionResponseCV);
    customValues.add(transactionIdCV);
    customValues.add(serviceNameEnCV);
    customValues.add(serviceNameArCV);
    Payment payment = Payment(
      transferTypeCode: PURCHASE_CASHCALL_SERVICES_TRANSFER_TYPE_CODE,
      amount: double.parse(dueAmount),
      customValues: customValues,
    );
    print('payment request');
    print(payment.toJson());
    paymentService.confirmMfsPayment(payment).then((res) {
      if (res.statusCode == 200) {
        Map<dynamic, dynamic> paymentRes =
            jsonDecode(res.data.customValues.mfs_transcation_response);
        print('payment response');
        print(res.data.toJson());
        print(res.data.customValues.toJson());
        print('mfs payment response');
        print(res.data.customValues.mfs_transcation_response);
        String state = jsonDecode(
            res.data.customValues.mfs_transcation_response)['statusCode'];
        if (!res.data.pending) {
          var outParams = jsonDecode(
              res.data.customValues.mfs_transcation_response)['outParameters'];
          Map<String, dynamic> summary = new HashMap();
          if (state == '0') {
            String formattedDate = DateFormat('dd-MM-yyyy kk:mm:ss a').format(DateTime.now());
            summary[getTranslated(context, "date")]=formattedDate;
            summary[getTranslated(context, "confirm_service_reference")] =
                res.data.transactionNumber;
            summary[getTranslated(context, "confirm_service_amount")] =
                dueAmount.toString();
            summary[getTranslated(context, "confirm_service_name")] =
                Localization.of(context).locale.languageCode == 'en'
                    ? widget.service.service_name
                    : widget.service.service_arabic_name;
            summary[getTranslated(context, "mfs_confirm_service_voucher_pin")] =
                outParams['OUT_PARAMETER_2'];
            summary[getTranslated(context, "amount")]= rechargeAmount.toString();
            summary[getTranslated(
                    context, "mfs_confirm_service_voucher_value")] =
                outParams['OUT_PARAMETER_3'];
            if (outParams['OUT_PARAMETER_4'] != " " &&
                outParams['OUT_PARAMETER_4'] != "" &&
                outParams['OUT_PARAMETER_4'] != null) {
              summary[getTranslated(
                      context, "mfs_confirm_service_expiry_date")] =
                  outParams['OUT_PARAMETER_4'];
            }

            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        TransactionSummary(transcationResponse: summary)));
          } else {
            Navigator.pop(context);
            CustomWidgetBuilder.buildSnackBar(
                scaffoldKey,
                context,
                paymentRes['outParameters'] != null &&
                        paymentRes['outParameters']['Message'] != null &&
                        paymentRes['outParameters']['Message'] != " "
                    ? paymentRes['outParameters']['Message']
                    : paymentRes['userAMsg']);
          }
        }
      } else {
        CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
            getTranslated(context, res.getErrorCode.toString()));
        Navigator.pop(context);
      }
    });
  }
}
