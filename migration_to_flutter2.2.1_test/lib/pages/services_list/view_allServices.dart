import 'dart:convert';
import 'package:cashless/Configurations.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Mfs/MfsCashcallServices.dart';
import 'package:cashless/models/Mfs/MfsCategories.dart';
import 'package:cashless/models/Mfs/MfsScenarioConfiguration.dart';
import 'package:cashless/models/Mfs/MfsServices.dart';
import 'package:cashless/models/Mfs/MfsSubCategory.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cashless/pages/OldMfsSelectedService.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';

class ViewAllServices extends StatefulWidget {
  MfsCashcallServices cashcall_services;
  ViewAllServices({Key key, @required this.cashcall_services}) : super();

  @override
  _ViewAllServicesState createState() => _ViewAllServicesState();
}

class _ViewAllServicesState extends State<ViewAllServices> {
  SearchBar searchBar;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  MfsCategories categories = MfsCategories();
  List<MfsServices> services = List();
  List<MfsCategories> allCategories = List();
  List<MfsCategories> _filteredCategories = List();
  List<MfsSubCategory> subCategories = List();
  List<bool> aciveCards = List<bool>();
  int activeService = 0;
  int lastIndex = 0;
  bool showAllServices = false;
  _ViewAllServicesState() {
    searchBar = new SearchBar(
        onChanged: (input) {
          setState(() {
            _filteredCategories.clear();
            if (input == "") {
              _filteredCategories.addAll(allCategories);
            } else {
              _filteredCategories.addAll(allCategories);
              for (int i = 0; i < allCategories.length; i++) { // loop overall categories
                if(allCategories[i].sub_category != null && allCategories[i].sub_category.length>0) {// check if category has subCategories
                  for (int j = 0; j < allCategories[i].sub_category
                      .length; j++) { // loop overall subcategories in category
                    _filteredCategories[i].sub_category[j].services.clear();
                    if (allCategories[i].sub_category[j].sub_category_code !=
                        null &&
                        allCategories[i].sub_category[j].services != null &&
                        allCategories[i].sub_category[j].services.length > 0) {
                      List<MfsServices> _filterSubCategoryService = [];
                      for (int k = 0; k <
                          allCategories[i].sub_category[j].services
                              .length; k++) { // loop over all services in sub Category
                        if (allCategories[i].sub_category[j]
                            .services[k]
                            .service_name
                            .toLowerCase()
                            .contains(input.toLowerCase())) {
                          _filterSubCategoryService.add(allCategories[i]
                              .sub_category[j].services[k]);
                        }
                      }
                      if (_filterSubCategoryService.length > 0) {
                        _filteredCategories[i].sub_category[j].services.addAll(
                            _filterSubCategoryService);
                      }
                    }
                  }
                }
                if(allCategories[i].services != null && allCategories[i].services.length >0){// loop overall category services
                  _filteredCategories[i].services.clear();
                  List<MfsServices> _filterCategoryService = [];
                  for(int j=0;j<allCategories[i].services.length;j++){
                    if (allCategories[i]
                        .services[j]
                        .service_name
                        .toLowerCase()
                        .contains(input.toLowerCase())) {
                      _filterCategoryService.add(allCategories[i]
                          .services[j]);
                    }
                  }
                  if(_filterCategoryService.length>0) {
                    _filteredCategories[i].services.addAll(_filterCategoryService);
                  }
                }
              }
             // print(_filteredSubCategries);
            }
            //_servicesCount = allSubCategries.length;
          });
        },
        onCleared: () {
          setState(() {
            _filteredCategories.clear();
            _filteredCategories.addAll(allCategories);
            //_servicesCount = _filteredSubCategries.length;
          });
        },
        onClosed: () {
          showAllServices = false;
        },
        setState: setState,
        onSubmitted: print,
        buildDefaultAppBar: buildAppBar,
        inBar: false);
  }
  @override
  void initState() {
    super.initState();
    _allServices();
    if (widget.cashcall_services != null &&
        widget.cashcall_services.cashcall_services.length > 0) {
      setState(() {
        _filteredCategories.addAll(allCategories);
        services = widget.cashcall_services.cashcall_services[0].services;
        subCategories =
            widget.cashcall_services.cashcall_services[0].sub_category;
        aciveCards = List.generate(
            widget.cashcall_services.cashcall_services.length,
            (i) => i == 0 ? true : false);
      });
    } else {
      // there is no available services
    }
  }

  void _allServices() {
    allCategories.clear();
    print(widget.cashcall_services.cashcall_services);
    allCategories.addAll(widget.cashcall_services.cashcall_services);

  }

  selectCategory(int index) {
    setState(() {
      services = widget.cashcall_services.cashcall_services[index].services;
      subCategories =
          widget.cashcall_services.cashcall_services[index].sub_category;
      aciveCards[lastIndex] = false;
      aciveCards[index] = true;
      lastIndex = index;
    });
  }

  stringToList(String s) {
    if (s != null) {
      if (s.length != 0 && s != "[]") {
        return jsonDecode(s);
      }
      return [];
    }
  }

  void gotoSelectedSubcategoryService(int subCategoryIndex, int serviceIndex) {
    dynamic scenarioConfigString = subCategories[subCategoryIndex]
        .services[serviceIndex]
        .scenario_configuration;
    if (scenarioConfigString != null &&
        jsonDecode(scenarioConfigString) != null) {
      MfsScenarioConfiguration scenarioConfiguration =
          MfsScenarioConfiguration.fromJson(jsonDecode(scenarioConfigString));
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => SelectedService(
                  service:
                      subCategories[subCategoryIndex].services[serviceIndex],
                  packages: stringToList(subCategories[subCategoryIndex]
                      .services[serviceIndex]
                      .Packages),
                  scenarioConfiguration: scenarioConfiguration)));
    } else {
      CustomWidgetBuilder.buildSnackBar(
          scaffoldKey, context, getTranslated(context, "service_unavailable"));
    }
  }

  void gotoSelectedService(int serviceIndex) {
    dynamic scenarioConfigString =
        services[serviceIndex].scenario_configuration;
    if (scenarioConfigString != null &&
        jsonDecode(scenarioConfigString) != null) {
      MfsScenarioConfiguration scenarioConfiguration =
          MfsScenarioConfiguration.fromJson(jsonDecode(scenarioConfigString));
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => SelectedService(
                  service: services[serviceIndex],
                  packages: stringToList(services[serviceIndex].Packages),
                  scenarioConfiguration: scenarioConfiguration)));
    } else {
      CustomWidgetBuilder.buildSnackBar(
          scaffoldKey, context, getTranslated(context, "service_unavailable"));
    }
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
        iconTheme: IconThemeData(color: PRIMARY_COLOR),
        backgroundColor: Colors.white,
        title: Text(
          "Cashcall Services",
          style: const TextStyle(
              color: PRIMARY_COLOR,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              fontSize: 13.6),
        ),
        centerTitle: true,
        actions: <Widget>[
          GestureDetector(
            child: ClipRRect(
              child: Image.asset(
                'lib/assets/services_list_search_ic.png',
              ),
            ),
            onTap: () {
              setState(() {
                showAllServices = true;
              });
              print("hello000000000000000000000000000000000000000000000000000000000000000");
              searchBar.beginSearch(context);
            },
          ),
        ]);
  }

  Widget _buildCahCallServices(BuildContext context) {
    return widget.cashcall_services.cashcall_services != null
        ? Container(
            child: SingleChildScrollView(
              //color: Colors.white.withOpacity(0.9)
              child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                Container(
                  color: Colors.white,
                  padding: EdgeInsets.fromLTRB(12, 11, 0, 0),
                  child: CustomWidgetBuilder.buildCategoryCards(
                      widget.cashcall_services.cashcall_services,
                      Localization.of(context).locale.languageCode == 'en'
                          ? 'category_name'
                          : 'category_arabic_name',
                      selectCategory,
                      active: aciveCards),
                ),
                SizedBox(
                  height: 20,
                ),
                subCategories.length > 0 &&
                        subCategories[0].sub_category_code != null
                    ? Column(
                        children: List.generate(subCategories.length, (index) {
                          return Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 10, top: 10, bottom: 6),
                                child: Row(
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(20.0),
                                      child: Container(
                                        color: Colors.blue,
                                        child: FadeInImage.assetNetwork(
                                            height: 25,
                                            width: 25,
                                            fit: BoxFit.fill,
                                            placeholder:
                                                'lib/assets/services_default.jpeg',
                                            image: MFS_IMGS_BASE_URL +
                                                "categories/" +
                                                subCategories[index]
                                                    .sub_category_code +
                                                ".png"),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 5),
                                      child: Text(
                                          Localization.of(context)
                                                      .locale
                                                      .languageCode ==
                                                  'en'
                                              ? subCategories[index]
                                                  .sub_category_name
                                              : subCategories[index]
                                                  .sub_category_arabic_name,
                                          style: const TextStyle(
                                              color: CASHCALL_TITLE,
                                              fontWeight: FontWeight.w400,
                                              fontStyle: FontStyle.normal,
                                              fontSize: 10.2)),
                                    )
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 10),
                                child: Column(
                                  children: List.generate(
                                      subCategories[index].services.length,
                                      (serviceIndex) {
                                    return buildServiceCard(
                                        subCategoryIndex: index,
                                        serviceIndex: serviceIndex,
                                        arabicName: subCategories[index]
                                            .services[serviceIndex]
                                            .service_arabic_name,
                                        englishName: subCategories[index]
                                            .services[serviceIndex]
                                            .service_name);
                                  }),
                                ),
                              ),
                            ],
                          );
                        }),
                      )
                    : Container(),
                services != null && services.length > 0
                    ? Column(
                        children: List.generate(services.length, (index) {
                          return buildServiceCard(
                              serviceIndex: index,
                              arabicName: services[index].service_arabic_name,
                              englishName: services[index].service_name);
                        }),
                      )
                    : Container()
              ]),
            ),
          )
        : CustomWidgetBuilder.buildSpinner(context);
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: BACKGROUND_COLOR,
      resizeToAvoidBottomInset: false,
      appBar: searchBar.build(context),
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: ListView(
          children: [
            !showAllServices?
                 Container(
                    width: size.width,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 6,
                        ),
                        Container(
                            padding: EdgeInsets.only(bottom: 10),
                            decoration: BoxDecoration(boxShadow: [
                              BoxShadow(
                                  color: const Color(0x1f000000),
                                  offset: Offset(0, 2),
                                  blurRadius: 3,
                                  spreadRadius: 0)
                            ], color: const Color(0xfffafafa)),
                            child: _buildCahCallServices(context)),
                      ],
                    ))
                : _buildAllServices(context),
          ],
        ),
      ),
    );
  }

  Widget _buildAllServices(BuildContext context) {
    return _filteredCategories != null && _filteredCategories.length > 0?
        Column(
          children:List.generate(_filteredCategories.length,(categoryIndex){
            return _filteredCategories[categoryIndex].sub_category != null &&
                _filteredCategories[categoryIndex].sub_category.length>0?
            Column(
              children: List.generate(_filteredCategories[categoryIndex].sub_category.length, (subCategoryIndex) {
                return _filteredCategories[categoryIndex].sub_category[subCategoryIndex].sub_category_code != null &&
                    _filteredCategories[categoryIndex].sub_category[subCategoryIndex].services != null &&
                    _filteredCategories[categoryIndex].sub_category[subCategoryIndex].services.length > 0
                    ? Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 10, top: 10, bottom: 6),
                      child: Row(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(20.0),
                            child: Container(
                              color: Colors.blue,
                              child: FadeInImage.assetNetwork(
                                  height: 25,
                                  width: 25,
                                  fit: BoxFit.fill,
                                  placeholder:
                                  'lib/assets/services_default.jpeg',
                                  image: MFS_IMGS_BASE_URL +
                                      "categories/" +
                                      _filteredCategories[categoryIndex].sub_category[subCategoryIndex]
                                          .sub_category_code +
                                      ".png"),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5),
                            child: Text(
                                Localization.of(context)
                                    .locale
                                    .languageCode ==
                                    'en'
                                    ? _filteredCategories[categoryIndex].sub_category[subCategoryIndex].sub_category_name
                                    : _filteredCategories[categoryIndex].sub_category[subCategoryIndex].sub_category_arabic_name,
                                style: const TextStyle(
                                    color: CASHCALL_TITLE,
                                    fontWeight: FontWeight.w400,
                                    fontStyle: FontStyle.normal,
                                    fontSize: 10.2)),
                          )
                        ],
                      ),
                    ),
                    _filteredCategories[categoryIndex].sub_category[subCategoryIndex].services !=null && _filteredCategories[categoryIndex].sub_category[subCategoryIndex].services.length>0 ? Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Column(
                        children: List.generate(
                            _filteredCategories[categoryIndex].sub_category[subCategoryIndex].services.length,
                                (serviceIndex) {
                              return buildServiceCard(
                                  subCategoryIndex: subCategoryIndex,
                                  serviceIndex: serviceIndex,
                                  arabicName: _filteredCategories[categoryIndex].sub_category[subCategoryIndex]
                                      .services[serviceIndex]
                                      .service_arabic_name,
                                  englishName: _filteredCategories[categoryIndex].sub_category[subCategoryIndex]
                                      .services[serviceIndex]
                                      .service_name);
                            }),
                      ),
                    ):Container(),
                    _filteredCategories[categoryIndex].services !=null && _filteredCategories[categoryIndex].services.length>0?
                    Column(
                      children: List.generate(_filteredCategories[categoryIndex].services.length, (serviceIndex) {
                        return buildServiceCard(
                            serviceIndex: serviceIndex,
                            arabicName: _filteredCategories[categoryIndex].services[serviceIndex].service_arabic_name,
                            englishName: _filteredCategories[categoryIndex].services[serviceIndex].service_name);
                      }),
                    ):Container(),
                  ],
                ):Container() ;
          }),
        ) : Container();
  })):Container();
  }

  Widget buildServiceCard(
      {int subCategoryIndex = -1,
      int serviceIndex,
      String arabicName,
      String englishName}) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10, bottom: 6),
      child: Container(
        padding: EdgeInsets.only(left: 7, top: 10, bottom: 11),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(1.704)),
            boxShadow: [
              BoxShadow(
                  color: const Color(0x1f000000),
                  offset: Offset(0, 1),
                  blurRadius: 3,
                  spreadRadius: 0)
            ],
            color: const Color(0xffffffff)),
        child: GestureDetector(
          onTap: () {
            if (subCategoryIndex == -1)
              gotoSelectedService(serviceIndex);
            else
              gotoSelectedSubcategoryService(subCategoryIndex, serviceIndex);
          },
          child: Row(
            children: [
              Image.asset("lib/assets/historyAccountIcWithBg.png"),
              SizedBox(width: 8),
              Container(
                width: 0.7 * size.width,
                child: Text(
                  Localization.of(context).locale.languageCode == 'en'
                      ? englishName
                      : arabicName,
                  style: const TextStyle(
                      color: PRIMARY_COLOR,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                      fontSize: 11.9),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              new Spacer(),
              Padding(
                padding: const EdgeInsets.only(right: 12),
                child: Image.asset("lib/assets/services_arrow.png"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
