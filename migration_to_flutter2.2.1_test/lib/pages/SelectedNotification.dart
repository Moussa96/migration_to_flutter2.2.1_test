import 'dart:convert';
import 'package:cashless/apis/Services/NotificationsService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/response/NotificationRes/NotificationRes.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:intl/intl.dart';

class SelectedNotification extends StatefulWidget {
  SelectedNotification({Key key}) : super();

  @override
  _SelectedNotificationState createState() => _SelectedNotificationState();
}

class _SelectedNotificationState extends State<SelectedNotification> {
  NotificationRes _notification;
  var subject, type, newBody, formattedDate;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  NotificationsService selectedNotificationService = NotificationsService();

  @override
  void initState() {
    super.initState();
    new Future.delayed(Duration.zero, () {
      final int id = ModalRoute.of(context).settings.arguments as int;
      loadNotification(id).then((result) {
        _notification = result.data;
        setState(() {
          subject = _notification.subject;
          var body = _notification.body;
          type = _notification.type;
          print("//////////////////////////////////");
          print(type);
          newBody = body.replaceAllMapped(RegExp(r'<a.*?$'), (match) {
            return '';
          });
          final f = new DateFormat('dd-MM-yyyy hh:mm');
          formattedDate = f
              .format(DateTime.parse(_notification.date.toString()).toLocal())
              .toString();
        });
      });
    });
  }

  void _showSnackBar(BuildContext context, String text) {
    Scaffold.of(context).showSnackBar(SnackBar(content: Text(text)));
  }

  loadNotification(id) async {
    return await selectedNotificationService.getNotification(id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.grey[200],
        key: scaffoldKey,
        appBar: ActionBar(context,
            titleText: getTranslated(context, "notifications_title"),
            icon: Icons.arrow_back_ios,
            pressFunc: () => Navigator.pop(context)),
        body: _notification != null
            ? Column(
                children: [
                  Container(
                      margin: EdgeInsets.only(top: 15, left: 8),
                      alignment: Alignment.centerLeft,
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: Icon(
                              type == 'PAYMENT'
                                  ? Icons.payment
                                  : type == 'TRANSACTION_FEEDBACK'
                                      ? Icons.swap_horizontal_circle
                                      : type == 'INVOICE'
                                          ? Icons.monetization_on
                                          : Icons.notifications,
                              size: 28,
                            ),
                          ),
                          Flexible(
                              child: Text(subject,
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold))),
                        ],
                      )),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Card(
                        color: Colors.blue[400],
                        margin: EdgeInsets.all(8),
                        child: Padding(
                          padding: const EdgeInsets.all(6.0),
                          child: Text(formattedDate,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold)),
                        ),
                      ),
                    ],
                  ),
                  Divider(),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Html(
                      data: newBody,
                      style: {
                        "html": Style.fromTextStyle(TextStyle(
                            fontSize: 18,
                            color: Colors.black.withOpacity(0.6)))},
                    ),
                  ),
                ],
              )
            : CustomWidgetBuilder.buildSpinner(context));
  }
}
