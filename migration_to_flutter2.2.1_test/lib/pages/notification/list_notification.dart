import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/NotificationsService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/response/NotificationRes/NotificationRes.dart';
import 'package:cashless/widgets/custom_widgets/CustomAppBar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cashless/Configurations.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:html/parser.dart';
import 'package:cashless/pages/error_page.dart';
import 'package:cashless/util/Util.dart';
class ListNotifications extends StatefulWidget {
  ListNotifications({Key? key}) : super();

  @override
  _ListNotificationsState createState() => _ListNotificationsState();
}

class _ListNotificationsState extends State<ListNotifications> {
  late Size size;
  List<NotificationRes> _notifications = [];
  bool _hasItem = true,
      _loading = true;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  NotificationsService listNotificationsService = new NotificationsService();

  @override
  void initState() {
    super.initState();
    loadNotifications().then((result) {
      if (result.statusCode == 200 &&
          result.data != null &&
          result.data!.isNotEmpty) {
        setState(() {
          ActionBar.counter = 0;
          _notifications = result.data!;
          _hasItem = true;
          _loading = false;
        });
      }else if (Util.isConnectionError(result.getErrorCode)) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ErrorPage(errorType: NETWORK_ERROR)));
      } else if (Util.isServerError(result.getErrorCode)) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ErrorPage(errorType: SERVER_ERROR)));
      }
      else {
        setState(() {
          _hasItem = false;
          _loading = false;
        });
      }

    });
  }

  Future<RetrofitResponse<List<NotificationRes>>> loadNotifications() async {
    return await listNotificationsService.getListNotifications();
  }

  readNotification(id) async {
    await listNotificationsService.getNotification(id);
  }

  Widget _buildEmptyNotification() {
    return Column(
      crossAxisAlignment:CrossAxisAlignment.center ,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Image.asset('lib/assets/notifications_empty_graphics.png'),
        SizedBox(width: 15,),
        Text(
            getTranslated(context,'notifications_list_empty_title'),
            style: GoogleFonts.lato(
                color:  PRIMARY_COLOR,
                fontWeight: FontWeight.w700,
                fontStyle:  FontStyle.normal,
                fontSize: 11.9
            ),
            textAlign: TextAlign.center
        ),
        SizedBox(width: 3,),


      ],
    );
  }
  String _parseHtmlString(String htmlString) {
    final document = parse(htmlString);
    final String parsedString = parse(document.body!.text).documentElement!.text;
    return parsedString;
  }

  Widget _buildNotificationCard(NotificationRes notification) {
    print(notification.toJson());
    var subject = notification.subject;
    var type = notification.type;
    var date = notification.date;
    var id = notification.id;
    var read = notification.read;
    var imagePath = getImagePath(type!, read!);
    Color cardBackground =
    read ? Colors.white:NOTIFICATION_BACKGROUND_COLOR;
    var body = notification.body;
    type = notification.type;
    String data = body!.replaceAllMapped(RegExp(r'<a.*?$'), (match) {
      return '';
    });
    data = _parseHtmlString(data);
    final f = new DateFormat('dd-MM-yyyy hh:mm');
    String formattedDate =
    f.format(DateTime.parse(date.toString()).toLocal()).toString();

    return Container(
      padding: EdgeInsets.symmetric(vertical: 10,horizontal: 10),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1, color: DIVIDER_COLOR),
        ),
        color: cardBackground,
      ),
      child: InkWell(
        onTap: () {
         /* setState(() {
            notification.read = true;
          });
          Navigator.pushNamed(context, '/selectednotification',
              arguments: id);*/
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
          Flexible(
            child: Image.asset(
                imagePath
              ),
            flex: 5,
          ),
          Flexible(
            flex: 100,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                    Text(subject!, style:GoogleFonts.lato(
                        color: read?NOTIFICATION_TEXT_COLOR:PRIMARY_COLOR,
                        fontWeight:read? FontWeight.w400:FontWeight.w700,
                        fontStyle:  FontStyle.normal,
                        fontSize: 12.5
                    ) ),
                    Text(formattedDate,
                        style: GoogleFonts.lato(
                            color: read?NOTIFICATION_TEXT_COLOR:PRIMARY_COLOR,
                            fontWeight:read? FontWeight.w400:FontWeight.w700,
                            fontStyle:  FontStyle.normal,
                            fontSize: 11.9
                        )),
                  ],),
                  SizedBox(height: 5.3,),
                  Text(
                      data,
                      style: GoogleFonts.lato(
                          color: NOTIFICATION_TEXT_COLOR,
                          fontWeight:read? FontWeight.w400:FontWeight.w700,
                          fontStyle:  FontStyle.normal,
                          fontSize: 11.9
                      ))
              ],),
            ),
          )

        ],),
      ),
    );
//    });
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: BACKGROUND_COLOR,
        key: scaffoldKey,
        appBar: CustomAppBar(
            title: getTranslated(context, 'notifications_title')
        ),
        body: _loading
            ? Padding(
          padding: const EdgeInsets.all(2.0),
          child: Center(
            child: SizedBox(
              child: CircularProgressIndicator(),
              height: 20,
              width: 20,
            ),
          ),
        )
            : _hasItem ? ListView(
          children:
               _notifications
              .map((e) => _buildNotificationCard(e))
              .toList()
        ):_buildEmptyNotification()
    );
  }
  String getImagePath(String type,bool read){
    String imageName = type == 'PAYMENT'
        ? 'payment'
        : type == 'TRANSACTION_FEEDBACK'
        ? 'transaction_feedback'
        : type == 'INVOICE'
        ? 'invoice'
        : type == 'ACCOUNT'
        ? 'account'
        : 'account';
    imageName += read?'_read':'';
    return 'lib/assets/$imageName.png';
  }
}
