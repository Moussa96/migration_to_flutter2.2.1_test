import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/NotificationsService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/response/NotificationRes/NotificationRes.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:cashless/Configurations.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
class OldListNotifications extends KFDrawerContent {
  OldListNotifications({Key? key}) : super();

  @override
  _ListNotificationsState createState() => _ListNotificationsState();
}

class _ListNotificationsState extends State<OldListNotifications> {
  List<NotificationRes> _notifications = [];
  bool _hasItem = true, _loading = true;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  NotificationsService listNotificationsService = new NotificationsService();

  @override
  void initState() {
    super.initState();
    loadNotifications().then((result) {
      setState(() {
        if (result.statusCode == 200 &&
            result.data != null &&
            result.data!.isNotEmpty) {
          ActionBar.counter = 0;
          _notifications = result.data!;
          _hasItem = true;
          _loading = false;
        } else {
          _hasItem = false;
          _loading = false;
        }
      });
    });
  }

  Future<RetrofitResponse<List<NotificationRes>>> loadNotifications() async {
    return await listNotificationsService.getListNotifications();
  }

  readNotification(id) async {
    await listNotificationsService.getNotification(id);
  }

  Widget _buildEmptyNotification() {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Text(
              'There are no Notifications to display',
              style: TextStyle(
                fontSize: 18.0,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildNotificationCard(NotificationRes notification) {
//    notifications.forEach((notification) {
    var subject = notification.subject;
    var type = notification.type;
    var date = notification.date;
    var id = notification.id;
    var read = notification.read;
    Color cardBackground =
        read != null && read ? Colors.white : Colors.blue[100]!.withOpacity(0.4);
    final f = new DateFormat('dd-MM-yyyy hh:mm');
    String formattedDate =
        f.format(DateTime.parse(date.toString()).toLocal()).toString();

    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: read != null && read ? 0 : 0.25,
      child: Card(
        elevation: 0,
        color: cardBackground,
        margin: EdgeInsets.only(top: 1),
        child: InkWell(
          highlightColor: Colors.grey[200],
          onTap: () {
            setState(() {
              notification.read = true;
            });
            Navigator.pushNamed(context, '/selectednotification',
                arguments: id);
          },
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Icon(
                    type == 'PAYMENT'
                        ? Icons.payment
                        : type == 'TRANSACTION_FEEDBACK'
                            ? Icons.swap_horizontal_circle
                            : type == 'INVOICE'
                                ? Icons.monetization_on
                                : Icons.notifications,
                    color: Colors.blue[700]),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text(subject!, style: MAJOR_TEXT_STYLE),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Text(formattedDate,
                                style: TextStyle(
                                    color: Colors.blue[700],
                                    fontWeight: FontWeight.w500)),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      secondaryActions: <Widget>[
        IconSlideAction(
          caption: 'Mark as read',
          color: Colors.green[500],
          icon: Icons.done,
          onTap: () {
            readNotification(id).then((result) {
              setState(() {
                notification.read = true;
              });
            });
          },
        ),
      ],
    );
//    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.grey[300],
      key: scaffoldKey,
      appBar: ActionBar(context,
          titleText: getTranslated(context, "notifications_title"),
          icon: Icons.arrow_back_ios,
          pressFunc: () => Navigator.pop(context)),
      // drawer: DrawerFragment(),
      body: _loading
          ? Padding(
              padding: const EdgeInsets.all(10.0),
              child: Center(
                child: SizedBox(
                  child: CircularProgressIndicator(),
                  height: 24,
                  width: 24,
                ),
              ),
            )
          : ListView(
              children: _hasItem
                  ? _notifications
                      .map((e) => _buildNotificationCard(e))
                      .toList()
                  : [_buildEmptyNotification()].toList(),
            ),
    );
  }
}
