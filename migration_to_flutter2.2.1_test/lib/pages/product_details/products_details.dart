import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/apis/Services/PendingPaymentsService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/OrderItems.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/QR.dart';
import 'package:cashless/models/requests/PendingStatusRequest.dart';
import 'package:cashless/pages/generated_payment_qr.dart';
import 'package:cashless/pages/pendingRequest.dart';
import 'package:cashless/pages/static_purchase.dart';
import 'package:cashless/pages/transaction_summary/old_transaction_summary.dart';
import 'package:cashless/util/QRGenerator.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:ticketview/ticketview.dart';

class ProductsDetails extends StatefulWidget {
  double? amount;
  bool? merchantFlow;

  // List<Product> products;
  OrderItems? orderItems = OrderItems();
  var merchantId;
  var transactionId;
  Payment? dynamicQRPayment;

  ProductsDetails(
      {Key? key,
      this.amount,
      // this.products,
      this.orderItems,
      this.merchantId,
      this.transactionId,
      this.merchantFlow = false,
      this.dynamicQRPayment})
      : super();

  @override
  _ProductsDetailsState createState() => _ProductsDetailsState();
}

class _ProductsDetailsState extends State<ProductsDetails> {
  bool confirmBtnEnabled = true;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  late PendingStatusRequest req;
  PendingPaymentsService pendingPaymentsService = new PendingPaymentsService();
  late QrImage _qrImage;

  PendingRequestAction(bool flag) {
    req = new PendingStatusRequest("request is " + flag.toString(), flag);
    print(req.toJson());
    pendingPaymentsService.getStatusOfPendingRequest(
        widget.transactionId.toString(), req);
  }

  void showAlertDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(backgroundColor: Colors.blue),
          Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: Text(getTranslated(context, "loading"))),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void gotoPurchaseQR(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Purchase(
                orderItems: widget.orderItems!,
                purchaseType: 'qr',
                isProductPurchase: true,
                amount: widget.amount!)));
  }

  void gotoPurchasePIN(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Purchase(
                orderItems: widget.orderItems!,
                purchaseType: 'pin',
                isProductPurchase: true,
                amount: widget.amount!)));
  }

  void generateDynamicQR(BuildContext context) async {
    List<CustomValue> customValues = [];
    CustomValue orderCustomValue =
        CustomValue(ORDER_ITEMS, jsonEncode(widget.orderItems));
    customValues.add(orderCustomValue);
    final String? loggedId = await Util.loadFromSharedPreferences(Util.USER_ID);

    Payment payment = Payment(
        customValues: customValues,
        amount: widget.amount,
        toMemberId: loggedId,
        transferTypeCode: PURCHASE_TRANSFER_TYPE_CODE);
    QR qr = QR(qrType: DYNAMIC_QR, payment: payment);
    print(qr.payment!.toJson());
    QRGenerator.generate(qr.toJson()).then((value) {
      setState(() {
        _qrImage = value as QrImage;
      });
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => GeneratedPaymentQR(_qrImage)));
    });
  }

  void confirmDynamicQRPayment(BuildContext context) {
    setState(() {
      confirmBtnEnabled = false;
    });
    showAlertDialog(context);
    Payment payment = widget.dynamicQRPayment!;

    print(payment.toJson());

    PaymentService paymentService = PaymentService();
    paymentService.confirmMemberPayment(payment).then((value) {
      if (value.statusCode == 200) {
        Map<String, dynamic> paymentInfo = new HashMap();
        paymentInfo[getTranslated(context, "from")] =
            value.data!.fromMemberName;
        paymentInfo[getTranslated(context, "to")] = value.data!.toMemberName;
        paymentInfo[getTranslated(context, "total_amount")] = payment.amount;
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => TransactionSummary(paymentInfo)));
      } else {
        Navigator.pop(context);
        setState(() {
          confirmBtnEnabled = true;
        });
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(getTranslated(context, "error_payment")),
            duration: Duration(seconds: 2)));
      }
    });
  }

  void confirmPayment(BuildContext context) async {
    setState(() {
      confirmBtnEnabled = false;
    });
    showAlertDialog(context);

    Payment payment = Payment();
    payment.transferTypeCode = AUTHORIZED_PURCHASE_TRANSFER_TYPE_CODE;
    payment.toMemberId = widget.merchantId.toString();
    payment.amount = widget.amount;
    List<CustomValue> customValues = [];
    CustomValue idCustomValue =
        CustomValue(CV_REFERENCE_TRANSACTION_ID_PURCHASE, Util.generateUUID());
    CustomValue orderCustomValue =
        CustomValue(ORDER_ITEMS, jsonEncode(widget.orderItems!.toJson()));

    if (payment.customValues == null) {
      customValues.add(idCustomValue);
      customValues.add(orderCustomValue);
      payment.customValues = customValues;
    } else {
      payment.customValues!.add(idCustomValue);
      payment.customValues!.add(orderCustomValue);
    }

    PaymentService paymentService = PaymentService();
    paymentService.confirmMemberPayment(payment).then((value) {
      if (value.statusCode == 200 && value.data!.pending!) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => PendingRequest()));
      } else {
        Timer(Duration(seconds: 1), () {
          Navigator.pop(context);
          setState(() {
            confirmBtnEnabled = true;
          });
        });
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(getTranslated(context, "error_payment")),
            duration: Duration(seconds: 2)));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white.withOpacity(0.9),
        resizeToAvoidBottomInset: false,
        appBar: ActionBar(context,
            titleText: getTranslated(context, "products_details_title"),
            icon: Icons.arrow_back_ios,
            pressFunc: () => Navigator.pop(context)),
        // drawer: DrawerFragment(),
        body: TicketView(
          backgroundColor: Colors.transparent,
          contentPadding: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
          triangleAxis: Axis.vertical,
          borderRadius: 6,
          drawTriangle: true,
          trianglePos: 2,
          dividerColor: Colors.black12,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                      getTranslated(context, "products_details_receipt_title"),
                      style: TextStyle(
                          fontSize: 30, decoration: TextDecoration.underline)),
                ),
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Chip(
                          label: Text(
                              getTranslated(context, "products_details_name"),
                              style: TextStyle(fontSize: 20)),
                          avatar: Icon(Icons.format_quote, color: Colors.blue)),
                      Chip(
                          label: Text(
                              getTranslated(context, "products_details_qnt"),
                              style: TextStyle(fontSize: 20)),
                          avatar: Icon(Icons.format_list_numbered,
                              color: Colors.blue)),
                      Chip(
                          label: Text(
                              getTranslated(context, "products_details_price"),
                              style: TextStyle(fontSize: 20)),
                          avatar: Icon(Icons.attach_money, color: Colors.blue)),
                    ]),
                Container(
                  margin: EdgeInsets.all(20),
                  // decoration: BoxDecoration(border: Border.all(color: Colors.black12)),
                  height: MediaQuery.of(context).size.height * 0.3,
                  child: ListView.builder(
                      itemCount: widget.orderItems!.items!.length!,
                      itemBuilder: (context, i) => Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Expanded(
                                    child: Center(
                                        child: Text(
                                            widget.orderItems!.items![i].title!,
                                            style: TextStyle(fontSize: 18)))),
                                Expanded(
                                    child: Center(
                                        child: Text(
                                            widget.orderItems!.items![i]
                                                .orderedAmount
                                                .toString(),
                                            style: TextStyle(fontSize: 18)))),
                                Expanded(
                                    child: Center(
                                        child: Text(
                                            widget.orderItems!.items![i].price
                                                    .toString() +
                                                " " +
                                                getTranslated(context, "units"),
                                            style: TextStyle(fontSize: 18))))
                              ]))),
                ),
                Text(
                    getTranslated(context, "products_details_total") +
                        MONEY_FORMATTER.format(widget.amount) +
                        " " +
                        getTranslated(context, "units"),
                    style: TextStyle(fontSize: 22, color: Colors.green)),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Divider(),
                ),
                if (!widget.merchantFlow!)
                  Text(getTranslated(context, "products_details_choose_type"),
                      style: TextStyle(fontSize: 20, color: Colors.black54)),
                widget.merchantFlow!
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Expanded(
                                child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: RaisedButton.icon(
                                  onPressed: () {
                                    PendingRequestAction(true);
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                PendingRequest(
                                                    paymentStatus: true)));
                                  },
                                  icon: Icon(Icons.check_circle),
                                  label: Text(
                                      getTranslated(context,
                                          "products_details_btn_accept"),
                                      style: TextStyle(
                                        fontSize: 20,
                                      )),
                                  color: Colors.green,
                                  textColor: Colors.white,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(18.0))),
                            )),
                            Expanded(
                                child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: ElevatedButton.icon(
                                onPressed: () {
                                  PendingRequestAction(false);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => PendingRequest(
                                              paymentStatus: true)));
                                },
                                icon: Icon(Icons.cancel),
                                label: Text(
                                    getTranslated(
                                        context, "products_details_btn_deny"),
                                    style: TextStyle(
                                      fontSize: 20,
                                    )),
                                style: ElevatedButton.styleFrom(
                                    primary: Colors.red,
                                    textStyle: TextStyle(color: Colors.white),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(18.0))),
                              ),
                            ))
                          ],
                        ),
                      )
                    : Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: widget.merchantId != null
                            ? CustomWidgetBuilder.buildRaisedButton(
                                context,
                                widget.dynamicQRPayment == null
                                    ? confirmPayment
                                    : confirmDynamicQRPayment,
                                null,
                                getTranslated(
                                    context, "products_details_btn_confirm"),
                                size: MainAxisSize.max,
                                isEnabled: confirmBtnEnabled)
                            : Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Expanded(
                                          child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: CustomWidgetBuilder
                                            .buildRaisedButton(context,
                                                gotoPurchaseQR, null, "QR",
                                                size: MainAxisSize.max),
                                      )),
                                      Expanded(
                                          child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: CustomWidgetBuilder
                                            .buildRaisedButton(context,
                                                gotoPurchasePIN, null, "PIN",
                                                size: MainAxisSize.max),
                                      )),
                                      Expanded(
                                          child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: CustomWidgetBuilder
                                            .buildRaisedButton(context,
                                                generateDynamicQR, null, "DQR",
                                                size: MainAxisSize.max),
                                      )),
                                    ],
                                  ),
                                ],
                              ),
                      ),
              ],
            ),
          ),
        ));
  }
}
