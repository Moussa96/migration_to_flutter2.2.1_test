import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/pages/DrawerMain.dart';
import 'package:cashless/pages/PendingPaymentRequests.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class PendingRequest extends StatelessWidget {
  PendingRequest({Key key, this.paymentStatus = false}) : super();
  bool paymentStatus;

  void gotoHome(BuildContext context) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (c) => DrawerMain()), (route) => false);
  }

  void returnToList(BuildContext context) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (c) => DrawerMain()), (route) => false);
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => PendingPaymentRequests()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[300],
        resizeToAvoidBottomInset: false,
        appBar: ActionBar(context),
        body: paymentStatus == false
            ? Card(

                margin: EdgeInsets.all(20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                          getTranslated(context, "pending_request_title"),
                          style: TextStyle(fontSize: 24),
                          textAlign: TextAlign.center),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Icon(Icons.check_circle,
                          color: Colors.green, size: 200.0),
                    ),
                    Text( getTranslated(context, "pending_request_done")),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: CustomWidgetBuilder.buildRaisedButton(
                          context,
                          gotoHome,
                          null,
                          getTranslated(context, "transaction_summary_btn"),
                          size: MainAxisSize.max,
                          icon: Icons.home),
                    ),
                  ],
                ))
            : Card(
                margin: EdgeInsets.all(20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                          getTranslated(context, "pending_request_done"),
                          style: TextStyle(fontSize: 24),
                          textAlign: TextAlign.center),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Icon(Icons.check_circle,
                          color: Colors.green, size: 200.0),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: CustomWidgetBuilder.buildRaisedButton(
                          context,
                          returnToList,
                          null,
                          getTranslated(
                              context, "pending_request_pending_list"),
                          size: MainAxisSize.max,
                          icon: Icons.list),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: CustomWidgetBuilder.buildRaisedButton(
                          context,
                          gotoHome,
                          null,
                          getTranslated(context, "transaction_summary_btn"),
                          size: MainAxisSize.max,
                          icon: Icons.home),
                    ),
                  ],
                )));
    /* return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          backgroundColor: Colors.grey[300],
          resizeToAvoidBottomInset: false,
          appBar: ActionBar(context),
          body: paymentStatus == false
              ? Card(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                  margin: EdgeInsets.all(20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Text(getTranslated(context, "pending_request_title"),
                            style: TextStyle(fontSize: 24),
                            textAlign: TextAlign.center),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child:
                            SpinKitRing(color: Colors.blue[700], size: 200.0, lineWidth: 10,),
                      ),
                      Text(getTranslated(context, "pending_request_waiting_for_response")),
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: CustomWidgetBuilder.buildRaisedButton(
                            context,
                            gotoHome,
                            null,
                            getTranslated(context, "transaction_summary_btn"),
                            size: MainAxisSize.max,
                            icon: Icons.home),
                      ),
                    ],
                  ))
              : Card(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                margin: EdgeInsets.all(20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(getTranslated(context, "pending_request_done"),
                          style: TextStyle(fontSize: 24),
                          textAlign: TextAlign.center),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Icon(Icons.check_circle,
                          color: Colors.green, size: 200.0),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child:
                      CustomWidgetBuilder.buildRaisedButton(
                      context,
                      returnToList,
                      null,
                      getTranslated(context, "pending_request_pending_list"),
                      size: MainAxisSize.max,
                      icon: Icons.list),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child:
                      CustomWidgetBuilder.buildRaisedButton(
                        context,
                        gotoHome,
                        null,
                        getTranslated(context, "transaction_summary_btn"),
                        size: MainAxisSize.max,
                        icon: Icons.home),
                    ),
                  ],
                )
          )
      ),
    );*/
  }
}
