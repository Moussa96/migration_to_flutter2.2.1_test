import 'package:cashless/apis/Services/TransactionHistoryService.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'fragments/action_bar.dart';
class EnquiryTranscation extends StatefulWidget {
  Map<dynamic, dynamic> transcationResponse = null;

  EnquiryTranscation(
    this.transcationResponse, {
    Key key,
  }) : super();

  @override
  _EnquiryTranscationState createState() =>
      _EnquiryTranscationState(transcationResponse);
}

class _EnquiryTranscationState extends State<EnquiryTranscation> {
  String refrenceTranscationId;
  TransactionHistoryService history = new TransactionHistoryService();
  Map<dynamic, dynamic> transcationResponse = null;
  List<Widget> widgettranscation;

  _EnquiryTranscationState(this.transcationResponse);

  List<Widget> transcationResponseDetails() {
    List<Widget> widget = new List();
    for (var entry in transcationResponse.entries) {
      print('in the loop');
      print(entry.key);
      print(entry.value);
      widget.add(CustomWidgetBuilder.buildText(entry.key + ' : ' + entry.value,
          fontSize: 15));
    }
    return widget;
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      widgettranscation = transcationResponseDetails();
    });
  }

//  Future<Map> transactionDetails() async {
//    Map<dynamic, dynamic> rpTranscation = null;
//    await Util.loadFromSharedPreferences(Util.ACCOUNT_ID).then((result) {
//      userId = result;
//    });
//    response = await history.getTransactionHistory(userId, customValueName: "ReferenceTransactionId", value: "ab4c0460-dc9d-4407-9f9c-81ef20fb8626");
//    print(response);
//    if (response.statusCode == 200) {
//      Map<dynamic, dynamic> rpTranscation = jsonDecode(response.body);
//        return rpTranscation;
//      } else {
//        return rpTranscation;
//      }
//  }

  List<Widget> details() {
    List<Widget> widget = new List();
    if (transcationResponse == null) {
      widget.add(CustomWidgetBuilder.buildText(
        'failed Transcation ',
        fontSize: 20,
      ));
      widget.add(CustomWidgetBuilder.buildText(
        'unpaid transcation',
        fontSize: 15,
      ));
    } else {
      if (transcationResponse['elements'][0]['customValues'][0]['value'] ==
          refrenceTranscationId) {
        widget.add(CustomWidgetBuilder.buildText(
          'successful Transcation ',
          fontSize: 20,
        ));
        widget.add(CustomWidgetBuilder.buildText(
            'Transaction Number : ' +
                transcationResponse['elements'][0]['transactionNumber'],
            fontSize: 10));
        widget.add(CustomWidgetBuilder.buildText(
            'From : ' +
                transcationResponse['elements'][0]['member']['username'],
            fontSize: 15));
        widget.add(CustomWidgetBuilder.buildText(
            'Total Amount : ' +
                transcationResponse['elements'][0]['formattedAmount'],
            fontSize: 15));
      } else {
        widget.add(CustomWidgetBuilder.buildText(
          'Pending Transcation ',
          fontSize: 20,
        ));
        widget.add(CustomWidgetBuilder.buildText(
          'pending confirmation from Customer',
          fontSize: 15,
        ));
      }
    }
    return widget;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: ActionBar(context,
          icon: Icons.arrow_back_ios, pressFunc: () => Navigator.pop(context)),
      // drawer: DrawerFragment(),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: ListView(
          children: <Widget>[
            Column(children: widgettranscation),
          ],
        ),
      ),
    );
  }
}
