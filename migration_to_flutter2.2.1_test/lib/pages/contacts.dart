import 'package:cashless/apis/Services/ContactService.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/pages/fragments/drawer_fragment.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';

class Contacts extends StatefulWidget {
  Contacts({Key key}) : super();

  @override
  _ContactsState createState() => _ContactsState();
}

class _ContactsState extends State<Contacts> {
  String _contactName;
  var _contact;
  final _formKey = GlobalKey<FormState>();
  ContactService _contactUseCase = ContactService();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void searchContact(BuildContext context) async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      try {
        _contact = await _contactUseCase.searchContact(_contactName);
      } catch (e) {
        _contact = null;
        FocusScope.of(context).unfocus();
        Scaffold.of(context).showSnackBar(
            SnackBar(duration: Duration(seconds: 5), content: Text('$e')));
      }
      setState(() {
        _buildSearchResults(context);
      });
    }
  }

  Widget _buildSearchForm(BuildContext context) {
    return Builder(
      builder: (BuildContext context) {
        return Container(
          margin: EdgeInsets.all(15.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(labelText: 'Contact Name'),
                  keyboardType: TextInputType.text,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Contact Name is required';
                    }
                    return null;
                  },
                  onSaved: (String value) {
                    _contactName = value;
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: CustomWidgetBuilder.buildRaisedButton(
                      context, searchContact, null, 'Search',
                      icon: Icons.search, size: MainAxisSize.max),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  addContact(BuildContext context) async {
    try {
      _contactUseCase.addToMyContacts(_contact['username']);
      setState(() {
        FocusScope.of(context).unfocus();
        Scaffold.of(context).showSnackBar(SnackBar(
            duration: Duration(seconds: 5),
            content: Text('Contact Added Successfully.')));
      });
    } catch (e) {
      FocusScope.of(context).unfocus();
      Scaffold.of(context).showSnackBar(SnackBar(
          duration: Duration(seconds: 5),
          content: Text('Cannot add contact please try again later.')));
    }
  }

  Widget _buildSearchResults(BuildContext context) {
    if (_contact != null) {
      String thumbnailUrl;
      if (_contact['images'] != null) {
        thumbnailUrl = _contact['images'][0]['thumbnailUrl'];
      }

      return Builder(builder: (BuildContext context) {
        return Card(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              ListTile(
                isThreeLine: true,
                leading: thumbnailUrl != null
                    ? Image.network(thumbnailUrl)
                    : Icon(Icons.contacts),
                title: Text(
                  _contact['name'].toString(),
                  style: TextStyle(
                    fontSize: 25,
                  ),
                ),
                subtitle: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Text('Email: ${_contact['email'].toString()}'),
                    ],
                  ),
                ),
              ),
              ButtonBar(
                children: <Widget>[
                  CustomWidgetBuilder.buildRaisedButton(
                      context, addContact, null, 'Add',
                      icon: Icons.contacts, size: MainAxisSize.max)
                ],
              ),
              SizedBox(
                height: 5.0,
              )
            ],
          ),
        );
      });
    } else {
      return Container();
//      return Card(
//        child: Padding(
//          padding: const EdgeInsets.all(10.0),
//          child: Text('Please enter valid username.'),
//        ),
//      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: ActionBar(context),
        drawer: DrawerFragment(),
        body: Container(
          child: ListView(
            children: <Widget>[
              _buildSearchForm(context),
              _buildSearchResults(context),
            ],
          ),
        ));
  }
}
