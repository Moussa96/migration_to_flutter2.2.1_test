import 'dart:io';

import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/util/QRGenerator.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission/permission.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:screenshot/screenshot.dart';

import 'fragments/KF_Drawer.dart';
import 'fragments/action_bar.dart';

class GeneratedQRCode extends KFDrawerContent {
  User user = User();

  GeneratedQRCode({Key key, this.user}) : super();

  @override
  _GeneratedQRCodeState createState() => _GeneratedQRCodeState();
}

class _GeneratedQRCodeState extends State<GeneratedQRCode> {
  QrImage qrImage;
  ScreenshotController screenshotController = ScreenshotController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    QRGenerator.generate(widget.user.toJson(),size: 75.0).then((value) =>
      setState(() {
        qrImage = value;
      })
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ActionBar(context,
          titleText: getTranslated(context, 'registeration_title'),
          icon: Icons.arrow_back_ios,
          pressFunc: () => Navigator.pop(context)),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Screenshot(
              controller: screenshotController,
              child: Center(
                child: Card(
                    child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: qrImage,
                )),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 50,horizontal: 30),
            child: CustomWidgetBuilder.buildRaisedButton(
                context, saveImage, null, 'Save photo',
                size: MainAxisSize.min),
          )
        ],
      ),
    );
  }

  void saveImage(BuildContext context) async {
    // take permission to save image in gallery
    List<PermissionName> permissionNames = [];
    permissionNames.add(PermissionName.Storage);
    var permissions = await Permission.requestPermissions(permissionNames);
    // check permission
    switch (permissions[0].permissionStatus) {
      case PermissionStatus.allow:
        print('allow');
        saveImageToGallery();
        break;
      default:
        print('default');
        if (Platform.isAndroid)
          CustomWidgetBuilder.buildAlertDialog(context,cancelDialog,null,"Error permission deny","ok");

    }

  }

  void cancelDialog(BuildContext context) async{
    Navigator.pop(context);
  }

  void saveImageToGallery()async{
    final directory = (await getApplicationDocumentsDirectory ()).path;
    String date = DateTime.now().toIso8601String();
    String fileName = '${widget.user.name} - ${widget.user.mobileNumber} $date';
    String path = '$directory/$fileName.png';
    screenshotController
        .capture(delay: Duration(milliseconds: 10), pixelRatio: 1.5,path: path)
        .then((File image) async {
      print("Capture Done");

      final result = await GallerySaver.saveImage(image.path);
/*      String dir = "/Internal storage/Pictures/$fileName.png";
      print(dir);
      final Email email = Email(
        body: ' Dear ${widget.user.name} , You\'ve successfully register in ICT event . find your QR code in attached file',
        subject: 'Cashcall Register Confirmation',
        recipients: ['maisamir896@gamil.com'],
        attachmentPaths: [dir],
        isHTML: false,
      );

      await FlutterEmailSender.send(email);*/
      if(result)
        CustomWidgetBuilder.buildAlertDialog(context,cancelDialog,null,"Photo saved in Gallery","ok");
      else
        CustomWidgetBuilder.buildAlertDialog(context,cancelDialog,null,"Error cannot save photo in gallery please check your available storage","ok");

    }).catchError((onError) {
      print(onError);
    });
  }
}
