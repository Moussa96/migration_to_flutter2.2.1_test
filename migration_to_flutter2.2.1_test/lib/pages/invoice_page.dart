import 'dart:convert';

import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/InvoiceService.dart';
import 'package:cashless/models/response/GetInvoicesResponse.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
class InvoicePage extends StatefulWidget {
  InvoicePage({Key key}) : super();

  @override
  _InvoicePageState createState() => _InvoicePageState();
}

class _InvoicePageState extends State<InvoicePage> {
  var _invoices = [];
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  InvoiceService invoicesUsecase = InvoiceService();

  @override
  void initState() {
    super.initState();
    loadTransactionHistory().then((result) {
      setState(() {});
    });
  }

  loadTransactionHistory() async {
    GetInvoicesResponse response =
        invoicesUsecase.getInvoices() as GetInvoicesResponse;
    _invoices = jsonDecode(response.toString());
  }

  Widget _buildEmptyTransaction() {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            'There are no invoices to pay',
            style: TextStyle(
              fontSize: 18.0,
            ),
          ),
        ],
      ),
    );
  }

  void payInvoice(BuildContext context, dynamic transaction) {
    Navigator.pushReplacementNamed(context, '/invoices/details',
        arguments: transaction);
  }

  Widget _buildInvoiceCard(dynamic transaction) {
    var memberName = 'System';
    var date = transaction['date'];
    var amount = transaction['amount'];
    var description = transaction['description'];
    if (transaction['fromMember'] != null) {
      memberName = transaction['fromMember']['name'];
    }
    return Card(
      margin: EdgeInsets.all(5.0),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Text(memberName.toString(), style: MAJOR_TEXT_STYLE),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          date.toString(),
                          style: TextStyle(color: MAJOR_COLOR),
                        ),
                        Text(
                          amount.toString(),
                          style: TextStyle(
                            color: amount < 0.0 ? Colors.red : Colors.green,
                            fontSize: 18,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        description != null ? Text(description) : Container(),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        CustomWidgetBuilder.buildRaisedButton(
                            context, payInvoice, transaction, 'Pay',
                            icon: Icons.payment, size: MainAxisSize.max),
                      ],
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 5.0,
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: ActionBar(context,
          icon: Icons.arrow_back_ios, pressFunc: () => Navigator.pop(context)),
      // drawer: DrawerFragment(),
      body: Builder(builder: (BuildContext context) {
        return Container(
          child: Column(
            children: _invoices != null
                ? _invoices.map((e) => _buildInvoiceCard(e)).toList()
                : [_buildEmptyTransaction()].toList(),
          ),
        );
      }),
    );
  }
}
