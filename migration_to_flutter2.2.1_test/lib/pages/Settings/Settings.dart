import 'package:cashless/Configurations.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/QR.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/pages/fragments/home_action_bar.dart';
import 'package:cashless/pages/fragments/navigation_bar.dart';
import 'package:cashless/pages/reset_password/reset_password.dart';
import 'package:cashless/util/QRGenerator.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:cashless/widgets/custom_widgets/SettingCustomWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:cashless/main.dart';

class Settings extends StatefulWidget {
  Settings({Key key}) : super();

  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Settings> {
  String name = "", email = "", mobileNumber = "", username = "";
  Size size;
  QrImage _qrImage;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPersonalData();
  }

  Widget buildSettingTopContent() {
    return Container(
        width: size.width,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment(0.5, 0.5386387563848061),
                end: Alignment(0.5, 2.962704824565857),
                colors: [PRIMARY_COLOR, const Color(0xff00a3da)])),
        child: Column(children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                width: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Card(
                    child: _qrImage,
                  ),
                ],
              ),
              SizedBox(
                width: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 26,
                  ),
                  Text(name,
                      style: GoogleFonts.lato(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                          fontSize: 13.6)),
                  SizedBox(
                    height: 7,
                  ),
                  Text(username,
                      style: GoogleFonts.lato(
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          fontSize: 11.9)),
                  SizedBox(
                    height: 7,
                  ),
                  Text(email,
                      style: GoogleFonts.lato(
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          fontSize: 11.9))
                ],
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Opacity(
            opacity: 0.1,
            child: Container(
                width: size.width * 0.8,
                height: 1,
                decoration: BoxDecoration(color: Colors.white)),
          ),
          SizedBox(height: 40,)
        ]
        ));
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: BACKGROUND_COLOR,
      resizeToAvoidBottomInset: false,
      appBar: HomeActionBar(context),
      body: ListView(
        children: [
          buildSettingTopContent(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 20,
              ),
              CustomWidgetBuilder.buildSettingTitle(
                  getTranslated(context, 'privacy_settings')),
              SizedBox(
                height: 8,
              ),
              CustomWidgetBuilder.buildSettingCategory(
                  context, onPress, new ChangePassword(),
                  imagePath: 'lib/assets/more_chnage_password.png',
                  title: getTranslated(context, 'change_password'),
                  iconPath: 'lib/assets/services_arrow.png'),
              SizedBox(
                height: 20,
              ),
              CustomWidgetBuilder.buildSettingTitle(
                  getTranslated(context, 'general_settings')),
              SizedBox(
                height: 8,
              ),
              CustomWidgetBuilder.buildSettingCategory(
                  context, buildChangeLanguageAlert, size.width,
                  imagePath: 'lib/assets/more_language_ic.png',
                  title: getTranslated(context, 'change_language'),
                  iconPath: 'lib/assets/services_arrow.png'),
              SizedBox(
                height: 1,
              ),
              CustomWidgetBuilder.buildSettingCategory(
                  context, buildLogoutAlert, null,
                  imagePath: 'lib/assets/logout_ic.png',
                  title: getTranslated(context, 'logout'),
                  iconPath: 'lib/assets/more_logout_ic.png'),
            ],
          )
        ],
      ),
      bottomNavigationBar: NavigationBar(
        activeItem: 4,
      ),
    );
  }

  void buildLogoutAlert(BuildContext context) {
    SettingCustomWidget.buildLogoutAlert(context, logout, size.width);
  }

  void getPersonalData() {
    // get name, email and mobile number
    Util.loadFromSharedPreferences(Util.USERNAME).then((usernameVal) {
      username = usernameVal;
      generateStaticQR();
    });
    Util.loadFromSharedPreferences(Util.NAME).then((nameVal) {
      name = nameVal;
      generateStaticQR();
    });
    Util.loadFromSharedPreferences(Util.EMAIL).then((emailVal) {
      email = emailVal;
    });
  }

  void generateStaticQR() async {
    final String loggedId = await Util.loadFromSharedPreferences(Util.USER_ID);
    final String groupCode = await Util.loadFromSharedPreferences(Util.GROUP);
    User user = User(id: loggedId, name: name, groupCode: groupCode);
    QR qr = QR(qrType: STATIC_QR, user: user);
    print(qr.user.toJson());
    QRGenerator.generate(qr.toJson(), size: 100).then((value) {
      setState(() {
        _qrImage = value;
      });
    });
  }

  void buildChangeLanguageAlert(BuildContext context, double width) {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
        ),
        builder: (context) => Container(
          child: Column(
            mainAxisSize:MainAxisSize.min ,
            children: [
              Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                          Radius.circular(12)
                      ),
                      color: const Color(0xccededed)
                  ),
                child:  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:[
                      SizedBox(height: 15,),
                      buiLdLanguageTitle(context),
                      SizedBox(height: 15,),
                      Column(
                        children:
                        List.generate(Language.LanguageList().length, (index) {
                          return buildLanguageTap(
                              context, _changeLang, Language.LanguageList()[index]);
                        }),
                      ),
                    ]
                )),
              SizedBox(height: 6.8,),
              Container(
                width: size.width,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                          Radius.circular(12)
                      ),
                      color: Colors.white
                  ),
                child: buildCancelBtn(context),
              )
            ],
          ),
        ));
  }

  void onPress(BuildContext context, Object object) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => object));
  }

  void logout(BuildContext context) {
    Util.clear();
    Navigator.of(context)
        .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
  }

  Widget buiLdLanguageTitle(BuildContext context) {
    return CustomWidgetBuilder.buildText(
        getTranslated(context, 'change_language'), color: LANGUAGE_ALERT_COLOR,
        fontWeight: FontWeight.w600,
        fontStyle:  FontStyle.normal,
        fontSize: 11.1);
  }

  Widget buildDivider() {
    return Opacity(
      opacity: 0.5,
      child: Container(
          width: size.width,
          height: 0.7,
          decoration: BoxDecoration(color: DIVIDER_COLOR)),
    );
  }

  buildLanguageTap(
      BuildContext context, Function changeLang, Language language) {
    return Column(
      children: [
        buildDivider(),
        InkWell(
          onTap: () {
            Navigator.pop(context);
            changeLang(context, language);
          },
          child: Container(
              padding: EdgeInsets.symmetric(vertical: 12,horizontal: size.width*0.3),
              child: CustomWidgetBuilder.buildText(language.name,color:LANGUAGE_COLOR,
                  fontWeight: FontWeight.w400,
                  fontStyle:  FontStyle.normal,
                  fontSize: 17.0)),
        )
      ],
    );
  }

  Future<void> _changeLang(BuildContext context, Language language) async {
    Locale _temp;
    switch (language.languageCode) {
      case 'en':
        _temp = Locale(language.languageCode, 'US');
        break;
      case 'ar':
        _temp = Locale(language.languageCode, 'EG');
        break;
      default:
        _temp = Locale(language.languageCode, 'US');
    }
    await Util.saveTOSharedPreferences(Util.LANGUAGE,language.languageCode);
    MyApp.setLocale(context, _temp);
  }

  Widget buildCancelBtn(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: InkWell(
        onTap: (){
          Navigator.pop(context);
        },
        child: CustomWidgetBuilder.buildText(getTranslated(context,'home_logout_cancel_btn'),
            color:  LANGUAGE_COLOR,
            fontWeight: FontWeight.w600,
            fontStyle:  FontStyle.normal,
            fontSize: 17.0),
      ),
    );
  }
}
