import 'dart:collection';
import 'dart:convert';

import 'package:cashless/Configurations.dart';
import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/QRResults.dart';
import 'package:cashless/pages/CashOutPayment.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/pages/fragments/drawer_fragment.dart';
import 'package:cashless/pages/purchase_payment.dart';
import 'package:cashless/util/QRGenerator.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PayThroughScanQR extends KFDrawerContent {
  final BuildContext menuScreenContext;
  final Function onScreenHideButtonPressed;
  final bool hideStatus;
  PayThroughScanQR({this.menuScreenContext, this.onScreenHideButtonPressed, this.hideStatus, Key key}) : super();

  @override
  _PayThroughScanQRState createState() => _PayThroughScanQRState();
}

class _PayThroughScanQRState extends State<PayThroughScanQR> {
  Map<String, dynamic> cashOutInfo = new HashMap();
  bool load = false;
  Map _paymentInfo = new Map();
  String message;
  final _formKey = GlobalKey<FormState>();
  Payment _paymentRequest = new Payment();
  bool isValid = false;

  @override
  void initState() {
    super.initState();
    scanQR(context);
  }

  void scanQR(BuildContext context) async {
    print('scan QR');
    QRResults result;
    QRGenerator.scan().then((value) {
      load = true;
      result = value as QRResults;
      if (result.status == 1) {
        _paymentInfo = jsonDecode(result.content);
        _paymentRequest.amount = double.parse(_paymentInfo['amount']);
        _paymentRequest.toMemberId = _paymentInfo['userId'];
        _paymentRequest.fromMemberId = _paymentInfo['memberId'];
        _paymentRequest.transferTypeId = _paymentInfo['transferTypeId'];
        print(_paymentInfo['createdTime']);
        dispachter(context);
      } else {
        setState(() {
          load = false;
          isValid = false;
          message = "The Payment Cancelled";
          Printmessage(message, context);
        });
      }
    });
  }

  void dispachter(BuildContext context) {
    if (_paymentInfo['QRCodeType'] == "cashOut") {
      _paymentRequest.customValues.add(CustomValue(CV_REFERENCE_TRANSACTION_ID_CASHOUT,_paymentInfo['ReferenceTransactionId']));
      DateTime createdTime = new DateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss")
          .parse(_paymentInfo['createdTime']);
      print(_paymentInfo['createdTime']);
      if (dynamicQRCode(createdTime) == true) {
        setState(() {
          Util.isValidPayment(_paymentRequest).then((value) {
            isValid = value;
            if (isValid == true) {
              print("i am here");
              print(_paymentRequest.amount);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CashOutPayment(_paymentRequest)));
            } else {
              message = "You are not eligible to do this payment";
              Printmessage(message, context);
            }
          });
        });
      } else {
        setState(() {
          load = false;
          isValid = false;
          message = "This QR code is expired. Payment confirmation should be done within 2 mins";
          Printmessage(message, context);
        });
      }
    } else if (_paymentInfo['QRCodeType'] == "purchase") {
      _paymentRequest.customValues.add(CustomValue(CV_REFERENCE_TRANSACTION_ID_PURCHASE, _paymentInfo['ReferenceTransactionId']));
      print("purchase");
      DateTime createdTime = new DateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss")
          .parse(_paymentInfo['createdTime']);
      _paymentRequest.description=_paymentInfo['description'];
      print(_paymentInfo['createdTime']);
      if (dynamicQRCode(createdTime) == true) {
        setState(() {
          Util.isValidPayment(_paymentRequest).then((value) {
            isValid = value;
            if (isValid == true) {
              print(_paymentRequest.amount);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PurchasePayment(_paymentRequest)));
            } else {
              message = "You are not eligible to do this payment";
              Printmessage(message, context);
            }
          });
        });
      } else {
        setState(() {
          load = false;
          isValid = false;
          message = "This QR code is expired. Payment confirmation should be done within 2 mins";
          Printmessage(message, context);
        });
      }
    } else {
      setState(() {
        load = false;
        isValid = false;
        message = "The Invalid Payment";
        Printmessage(message, context);
      });
    }
  }

  bool dynamicQRCode(DateTime createdTime) {
    String tn =
        new DateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss").format(DateTime.now());
    DateTime timeNow =
        new DateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss").parse(tn);
    Duration total = timeNow.difference(createdTime);
    if (total.inSeconds <= 120) {
      return true;
    } else {
      return false;
    }
  }

  Widget Printmessage(String message, BuildContext context) {
    Widget widget;
    if (message == null) {
      widget = Container();
    } else {
      widget = CustomWidgetBuilder.messageText(
          "payment Info",
          TextStyle(
              fontSize: 35, fontWeight: FontWeight.bold, color: Colors.amber),
          message,
          TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold,
              color: Colors.redAccent));
    }
    return widget;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // drawer: DrawerFragment(),
        resizeToAvoidBottomInset: false,
        appBar: ActionBar(context, icon: Icons.arrow_back_ios,pressFunc:  () => Navigator.pop(context)),
        body: Builder(builder: (BuildContext context) {
          return Container(
              child: load
                  ? Container(
                      child: isValid
                          ? Form(
                              key: _formKey,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                              ),
                            )
                          : Container(
                              child: Printmessage(message, context),
                            ))
                  : Container(
                      child: Printmessage(message, context),
                    ));
        }));
  }
}
