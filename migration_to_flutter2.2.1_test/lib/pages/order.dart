import 'dart:convert';

import 'package:cashless/models/QRResults.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/pages/fragments/drawer_fragment.dart';
import 'package:cashless/util/QRGenerator.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'dynamicQR.dart';

class Order extends KFDrawerContent {
  Order({Key key}) : super();

  @override
  _OrderState createState() => _OrderState();
}

class _OrderState extends State<Order> {
  final utility = QRGenerator();
  TextEditingController _idController = TextEditingController();
  TextEditingController _amountController = TextEditingController();
  TextEditingController _description = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _enabled = true;
  int loginId;

  void scanQR(BuildContext context) async {
    print('Scan QR');
    QRResults result;
    QRGenerator.scan().then((value) {
      result = value as QRResults;
      Map _userInfo = jsonDecode(result.content);
      setState(() {
        _enabled = false;
        _idController.text = _userInfo['id'];
      });
    });
  }

  void generateQR(BuildContext context) async {
    print('generate QR');
    Map<String, dynamic> _purchaseinfo = {};
    if (_formKey.currentState.validate()) {
      await Util.loadFromSharedPreferences(Util.USER_ID).then((result) {
        _purchaseinfo['userId'] = result;
      });
      _purchaseinfo['memberId'] = _idController.text;
      _purchaseinfo['amount'] = _amountController.text;
      _purchaseinfo['createdTime'] =new DateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss").format(DateTime.now());
      _purchaseinfo['transferTypeId'] = "9918";
      _purchaseinfo['ReferenceTransactionId'] = Util.generateUUID();
      _purchaseinfo['QRCodeType'] = "purchase";
      _purchaseinfo['description'] = _description.text;
      print(_purchaseinfo['createdTime']);
      print(_purchaseinfo);
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => DynamicQR(_purchaseinfo)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // drawer: DrawerFragment(),
      appBar: ActionBar(context, icon: Icons.arrow_back_ios,pressFunc:  () => Navigator.pop(context)),
        body: Container(
          child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                      child:Form(
                        key: _formKey,
                        child: Container(
                            child: Container(
                              padding: const EdgeInsets.symmetric(vertical: 20.0),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                                   child: Center(
                                        child: Text('Add Order',
                                          style: TextStyle(fontSize: 30,color: Colors.black54,),
                                        )),
                                   ),
                                  Container(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 20),
                                      child: CustomWidgetBuilder.buildRaisedButton(context, scanQR, null, 'Scan QR Code', icon: Icons.scanner, size: MainAxisSize.max),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 10),
                                    child: CustomWidgetBuilder.buildFormInputText(controller: _idController, hint: 'User Id', errorMessage: 'User Id is required.', isEnabled: _enabled,),
                                  ),
                                  Padding(
                                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                                      child: CustomWidgetBuilder.buildFormInputText(controller:_description, hint: 'Description', errorMessage: 'Description is required.')
                                  ),
                                  Padding(
                                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                                      child: CustomWidgetBuilder.buildFormInputText(controller: _amountController, hint: 'Amount', errorMessage: 'Amount is required.', inputType: TextInputType.number,
                                      )),
                                  Container(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 16.0, horizontal: 20),
                                      child: CustomWidgetBuilder.buildRaisedButton(
                                          context,
                                          generateQR,
                                          null,
                                          'generate QR code',
                                          size: MainAxisSize.max),
                                    ),
                                  ),
                                ],
                              ),
                            )),
                      )
                  ),
                ],
              ),
            ),
          ),
          );
  }
}
