import 'dart:convert';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/ContactService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/pages/home/home.dart';
import 'package:cashless/util/Util.dart';
import 'package:flutter/cupertino.dart';
import 'package:cashless/widgets/custom_widgets/CustomAppBar.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:xml2json/xml2json.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cashless/widgets/custom_widgets/CustomTextFormField.dart';

class ChangePassword extends StatefulWidget {
  ChangePassword({Key? key}) : super();

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
} //end of class

class _ChangePasswordState extends State<ChangePassword> {
  final _formKey = GlobalKey<FormState>();
  late User user;
  late Size size;
  ContactService _contactsService = ContactService();
  TextEditingController oldPassword = new TextEditingController();
  TextEditingController newPassword = new TextEditingController();
  TextEditingController confirmNewPassword = new TextEditingController();
  final Xml2Json xml2Json = Xml2Json();

  void backToHome(BuildContext context)async{
    Navigator.pop(context);
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (c) => ConsumerHome()), (route) => false);
  }
  void clearFormData(){
    oldPassword.clear();
    newPassword.clear();
    confirmNewPassword.clear();
  }

  void resetPassword(BuildContext context) async {
    if (_formKey.currentState!.validate()) {
      if (newPassword.text != confirmNewPassword.text) {
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text(getTranslated(context, "reset_password_no_matching"))));
      } else if (oldPassword.text != newPassword.text) {
        _formKey.currentState!.save();
        if (newPassword.text.length <= 3) {
          ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(content: Text(getTranslated(context, "reset_password_length_error"))));
        }
        String? username = await Util.loadFromSharedPreferences(Util.USERNAME);
        Response response = await _contactsService.restPassword(SOAP_USERNAME,
            SOAP_PASSWORD, username!, oldPassword.text, newPassword.text);
        xml2Json.parse(response.body);
        var jsonString = xml2Json.toParker();
        var data = jsonDecode(jsonString);
        if (response.statusCode == 200) {
          if (data['soap:Envelope']['soap:Body']
          ['ns2:changeCredentialsResponse']['return'] ==
              'SUCCESS') {
            Util.updateAuthorization(username, newPassword.text);
            clearFormData();
            buildConfirmationPopUp();
          } else {
            if (data['soap:Envelope']['soap:Body']
            ['ns2:changeCredentialsResponse']['return'] ==
                'INVALID_CREDENTIALS')
              Scaffold.of(context)
                  .showSnackBar(SnackBar(content: Text(getTranslated(context,'reset_password_invalid_password'))));
            else if (data['soap:Envelope']['soap:Body']
            ['ns2:changeCredentialsResponse']['return'] ==
                'BLOCKED_CREDENTIALS')
              Scaffold.of(context).showSnackBar(SnackBar(
                  content:
                  Text(getTranslated(context, "reset_password_blocked_credential"))));
            else
              Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text(data['soap:Envelope']['soap:Body']
                  ['ns2:changeCredentialsResponse']['return'])));
          }
        } else {
          Scaffold.of(context)
              .showSnackBar(SnackBar(content: Text(getTranslated(context, "reset_password_try_again"))));
        }
      } else {
        Scaffold.of(context).showSnackBar(SnackBar(content: Text(getTranslated(context, "reset_password_no_change"))));
      }
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(getTranslated(context, "registeration_snack_error"))));
    }
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: BACKGROUND_COLOR,
        appBar: CustomAppBar(
            title: getTranslated(context, 'change_password_title')
        ),
        body: ListView(
          children: <Widget>[
            buildTopCard(),
            Builder(
              builder: (BuildContext context) {
                return Container(
                  padding: EdgeInsets.symmetric(vertical: 20,horizontal: 10),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 4,),
                        CustomTextFormField(
                          focusBorderColor: SECONDARY_COLOR,
                          label: getTranslated(context, 'current_password'),
                          errorMessage:
                          getTranslated(context, 'reset_password_current_password_error_message'),
                          isPassword: true,
                          addShowBtn: true,
                          controller: oldPassword,
                        ),
                        SizedBox(height: 28,),
                        CustomTextFormField(
                          focusBorderColor: SECONDARY_COLOR,
                          label: getTranslated(context, 'reset_password_new_password_hint'),
                          errorMessage:
                          getTranslated(context, 'reset_password_new_password_error_message'),
                          isPassword: true,
                          addShowBtn: true,
                          controller: newPassword,
                        ),
                        SizedBox(height: 28,),
                        CustomTextFormField(
                          focusBorderColor: SECONDARY_COLOR,
                          label: getTranslated(context, "reset_password_confirm_password_hint"),
                          errorMessage:
                          getTranslated(context, 'reset_password_confirm_password_error_message'),
                          isPassword: true,
                          addShowBtn: true,
                          controller: confirmNewPassword,
                        ),
                        SizedBox(height: 28,),
                        CustomWidgetBuilder.buildRaisedButton(
                            context, resetPassword, null, getTranslated(context, "reset_password_btn"),
                            size: MainAxisSize.max,color: SECONDARY_COLOR,
                            fontColor:  Colors.white,
                            fontWeight: FontWeight.w700,
                            fontStyle:  FontStyle.normal,
                            fontSize: 15),
                      ],
                    ),
                  ),
                );
              },
            ),
          ],
        ));
  }

  Widget buildTopCard() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: [
            Image.asset('lib/assets/my_qr_code_graphics.png'),
            SizedBox(width: 11,),
            Flexible(
              child: RichText(
                overflow: TextOverflow.clip,
                  text: TextSpan(
                      children: [
                        TextSpan(
                            style: GoogleFonts.lato(
                                color:  const Color(0xff2d3859),
                                fontWeight: FontWeight.w700,
                                fontStyle:  FontStyle.normal,
                                fontSize: 11.9
                            ),
                            text: getTranslated(context,'change_password_congratolations')),
                        TextSpan(
                            style: GoogleFonts.lato(
                                color:  const Color(0xff2d3859),
                                fontWeight: FontWeight.w400,
                                fontStyle:  FontStyle.normal,
                                fontSize: 11.9
                            ),
                            text:getTranslated(context,'change_password_congratolations_message'))
                      ]
                  )
              ),
            ),
          ],
        ),
      ),
    );
  }

  void buildConfirmationPopUp() {
    showModalBottomSheet(
        context: context,
        builder: (context) => Container(

      decoration: BoxDecoration(
          boxShadow: [BoxShadow(
              color: GRAY_BOXBORDER_COLOR,
              offset: Offset(0,2),
              blurRadius: 4,
              spreadRadius: 0
          )] ,
          color: BACKGROUND_COLOR
      ),
      child: SingleChildScrollView(
        padding: const EdgeInsets.only(left: 10,right: 10),
        child: Column(
          children: [
            SizedBox(height: 10,),
            Image.asset('lib/assets/more_change_successfully.png'),
            SizedBox(height: 10,),
            Text(
                getTranslated(context, 'change_password_successfully_title'),
                style: GoogleFonts.lato(
                    color:  PRIMARY_COLOR,
                    fontWeight: FontWeight.w700,
                    fontStyle:  FontStyle.normal,
                    fontSize: 13.6
                ),
                textAlign: TextAlign.center
            ),
            SizedBox(height: 6,),
            Text(
                getTranslated(context, 'change_password_successfully_message'),
                style: GoogleFonts.lato(
                    color:  CASHCALL_TITLE,
                    fontWeight: FontWeight.w400,
                    fontStyle:  FontStyle.normal,
                    fontSize: 11.9
                ),
                textAlign: TextAlign.center
            ),
            SizedBox(height: 15.3,),
            Text(
                getTranslated(context, 'transaction_summary_tanks_message'),
                style: GoogleFonts.lato(
                    color:  PRIMARY_COLOR,
                    fontWeight: FontWeight.w400,
                    fontStyle:  FontStyle.normal,
                    fontSize: 11.9
                ),
                textAlign: TextAlign.center
            ),
            SizedBox(height: 20.7,),
            CustomWidgetBuilder.buildRaisedButton(
                context, backToHome, null, getTranslated(context, "home_done_btn"),
                size: MainAxisSize.max,color: SECONDARY_COLOR,
                fontColor:  Colors.white,
                fontWeight: FontWeight.w400,
                fontStyle:  FontStyle.normal,
                fontSize: 15),
            SizedBox(height: 20,),
          ],
        ),

      )

    ),
    );
  }
} //end of class
