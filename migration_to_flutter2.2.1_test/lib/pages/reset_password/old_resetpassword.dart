import 'dart:convert';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/ContactService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:xml2json/xml2json.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';

import '../fragments/action_bar.dart';

class ResetPassword extends KFDrawerContent {
  ResetPassword({Key? key}) : super();

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
} //end of class

class _ResetPasswordState extends State<ResetPassword> {
  final _formKey = GlobalKey<FormState>();
  late User user;
  ContactService _contactsService = ContactService();
  TextEditingController oldPassword = new TextEditingController();
  TextEditingController newPassword = new TextEditingController();
  TextEditingController confirmNewPassword = new TextEditingController();
  final Xml2Json xml2Json = Xml2Json();

  void resetPassword(BuildContext context) async {
    if (_formKey.currentState!.validate()) {
      if (newPassword.text != confirmNewPassword.text) {
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text(getTranslated(context, "reset_password_no_matching"))));
      } else if (oldPassword.text != newPassword.text) {
        _formKey.currentState!.save();
        if (newPassword.text.length <= 3) {
          Scaffold.of(context).showSnackBar(
              SnackBar(content: Text(getTranslated(context, "reset_password_length_error"))));
        }
        String? username = await Util.loadFromSharedPreferences(Util.USERNAME);
        Response response = await _contactsService.restPassword(SOAP_USERNAME,
            SOAP_PASSWORD, username!, oldPassword.text, newPassword.text);
        xml2Json.parse(response.body);
        var jsonString = xml2Json.toParker();
        var data = jsonDecode(jsonString);
        if (response.statusCode == 200) {
          if (data['soap:Envelope']['soap:Body']
                  ['ns2:changeCredentialsResponse']['return'] ==
              'SUCCESS') {
            Scaffold.of(context).showSnackBar(
                SnackBar(content: Text(getTranslated(context, "reset_password_successful_reset"))));
            Util.updateAuthorization(username, newPassword.text);
            Navigator.pop(context);
          } else {
            if (data['soap:Envelope']['soap:Body']
                    ['ns2:changeCredentialsResponse']['return'] ==
                'INVALID_CREDENTIALS')
              ScaffoldMessenger.of(context)
                  .showSnackBar(SnackBar(content: Text('Wrong password')));
            else if (data['soap:Envelope']['soap:Body']
                    ['ns2:changeCredentialsResponse']['return'] ==
                'BLOCKED_CREDENTIALS')
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content:
                      Text(getTranslated(context, "reset_password_blocked_credential"))));
            else
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text(data['soap:Envelope']['soap:Body']
                      ['ns2:changeCredentialsResponse']['return'])));
          }
        } else {
          Scaffold.of(context)
              .showSnackBar(SnackBar(content: Text(getTranslated(context, "reset_password_try_again"))));
        }
      } else {
        Scaffold.of(context).showSnackBar(SnackBar(content: Text(getTranslated(context, "reset_password_no_change"))));
      }
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(getTranslated(context, "registeration_snack_error"))));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ActionBar(context,
            icon: Icons.arrow_back_ios,
            pressFunc: () => Navigator.pop(context)),
        body: ListView(
          children: <Widget>[
            Builder(
              builder: (BuildContext context) {
                return Container(
                  margin: EdgeInsets.all(15.0),
                  padding: EdgeInsets.all(10.0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        CustomWidgetBuilder.buildText(getTranslated(context, "reset_password_title"),
                            fontSize: 23),
                        CustomWidgetBuilder.buildFormInputText(
                            controller: oldPassword,
                            isPassword: true,
                            hint: getTranslated(context, "reset_password_old_password_hint"),
                            errorMessage: getTranslated(context, "old password is required.")),
                        CustomWidgetBuilder.buildFormInputText(
                            controller: newPassword,
                            hint: getTranslated(context, "reset_password_new_password_hint"),
                            errorMessage: getTranslated(context, "reset_password_new_password_error_message"),
                            isPassword: true),
                        CustomWidgetBuilder.buildFormInputText(
                            controller: confirmNewPassword,
                            hint: getTranslated(context, "reset_password_confirm_password_hint"),
                            errorMessage: getTranslated(context, "reset_password_confirm_password_error_message"),
                            valueResult: newPassword.text,
                            isPassword: true),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: CustomWidgetBuilder.buildRaisedButton(
                              context, resetPassword, null, getTranslated(context, "reset_password_reset_btn"),
                              size: MainAxisSize.max),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ],
        ));
  } //end of widget,Scaffold
} //end of class
