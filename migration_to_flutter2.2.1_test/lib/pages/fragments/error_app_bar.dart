import 'package:cashless/Configurations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ErrorAppBar extends PreferredSize {
  final double height;


  ErrorAppBar(BuildContext context,{this.height = 56,}) : super(child: AppBar(), preferredSize: Size.fromHeight(height));

  @override
  Size get preferredSize => Size.fromHeight(height);

  Widget build(BuildContext context) {
    return AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: PRIMARY_COLOR,
        // padding: const EdgeInsets.fromLTRB(12, 16, 0, 16),
        title: Image.asset('lib/assets/navbarMainLogo.png'),
      centerTitle: true,
    );
  }
}
