import 'package:cashless/Configurations.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/main.dart';
import 'package:cashless/pages/notification/OldListNotifications.dart';
import 'package:cashless/util/Util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ActionBar extends PreferredSize {
   late final double height;
   static int counter = 0;
   late IconData btnIcon, btnIcon2, btnIcon3,btnIcon4;
   late VoidCallback onPress, onPress3;
   late Function onPress2, onPress4;
   Function? _setLang;
   late String title;
   late BuildContext _context;
   late bool sLang;

  ActionBar(BuildContext context,{this.height = kToolbarHeight, titleText, icon,
            pressFunc, icon2, pressFunc2, icon3, pressFunc3,icon4, pressFunc4,
            setLang,sLang = false})
            :super(child: ActionBar(context, height: height), preferredSize: Size.fromHeight(height) ) {
    btnIcon = icon;
    onPress = pressFunc;
    btnIcon2 = icon2;
    onPress2 = pressFunc2;
    btnIcon3 = icon3;
    onPress3 = pressFunc3;
    title = titleText;
    onPress4 = pressFunc4;
    btnIcon4 = icon4;
    _context = context;
    this.sLang = sLang;
    if(setLang!=null) {
      _setLang = setLang(context);
    }
  }

  @override
  Size get preferredSize => Size.fromHeight(height);

  void _changeLang(BuildContext context,Language language) {
    Locale _temp;
    switch(language.languageCode){
      case 'en':
        _temp = Locale(language.languageCode, 'US');
        break;
      case 'ar':
        _temp = Locale(language.languageCode, 'EG');
        break;
      default:
        _temp = Locale(language.languageCode, 'US');
    }
    Util.saveTOSharedPreferences(Util.LANGUAGE,language.languageCode);
    MyApp.setLocale(context, _temp);
    if(_setLang !=null){
      _setLang!(context);
    }
  }

  Widget build(BuildContext context) {
    return AppBar(
        elevation: 0,
        backgroundColor: MINOR_COLOR,
        title: title != null? Text(title, style: TextStyle(fontSize: 16)):Container(),
        centerTitle: true,
        leading: Container(
            margin: EdgeInsets.fromLTRB(10,7,7,7),
            child: btnIcon!=null?
            MaterialButton(
              onPressed : onPress,
              textColor: MINOR_COLOR,
              color: Colors.white,
              child: Icon(btnIcon),
              padding: EdgeInsets.all(8),
              shape: CircleBorder(),
            ) :Container()),
        actions: <Widget>[
          sLang ? Padding(
            padding: const EdgeInsets.all(8.0),
            child: DropdownButton(icon: Icon(Icons.language, color: Colors.white), underline: SizedBox(),
              items: Language.LanguageList().map<DropdownMenuItem<Language>>((lang)=>
                  DropdownMenuItem(
                      value: lang,
                      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [Text(lang.name, style: TextStyle(fontSize: 20)), Text(lang.flag)
                          ])
                  )).toList(),
              onChanged: (Language? language){
                _changeLang(context,language!);
              },
            ),
          ):Container(),
          btnIcon3!=null?new Stack(
            children: <Widget>[
              new IconButton(icon: Icon(Icons.notifications),splashRadius: 25, onPressed:() {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            OldListNotifications()));
              }),
              counter != 0 ? new Positioned(
                right: 11,
                top: 11,
                child: new Container(
                  padding: EdgeInsets.all(2),
                  decoration: new BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  constraints: BoxConstraints(
                    minWidth: 14,
                    minHeight: 14,
                  ),
                  child: Text(
                    '$counter',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 8,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ) : Container()
            ],
          ):Container(),
          btnIcon3!=null?IconButton(icon: Icon(btnIcon3), splashRadius: 25, onPressed: onPress3):Container(),
          btnIcon4!=null?IconButton(icon: Icon(btnIcon4),splashRadius: 25, onPressed: (){onPress4(_context);}):Container(),
        ]
    );
  }
}
