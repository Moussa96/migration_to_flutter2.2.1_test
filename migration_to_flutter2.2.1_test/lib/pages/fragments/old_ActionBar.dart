import 'package:cashless/util/Util.dart';
import 'package:flutter/material.dart';

class oldActionBar extends PreferredSize {
  //final Widget child;
  final double height;

  oldActionBar({this.height = kToolbarHeight}) : super(child: oldActionBar(height: height), preferredSize: Size.fromHeight(height));

  @override
  Size get preferredSize => Size.fromHeight(height);

  Widget build(BuildContext context) {
    return AppBar(
      leading: IconButton(
        icon: Icon(Icons.arrow_back_ios),
        color: Colors.white,
        onPressed: () => Navigator.pop(context),
      ),
      // title: Text('New Giza Community'),
      actions: <Widget>[

//        IconButton(
//          icon: const Icon(Icons.receipt),
//          color: Colors.white,
//          tooltip: 'Invoices',
//          onPressed: () {
//            //scaffoldKey.currentState.showSnackBar(snackBar);
//            Navigator.pushReplacementNamed(context, '/invoices');
//          },
//        ),
//        IconButton(
//          icon: const Icon(Icons.contacts),
//          color: Colors.white,
//          tooltip: 'Transactions History',
//          onPressed: () {
//            //scaffoldKey.currentState.showSnackBar(snackBar);
//            Navigator.pushReplacementNamed(context, '/contacts');
//          },
//          ),
        IconButton(
          icon: const Icon(Icons.exit_to_app),
          color: Colors.white,
          tooltip: 'Transactions History',
          onPressed: () {
            Util.clear();
            Navigator.pushReplacementNamed(context, '/');
          },
        ),
      ],
    );
  }
}