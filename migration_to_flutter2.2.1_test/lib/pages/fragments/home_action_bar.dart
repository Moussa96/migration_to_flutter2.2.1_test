import 'package:cashless/Configurations.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/util/Util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeActionBar extends PreferredSize {
  final double height;
  static int counter = 0;


  HomeActionBar(BuildContext context,{this.height = 56,}) : super(child: ActionBar(context), preferredSize: Size.fromHeight(height));

  @override
  Size get preferredSize => Size.fromHeight(height);

  Widget build(BuildContext context) {
    return AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: PRIMARY_COLOR,
        // padding: const EdgeInsets.fromLTRB(12, 16, 0, 16),
        title: Image.asset('lib/assets/navbarMainLogo.png'),
        actions: <Widget>[
              new Stack(children:[InkWell(
                onTap: () {
                  Util.listNotifications(context);
                },
                child: Container(
                  padding: EdgeInsets.only(top: 11,right: 11,left: 11),
                  child: ClipRRect(
                    child: Image.asset('lib/assets/notificationsIc.png',),
                  ),),
              ),
                counter != 0 ? new Positioned(
                  right: 11,
                  top: 11,
                  child: new Container(
                    padding: EdgeInsets.all(2),
                    decoration: new BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    constraints: BoxConstraints(
                      minWidth: 14,
                      minHeight: 14,
                    ),
                    child: Text(
                      '$counter',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ) : Positioned(
                  right: 12,
                  top: 11,
                  child: Container(
                      width: 7,
                      height: 7,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(6),
                          boxShadow: [BoxShadow(
                              color: const Color(0x80000000),
                              offset: Offset(0,1),
                              blurRadius: 2,
                              spreadRadius: 0
                          )] ,
                          color: APPBAR_MAIN_COLOR
                      )
                  ),
                )
              ]),
        ]
    );
  }
}
