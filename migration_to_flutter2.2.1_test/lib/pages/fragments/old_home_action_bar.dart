import 'package:cashless/util/Util.dart';
import 'package:flutter/material.dart';

class HomeActionBar extends PreferredSize {
  final double height;

  HomeActionBar({this.height = kToolbarHeight}) : super(child: HomeActionBar(height: height), preferredSize: Size.fromHeight(height));

  @override
  Size get preferredSize => Size.fromHeight(height);

  Widget build(BuildContext context) {
    return AppBar(
      leading: IconButton(
        icon: Icon(Icons.menu),
        color: Colors.white,
        onPressed: () => Scaffold.of(context).openDrawer(),

      ),
      //leading: SettingsDropDown(),
      // title: Text('New Giza Community'),
      actions: <Widget>[
        IconButton(
          icon: const Icon(Icons.exit_to_app),
          color: Colors.white,
          tooltip: 'Transactions History',
          onPressed: () {
            Util.clear();
            Navigator.pushReplacementNamed(context, '/');
          },
        ),
      ],
    );
  }
}
