import 'package:cashless/apis/Services/AccountService.dart';
import 'package:cashless/models/Balance.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/Configurations.dart';
import 'package:flutter/material.dart';

class DrawerFragment extends StatefulWidget {
  DrawerFragment({Key? key}) : super(key: key);

  @override
  _DrawerFragmentState createState() => _DrawerFragmentState();
}

class _DrawerFragmentState extends State<DrawerFragment> {
  String name = 'Cashless';
  AccountService _accountUseCase = AccountService();
  Balance _balance = Balance();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
      setState(() {
        Util.loadFromSharedPreferences(Util.NAME).then((result) {
          setState(() {
            name = result!;
          });
        });
        _accountUseCase.listAccounts().then((value) => {
          setState(() {
            if(value.data != null)
              _balance = Util.getBalanceInfo(value.data!);
          })
        });

      });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: MAJOR_COLOR,
            ),
            child: Column(
              children: <Widget>[
                Image.asset('lib/assets/logo.png'),
                Text(
                  name,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical:2.0),
                  child: Text(
                    "Balance is : "+_balance.balance.toString()+' '+_balance.currency,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 10,
                    ),
                  ),
                ),
/*                Text(
                  "available Balance is : "+_balance.balance.toString()+' '+_balance.currency,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 10,
                  ),
                ),*/
              ],
            ),
          ),
          /*ListTile(
            leading: Icon(Icons.account_balance),
            title: Text('Transactions'),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/home');
            },
          ),*/
          /*ListTile(
            leading: Icon(Icons.payment),
            title: Text('Payment'),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/payment');
            },
          ),*/
//          ListTile(
//            leading: Icon(Icons.receipt),
//            title: Text('Invoices'),
//            onTap: () {
//              Navigator.pushReplacementNamed(context, '/invoices');
//            },
//          ),
          /*ListTile(
            leading: Icon(Icons.contacts),
            title: Text('Contacts'),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/contacts');
            },
          ),*/
//          ListTile(
//            leading: Icon(Icons.account_balance_wallet),
//            title: Text('Load Wallet'),
//            onTap: () {
//              Navigator.pushReplacementNamed(context, '/load_wallet');
//            },
//          ),
          ListTile(
            leading: Icon(Icons.account_circle),
            title: Text('Profile'),
            onTap: () {
              Navigator.pushNamed(context, '/profile');
            },
          ),
       /*   _agent ?
          ListTile(
            leading: Icon(Icons.group_add),
            title: Text('Register'),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/register');
            },
          ):Container(),
           _agent ? ListTile(
            leading: Icon(Icons.transform),
            title: Text('Cash IN'),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/transferBalance');
            },
          ):Container(

           ),
          ! _agent ? ListTile(
            leading: Icon(Icons.account_balance),
            title: Text('Purchase'),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/transferBalance');
            },
          ) : Container(

          ),
          ! _agent ? ListTile(
            leading: Icon(Icons.payment),
            title: Text('Scan QR'),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/scan');
            },
          ) : Container(

          ),*/

        ],
      ),
    );
  }
}
