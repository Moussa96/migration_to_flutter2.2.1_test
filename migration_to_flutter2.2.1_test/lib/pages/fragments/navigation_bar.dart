import 'package:cashless/Configurations.dart';
import 'package:cashless/pages/Settings/Settings.dart';
import 'package:cashless/pages/marketplace/market_place.dart';
import 'package:cashless/pages/transaction_history/transaction_history.dart';
import 'package:cashless/widgets/custom_widgets/HomeCustomWidget.dart';
import 'package:cashless/pages/home/home.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';

class NavigationBar extends StatefulWidget {
  int? activeItem;
  NavigationBar({Key? key,this.activeItem}) : super();

  @override
  _NavigationBarState createState() => _NavigationBarState();
}

class _NavigationBarState extends State<NavigationBar> {
  late Size size;
  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Container(
      height: 0.059 * size.height,
      width: size.width,
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
            color: NAVBAR_COLOR,
            offset: Offset(0, 2),
            blurRadius: 4,
            spreadRadius: 0)
      ], color: Colors.white),
      child: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          // base
          Positioned(
            top: -size.height*0.005,
            left: 0.4 * size.width,
            child: Center(
              child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                          Radius.circular(3.408)
                      ),
                      boxShadow: [BoxShadow(
                          color: GRAY_BOXBORDER_COLOR,
                          offset: Offset(0,2),
                          blurRadius: 3,
                          spreadRadius: 0
                      )] ,
                      gradient: LinearGradient(
                          begin: Alignment(0.5, 0),
                          end: Alignment(0.5, 1.3927819690648153),
                          colors: [SECOND_COLOR, PAY_NOW_COLOR])
                  ),
                child: InkWell(
                    onTap: () {
                      HomeCustomWidget.buildPayPopUp(context);
                    },
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: size.width*0.044,vertical: size.height*0.011),
                      child: Column(children: [
                        Image.asset(
                            'lib/assets/tabbar_pay_with_bg.png'),
                        Text(getTranslated(context,'pay'),style: GoogleFonts.lato(color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontStyle:  FontStyle.normal,
                            fontSize: 10.2),)
                      ],),
                    )
                ),
              ),
            ),
          ),
          Container(
            width: size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                buildNavbarItem(
                    label: getTranslated(context,'navbar_home'),
                    path: widget.activeItem == 1 ?'lib/assets/tabbar_home_ic.png':'lib/assets/tabbar_home_inactive_ic.png',
                    router: widget.activeItem==1 ?null:ConsumerHome(),
                    active: widget.activeItem==1),
                buildNavbarItem(
                    label: getTranslated(context,'navbar_stores'),
                    path: widget.activeItem==2?'lib/assets/tabbar_stores_active_ic.png':'lib/assets/tabbar_stores_ic.png',
                    router: widget.activeItem==2 ?null:MarketPlace(mContext: context,),
                    active :widget.activeItem==2,
                ),
                Container(
                  width: size.width * 0.2,
                ),
                buildNavbarItem(
                    label: getTranslated(context,'navbar_history'),
                    path: widget.activeItem == 3 ?'lib/assets/tabbar_history_active_ic.png':'lib/assets/tabbar_history_ic.png',
                    router: widget.activeItem==3 ?null:TransactionHistory(),
                    active :widget.activeItem==3,
                ),
                buildNavbarItem(
                    label: getTranslated(context,'mavbar_more'),
                    path: widget.activeItem == 4 ?'lib/assets/tabbar_more_active_ic.png':'lib/assets/tabbar_more_ic.png',
                    router: widget.activeItem==4 ?null:Settings(),
                    active :widget.activeItem==4,),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget buildNavbarItem(
      {required String label, required String path, Widget? router, bool active: false}) {
    return new InkWell(
        onTap: router != null
            ? () {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => router));
              }
            : null,
        child: Container(
          decoration: active
              ? BoxDecoration(
                  border: Border(
                    bottom: BorderSide(width: 1.0, color: PRIMARY_COLOR),
                  ),
                )
              : null,
          padding: EdgeInsets.only(top: size.height*0.011),
          child: Column(
            children: [
              Container(
                  child: Image.asset(path)),
              Text(
                label,
                style: active
                    ? GoogleFonts.lato(fontSize:9.4,color: PRIMARY_COLOR)
                    : GoogleFonts.lato(fontSize:9.4,color: PALE_GRAY),
              ),
            ],
          ),
        ));
  }
}
