import 'package:flutter/material.dart';

class SearchBar {
  Widget Build(BuildContext context){
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
      child: Column(
        children:[
          TextField(
            decoration: InputDecoration(hintText: "Search merchants..",
            suffixIcon: Icon(Icons.search),
          border: OutlineInputBorder()),
          )]
        )
    );
  }
}
