import 'dart:collection';
import 'dart:convert';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/MfsRestClientImpl.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/Mfs/MfsScenarioConfiguration.dart';
import 'package:cashless/models/Mfs/MfsTrxParams.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/requests/MfsServiceCalculatorRequest.dart';
import 'package:cashless/models/response/MfsServiceCalculatorResponse.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/pages/transaction_summary/old_transaction_summary.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MfsConfirmPayment extends KFDrawerContent {
  MfsScenarioConfiguration scenarioConfiguration;
  MfsServiceCalculatorResponse response;
  String account;
  String accountLabel;
  String serviceName;
  String serviceNameAr;
  MfsServiceCalculatorRequest serviceCalcRequest = MfsServiceCalculatorRequest();
  MfsConfirmPayment({Key key, this.scenarioConfiguration, this.response, this.account, this.accountLabel, this.serviceName, this.serviceNameAr, this.serviceCalcRequest}) : super();

  @override
  _MfsConfirmPaymentState createState() => _MfsConfirmPaymentState();
}

class _MfsConfirmPaymentState extends State<MfsConfirmPayment> {
  bool _checked = false;
  String paymentType;
  List<MfsTrxParams> trxParams = List();
  int _amountIndex = 0;
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  MfsRestClientImpl _mfsRestService = MfsRestClientImpl();
  TextEditingController _amountController= TextEditingController();
  var rechargeAmount;
  var dueAmount;


  @override
  void initState(){
    paymentType = widget.scenarioConfiguration.pay_type !=null?
      widget.scenarioConfiguration.pay_type:MFS_PAY_TYPE_EXACT;
    rechargeAmount = widget.response.input_transaction_value;
    dueAmount = widget.response.receipt_Deducted_Amount;
    trxParams = widget.scenarioConfiguration.trx_params;
    if(trxParams!=null) {
      for (int i = 0; i < trxParams.length; i++) {
        if (trxParams[i].json_id == 'transaction_value') {
          _amountIndex = 0;
        }
      }
    }
  }

  showAlertDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
        content: new Row(children: [
          CircularProgressIndicator(backgroundColor: Colors.blue),
          Container(
              margin: EdgeInsets.symmetric(horizontal: 5),
              child: Text(getTranslated(context, "loading"))
          ),
        ])
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  bool isNumeric(String s) {
    if(s == null) {
      return false;
    }
    return double.parse(s, (e) => null) != null;
  }

  void changePaidAmount(BuildContext context){
    if(_formKey.currentState.validate()){
      if(paymentType == MFS_PAY_TYPE_DOWN_PARTIAL && (double.parse(_amountController.text) >= double.parse(widget.response.receipt_Deducted_Amount))){
        CustomWidgetBuilder.buildSnackBar(scaffoldKey, context, getTranslated(context, 'mfs_confirm_service_err_less'));
      }
      else if(paymentType == MFS_PAY_TYPE_UP_PARTIAL && (double.parse(_amountController.text) <= double.parse(widget.response.receipt_Deducted_Amount))){
        CustomWidgetBuilder.buildSnackBar(scaffoldKey, context, getTranslated(context, 'mfs_confirm_service_err_more'));
      }
      else{
        showAlertDialog(context);
        MfsServiceCalculatorRequest newAmountServiceCalcRequest = widget.serviceCalcRequest;
        newAmountServiceCalcRequest.transactionValue = _amountController.text;
        _mfsRestService.confirmServiceCalculator(newAmountServiceCalcRequest).then((res){

          if(res.statusCode == 200) {
            Map<dynamic,dynamic> serviceCalcOutParams =
            jsonDecode(res.data.out_parameters);
            var outParams = jsonDecode(res.data.out_parameters);
            if(outParams != null && outParams['OUT_PARAMETER_1'] != 0 &&outParams['OUT_PARAMETER_1'] != null && outParams['OUT_PARAMETER_1'] != "" && outParams['OUT_PARAMETER_1'] != " "&& outParams['OUT_PARAMETER_1'] != "0"&& outParams['OUT_PARAMETER_1'] != "0.0"){
              CustomWidgetBuilder.buildSnackBar(
                  scaffoldKey,
                  context, "There is no due bill");
            }
            else if(res.data.status_code == '0') {
              print(res.data.toJson());
              setState(() {
                dueAmount = res.data.receipt_Deducted_Amount;
                rechargeAmount = res.data.input_transaction_value;
              });
              Navigator.pop(context);
              _amountController.clear();
            }
            else{
              CustomWidgetBuilder.buildSnackBar(
                  scaffoldKey,
                  context,
                  serviceCalcOutParams != null &&
                  serviceCalcOutParams['Message']!=null && serviceCalcOutParams['Message'] !=" "?
                  serviceCalcOutParams['Message']
                  : res.data.user_a_msg);
              Navigator.pop(context);
            }
          }
          else{
            CustomWidgetBuilder.buildSnackBar(scaffoldKey, context, getTranslated(context, res.getErrorCode));
            Navigator.pop(context);
          }
        });
      }
    }
    else{
      CustomWidgetBuilder.buildSnackBar(scaffoldKey, context, getTranslated(context, "mfs_confirm_service_err_invalid"));
    }
  }

  confirmPayment(BuildContext context){
    showAlertDialog(context);
    MfsServiceCalculatorRequest transactionRequest = widget.serviceCalcRequest;
    print('transaction request');
    print(transactionRequest.toJson());

    PaymentService paymentService = PaymentService();
    CustomValue transactionRequestCV = CustomValue("mfs_transcation_request", jsonEncode(transactionRequest));
    CustomValue transactionResponseCV = CustomValue("mfs_transcation_response", "mfs_transcation_response");
    CustomValue transactionIdCV = CustomValue("mfs_transcation_id", "mfs_transcation_id");
    CustomValue serviceNameEnCV = CustomValue(EXTERNAL_SERVICE_NAME_EN, widget.serviceName);
    CustomValue serviceNameArCV = CustomValue(EXTERNAL_SERVICE_NAME_AR, widget.serviceNameAr);
    // CustomValue accessTokenCV = CustomValue("access_token", "access_token");
    List<CustomValue> customValues = List<CustomValue>();
    customValues.add(transactionRequestCV);
    customValues.add(transactionResponseCV);
    customValues.add(transactionIdCV);
    customValues.add(serviceNameEnCV);
    customValues.add(serviceNameArCV);
    // customValues.add(accessTokenCV);
    Payment payment = Payment(
      transferTypeCode: PURCHASE_CASHCALL_SERVICES_TRANSFER_TYPE_CODE,
      amount: double.parse(dueAmount),
      customValues: customValues,
      // toMemberId: MFS_TO_MEMBER_ID
    );
    print('payment request');
    print(payment.toJson());
    paymentService.confirmMfsPayment(payment).then((res) {
      if(res.statusCode == 200) {
        Map<dynamic,dynamic> paymentRes =
        jsonDecode(res.data.customValues.mfs_transcation_response);
        print('payment response');
        print(res.data.toJson());
        print('mfs payment response');
        print(res.data.customValues.mfs_transcation_response);
        String state = jsonDecode(res.data.customValues.mfs_transcation_response)['statusCode'];
        if (!res.data.pending) {
          var outParams = jsonDecode(res.data.customValues.mfs_transcation_response)['outParameters'];
          Map<String, dynamic> summary = new HashMap();
          if(state == '0'){
            summary[getTranslated(context, "mfs_confirm_service_from")] = res.data.fromMemberName;
            summary[getTranslated(context, "mfs_confirm_service_amount")] = dueAmount.toString();
            summary[getTranslated(context, "mfs_confirm_service_name")] = widget.serviceName;
            if(widget.scenarioConfiguration.scenario_id == MFS_SCENARIO_VOUCHER) {
              summary[getTranslated(context, "mfs_confirm_service_voucher_pin")] = outParams['OUT_PARAMETER_2'];
              summary[getTranslated(context, "mfs_confirm_service_voucher_value")] = outParams['OUT_PARAMETER_3'];
              summary[getTranslated(context, "mfs_confirm_service_expiry_date")] = outParams['OUT_PARAMETER_4'];
            }
            Navigator.push(context, MaterialPageRoute(builder: (context) => TransactionSummary(summary)));
          }
          else{
            Navigator.pop(context);
            CustomWidgetBuilder.buildSnackBar(
                scaffoldKey,
                context,
                paymentRes['outParameters']!=null &&
                paymentRes['outParameters']['Message']!=null && paymentRes['outParameters']['Message'] !=" "?
                paymentRes['outParameters']['Message']
                :paymentRes['userAMsg']
            );
          }
        }
      }
      else{
        CustomWidgetBuilder.buildSnackBar(scaffoldKey, context, getTranslated(context, res.getErrorCode.toString()));
        Navigator.pop(context);
      }
     });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.white.withOpacity(0.9),
      resizeToAvoidBottomInset: false,
      appBar:
      ActionBar(context,
          titleText: getTranslated(context, 'mfs_confirm_service_title'),
          icon: Icons.arrow_back_ios,
          pressFunc: () => Navigator.pop(context)
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(widget.accountLabel, style: TextStyle(fontSize: 18, color: MINOR_COLOR)),
                Text(widget.account, style: TextStyle(fontSize: 18))
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Divider(),
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(getTranslated(context, "mfs_confirm_service_due_amount"), style: TextStyle(fontSize: 18, color: MINOR_COLOR)),
                Text(dueAmount, style: TextStyle(fontSize: 22))
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Divider(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(getTranslated(context, "mfs_confirm_service_amount"), style: TextStyle(fontSize: 18, color: MINOR_COLOR)),
                Text(rechargeAmount, style: TextStyle(fontSize: 22))
              ],
            ),

            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Divider(),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: CustomWidgetBuilder.buildRaisedButton(context, confirmPayment, null, getTranslated(context, "mfs_confirm_service_confirm_btn"), size: MainAxisSize.max),
            ),
            paymentType!=MFS_PAY_TYPE_EXACT?CheckboxListTile(
                title:
                Text(getTranslated(context, "mfs_confirm_service_change_amount")),
                activeColor: Colors.blue,
                value: _checked,
                onChanged: (val) {
                  setState(() {
                    _checked = val;
                  });
                }):Container(),
            _checked ? Container(
              margin: EdgeInsets.symmetric(horizontal: 15),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.blueAccent, width: 2),
                borderRadius: BorderRadius.all(Radius.circular(25.0)
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: Form(
                            key: _formKey,
                            child: TextFormField(
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintStyle: TextStyle(fontSize: 18),
                                hintText: Localization.of(context).locale.languageCode=='en'?trxParams[_amountIndex].label_en:trxParams[_amountIndex].label,
                              ),
                              style: TextStyle(fontSize: 18),
                              keyboardType: TextInputType.numberWithOptions(
                                decimal: true),
                              controller: _amountController,
                              validator: (value) {
                                if(!isNumeric(value)){
                                  return Localization.of(context).locale.languageCode=='en'?trxParams[_amountIndex].err_en:trxParams[_amountIndex].err;
                                }
                                if(double.parse(value) > double.parse(trxParams[_amountIndex].max.toString()) || double.parse(value) < double.parse(trxParams[_amountIndex].min.toString())){
                                  return Localization.of(context).locale.languageCode=='en'?trxParams[_amountIndex].err_en:trxParams[_amountIndex].err;
                                }
                                return null;
                              },
                            ),
                          ),
                        ),
                        CustomWidgetBuilder.buildRaisedButton(context, changePaidAmount, null, getTranslated(context, "mfs_confirm_service_change_btn"), size: MainAxisSize.min),
                      ],
                    ),
                    Text(Localization.of(context).locale.languageCode=='en'?trxParams[_amountIndex].helper_en:trxParams[_amountIndex].helper, style: TextStyle(color: Colors.grey)),
                  ],
                ),
              ),
            ): Container(),
          ],
        ),
      )
    );
  }
}