import 'dart:collection';
import 'dart:convert';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/apis/Services/PendingPaymentsService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/OrderItems.dart';
import 'package:cashless/models/Product.dart';
import 'package:cashless/models/response/transactionRes/GetTransHisResCustomElements.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/pages/product_details/old_products_details.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:xml2json/xml2json.dart';

class PendingPaymentRequests extends KFDrawerContent {
  PendingPaymentRequests({Key key}) : super();

  @override
  _PendingPaymentRequestsState createState() => _PendingPaymentRequestsState();
}

class _PendingPaymentRequestsState extends State<PendingPaymentRequests> {
  var _pendingPayments = [];
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  PendingPaymentsService pendingPaymentsService = new PendingPaymentsService();

  PaymentService paymentService = PaymentService();
  final Xml2Json xml2Json = Xml2Json();
  int _currentPage = 0;
  var memberType;
  bool _isLoading = true;
  bool _hasMore = true;

  @override
  void initState() {
    super.initState();
    _isLoading = true;
    _hasMore = true;
    Util.loadFromSharedPreferences(Util.GROUP).then((value) {
      memberType = value;
    });
    loadPendingPaymentsRequests();
  }

  loadPendingPaymentsRequests() async {
    _isLoading = true;
    List<GetTransHisResCustomElements> fetchedList;

    Map<String, dynamic> params = new HashMap<String, dynamic>();
    params.putIfAbsent("currentPage", () => _currentPage);

    await pendingPaymentsService.getPendingPayment(params).then((value) => {
      if (value.data != null)
        {
          setState(() {
            fetchedList = value.data.elements;
          })
        },
    });

    if (fetchedList == null) {
      setState(() {
        _isLoading = false;
        _hasMore = false;
      });
    } else {
      setState(() {
        _currentPage++;
        _isLoading = false;
        _pendingPayments.addAll(fetchedList);
      });
    }
  }

  Widget _buildEmptyPendingPaymentCard() {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Text(
              getTranslated(context, 'pending_Payments_noTransaction'),
              style: TextStyle(
                fontSize: 18.0,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPendingPaymentCard(
      BuildContext context, GetTransHisResCustomElements transaction) {
    var memberName;
    var date = transaction.formattedDate;
    var formattedAmount = transaction.formattedAmount;
    double amount = 0.0;
    var id = transaction.transactionNumber;
    var transactionCode = transaction.transferType.code;
    var notes = transaction.description;
    var description = getTranslated(context, transactionCode);
    if (transaction.member != null) {
      memberName = transaction.member.name;
    } else {
      memberName = transaction.transferType.to.name;
    }
    // bool canChargeBack = false;
    bool notPurchase = false;
    OrderItems orderItems = OrderItems();
    List<Product> products = List();
    var merchantId;
    if (transaction.customValues != null) {
      OrderItems productList;
      transaction.customValues.forEach((cv) {
        if (cv.internalName == 'ORDER_ITEMS') {
          productList = OrderItems.fromJson(jsonDecode(cv.value));
        }
      });

      if (productList.items != null) {
        for (var pro in productList.items) {
          Product product = new Product();
          amount += pro.totalPrice;

          product.productId = pro.productId;
          merchantId = product.merchantId = pro.merchantId;
          product.categoryId = pro.categoryId;
          product.totalPrice = pro.totalPrice;
          product.orderedAmount = pro.orderedAmount;
          product.price = pro.price;
          product.title = pro.title;

          products.add(product);
        }
        orderItems.items = products;
      }
    }
    // if (transactionCode == PURCHASE_TRANSFER_TYPE_CODE &&
    //     memberType == GROUP_MERCHANT)
    //   canChargeBack = true;
    // else
    //   notPurchase = true;
    // if (transaction.chargedBack)
    //   canChargeBack = false;
    // else if (transaction.chargedBackOfTransferID != null) {
    //   canChargeBack = false;
    //   description = getTranslated(context, 'CHARGE_BACK');
    // }

    // Color cardBackground = canChargeBack || notPurchase
    //     ? Colors.white
    //     : Colors.blue[100].withOpacity(0.4);
    return Card(
      margin: EdgeInsets.all(5.0),
      child: InkWell(
        highlightColor: Colors.white60,
        onTap: () {
          if (orderItems.items != null && orderItems.items.length != 0) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ProductsDetails(
                        amount: amount,
                        // products: products,
                        notes:notes,
                        orderItems: orderItems,
                        merchantId: merchantId,
                        transactionId: transaction.id,
                        merchantFlow: true)));
          } else {
            CustomWidgetBuilder.buildSnackBar(_scaffoldKey, context,
                getTranslated(context, "SOMETHING_WRONG"));
          }
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment:MainAxisAlignment.start ,
            crossAxisAlignment:CrossAxisAlignment.start ,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Text(memberName.toString(), style: MAJOR_TEXT_STYLE),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  child: Column(
                    mainAxisAlignment:MainAxisAlignment.start ,
                    crossAxisAlignment:CrossAxisAlignment.start ,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            date.toString(),
                            style: TextStyle(color: MAJOR_COLOR),
                          ),
                          Text(
                            formattedAmount.toString(),
                            style: TextStyle(
                              color: amount < 0.0 ? Colors.red : Colors.green,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          description != null
                              ? Flexible(child: Text(description))
                              : Container(),
                          id != null
                              ? Flexible(
                              child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                        '${getTranslated(context, 'transactionID')} : '),
                                    Text(id)
                                  ]))
                              : Container(),
                        ],
                      ),
                      notes != null?Text(notes,textAlign: TextAlign.left,style: TextStyle(color: GRAY_COLOR,fontSize: 11.1),):Container()
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 5.0,
              )
            ],
          ),
        ),
      ),
    );
  }

  // void chargeBack(BuildContext context, var transaction) {
  //   paymentService
  //   .chargeBack(SOAP_USERNAME, SOAP_PASSWORD, transaction.id.toString())
  //   .then((value) => {
  //     Navigator.pop(context),
  //     if (iscanChargeBack(value.body))
  //       {
  //         setState(() {
  //           transaction.chargedBack = true;
  //           _pendingPayments = [];
  //           _currentPage = 0;
  //           _hasMore = true;
  //         }),
  //         loadPendingPaymentsRequests(),
  //         _scaffoldKey.currentState.showSnackBar(SnackBar(
  //             content: Text(getTranslated(
  //                 context, 'transactionHistory_successfulChargeBack'))))
  //       }
  //     else
  //       _scaffoldKey.currentState.showSnackBar(SnackBar(
  //           content: Text(getTranslated(
  //               context, 'transactionHistory_failedChargeBack'))))
  //   });
  // }

  // bool iscanChargeBack(String body) {
  //   xml2Json.parse(body);
  //   var jsonString = xml2Json.toParker();
  //   var data = jsonDecode(jsonString);
  //   print(data.toString());
  //   if (data['soap:Envelope']['soap:Body']['ns2:chargebackResponse']['return']
  //               ['status'] !=
  //           null &&
  //       data['soap:Envelope']['soap:Body']['ns2:chargebackResponse']['return']
  //               ['status'] ==
  //           'SUCCESS') return true;
  //   return false;
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white70.withOpacity(0.9),
        key: _scaffoldKey,
        appBar: ActionBar(context,
            icon: Icons.arrow_back_ios,
            pressFunc: () => Navigator.pop(context)),
        body: Builder(builder: (context) {
          return Container(
            child: _pendingPayments.isEmpty && !_hasMore
                ? _buildEmptyPendingPaymentCard()
                : ListView.builder(
              itemCount: _hasMore
                  ? _pendingPayments.length + 1
                  : _pendingPayments.length,
              itemBuilder: (context, index) {
                if (index >= _pendingPayments.length) {
                  if (!_isLoading) {
                    loadPendingPaymentsRequests();
                  }
                  return Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Center(
                      child: SizedBox(
                        child: CircularProgressIndicator(),
                        height: 24,
                        width: 24,
                      ),
                    ),
                  );
                }
                return _buildPendingPaymentCard(
                    context, _pendingPayments[index]);
              },
            ),
          );
        }));
  }
}