import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/PendingPaymentsService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/requests/PendingStatusRequest.dart';
import 'package:cashless/models/response/transactionRes/GetTransHisResCustomElements.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:flutter/material.dart';

class MarketPlaceRequests extends KFDrawerContent {
  GetTransHisResCustomElements transaction;

  MarketPlaceRequests({Key key, this.transaction}) : super();

  @override
  _MarketPlaceRequestsState createState() => _MarketPlaceRequestsState();
}

class _MarketPlaceRequestsState extends State<MarketPlaceRequests> {
  var memberType;
  PendingStatusRequest req;

  PendingPaymentsService pendingPaymentsService = new PendingPaymentsService();

  PendingRequestAction(bool flag) {
    print("I am in PendingRequestAction ");
    var trans = widget.transaction;
    req = new PendingStatusRequest("request is " + flag.toString(), flag);
    print("===================== Before send ing reques ");
    print(req.toJson());
    pendingPaymentsService.getStatusOfPendingRequest(trans.id.toString(), req);

  }

  @override
  Widget build(BuildContext context) {
    var transaction = widget.transaction;
    var memberName;
    var date = transaction.date;
    var formattedAmount = transaction.formattedAmount;
    var formattedDate = transaction.formattedDate;
    var amount = transaction.amount;

    var id = transaction.transactionNumber;

    var transactionCode = transaction.transferType.code;
    var description = getTranslated(context, transactionCode);
    if (transaction.member != null) {
      memberName = transaction.member.name;
    } else {
      memberName = transaction.transferType.to.name;
    }
    bool canChargeBack = false;
    bool notPurchase = false;

//    if (transactionCode == PURCHASE_TRANSFER_TYPE_CODE &&
//        memberType == GROUP_MERCHANT)
//      canChargeBack = true;
//    else
//      notPurchase = true;
    if (transaction.chargedBack)
      canChargeBack = false;
    else if (transaction.chargedBackOfTransferID != null) {
      canChargeBack = false;
      description = getTranslated(context, 'CHARGE_BACK');
    }

    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white70.withOpacity(0.9),
        appBar: ActionBar(context,
            titleText: getTranslated(context, 'marketplace_title'),
            icon: Icons.arrow_back_ios,
            pressFunc: () => Navigator.pop(context)),
        // drawer: DrawerFragment(),
        body: Column(
          children: List.generate(1, (index) {
            return Card(
              child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                      children: [
                      Text(memberName.toString(), style: MAJOR_TEXT_STYLE),
              Text(
                transaction.transactionNumber.toString(),
                style: TextStyle(color: MAJOR_COLOR),
              ),
              Text(
                transaction.transferType.name.toString(),
                style: TextStyle(color: MAJOR_COLOR),
              ),
              Text(
                transaction.transferType.from.name.toString(),
                style: TextStyle(color: MAJOR_COLOR),
              ),
              Text(
                transaction.transferType.to.name.toString(),
                style: TextStyle(color: MAJOR_COLOR),
              ),
              Text(
                transaction.transferType.name.toString(),
                style: TextStyle(color: MAJOR_COLOR),
              ),
              Text(
                transaction.member.username.toString(),
                style: TextStyle(color: MAJOR_COLOR),
              ),
              Text(
                transaction.member.email.toString(),
                style: TextStyle(color: MAJOR_COLOR),
              ),
              Text(
                transaction.member.groupCode.toString(),
                style: TextStyle(color: MAJOR_COLOR),
              ),
              Text(
                transaction.member.groupName.toString(),
                style: TextStyle(color: MAJOR_COLOR),
              ),
              Text(
                transaction.member.groupNature.toString(),
                style: TextStyle(color: MAJOR_COLOR),
              ),
              Text(
                formattedDate.toString(),
                style: TextStyle(color: MAJOR_COLOR),
              ),
              Text(
                formattedAmount.toString(),
                style: TextStyle(
                  color: amount < 0.0 ? Colors.red : Colors.green,
                  fontSize: 18,
                ),
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
              Expanded(
              child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: RaisedButton.icon(
                  onPressed: () {
                    PendingRequestAction(true);
                  },
                  icon: Icon(Icons.check_circle),
                  label: Text("Accept"),
                  color: Colors.green,
                  textColor: Colors.white),
            )),
            Expanded(
            child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: RaisedButton.icon(
            onPressed: PendingRequestAction(false),
            icon: Icon(Icons.cancel),
            label: Text("Deny"),
            color: Colors.red,
            textColor: Colors.white,
          ),
        ))],
    )
    ],
    ),
    ),
    );
  }

  )

  ,

  )

  );
}}
