import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/pages/DrawerMain.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
class GeneratedPaymentQR extends StatelessWidget {
  QrImage qrImage;
  GeneratedPaymentQR(this.qrImage , {Key key,}) : super();

  void gotoHome(BuildContext context){
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (c) => DrawerMain()), (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Colors.grey[300],
        resizeToAvoidBottomInset: false,
        appBar: ActionBar(context,
            icon: Icons.arrow_back_ios,
            pressFunc: () => Navigator.pop(context)
        ),
        // drawer: DrawerFragment(),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Card(
            elevation: 1,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Column(
              children: <Widget>[
                CustomWidgetBuilder.buildText("Your QR code is ready!",fontSize: 22),
                CustomWidgetBuilder.buildText("Please scan to complete payment",fontSize: 16, textColor: Colors.grey),
                Container(
                  padding: EdgeInsets.all(5),
                  width: MediaQuery.of(context).size.width*0.85,
                  height: MediaQuery.of(context).size.width*0.85,
                    child: qrImage
                ),
                Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: CustomWidgetBuilder.buildRaisedButton(context, gotoHome, null, getTranslated(context, "transaction_summary_btn"), size: MainAxisSize.max,icon: Icons.home),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

