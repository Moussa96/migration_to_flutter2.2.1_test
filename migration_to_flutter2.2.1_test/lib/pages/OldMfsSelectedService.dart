import 'dart:collection';
import 'dart:convert';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/MfsRestClientImpl.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Mfs/MfsScenarioConfiguration.dart';
import 'package:cashless/models/Mfs/MfsTrxParams.dart';
import 'package:cashless/models/requests/MfsServiceCalculatorRequest.dart';
import 'package:cashless/models/Mfs/MfsServices.dart';
import 'package:cashless/pages/MfsConfirmPayment.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SelectedService extends StatefulWidget {
  List<dynamic> packages = [];
  MfsScenarioConfiguration scenarioConfiguration;
  MfsServices service;

  SelectedService(
      {Key key, this.packages, this.service, this.scenarioConfiguration})
      : super();

  @override
  _SelectedServiceState createState() => _SelectedServiceState();
}

class _SelectedServiceState extends State<SelectedService> {
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController _accountController = TextEditingController();
  List<MfsTrxParams> trxParameters;
  bool _loading = false;
  bool _btnEnabled = false;
  Color selectedColor = Colors.blue[400];
  List<Color> cardColor = List<Color>();
  List<TextEditingController> _trxParamsControllers = List();
  int lastIndex = -1;
  MfsRestClientImpl _mfsRestService = MfsRestClientImpl();

  @override
  void initState() {
    super.initState();
    trxParameters = widget.scenarioConfiguration.trx_params;
    if (trxParameters != null) {
      for (int i = 0; i < trxParameters.length; i++) {
        _trxParamsControllers.add(TextEditingController());
      }
    }
    widget.packages
      ..sort((a, b) => a['package_transaction_value']
          .compareTo(b['package_transaction_value']));
    cardColor = List.generate(widget.packages.length, (i) => MINOR_COLOR);
  }

  void billEnquireAndServiceCalculation(BuildContext context) async {
    if (_formKey.currentState.validate()) {
      var account = _accountController.text;
      setState(() {
        _loading = true;
      });
      MfsServiceCalculatorRequest billEnqRequest = MfsServiceCalculatorRequest(
          serviceProviderCode: widget.service.service_provider_code,
          serviceCode: widget.service.prereq_service_code,
          externalRefNumber: Util.generateUUID(),
          externalTxnId: Util.generateUUID(),
          userBAccountId: account,
          transactionValue: "0",
          inParameters: "{}");
      print("bill enq request");
      print(billEnqRequest.toJson());
      _mfsRestService
          .confirmBillEnquiry(billEnqRequest)
          .then((billEnqResponse) {
        if (billEnqResponse.statusCode == 200) {
          MfsServiceCalculatorRequest serviceCalcRequest = billEnqRequest;
          print("bill enq response");
          print(billEnqResponse.data.toJson());

          Map<dynamic, dynamic> billEnquiryOutParams =
              jsonDecode(billEnqResponse.data.out_parameters);

          if (billEnqResponse.data.status_code == '0') {
            serviceCalcRequest.transactionValue = billEnquiryOutParams != null
                ? billEnquiryOutParams['OUT_PARAMETER_1']
                : null;

            if (trxParameters != null) {
              for (int i = 0; i < trxParameters.length; i++) {
                billEnquiryOutParams[trxParameters[i].json_id] =
                    _trxParamsControllers[i].text;
              }
            }

            serviceCalcRequest.inParameters = jsonEncode(billEnquiryOutParams);

            serviceCalcRequest.serviceCode = widget.service.service_code;
            print("service calc request");

            print(serviceCalcRequest.toJson());

            _mfsRestService
                .confirmServiceCalculator(serviceCalcRequest)
                .then((serviceCalcResponse) {
              if (serviceCalcResponse.statusCode == 200) {
                print("service calc response");
                print(serviceCalcResponse.data.toJson());

                Map<dynamic, dynamic> serviceCalcOutParams =
                    jsonDecode(serviceCalcResponse.data.out_parameters);

                if (serviceCalcResponse.data.status_code == '0') {
                  setState(() {
                    _loading = false;
                    _accountController.clear();
                  });
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MfsConfirmPayment(
                                response: serviceCalcResponse.data,
                                scenarioConfiguration:
                                    widget.scenarioConfiguration,
                                account: account,
                                accountLabel: Localization.of(context)
                                            .locale
                                            .languageCode ==
                                        'en'
                                    ? widget
                                        .scenarioConfiguration.account.label_en
                                    : widget
                                        .scenarioConfiguration.account.label,
                                serviceName: widget.service.service_name,
                                serviceNameAr:
                                    widget.service.service_arabic_name,
                                serviceCalcRequest: serviceCalcRequest,
                              )));
                } else {
                  CustomWidgetBuilder.buildSnackBar(
                      scaffoldKey,
                      context,
                      serviceCalcOutParams != null &&
                              serviceCalcOutParams['Message'] != null
                          ? serviceCalcOutParams['Message']
                          : serviceCalcResponse.data.user_a_msg);
                  setState(() {
                    _loading = false;
                  });
                }
              } else {
                CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
                    getTranslated(context, serviceCalcResponse.getErrorCode));
                setState(() {
                  _loading = false;
                });
              }
            });
          } else {
            CustomWidgetBuilder.buildSnackBar(
                scaffoldKey,
                context,
                billEnquiryOutParams != null &&
                        billEnquiryOutParams['Message'] != null
                    ? billEnquiryOutParams['Message']
                    : billEnqResponse.data.user_a_msg);
            setState(() {
              _loading = false;
            });
          }
        } else {
          CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
              getTranslated(context, billEnqResponse.getErrorCode));
          setState(() {
            _loading = false;
          });
        }
      });
    }
  }

  void EVoucherServiceCalc(BuildContext context) {
    setState(() {
      _btnEnabled = false;
      _loading = true;
    });
    Map<String, String> inParams = new HashMap();
    if (trxParameters != null) {
      for (int i = 0; i < trxParameters.length; i++) {
        inParams[trxParameters[i].json_id] = _trxParamsControllers[i].text;
      }
    }
    inParams["in_param_1"] = widget.packages[lastIndex]['package_code'];
    inParams["in_param_2"] = widget.packages[lastIndex]['evd_selector'];
    inParams["in_param_3"] = "1";
    print(inParams);

    MfsServiceCalculatorRequest serviceCalcRequest =
        MfsServiceCalculatorRequest(
            serviceProviderCode: widget.service.service_provider_code,
            serviceCode: widget.service.service_code,
            externalRefNumber: Util.generateUUID(),
            externalTxnId: Util.generateUUID(),
            userBAccountId: widget.service.service_code,
            transactionValue: widget.packages[lastIndex]
                    ['package_transaction_value']
                .toString(),
            inParameters: jsonEncode(inParams));
    _mfsRestService
        .confirmServiceCalculator(serviceCalcRequest)
        .then((serviceCalcResponse) {
      if (serviceCalcResponse.statusCode == 200) {
        print(serviceCalcResponse.data.toJson());
        Map<dynamic, dynamic> serviceCalcOutParams =
            jsonDecode(serviceCalcResponse.data.out_parameters);
        if (serviceCalcResponse.data.status_code == '0') {
          setState(() {
            _loading = false;
            _btnEnabled = true;
          });
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => MfsConfirmPayment(
                        response: serviceCalcResponse.data,
                        scenarioConfiguration: widget.scenarioConfiguration,
                        serviceName: widget.service.service_name,
                        serviceNameAr: widget.service.service_arabic_name,
                        serviceCalcRequest: serviceCalcRequest,
                        accountLabel: getTranslated(
                            context, "mfs_confirm_service_package_name"),
                        account: Localization.of(context).locale.languageCode ==
                                'en'
                            ? widget.packages[lastIndex]['package_name']
                            : widget.packages[lastIndex]['package_arabic_name'],
                      )));
        } else {
          CustomWidgetBuilder.buildSnackBar(
              scaffoldKey,
              context,
              serviceCalcOutParams != null &&
                      serviceCalcOutParams['Message'] != null
                  ? serviceCalcOutParams['Message']
                  : serviceCalcResponse.data.user_a_msg);
          setState(() {
            _loading = false;
            _btnEnabled = true;
          });
        }
      } else {
        CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
            getTranslated(context, serviceCalcResponse.getErrorCode));
        setState(() {
          _loading = false;
          _btnEnabled = true;
        });
      }
    });
  }

  void chargeServiceCalc(BuildContext context) {
    if (_formKey.currentState.validate()) {
      var account = _accountController.text;
      var amount;
      Map<String, String> inParams = new HashMap();
      for (int i = 0; i < trxParameters.length; i++) {
        if (trxParameters[i].json_id == "transaction_value") {
          amount = _trxParamsControllers[i].text;
        } else {
          inParams[trxParameters[i].json_id] = _trxParamsControllers[i].text;
        }
      }
      print(inParams);
      setState(() {
        _loading = true;
      });
      MfsServiceCalculatorRequest serviceCalcRequest =
          MfsServiceCalculatorRequest(
              serviceProviderCode: widget.service.service_provider_code,
              serviceCode: widget.service.service_code,
              externalRefNumber: Util.generateUUID(),
              externalTxnId: Util.generateUUID(),
              userBAccountId: _accountController.text,
              transactionValue: amount,
              inParameters: jsonEncode(inParams));
      print(serviceCalcRequest.toJson());
      _mfsRestService
          .confirmServiceCalculator(serviceCalcRequest)
          .then((serviceCalcResponse) {
        if (serviceCalcResponse.statusCode == 200) {
          print(serviceCalcResponse.data.toJson());

          Map<dynamic, dynamic> serviceCalcOutParams =
              jsonDecode(serviceCalcResponse.data.out_parameters);

          if (serviceCalcResponse.data.status_code == "0") {
            setState(() {
              _loading = false;
              _accountController.clear();
            });
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => MfsConfirmPayment(
                          response: serviceCalcResponse.data,
                          scenarioConfiguration: widget.scenarioConfiguration,
                          serviceName: widget.service.service_name,
                          serviceNameAr: widget.service.service_arabic_name,
                          serviceCalcRequest: serviceCalcRequest,
                          account: account,
                          accountLabel: Localization.of(context)
                                      .locale
                                      .languageCode ==
                                  'en'
                              ? widget.scenarioConfiguration.account.label_en
                              : widget.scenarioConfiguration.account.label,
                        )));
          } else {
            CustomWidgetBuilder.buildSnackBar(
                scaffoldKey,
                context,
                serviceCalcOutParams != null &&
                        serviceCalcOutParams['Message'] != null
                    ? serviceCalcOutParams['Message']
                    : serviceCalcResponse.data.user_a_msg);
            setState(() {
              _loading = false;
            });
          }
        } else {
          CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
              getTranslated(context, serviceCalcResponse.getErrorCode));
          setState(() {
            _loading = false;
          });
        }
      });
    }
  }

  void chargePackagesServiceCalc(BuildContext context) {
    if (_formKey.currentState.validate()) {
      if (widget.packages.length == 0 || lastIndex != -1) {
        var account = _accountController.text;
        var amount =
            widget.packages[lastIndex]['package_transaction_value'].toString();

        Map<String, String> inParams = new HashMap();
        if (trxParameters != null) {
          for (int i = 0; i < trxParameters.length; i++) {
            inParams[trxParameters[i].json_id] = _trxParamsControllers[i].text;
          }
        }
        inParams["in_param_1"] = widget.packages[lastIndex]['package_code'];
        inParams["in_param_2"] = widget.packages[lastIndex]['evd_selector'];
        inParams["in_param_3"] = "1";
        print(inParams);

        setState(() {
          _loading = true;
        });

        MfsServiceCalculatorRequest serviceCalcRequest =
            MfsServiceCalculatorRequest(
                serviceProviderCode: widget.service.service_provider_code,
                serviceCode: widget.service.service_code,
                externalRefNumber: Util.generateUUID(),
                externalTxnId: Util.generateUUID(),
                userBAccountId: _accountController.text,
                transactionValue: amount,
                inParameters: jsonEncode(inParams));
        print(serviceCalcRequest.toJson());
        _mfsRestService
            .confirmServiceCalculator(serviceCalcRequest)
            .then((serviceCalcResponse) {
          if (serviceCalcResponse.statusCode == 200) {
            print(serviceCalcResponse.data.toJson());
            Map<dynamic, dynamic> serviceCalcOutParams =
                jsonDecode(serviceCalcResponse.data.out_parameters);
            if (serviceCalcResponse.data.status_code == "0") {
              setState(() {
                _loading = false;
                _accountController.clear();
              });
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MfsConfirmPayment(
                            response: serviceCalcResponse.data,
                            scenarioConfiguration: widget.scenarioConfiguration,
                            serviceName: widget.service.service_name,
                            serviceNameAr: widget.service.service_arabic_name,
                            serviceCalcRequest: serviceCalcRequest,
                            account: account,
                            accountLabel: Localization.of(context)
                                        .locale
                                        .languageCode ==
                                    'en'
                                ? widget.scenarioConfiguration.account.label_en
                                : widget.scenarioConfiguration.account.label,
                          )));
            } else {
              CustomWidgetBuilder.buildSnackBar(
                  scaffoldKey,
                  context,
                  serviceCalcOutParams != null &&
                          serviceCalcOutParams['Message'] != null
                      ? serviceCalcOutParams['Message']
                      : serviceCalcResponse.data.user_a_msg);
              setState(() {
                _loading = false;
              });
            }
          } else {
            CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
                getTranslated(context, serviceCalcResponse.getErrorCode));
            setState(() {
              _loading = false;
            });
          }
        });
      } else {
        CustomWidgetBuilder.buildSnackBar(scaffoldKey, context,
            getTranslated(context, 'mfs_selected_service_choose_package'));
      }
    }
  }

  selectPackage(int index) {
    setState(() {
      _btnEnabled = true;
      if (lastIndex != -1) cardColor[lastIndex] = MINOR_COLOR;
      cardColor[index] = selectedColor;
      lastIndex = index;
    });
  }

  Widget _buildChargeView(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                    Localization.of(context).locale.languageCode == 'en'
                        ? widget.service.service_name
                        : widget.service.service_arabic_name,
                    style:
                        TextStyle(fontSize: 20, fontWeight: FontWeight.w500)),
              )),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  CustomWidgetBuilder.buildFormInputText(
                      expression: widget.scenarioConfiguration.account.regex !=
                              null
                          ? RegExp(widget.scenarioConfiguration.account.regex)
                          : RegExp(""),
                      expErrorMessage:
                          widget.scenarioConfiguration.account.err_en,
                      errorMessage:
                          Localization.of(context).locale.languageCode == 'en'
                              ? widget.scenarioConfiguration.account.err_en
                              : widget.scenarioConfiguration.account.err,
                      hint: Localization.of(context).locale.languageCode == 'en'
                          ? widget.scenarioConfiguration.account.label_en
                          : widget.scenarioConfiguration.account.label,
                      inputType: TextInputType.number,
                      controller: _accountController),
                  trxParameters != null
                      ? SingleChildScrollView(
                          child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: trxParameters.length,
                          itemBuilder: (BuildContext context, int i) => Column(
                            children: [
                              CustomWidgetBuilder.buildFormInputText(
                                controller: _trxParamsControllers[i],
                                expression: trxParameters[i].regex != null
                                    ? RegExp(trxParameters[i].regex)
                                    : null,
                                expErrorMessage: trxParameters[i].err_en,
                                errorMessage: Localization.of(context)
                                            .locale
                                            .languageCode ==
                                        'en'
                                    ? trxParameters[i].err_en
                                    : trxParameters[i].err,
                                maxValue: trxParameters[i].max,
                                minValue: trxParameters[i].min,
                                hint: Localization.of(context)
                                            .locale
                                            .languageCode ==
                                        'en'
                                    ? trxParameters[i].label_en
                                    : trxParameters[i].label,
                                inputType: TextInputType.numberWithOptions(
                                    decimal: true),
                              ),
                              trxParameters[i].helper_en != null
                                  ? Align(
                                      alignment: Alignment.topRight,
                                      child: Text(Localization.of(context)
                                                  .locale
                                                  .languageCode ==
                                              'en'
                                          ? trxParameters[i].helper_en
                                          : trxParameters[i].helper))
                                  : Container()
                            ],
                          ),
                        ))
                      : Container()
                ],
              ),
            ),
          ),
          widget.packages.length > 0
              ? CustomWidgetBuilder.buildCategoryCards(
                  widget.packages,
                  Localization.of(context).locale.languageCode == 'en'
                      ? 'package_name'
                      : 'package_arabic_name',
                  selectPackage,
                  width: 125,
                  height: 100,
                  //cardColor: cardColor,
                  showImg: false)
              : Container(),
          Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(children: [
                Expanded(
                    child: CustomWidgetBuilder.buildRaisedButton(
                        context,
                        widget.scenarioConfiguration.scenario_id
                                    .toLowerCase() ==
                                MFS_SCENARIO_CHARGE
                            ? chargeServiceCalc
                            : chargePackagesServiceCalc,
                        null,
                        getTranslated(context, "continue"),
                        size: MainAxisSize.max,
                        isEnabled: !_loading)),
                _loading
                    ? Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation<Color>(
                                Colors.blue[700])),
                      )
                    : Container()
              ]))
        ]));
  }

  Widget _buildBillView(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                    Localization.of(context).locale.languageCode == 'en'
                        ? widget.service.service_name
                        : widget.service.service_arabic_name,
                    style:
                        TextStyle(fontSize: 20, fontWeight: FontWeight.w500)),
              )),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  CustomWidgetBuilder.buildFormInputText(
                    expression:
                        widget.scenarioConfiguration.account.regex != null
                            ? RegExp(widget.scenarioConfiguration.account.regex)
                            : null,
                    expErrorMessage:
                        Localization.of(context).locale.languageCode == 'en'
                            ? widget.scenarioConfiguration.account.err_en
                            : widget.scenarioConfiguration.account.err,
                    errorMessage:
                        Localization.of(context).locale.languageCode == 'en'
                            ? widget.scenarioConfiguration.account.err_en
                            : widget.scenarioConfiguration.account.err,
                    hint: Localization.of(context).locale.languageCode == 'en'
                        ? widget.scenarioConfiguration.account.label_en
                        : widget.scenarioConfiguration.account.label,
                    inputType: TextInputType.number,
                    controller: _accountController,
                  ),
                  trxParameters != null
                      ? SingleChildScrollView(
                          child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: trxParameters.length,
                          itemBuilder: (BuildContext context, int i) =>
                              trxParameters[i].label_en != "Amount"
                                  ? Column(children: [
                                      CustomWidgetBuilder.buildFormInputText(
                                          expression: trxParameters[i].regex !=
                                                  null
                                              ? RegExp(trxParameters[i].regex)
                                              : null,
                                          expErrorMessage:
                                              trxParameters[i].err_en,
                                          errorMessage: Localization.of(context)
                                                      .locale
                                                      .languageCode ==
                                                  'en'
                                              ? trxParameters[i].err_en
                                              : trxParameters[i].err,
                                          maxValue: trxParameters[i].max,
                                          minValue: trxParameters[i].min,
                                          hint: Localization.of(context)
                                                      .locale
                                                      .languageCode ==
                                                  'en'
                                              ? trxParameters[i].label_en
                                              : trxParameters[i].label,
                                          inputType:
                                              TextInputType.numberWithOptions(
                                                  decimal: true),
                                          controller: _trxParamsControllers[i]),
                                      trxParameters[i].helper_en != null
                                          ? Align(
                                              alignment: Alignment.topRight,
                                              child: Text(Localization.of(
                                                              context)
                                                          .locale
                                                          .languageCode ==
                                                      'en'
                                                  ? trxParameters[i].helper_en
                                                  : trxParameters[i].helper))
                                          : Container()
                                    ])
                                  : Container(),
                        ))
                      : Container()
                ],
              ),
            ),
          ),
          widget.packages.length > 0
              ? CustomWidgetBuilder.buildCategoryCards(
                  widget.packages,
                  Localization.of(context).locale.languageCode == 'en'
                      ? 'package_name'
                      : 'package_arabic_name',
                  selectPackage,
                  width: 125,
                  height: 100,
                  //cardColor: cardColor,
                  showImg: false)
              : Container(),
          Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(children: [
                Expanded(
                    child: CustomWidgetBuilder.buildRaisedButton(
                        context,
                        billEnquireAndServiceCalculation,
                        null,
                        getTranslated(context, "continue"),
                        size: MainAxisSize.max,
                        isEnabled: !_loading)),
                _loading
                    ? Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation<Color>(
                                Colors.blue[700])),
                      )
                    : Container()
              ]))
        ]));
  }

  Widget _buildEvoucherView(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                    Localization.of(context).locale.languageCode == 'en'
                        ? widget.service.service_name
                        : widget.service.service_arabic_name,
                    style:
                        TextStyle(fontSize: 20, fontWeight: FontWeight.w500)),
              )),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  widget.scenarioConfiguration.account.label_en != "Packages"
                      ? CustomWidgetBuilder.buildFormInputText(
                          expression: widget
                                      .scenarioConfiguration.account.regex !=
                                  null
                              ? RegExp(
                                  widget.scenarioConfiguration.account.regex)
                              : null,
                          expErrorMessage:
                              widget.scenarioConfiguration.account.err_en,
                          errorMessage:
                              Localization.of(context).locale.languageCode ==
                                      'en'
                                  ? widget.scenarioConfiguration.account.err_en
                                  : widget.scenarioConfiguration.account.err,
                          hint: Localization.of(context).locale.languageCode ==
                                  'en'
                              ? widget.scenarioConfiguration.account.label_en
                              : widget.scenarioConfiguration.account.label,
                          inputType: TextInputType.number,
                          controller: _accountController,
                        )
                      : Container(),
                  trxParameters != null
                      ? SingleChildScrollView(
                          child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: trxParameters.length,
                          itemBuilder: (BuildContext context, int i) => Column(
                            children: [
                              CustomWidgetBuilder.buildFormInputText(
                                  expErrorMessage: trxParameters[i].err_en,
                                  errorMessage: Localization.of(context)
                                              .locale
                                              .languageCode ==
                                          'en'
                                      ? trxParameters[i].err_en
                                      : trxParameters[i].err,
                                  hint: Localization.of(context)
                                              .locale
                                              .languageCode ==
                                          'en'
                                      ? trxParameters[i].label_en
                                      : trxParameters[i].label,
                                  maxValue: trxParameters[i].max,
                                  minValue: trxParameters[i].min,
                                  inputType: TextInputType.numberWithOptions(
                                      decimal: true),
                                  controller: _trxParamsControllers[i]),
                              trxParameters[i].helper_en != null
                                  ? Align(
                                      alignment: Alignment.topRight,
                                      child: Text(Localization.of(context)
                                                  .locale
                                                  .languageCode ==
                                              'en'
                                          ? trxParameters[i].helper_en
                                          : trxParameters[i].helper))
                                  : Container()
                            ],
                          ),
                        ))
                      : Container()
                ],
              ),
            ),
          ),
          widget.packages.length > 0
              ? CustomWidgetBuilder.buildCategoryCards(
                  widget.packages,
                  Localization.of(context).locale.languageCode == 'en'
                      ? 'package_name'
                      : 'package_arabic_name',
                  selectPackage,
                  width: 125,
                  height: 100,
                  //cardColor: cardColor,
                  showImg: false)
              : Container(),
          Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(children: [
                Expanded(
                    child: CustomWidgetBuilder.buildRaisedButton(
                        context,
                        EVoucherServiceCalc,
                        null,
                        getTranslated(context, "continue"),
                        size: MainAxisSize.max,
                        isEnabled: _btnEnabled)),
                _loading
                    ? Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: CircularProgressIndicator(
                            valueColor: new AlwaysStoppedAnimation<Color>(
                                Colors.blue[700])),
                      )
                    : Container()
              ]))
        ]));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.white.withOpacity(0.9),
        resizeToAvoidBottomInset: false,
        appBar: ActionBar(context,
            titleText: getTranslated(context, 'home_CashcallServices'),
            icon: Icons.arrow_back_ios,
            pressFunc: () => Navigator.pop(context)),
        body: widget.scenarioConfiguration.scenario_id.toLowerCase() ==
                MFS_SCENARIO_BILL
            ? _buildBillView(context)
            : widget.scenarioConfiguration.scenario_id.toLowerCase() ==
                    MFS_SCENARIO_VOUCHER
                ? _buildEvoucherView(context)
                : widget.scenarioConfiguration.scenario_id.toLowerCase() ==
                            MFS_SCENARIO_CHARGE ||
                        widget.scenarioConfiguration.scenario_id
                                .toLowerCase() ==
                            MFS_SCENARIO_CHARGE_PACKAGES
                    ? _buildChargeView(context)
                    : Center(
                        child: Text(
                            getTranslated(context, "service_unavailable"),
                            style: TextStyle(fontSize: 30))));
  }
}
