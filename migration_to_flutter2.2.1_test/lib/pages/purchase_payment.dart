import 'dart:collection';
import 'dart:convert';

import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/models/Fees.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/response/MakePaymentResponse.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/pages/transaction_summary/old_transaction_summary.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
class PurchasePayment extends StatelessWidget {
  Payment _paymentRequest = new Payment();
  Map<String, dynamic> purchaseInfo = new HashMap();
  PaymentService _paymentUseCase = new PaymentService();

  PurchasePayment(
    this._paymentRequest, {
    Key key,
  }) : super();

  void doPayment(BuildContext context) async {
    RetrofitResponse makePaymentResponse =
        await _paymentUseCase.makePayment(_paymentRequest);
    RetrofitResponse response =
        await _paymentUseCase.confirmMemberPayment(_paymentRequest);
    if (response.statusCode == 200) {
      MakePaymentResponse confirmRS = makePaymentResponse.data;
      purchaseInfo['TO'] = confirmRS.toMember.username;
      purchaseInfo['Total Amount'] = _paymentRequest.amount;
      purchaseInfo['Description'] = _paymentRequest.description;
      List<Fees> fees = confirmRS.fees;
      if (fees != null) {
        print(fees.length);
        for (var fee in fees)
          purchaseInfo[fee.name] = fee.formattedAmount.toString();
      }
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => TransactionSummary(purchaseInfo)));
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text('Payment done successfully.')));
    } else {
      Scaffold.of(context).showSnackBar(
          SnackBar(content: Text('Payment cannot be performed.')));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: ActionBar(context,
            icon: Icons.arrow_back_ios,
            pressFunc: () => Navigator.pop(context)),
        body: Builder(builder: (BuildContext context) {
          return Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 50),
                    child: Text(
                      "Payment Info",
                      style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.bold,
                          color: Colors.amber),
                    ),
                  ),
                ),
                Container(
                  child: CustomWidgetBuilder.buildText(
                      "To User Wallet : " + _paymentRequest.toMemberId),
                ),
                Container(
                  child: CustomWidgetBuilder.buildText(
                      "Amount : " + _paymentRequest.amount.toString()),
                ),
                Container(
                  child: CustomWidgetBuilder.buildText(
                      "Descripation : " + _paymentRequest.description),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 16.0, horizontal: 20),
                  child: CustomWidgetBuilder.buildRaisedButton(
                      context, doPayment, null, 'Confirm',
                      size: MainAxisSize.max),
                )
              ],
            ),
          );
        }));
  }
}
