import 'dart:convert';
import 'dart:io';

import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/ContactService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/util/NFCUtil.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart';
import 'package:screenshot/screenshot.dart';
import 'package:xml2json/xml2json.dart';

import 'generatedQRCode.dart';

class Registration extends KFDrawerContent {
  Registration({Key key}) : super();

  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration> {
  File _imageFile;
  //Create an instance of ScreenshotController
  ScreenshotController screenshotController = ScreenshotController();
  final _formKey = GlobalKey<FormState>();
  User user = User();
  bool _sucess = false;
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _usernameController = TextEditingController();
  final _mobileNumberController = TextEditingController();
  final _emailController = TextEditingController();
  final _nationalIDController = TextEditingController();
  final contact = ContactService();
  final Xml2Json xml2Json = Xml2Json();
  GlobalKey globalKey = new GlobalKey();
  var data;
  void register(BuildContext context) async {
    // Validate returns true if the form is valid, or false
    // otherwise.
    if (_formKey.currentState.validate()) {
      // If the form is valid, display a Snackbar.
      _formKey.currentState.save();
      user.name = '${_firstNameController.text} ${_lastNameController.text}';
      user.email = _emailController.text;
      user.username = _mobileNumberController.text;
      user.nationalId = _nationalIDController.text;
      user.mobileNumber = _mobileNumberController.text;
      Response response = await contact.register(SOAP_USERNAME, SOAP_PASSWORD,user);
      xml2Json.parse(response.body);
      var jsonString = xml2Json.toParker();
      data = jsonDecode(jsonString);
      print('///////////////////////////'+data.toString());
      if(response.statusCode == 200){
        setState(() {
          _sucess = true;
        });
        Scaffold.of(context).showSnackBar(SnackBar(content: Text('Member register successfully with mobile number ${data['soap:Envelope']['soap:Body']['ns2:registerMemberResponse']['return']['username']}')));
      }else{

        Scaffold.of(context).showSnackBar(SnackBar(content: Text(data['soap:Envelope']['soap:Body']['soap:Fault']['faultstring'])));
      }

    } else {
      Scaffold.of(context).showSnackBar(SnackBar(content: Text(getTranslated(context, "registeration_snack_error"))));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // drawer: DrawerFragment(),
        appBar: ActionBar(context,titleText: getTranslated(context, 'registeration_title'), icon: Icons.arrow_back_ios,pressFunc:  () => Navigator.pop(context)),
        body: ListView(
          children: <Widget>[
            Builder(
              builder: (BuildContext context) {
                return Container(
                  margin: EdgeInsets.all(15.0),
                  padding: EdgeInsets.all(10.0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                         // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Flexible(child: CustomWidgetBuilder.buildFormInputText(controller: _firstNameController, hint: getTranslated(context, 'registeration_firstname'), errorMessage: getTranslated(context, 'registeration_firstname_required'),isEnabled: !_sucess),flex: 14,),
                            Flexible(child: CustomWidgetBuilder.buildAsteriskText('*'),flex: 1,),
                          ],
                        ),
                        Row(
                          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Flexible(child:CustomWidgetBuilder.buildFormInputText(controller: _lastNameController, hint: getTranslated(context, 'registeration_lastname'), errorMessage: getTranslated(context, 'registeration_lastname_required'),isEnabled: !_sucess),flex: 14,),
                            Flexible(child: CustomWidgetBuilder.buildAsteriskText('*'),flex: 1,),
                          ],
                        ),
   /*                     Row(
                          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Flexible(child: CustomWidgetBuilder.buildFormInputText(controller: _usernameController, hint: getTranslated(context, 'registeration_username'), errorMessage: getTranslated(context, 'registeration_username_required'),isEnabled: !_sucess),flex: 14,),
                            Flexible(child: CustomWidgetBuilder.buildAsteriskText('*'),flex: 1,),
                          ],
                        ),*/
                        Row(
                          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Flexible(child:CustomWidgetBuilder.buildFormInputText(controller: _emailController, hint: getTranslated(context, 'registeration_email'), expression: Util.emailExp, expErrorMessage: 'Please enter a valid Email',isEnabled: !_sucess),flex: 14,),
                            Flexible(child: CustomWidgetBuilder.buildAsteriskText('*'),flex: 1,),
                          ],
                        ),
                        Row(
                          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          //crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Flexible(child:CustomWidgetBuilder.buildFormInputText(controller: _mobileNumberController, hint: getTranslated(context, 'registeration_mobile'), errorMessage: getTranslated(context, 'registeration_mobile_required'), inputType: TextInputType.number,maxLength: 11,isEnabled: !_sucess), flex: 14,),
                            Flexible(child: CustomWidgetBuilder.buildAsteriskText('*'),flex: 1,),
                          ],
                        ),
                        Row(
                          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Flexible(child: CustomWidgetBuilder.buildFormInputText(controller: _nationalIDController, hint: getTranslated(context, 'registeration_national_id'), inputType: TextInputType.number, maxLength: 14,isEnabled: !_sucess),flex: 14,),
                            Flexible(child: CustomWidgetBuilder.buildAsteriskText(' '),flex: 1,),
                          ],
                        ),

                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: CustomWidgetBuilder.buildRaisedButton(context, register, null, getTranslated(context, 'registeration_btn_register'), size: MainAxisSize.max,isEnabled: !_sucess),
                        ),

                        _sucess? Row(
                          children: [
                            Expanded(
                              child: CustomWidgetBuilder.buildRaisedButton(context, generateQR , null, getTranslated(context, 'registeration_btn_generateQR'), size: MainAxisSize.min),
                            ),

                          ],
                        ) : Container(),
                        _sucess? Padding(
                          padding: const EdgeInsets.only(top:15.0),
                          child: Row(
                            children: [
                              // SizedBox(height: 80),
                              Expanded(
                                child: CustomWidgetBuilder.buildRaisedButton(context, initializeNFC, null, getTranslated(context, 'registeration_btn_initializeNFC'), size: MainAxisSize.min),
                              )
                            ],
                          ),
                        ) : Container(),
                      ],
                    ),
                  ),
                );
              },
            ),
          ],
        ));
  }

  void onDone(BuildContext context) {
    print('onDone function');
    // Show dialog on Android (iOS has it's own one)
    if (Platform.isAndroid) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: const Text("Scan the tag you want to write to"),
          actions: <Widget>[
            FlatButton(
              child: const Text("OK"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
        barrierDismissible: false,
      );
    }

  }
  void initializeNFC(BuildContext context)async{
    print('initializeNFC function');
    User user = User();
    user.id = data['soap:Envelope']['soap:Body']['ns2:registerMemberResponse']['return']['id'];
    user.mobileNumber = data['soap:Envelope']['soap:Body']['ns2:registerMemberResponse']['return']['username'];
    NFCUtil.writeNFC(context, user, onDone);
  }
  void generateQR(BuildContext context)async{
    print('generateQR function');
    user.id = data['soap:Envelope']['soap:Body']['ns2:registerMemberResponse']['return']['id'];
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => GeneratedQRCode(user: user)));
  }

}
