import 'dart:collection';
import 'dart:convert';
import 'package:cashless/pages/error_page.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/apis/Services/TransactionHistoryService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/response/transactionRes/GetTransHisResCustomElements.dart';
import 'package:cashless/pages/fragments/navigation_bar.dart';
import 'package:cashless/pages/fragments/home_action_bar.dart';
import 'package:cashless/pages/home/home.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:cashless/widgets/custom_widgets/CustomText.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:xml2json/xml2json.dart';
import 'package:google_fonts/google_fonts.dart';
class TransactionHistory extends StatefulWidget {
  final BuildContext menuScreenContext;
  final Function onScreenHideButtonPressed;
  final bool hideStatus;
  TransactionHistory(
      {Key key,
      @required this.menuScreenContext,
      this.onScreenHideButtonPressed,
      this.hideStatus = false})
      : super();

  @override
  _TransactionHistoryState createState() => _TransactionHistoryState();
}

class _TransactionHistoryState extends State<TransactionHistory> {
  var _transactions = [];
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  TransactionHistoryService transactionsHistoryService =
      new TransactionHistoryService();

  PaymentService paymentService = PaymentService();
  final Xml2Json xml2Json = Xml2Json();
  int _currentPage = 0;
  var memberType;
  bool _isLoading = true;
  bool _hasMore = true;

  @override
  void initState() {
    super.initState();
    _isLoading = true;
    _hasMore = true;
    Util.loadFromSharedPreferences(Util.GROUP).then((value) {
      memberType = value;
    });
    loadTransactionHistory();
  }

  loadTransactionHistory() async {
    _isLoading = true;
    List<GetTransHisResCustomElements> fetchedList;
    String accountID = await Util.loadFromSharedPreferences(Util.ACCOUNT_ID);
    print(accountID);
    Map<String, dynamic> params = new HashMap<String, dynamic>();
    params.putIfAbsent("currentPage", () => _currentPage);

    await transactionsHistoryService
        .getTransactionHistory(accountID, params)
        .then((value) => {
              if (value.data != null && value.data.elements != null)
                {
                  setState(() {
                    _isLoading = false;
                    fetchedList = value.data.elements;
                    fetchedList.forEach((e) => print(e.toJson()));
                  })
                }
              else if (_currentPage == 0 && Util.isConnectionError(value.getErrorCode))
                {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              ErrorPage(errorType: NETWORK_ERROR)))
                }
              else if (_currentPage == 0 &&Util.isServerError(value.getErrorCode))
                  {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ErrorPage(errorType: SERVER_ERROR)))
                  }
            });

    if (fetchedList == null) {
      setState(() {
        _isLoading = false;
        _hasMore = false;
      });
    } else {
      setState(() {
        _currentPage++;
        print("_currentPage");
        print(_currentPage);
        _isLoading = false;
        _transactions.addAll(fetchedList);
      });
    }
  }

  Widget _buildEmptyTransaction() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset('lib/assets/history_empty_graphics.png'),
        SizedBox(
          height: 15,
        ),
        Text(getTranslated(context, 'tranaction_history_empty_list'),
            style: GoogleFonts.lato(
                color: PRIMARY_COLOR,
                fontWeight: FontWeight.w700,
                fontStyle: FontStyle.normal,
                fontSize: 12.9),
            textAlign: TextAlign.center),
        SizedBox(
          height: 3,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Text(
              getTranslated(context, 'tranaction_history_empty_list_message'),
              style: GoogleFonts.lato(
                  color: MAIN_COLOR,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  fontSize: 14.9),
              textAlign: TextAlign.center),
        ),
        SizedBox(
          height: 14,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 64),
          child: CustomWidgetBuilder.buildRaisedButton(
              context,
              goToCashCallServices,
              null,
              getTranslated(
                  context, 'tranaction_history_view_cashcall_services'),
              color: Colors.white,
              fontColor: SECONDARY_COLOR,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              fontSize: 13.6),
        ),
      ],
    );
  }

  void goToCashCallServices(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => ConsumerHome()));
  }

  Widget _buildTransactionCard(
      BuildContext context, GetTransHisResCustomElements transaction) {
    var memberName;
    var date = transaction.formattedDate;
    var formattedAmount = transaction.formattedAmount;
    var amount = transaction.amount;
    var id = transaction.transactionNumber;
    var transactionCode = transaction.transferType.code != null
        ? transaction.transferType.code
        : "DEFAULT_TRANSACTION_CODE";
    var imgUrl;
    var serviceNameEn;
    var serviceNameAr;
    var serviceCode;
    if (transaction.customValues != null) {
      transaction.customValues.forEach((cv) {
        if (cv.internalName == EXTERNAL_SERVICE_NAME_EN) {
          serviceNameEn = cv.value;
        }
        if (cv.internalName == EXTERNAL_SERVICE_NAME_AR) {
          serviceNameAr = cv.value;
        }
        if (cv.internalName == EXTERNAL_SERVICE_CODE) {
          serviceCode = cv.value;
        }
      });
    }
    var description = transactionCode != null
        ? getTranslated(context, transactionCode)
        : getTranslated(context, "DEFAULT_TRANSACTION_CODE");
    if (transaction.member != null) {
      memberName = transaction.member.name;
    } else {
      memberName = transaction.transferType.to.name;
    }
    bool canChargeBack = false;
    bool notPurchase = false;
    imgUrl = getServiceImageURL(transactionCode, serviceCode);

    if ((transactionCode == PURCHASE_TRANSFER_TYPE_CODE ||
            transactionCode == AUTHORIZED_PURCHASE_TRANSFER_TYPE_CODE) &&
        memberType == GROUP_MERCHANT) {
      canChargeBack = true;
    } else {
      notPurchase = true;
    }
    if (transaction.chargedBack) {
      canChargeBack = false;
    } else if (transaction.chargedBackOfTransferID != null) {
      canChargeBack = false;
      description = getTranslated(context, 'CHARGE_BACK');
    }

    Color cardBackground = canChargeBack || notPurchase
        ? Colors.white
        : Colors.blue[100].withOpacity(0.4);
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: canChargeBack ? 0.25 : 0,
      child: Container(
        color: cardBackground,
        padding: EdgeInsets.all(12),
        margin: EdgeInsets.only(bottom: 1),
        child: Column(
          children: <Widget>[
            id != null
                ? Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        child: Image.asset("lib/assets/historyBarcode.png"),
                      ),
                      CustomText(text: id, fontSize: 12, color: CASHCALL_TITLE)
                    ],
                  )
                : Container(),
            SizedBox(height: 10),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                FadeInImage(
                  image: NetworkImage(imgUrl),
                  placeholder:
                      AssetImage("lib/assets/historyAccountIcWithBg.png"),
                  width: 30,
                  height: 40,
                ),
                SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Flexible(
                            child: CustomText(
                                text: memberName.toString(),
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                          Spacer(),
                          CustomText(
                            text: amount<0.0 && Localization.of(context).locale.languageCode == 'ar'?
                                '${-1*amount}- EGP'
                                : formattedAmount.toString(),
                            color: amount < 0.0 ? Colors.black : Colors.green,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Flexible(
                            child: CustomText(
                              text: date.toString(),
                              color: Colors.grey,
                              fontSize: 12,
                            ),
                          ),
                          Spacer(),
                          serviceNameEn != null
                              ? Flexible(
                                  child: CustomText(
                                    text: Localization.of(context)
                                                .locale
                                                .languageCode ==
                                            'en'
                                        ? serviceNameEn
                                        : serviceNameAr,
                                    fontSize: 12,
                                    color: Colors.grey,
                                  ),
                                )
                              : description != null
                                  ? Flexible(
                                      child: CustomText(
                                        text: description,
                                        fontSize: 12,
                                        color: Colors.grey,
                                      ),
                                    )
                                  : Spacer()
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 5.0,
            )
          ],
        ),
      ),
      secondaryActions: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: IconSlideAction(
            caption: getTranslated(context, 'transactionHistory_chargeBack'),
            color: Colors.green[500],
            icon: Icons.done,
            onTap: () {
              print('charge back function$id');
              CustomWidgetBuilder.buildAlertDialog(
                  context,
                  chargeBack,
                  transaction,
                  '${getTranslated(context, 'transactionHistory_confirmChargeBackMessage')} ${transaction.transactionNumber}',
                  getTranslated(
                      context, 'transactionHistory_chargeBackConfirm'),
                  cancelButton: true,
                  labelCancelButton: getTranslated(
                      context, 'transactionHistory_cancelChargeBack'));
            },
          ),
        ),
      ],
    );
  }

  void chargeBack(BuildContext context, var transaction) {
    paymentService
        .chargeBack(SOAP_USERNAME, SOAP_PASSWORD, transaction.id.toString())
        .then((value) => {
              Navigator.pop(context),
              if (iscanChargeBack(value.body))
                {
                  setState(() {
                    transaction.chargedBack = true;
                    _transactions = [];
                    _currentPage = 0;
                    _hasMore = true;
                  }),
                  loadTransactionHistory(),
                  _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content: Text(getTranslated(
                          context, 'transactionHistory_successfulChargeBack'))))
                }
              else
                _scaffoldKey.currentState.showSnackBar(SnackBar(
                    content: Text(getTranslated(
                        context, 'transactionHistory_failedChargeBack'))))
            });
  }

  bool iscanChargeBack(String body) {
    xml2Json.parse(body);
    var jsonString = xml2Json.toParker();
    var data = jsonDecode(jsonString);
    print(data.toString());
    if (data['soap:Envelope']['soap:Body']['ns2:chargebackResponse']['return']
                ['status'] !=
            null &&
        data['soap:Envelope']['soap:Body']['ns2:chargebackResponse']['return']
                ['status'] ==
            'SUCCESS') return true;
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: BACKGROUND_COLOR,
      key: _scaffoldKey,
      appBar: HomeActionBar(context),
      body: Builder(builder: (context) {
        return Container(
          child: _transactions.isEmpty && !_hasMore
              ? _buildEmptyTransaction()
              : ListView.builder(
                  itemCount: _hasMore
                      ? _transactions.length + 1
                      : _transactions.length,
                  itemBuilder: (context, index) {
                    if (index >= _transactions.length) {
                      if (!_isLoading) {
                        loadTransactionHistory();
                      }
                      return Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Center(
                          child: SizedBox(
                            child: CircularProgressIndicator(),
                            height: 24,
                            width: 24,
                          ),
                        ),
                      );
                    }
                    return _buildTransactionCard(context, _transactions[index]);
                  },
                ),
        );
      }),
      bottomNavigationBar: NavigationBar(
        activeItem: 3,
      ),
    );
  }

  String getServiceImageURL(String transactionCode, String serviceCode) {
    String imgUrl;
    if (transactionCode.contains(CASH_IN_TRANSFER_TYPE_CODE) ||
        transactionCode.contains(CASH_OUT_TRANSFER_TYPE_CODE)) {
      imgUrl = "lib/assets/historyAgentIcWithBg.png";
    } else if (transactionCode == PURCHASE_TRANSFER_TYPE_CODE ||
        transactionCode == AUTHORIZED_PURCHASE_TRANSFER_TYPE_CODE) {
      imgUrl = "lib/assets/historyMemberIcWithBg.png";
    } else if (serviceCode != null &&
        transactionCode == PURCHASE_CASHCALL_SERVICES_TRANSFER_TYPE_CODE) {
      imgUrl = MFS_IMGS_BASE_URL + "services/" + serviceCode + ".png";
    } else {
      imgUrl = "lib/assets/historyAccountIcWithBg.png";
    }
    return imgUrl;
  }
}
