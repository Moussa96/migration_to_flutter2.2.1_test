import 'dart:collection';
import 'dart:convert';

import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/apis/Services/TransactionHistoryService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/response/transactionRes/GetTransHisResCustomElements.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:xml2json/xml2json.dart';


class TransactionHistory extends KFDrawerContent {

  TransactionHistory({Key key}) : super();

  @override
  _OldTransactionHistoryState createState() => _OldTransactionHistoryState();
}

class _OldTransactionHistoryState extends State<TransactionHistory> {
  var _transactions = [];
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  TransactionHistoryService transactionsHistoryService =
  new TransactionHistoryService();

  PaymentService paymentService = PaymentService();
  final Xml2Json xml2Json = Xml2Json();
  int _currentPage = 0;
  var memberType;
  bool _isLoading = true;
  bool _hasMore = true;

  @override
  void initState() {
    super.initState();

    _isLoading = true;
    _hasMore = true;
    Util.loadFromSharedPreferences(Util.GROUP).then((value) {
      memberType = value;
    });
    loadTransactionHistory().then((result) {
      setState(() {});
    });
  }

  loadTransactionHistory() async {
    _isLoading = true;
    List<GetTransHisResCustomElements> fetchedList;

    String accountID = await Util.loadFromSharedPreferences(Util.ACCOUNT_ID);
    Map<String, dynamic> params = new HashMap<String, dynamic>();
    params.putIfAbsent("currentPage", () => _currentPage);

    await transactionsHistoryService
        .getTransactionHistory(accountID, params)
        .then((value) => {
      if (value.data != null)
        {
          setState(() {
            fetchedList = value.data.elements;
            fetchedList.forEach((e) => print(e.toJson()));
          })
        },
    });

    if (fetchedList == null) {
      setState(() {
        _isLoading = false;
        _hasMore = false;
      });
    } else {
      setState(() {
        _currentPage++;
        print("_currentPage");
        print(_currentPage);
        _isLoading = false;
        _transactions.addAll(fetchedList);
      });
    }
  }

  Widget _buildEmptyTransaction() {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Text(
              getTranslated(context, 'transactionHistory_noTransaction'),
              style: TextStyle(
                fontSize: 18.0,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildTransactionCard(
      BuildContext context, GetTransHisResCustomElements transaction) {
    var memberName;
    var date = transaction.formattedDate;
    var formattedAmount = transaction.formattedAmount;
    var amount = transaction.amount;
    var id = transaction.transactionNumber;
    var transactionCode = transaction.transferType.code;
    var serviceNameEn;
    var serviceNameAr;
    if(transaction.customValues!=null) {
      transaction.customValues.forEach((cv) {
        if (cv.internalName == EXTERNAL_SERVICE_NAME_EN) {
          serviceNameEn = cv.value;
        }
        if (cv.internalName == EXTERNAL_SERVICE_NAME_AR) {
          serviceNameAr = cv.value;
        }
      });
    }
    var description = transactionCode != null ?
    getTranslated(context, transactionCode)
        :getTranslated(context, "DEFAULT_TRANSACTION_CODE");
    if (transaction.member != null) {
      memberName = transaction.member.name;
    } else {
      memberName = transaction.transferType.to.name;
    }
    bool canChargeBack = false;
    bool notPurchase = false;

    if ((transactionCode == PURCHASE_TRANSFER_TYPE_CODE || transactionCode == AUTHORIZED_PURCHASE_TRANSFER_TYPE_CODE) &&
        memberType == GROUP_MERCHANT)
      canChargeBack = true;
    else
      notPurchase = true;
    if (transaction.chargedBack)
      canChargeBack = false;
    else if (transaction.chargedBackOfTransferID != null) {
      canChargeBack = false;
      description = getTranslated(context, 'CHARGE_BACK');
    }

    Color cardBackground = canChargeBack || notPurchase
        ? Colors.white
        : Colors.blue[100].withOpacity(0.4);
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: canChargeBack ? 0.25 : 0,
      child: Card(
        margin: EdgeInsets.all(5.0),
        color: cardBackground,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Text(memberName.toString(), style: MAJOR_TEXT_STYLE),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            date.toString(),
                            style: TextStyle(color: MAJOR_COLOR),
                          ),
                          Text(
                            formattedAmount.toString(),
                            style: TextStyle(
                              color: amount < 0.0 ? Colors.red : Colors.green,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          description != null
                              ? Flexible(child: Text(description))
                              : Container(),
                          id != null
                              ? Flexible(
                              child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                        '${getTranslated(context, 'transactionID')} : '),
                                    Text(id)
                                  ]))
                              : Container(),
                        ],
                      ),
                      serviceNameEn != null?
                      Text(
                          Localization.of(context).locale.languageCode == 'en'?
                          serviceNameEn:serviceNameAr
                      ):Container()
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 5.0,
              )
            ],
          ),
        ),
      ),
      secondaryActions: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: IconSlideAction(
            caption: getTranslated(context, 'transactionHistory_chargeBack'),
            color: Colors.green[500],
            icon: Icons.done,
            onTap: () {
              print('charge back function$id');
              CustomWidgetBuilder.buildAlertDialog(
                  context,
                  chargeBack,
                  transaction,
                  '${getTranslated(context, 'transactionHistory_confirmChargeBackMessage')} ${transaction.transactionNumber}',
                  getTranslated(
                      context, 'transactionHistory_chargeBackConfirm'),
                  cancelButton: true,
                  labelCancelButton: getTranslated(
                      context, 'transactionHistory_cancelChargeBack'));
            },
          ),
        ),
      ],
    );
  }

  void chargeBack(BuildContext context, var transaction) {
    paymentService
        .chargeBack(SOAP_USERNAME, SOAP_PASSWORD, transaction.id.toString())
        .then((value) => {
      Navigator.pop(context),
      if (iscanChargeBack(value.body))
        {
          setState(() {
            transaction.chargedBack = true;
            _transactions = [];
            _currentPage = 0;
            _hasMore = true;
          }),
          loadTransactionHistory(),
          _scaffoldKey.currentState.showSnackBar(SnackBar(
              content: Text(getTranslated(
                  context, 'transactionHistory_successfulChargeBack'))))
        }
      else
        _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(getTranslated(
                context, 'transactionHistory_failedChargeBack'))))
    });
  }

  bool iscanChargeBack(String body) {
    xml2Json.parse(body);
    var jsonString = xml2Json.toParker();
    var data = jsonDecode(jsonString);
    print(data.toString());
    if (data['soap:Envelope']['soap:Body']['ns2:chargebackResponse']['return']
    ['status'] !=
        null &&
        data['soap:Envelope']['soap:Body']['ns2:chargebackResponse']['return']
        ['status'] ==
            'SUCCESS') return true;
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white70.withOpacity(0.9),
        key: _scaffoldKey,
        appBar: ActionBar(context,
            icon: Icons.arrow_back_ios,
            pressFunc: () => Navigator.pop(context)),
        body: Builder(builder: (context) {
          return Container(
            child: _transactions.isEmpty && !_hasMore
                ? _buildEmptyTransaction()
                : ListView.builder(
              itemCount: _hasMore
                  ? _transactions.length + 1
                  : _transactions.length,
              itemBuilder: (context, index) {
                // Uncomment the following line to see in real time how ListView.builder works
                // print('ListView.builder is building index $index');
                if (index >= _transactions.length) {
                  // Don't trigger if one async loading is already under way
                  if (!_isLoading) {
                    loadTransactionHistory();
                  }
                  return Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Center(
                      child: SizedBox(
                        child: CircularProgressIndicator(),
                        height: 24,
                        width: 24,
                      ),
                    ),
                  );
                }
                return _buildTransactionCard(
                    context, _transactions[index]);
              },
            ),
          );
        }));
  }
}
