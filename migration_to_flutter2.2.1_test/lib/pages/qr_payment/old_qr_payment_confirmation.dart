import 'dart:collection';

import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/QR.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/pages/transaction_summary/old_transaction_summary.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';

class QRPaymentConfirmation extends KFDrawerContent {
  QR? qr;
  QRPaymentConfirmation({this.qr, Key? key}) : super();
  @override
  _QRPaymentConfirmationState createState() => _QRPaymentConfirmationState();
}

class _QRPaymentConfirmationState extends State<QRPaymentConfirmation> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _amountController = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _loading = false;

  bool isNumeric(String? s) {
    if(s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  void confirmPayment(BuildContext context) {
    if(_formKey.currentState!.validate()){
      setState(() {
        _loading = true;
      });
      Payment payment = Payment(
          amount: double.parse(_amountController.text),
          toMemberId: widget.qr!.user!.id,
          transferTypeCode: widget.qr!.user!.groupCode == GROUP_CONSUMER?
          MONEY_TRANSFER_TYPE_CODE:PURCHASE_TRANSFER_TYPE_CODE
      );
      print(payment.toJson());
      PaymentService paymentService = PaymentService();
      paymentService.confirmMemberPayment(payment).then((value) {
        if (value.statusCode == 200) {
          Map<String, dynamic> paymentInfo = new HashMap();
          paymentInfo[getTranslated(context, "from")] =
              value.data!.fromMemberName;
          paymentInfo[getTranslated(context, "to")] = value.data!.toMemberName;
          paymentInfo[getTranslated(context, "total_amount")] = payment.amount;
          setState(() {
            _loading = false;
          });
          Navigator.push(
              context, MaterialPageRoute(builder: (context) =>
              TransactionSummary(paymentInfo))
          );
        } else {
          setState(() {
            _loading = false;
          });
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(getTranslated(context, "error_payment")),
            duration: Duration(seconds: 2))
          );
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: ActionBar(context,
          icon: Icons.arrow_back_ios, pressFunc: () => Navigator.pop(context)),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Text(
              getTranslated(context, "qr_payment_confirmation_msg")
                  + widget.qr!.user!.name!,
              style: TextStyle(fontSize: 30),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 30),
            Form(
              key: _formKey,
              child: TextFormField(
                keyboardType: TextInputType.numberWithOptions(
                    decimal: true),
                style: TextStyle(fontSize: 50, color: Colors.green),
                textAlign: TextAlign.center,
                showCursor: false,
                controller: _amountController,
                validator: (String? value) {
                  if (value == null || value.isEmpty || !isNumeric(value)) {
                    return getTranslated(
                        context,
                        "qr_payment_confirmation_amount_error"
                    );
                  }
                  return null;
                },
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "0.00",
                  hintStyle: TextStyle(fontSize: 50),
                ),
              ),
            ),
            SizedBox(height: 30),
            Row(
              children: [
                Expanded(
                  child: CustomWidgetBuilder.buildRaisedButton(context, confirmPayment, null,
                      getTranslated(context, "products_details_btn_confirm"),
                      size: MainAxisSize.max, isEnabled: !_loading),
                ),
                _loading ?
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue[700]!)),
                ): Container()
              ],
            )
          ],
        ),
      ),
    );
  }
}
