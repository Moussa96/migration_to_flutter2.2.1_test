import 'dart:convert';
import 'package:cashless/Configurations.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/OrderItems.dart';
import 'package:cashless/models/QR.dart';
import 'package:cashless/models/QRResults.dart';
import 'package:cashless/pages/product_details/old_products_details.dart';
import 'package:cashless/pages/qr_payment/qr_payment_confirmation.dart';
import 'package:cashless/util/QRGenerator.dart';
import 'package:cashless/util/Util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
class QRPayment extends StatefulWidget {
  QRPayment({Key? key}) : super();

  @override
  _QRPaymentState createState() => _QRPaymentState();
}

class _QRPaymentState extends State<QRPayment> {
  bool wrongQR = false;
  bool showDismiss = false;
  late Size size;
  @override
  void initState() {
    super.initState();
    scanQR(context);
  }

  void scanQR(BuildContext context) async {
    QRResults result;
    QRGenerator.scan(encrypt: false).then((value) async {
      result = value as QRResults;
      try {
        if (result.status == 1) {
          var content = await Util.decrypt(result.content);
          print(content);
          QR qr = QR.fromJson(jsonDecode(content));

          if (qr.qrType == DYNAMIC_QR) {
            dynamicQRFlow(qr);
          } else if (qr.qrType == STATIC_QR) {
            staticQRFlow(qr);
          } else {
            throw Exception("wrong qr");
          }
        } else {
          throw Exception("wrong qr");
        }
      } catch (e) {
        setState(() {
          showDismiss = true;
          wrongQR = true;
        });
      }
    });
  }

  void dynamicQRFlow(QR qr) {
    late OrderItems items;
    qr.payment!.customValues!.forEach((cv) {
      if (cv.internalName == ORDER_ITEMS) {
        items = OrderItems.fromJson(jsonDecode(cv.value!));
      }
    });
    Navigator.pop(context);
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProductsDetails(
                  orderItems: items,
                  amount: qr.payment!.amount,
                  // products: qr.payment.products,
                  merchantId: qr.payment!.toMemberId,
                  dynamicQRPayment: qr.payment,
                )));
  }

  void staticQRFlow(QR qr) {
    Navigator.pop(context);
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => QRPaymentConfirmation(qr: qr)));
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(wrongQR?'lib/assets/errorScanningQR.png':'lib/assets/scanQRBackground.png'),
                  fit: BoxFit.cover)),
          child: Column(children: [
            buildTopWidget(context),
            buildErrorWidget(context)
          ],)

        ));
  }

  Widget buildTopWidget(BuildContext context) {
    return Column(
      children: [
        Opacity(
          opacity: 0.72,
          child: Container(
            width: size.width,
            decoration: BoxDecoration(color: BLACK_COLOR),
            child: Column(
              children: [
                SizedBox(
                  height: size.height * 0.04,
                ),
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(11.0),
                    child: Row(
                      children: [
                        Image.asset('lib/assets/cancel_ic.png'),
                        SizedBox(
                          width: 8,
                        ),
                        Text(getTranslated(context, 'home_logout_cancel_btn'),
                            style: GoogleFonts.lato(
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontStyle: FontStyle.normal,
                                fontSize: 15))
                      ],
                    ),
                  ),
                ),
                Text(getTranslated(context, 'qr_payment_scan'),
                    style: GoogleFonts.lato(
                        color: Colors.white,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                        fontSize: 14),
                    textAlign: TextAlign.center),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 17,vertical: 10),
                  child: Text(
                      getTranslated(context, 'qr_payment_message'),
                      style: GoogleFonts.lato(
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          fontSize: 13),
                      textAlign: TextAlign.center),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
  Widget buildErrorWidget(BuildContext context){
    return showDismiss ? Opacity(
      opacity : 0.75,
      child:   Container(
        padding: EdgeInsets.symmetric(vertical: 15),
          width: size.width,
          decoration: BoxDecoration(
              color: RED_COLOR
          ),
        child: Row(
          children: [
            SizedBox(width: 10,),
            Image.asset('lib/assets/alert_ic.png'),
            SizedBox(width: 7,),
            Text(
                getTranslated(context, 'qr_payment_error_message'),
                style: const TextStyle(
                    color:  Colors.white,
                    fontWeight: FontWeight.w400,
                    fontFamily: "Lato",
                    fontStyle:  FontStyle.normal,
                    fontSize: 11.9
                ),
                textAlign: TextAlign.center
            ),
            Spacer(),
            InkWell(
              onTap: (){
                setState(() {
                  showDismiss = false;
                });
              },
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Text(
                    getTranslated(context, 'dismiss'),
                    style: GoogleFonts.lato(
                        color:  Colors.white,
                        fontWeight: FontWeight.w400,
                        fontStyle:  FontStyle.normal,
                        fontSize: 10.2
                    ),
                    textAlign: TextAlign.right
                ),
              ),
            ),
            SizedBox(width: 5,),
          ],
        ),
      ),
    ):Container();
  }

}
