import 'dart:convert';
import 'package:cashless/Configurations.dart';
import 'package:cashless/models/OrderItems.dart';
import 'package:cashless/models/QR.dart';
import 'package:cashless/models/QRResults.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/pages/product_details/old_products_details.dart';
import 'package:cashless/pages/qr_payment/old_qr_payment_confirmation.dart';
import 'package:cashless/util/QRGenerator.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class QRPayment extends KFDrawerContent {
  QRPayment({Key? key}) : super();

  @override
  _QRPaymentState createState() => _QRPaymentState();
}

class _QRPaymentState extends State<QRPayment> {
  bool wrongQR = false;

  @override
  void initState() {
    super.initState();
    scanQR(context);
  }

  void scanQR(BuildContext context) async {
    print('scan QR');
    QRResults result;
    QRGenerator.scan(encrypt: false).then((value) async{
      result = value as QRResults;
      try {
        if (result.status == 1) {
          var content = await Util.decrypt(result.content);
          print(content);
          QR qr = QR.fromJson(jsonDecode(content));

          if (qr.qrType == DYNAMIC_QR) {
            dynamicQRFlow(qr);
          }
          else if (qr.qrType == STATIC_QR) {
            staticQRFlow(qr);
          }
          else {
            throw Exception("wrong qr");
          }
        }
        else {
          throw Exception("wrong qr");
        }
      }
      catch(e){
        setState(() {
          wrongQR = true;
        });
      }
     });
  }

  void dynamicQRFlow(QR qr){
    OrderItems? items;
    qr.payment!.customValues!.forEach((cv) {
      if(cv.internalName == ORDER_ITEMS){
        items = OrderItems.fromJson(jsonDecode(cv.value!));
      }
    });
    Navigator.pop(context);
    Navigator.push(context, MaterialPageRoute(
      builder: (context) =>
        ProductsDetails(
          orderItems: items,
          amount: qr.payment!.amount,
          // products: qr.payment.products,
          merchantId: qr.payment!.toMemberId,
          dynamicQRPayment: qr.payment,
        )
    ));
  }

  void staticQRFlow(QR qr){
    Navigator.pop(context);
    Navigator.push(context, MaterialPageRoute(
      builder: (context) =>
       QRPaymentConfirmation(qr: qr)
    ));
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.grey[300],
        appBar: ActionBar(context, icon: Icons.arrow_back_ios,pressFunc:  () => Navigator.pop(context)),
        body: wrongQR?
        Card(
          elevation: 1,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          margin: EdgeInsets.all(20),
          child: Column(
            children: [
              SizedBox(height: 40),
              Icon(Icons.cancel, color: Colors.red, size: 200),
              SizedBox(height: 30),
              Text("Wrong QR Code!", style: TextStyle(fontSize: 40), textAlign: TextAlign.center),
              Text("Please try again.", style: TextStyle(fontSize: 32), textAlign: TextAlign.center),
              SizedBox(height: 40),

              Padding(
                padding: const EdgeInsets.all(15.0),
                child: CustomWidgetBuilder.buildRaisedButton(context, scanQR, null, "Scan Again", size: MainAxisSize.max),
              )
            ],
          ),
        ):Container()
    );
  }
}
