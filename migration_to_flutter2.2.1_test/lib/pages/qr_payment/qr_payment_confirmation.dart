import 'dart:collection';

import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/QR.dart';
import 'package:cashless/pages/transaction_summary/transaction_summary.dart';
import 'package:cashless/pages/rejected_transaction.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:cashless/widgets/custom_widgets/CustomAppBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
class QRPaymentConfirmation extends StatefulWidget {
  QR? qr;
  QRPaymentConfirmation({this.qr, Key? key}) : super();
  @override
  _QRPaymentConfirmationState createState() => _QRPaymentConfirmationState();
}

class _QRPaymentConfirmationState extends State<QRPaymentConfirmation> {
  late Size size;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _amountController = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _loading = false;


  void confirmPayment(BuildContext context) {
    if(_formKey.currentState!.validate()){
      setState(() {
        _loading = true;
      });
      Payment payment = Payment(
          amount: double.parse(_amountController.text),
          toMemberId: widget.qr!.user!.id,
          transferTypeCode: widget.qr!.user!.groupCode == GROUP_CONSUMER?
          MONEY_TRANSFER_TYPE_CODE:PURCHASE_TRANSFER_TYPE_CODE
      );
      PaymentService paymentService = PaymentService();
      Map<String, dynamic> paymentInfo = new HashMap();
      paymentService.confirmMemberPayment(payment).then((value) {
        if (value.statusCode == 200) {
          paymentInfo[getTranslated(context, "from")] =
              value.data!.fromMemberName;
          paymentInfo[getTranslated(context, "receiver")] = value.data!.toMemberName;
          paymentInfo[getTranslated(context, "amount")] = payment.amount;
          paymentInfo[getTranslated(context, "confirm_service_reference")] = value.data!.transactionNumber;
          setState(() {
            _loading = false;
          });
          Navigator.push(
              context, MaterialPageRoute(builder: (context) =>
              TransactionSummary(transcationResponse:paymentInfo))
          );
        } else {
          setState(() {
            _loading = false;
          });
          paymentInfo[getTranslated(context, "amount")] = payment.amount;
          paymentInfo[getTranslated(context, "receiver")] = widget.qr!.user!.name;
          Navigator.push(
              context, MaterialPageRoute(builder: (context) =>
              RejectedTransaction(transcation:paymentInfo,errorMessage:getTranslated(context, "rejected_transaction_message"),hideBalance: true,))
          );
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: BACKGROUND_COLOR,
      key: _scaffoldKey,
      appBar: CustomAppBar(title:getTranslated(context,'qr_payment_confirmation_title')),
      body:buildBody(),
    );
  }
  Widget buildMessageWidget(){
    return RichText(
        textScaleFactor: 1.1,
        textAlign: TextAlign.center,
        text: TextSpan(
            children: [
              TextSpan(
                  style: GoogleFonts.lato(
                      color:  PRIMARY_COLOR,
                      fontWeight: FontWeight.w400,
                      fontStyle:  FontStyle.normal,
                      fontSize: 14
                  ),
                  text: getTranslated(context, 'qr_payment_confirmation_message_part_one')),
              TextSpan(
                  style:GoogleFonts.lato(
                      color:  PRIMARY_COLOR,
                      fontWeight: FontWeight.w700,
                      fontStyle:  FontStyle.normal,
                      fontSize: 14
                  ),
                  text: widget.qr!.user!.name),
              TextSpan(
                  style: GoogleFonts.lato(
                      color:  PRIMARY_COLOR,
                      fontWeight: FontWeight.w400,
                      fontStyle:  FontStyle.normal,
                      fontSize: 14
                  ),
                  text: getTranslated(context, 'qr_payment_confirmation_message_part_two'))
            ]
        )
    );
  }
  Widget buildAmountInputForm(){
    return TextFormField(
      keyboardType: TextInputType.numberWithOptions(
          decimal: true),
      style: TextStyle(fontSize: 50, color: PRIMARY_COLOR ),
      textAlign: TextAlign.center,
      showCursor: false,
      controller: _amountController,
      validator: (String? value) {
        if (value == null || value.isEmpty || !Util.isNumeric(value)) {
          return getTranslated(
              context,
              "qr_payment_confirmation_amount_error"
          );
        }
        return null;
      },
      decoration: InputDecoration(
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(3.408)),
          borderSide: BorderSide(
            color: SECONDARY_COLOR,
            width: 0.9,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(3.408)),
          borderSide: BorderSide(
            color: BORDER_COLOR,
            width: 0.9,
          ),
        ),
        hintText: "0.00",
        hintStyle: GoogleFonts.lato(fontSize: 50,color: CASHCALL_TITLE),
      ),
    );
  }
  Widget buildForm(){
    return Form(
      key: _formKey,
      child: Container(
        width: size.width*0.6,
        child: buildAmountInputForm(),
      ),
    );
  }
  Widget buildButton(){
    return Row(
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: CustomWidgetBuilder.buildRaisedButton(context, confirmPayment, null,
                getTranslated(context, "send"),
                size: MainAxisSize.max, isEnabled: !_loading,color: SECONDARY_COLOR),
          ),
        ),
        _loading ?
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(SECONDARY_COLOR)),
        ): Container()
      ],
    );
  }
  Widget buildBody(){
    return ListView(
      children: [
        Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: buildMessageWidget(),
              ),
              SizedBox(height: 30),
              Text(
                  getTranslated(context,'amount'),
                  style: GoogleFonts.lato(
                      color:  TEXT_PRIMARY_COLOR,
                      fontWeight: FontWeight.w700,
                      fontStyle:  FontStyle.normal,
                      fontSize: 14
                  )
              ),
              SizedBox(height: 8),
              buildForm(),
              SizedBox(height: 5),
              Text(
                  getTranslated(context, 'egyption_pound'),
                  style: GoogleFonts.lato(
                      color:  CURRENCY_HINT_COLOR,
                      fontWeight: FontWeight.w300,
                      fontStyle:  FontStyle.normal,
                      fontSize: 10.2
                  ),
                  textAlign: TextAlign.center
              ),
              SizedBox(height: 100),
              buildButton()
            ],
          ),
        ),
      ],
    );
  }
}
