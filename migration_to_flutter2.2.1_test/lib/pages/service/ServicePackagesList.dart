import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/services/MFS_Service.dart';
import 'package:cashless/models/services/Package.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/pages/service/serviceDispatcher.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ServicePackagesList extends StatefulWidget{
  MFS_Service service;
  ServicePackagesList(this.service,{Key? key}) : super(key: key);
  @override
  _ServicePackagesListState createState() => _ServicePackagesListState();
}
class _ServicePackagesListState extends State<ServicePackagesList> {
  late List<Package> _filteredPackagesList,_packagesList;
  var _packagesCount ;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initServicesList();
  }
  void initServicesList(){
    _packagesCount = widget.service.packages.length;
    _packagesList = widget.service.packages;
    _filteredPackagesList = widget.service.packages;
  }
  Widget _buildEmptyCard() {
    return Card(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Text(
          getTranslated(context, 'servicePackageList_nopackage'),
          style: TextStyle(fontSize: 28.0),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  _searchBar() {
    return Padding(
        padding: const EdgeInsets.all(5),
        child:
        TextField(
          decoration: InputDecoration(hintText: getTranslated(context, 'servicePackageList_search'),
            suffixIcon: Icon(Icons.search,color: Colors.blue[700]),
            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.blue[700]!,width: 2)),
            enabledBorder: OutlineInputBorder(borderSide: BorderSide(width: 0)),
          ),
          onChanged: (input) {
            setState(() {
              _filteredPackagesList= [];
              if(input==""){
                _filteredPackagesList = _packagesList;
              }else{
                for( int i=0;i<_packagesList.length;i++){
                  if(_packagesList[i].packageName.toLowerCase().contains(input.toLowerCase())){
                    _filteredPackagesList.add(_packagesList[i]);
                  }
                }
                print(_filteredPackagesList);
              }
              _packagesCount = _filteredPackagesList.length;
            });
          },
        )
    );
  }

  _gridView() {
    return Expanded(child: GridView.count(
        crossAxisCount: 3,
        children: List.generate(_packagesCount, (index) {
          return Card(
              color: Colors.white,
              margin: EdgeInsets.all(5),
              child:  InkWell(
                  highlightColor: Colors.white60,
                  onTap: () {
                    Navigator.push(context,  MaterialPageRoute(builder: (context) => ServiceDispatcher(widget.service,package: _packagesList[index])));

                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // ClipRRect(
                      // borderRadius: BorderRadius.circular(80.0),
                      // child:
                      // Image.network(
                      //     "https://buyselllet.ae/assets/developerimages/thumbs/img-not-found.png", height:50
                      // ),
                    //),
                      Text(Localization.of(context).locale.languageCode=='en'?_packagesList[index].packageName:_packagesList[index].packageArabicName,style: TextStyle(fontWeight: FontWeight.w500),textAlign: TextAlign.center),
                    ],
                  ))
          );
        }))
    );
  }

  Widget _buildServicesGrid(BuildContext context){
    return _packagesCount == 0 ?
    Column(
        children: [
          _searchBar()
          ,_buildEmptyCard()]
    )
        :_packagesCount == null ?
    CustomWidgetBuilder.buildSpinner(context)
        :Column(
        children: [
          _searchBar()
          ,_gridView()]
    );
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white70.withOpacity(0.9),
        appBar: ActionBar(context, titleText: getTranslated(context, 'servicePackagesList_title'), icon: Icons.arrow_back_ios,pressFunc:  () => Navigator.pop(context)),
        body: _buildServicesGrid(context)
    );
  }
}