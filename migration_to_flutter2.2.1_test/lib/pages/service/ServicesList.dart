import 'dart:convert';

import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/services/MFS_Service.dart';
import 'package:cashless/models/services/ServiceProvider.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/pages/service/serviceDispatcher.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'ServicePackagesList.dart';

class ServicesList extends StatefulWidget{
  ServiceProvider serviceProvider;
  ServicesList(this.serviceProvider,{Key? key}) : super(key: key);
  @override
  _ServicesListState createState() => _ServicesListState();

}
class _ServicesListState extends State<ServicesList> {
  late List<MFS_Service> _filteredServicesList,_servicesList;
  var _servicesCount ;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initServicesList();
  }
  void initServicesList(){
    print(widget.serviceProvider);
    _servicesCount = widget.serviceProvider.services.length;
    _servicesList = widget.serviceProvider.services;
    _filteredServicesList = widget.serviceProvider.services;
  }
  Widget _buildEmptyCard() {
    return Card(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Text(
          getTranslated(context, 'serviceList_noServices'),
          style: TextStyle(fontSize: 28.0),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }


  _searchBar() {
    return Padding(
        padding: const EdgeInsets.all(5),
        child:
        TextField(
          decoration: InputDecoration(hintText: getTranslated(context, 'serviceList_search'),
            suffixIcon: Icon(Icons.search,color: Colors.blue[700]),
            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.blue[700]!,width: 2)),
            enabledBorder: OutlineInputBorder(borderSide: BorderSide(width: 0)),
          ),
          onChanged: (input) {
            setState(() {
              _filteredServicesList= [];
              if(input==""){
                _filteredServicesList = _servicesList;
              }else{
                for( int i=0;i<_servicesList.length;i++){
                  if(_servicesList[i].serviceName.toLowerCase().contains(input.toLowerCase())){
                    _filteredServicesList.add(_servicesList[i]);
                  }
                }
                print(_filteredServicesList);
              }
              _servicesCount = _filteredServicesList.length;
            });
          },
        )
    );
  }

  _gridView() {
    return Expanded(child: GridView.count(
        crossAxisCount: 3,
        children: List.generate(_servicesCount, (index) {
          return Card(
              color: Colors.white,
              margin: EdgeInsets.all(5),
              child:  InkWell(
                  highlightColor: Colors.white60,
                  onTap: () {
                    if(_servicesList[index].packages != null && _servicesList[index].packages.length > 0)
                      Navigator.push(context,  MaterialPageRoute(builder: (context) => ServicePackagesList(_servicesList[index])));
                    else
                      Navigator.push(context,  MaterialPageRoute(builder: (context) => ServiceDispatcher(_servicesList[index])));
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                    // ClipRRect(
                    //   borderRadius: BorderRadius.circular(80.0),
                    //   child: Image.network(
                    //       "https://buyselllet.ae/assets/developerimages/thumbs/img-not-found.png", height:40
                    //   ),
                    // ),
                      Text(Localization.of(context).locale.languageCode=='en'?_servicesList[index].serviceName:_servicesList[index].serviceArabicName,style: TextStyle(fontWeight: FontWeight.w500),textAlign: TextAlign.center),
                    ],
                  ))
          );
        }))
    );
  }

  Widget _buildServicesGrid(BuildContext context){
    return _servicesCount == 0 ?
    Column(
        children: [
          _searchBar()
          ,_buildEmptyCard()]
    )
        :_servicesCount == null ?
    CustomWidgetBuilder.buildSpinner(context)
        :Column(
        children: [
          _searchBar()
          ,_gridView()]
    );
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white70.withOpacity(0.9),
        appBar: ActionBar(context, titleText: getTranslated(context, 'servicesList_title'), icon: Icons.arrow_back_ios,pressFunc:  () => Navigator.pop(context)),
        body: _buildServicesGrid(context)
    );
  }
}