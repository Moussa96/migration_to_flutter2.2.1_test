import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/services/MFS_Service.dart';
import 'package:cashless/models/services/Package.dart';
import 'package:cashless/models/services/ServiceScenario.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:flutter/material.dart';

import 'ServiceForm.dart';

class ServiceDispatcher extends StatefulWidget {
  MFS_Service service;
  Package? package;
  ServiceDispatcher(this.service,{Key? key,this.package}) : super(key: key);

  @override
  _ServiceDispatcherState createState() => _ServiceDispatcherState();
}
class _ServiceDispatcherState extends State<ServiceDispatcher> {
  late String scenario;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    scenario = widget.service.serviceScenario.scenarioID;

  }
  // TODO: remove "?"
  Widget? _buildServiceForm(){
    if(scenario == ServiceScenario.BILL)  ;// bill form
    else if(scenario == ServiceScenario.CHARGE) {
      //return ServiceForm(widget.service);
    }
    else if(scenario == ServiceScenario.CHARGE_PACKAGES) {
      //return ServiceForm(widget.service);
    }
    else if(scenario == ServiceScenario.VOUCHER)  ;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white70.withOpacity(0.9),
        appBar: ActionBar(context, titleText: getTranslated(context, 'servicesList_title'), icon: Icons.arrow_back_ios,pressFunc:  () => Navigator.pop(context)),
        body: _buildServiceForm()
    );
  }
}