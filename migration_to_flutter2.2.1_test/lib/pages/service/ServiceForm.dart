/*
import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'package:cashless/apis/MFSIntegrationService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/CustomValue.dart';
import 'package:cashless/models/services/SystemPayment.dart';
import 'package:cashless/models/services/MFS_Service.dart';
import 'package:cashless/models/services/ScenarioAccount.dart';
import 'package:cashless/models/services/ScenarioParameter.dart';
import 'package:cashless/models/services/MFS_Value.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ServiceForm extends KFDrawerContent {
  MFS_Service mfs_service;
  ServiceForm(this.mfs_service,{Key key}) : super();
  @override
  _ServiceFormState createState() => _ServiceFormState();
}
class _ServiceFormState extends State<ServiceForm> {
  final _formKey = GlobalKey<FormState>();
  CustomValue customValue;
  SystemPayment serviceData = SystemPayment();
  TextEditingController _controller = TextEditingController();
  MFSIntegrationService mfsIntegrationService = MFSIntegrationService();
  List<TextEditingController> _controllers = new List();
  ScenarioAccount _accountField;
  List<ScenarioParameter> _trxFields = List();
  @override
  initState() {
    super.initState();
    setState(() {
      _accountField = widget.mfs_service.serviceScenario.scenarioAccount;
      _trxFields = widget.mfs_service.serviceScenario.transactionParam;
    });
  }

  requestTransactionDetails(id) async{
    var customVals;
    await mfsIntegrationService.getTransactionDetails(id).then((response) async {
      customVals = await jsonDecode(response.body)['accountHistoryTransfer']['customValues'];
      if(customVals.length == 1){
        print('trying again');
        Timer(Duration(seconds: 2), () {
          requestTransactionDetails(id);
        });
      }
    });
    return customVals;
  }


  validate(BuildContext context) {
    MFS_Value value = MFS_Value();
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      value.serviceProviderCode = widget.mfs_service.serviceProviderCode;
      value.serviceCode = widget.mfs_service.serviceCode;
      value.externalRefNumber= Random().nextInt(99999999).toString();
      final f = new DateFormat('MMM dd yyyy hh:mm:ss a');
      value.externalTxnDateTime = f.format(DateTime.now()).toString();
      value.externalTxnId = 'sts.'+widget.mfs_service.serviceCode+Random().nextInt(999999999).toString();
      value.userB_AccountId = _controller.text;
      value.inParameters = {};
      value.transactionValue = _controllers[0].text;
      // for(var i = 0; i<_trxLength; i++){
      //   print(_trxFields[i].jsonId);
      //   value.transactionValue[_trxFields[i].jsonId] = _controllers[i].text;
      // }
      customValue = CustomValue('transcationRequest', value.toString());
      serviceData.transferTypeId = '52696';
      serviceData.amount = double.parse(_controllers[0].text);
      serviceData.customValues.add(customValue);
      // mfsIntegrationService.submitServiceData(serviceData).then((response) {
      //   if(response == null || response.statusCode != 200){
      //     Scaffold.of(context).showSnackBar(SnackBar(content: Text(getTranslated(context, "registeration_snack_error"))));
      //   }else{
      //     String _id = jsonDecode(response.body)['id'].toString();
      //     requestTransactionDetails(_id).then((res){
      //       print(res);
      //     });
      //
      //
      //     Map <String, String> summary = new HashMap();
      //     summary['To'] = widget.mfs_service.serviceProviderName;
      //     summary['Amount'] = _controllers[0].text;
      //     summary['Mobile Number'] = _controller.text;
      //     //Navigator.push(context,  MaterialPageRoute(builder: (context) => TransactionSummary(summary)));
      //   }
      // });
      String _id = "52820";
      requestTransactionDetails(_id).then((res){
        print(res[1]);
      });
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(content: Text(getTranslated(context, "registeration_snack_error"))));
    }
  }

  @override
  Widget build(BuildContext context) {
    var accountLabel, accountErr;
    ScenarioAccount account = ScenarioAccount(_accountField.label, _accountField.labelEn,_accountField.regex, _accountField.error,_accountField.errorEn);

    if(Localization.of(context).locale.languageCode == 'en'){
      accountLabel = account.labelEn;
      accountErr = account.errorEn;
    }else{
      accountLabel = account.label;
      accountErr = account.error;
    }
    RegExp accountRegex = new RegExp(account.regex!=null?account.regex:r'');

    return Builder(builder: (BuildContext context) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                child: Center(
                    child: CustomWidgetBuilder.buildText(
                        Localization.of(context).locale.languageCode == 'en' ?
                        widget.mfs_service.serviceName
                        :widget.mfs_service.serviceArabicName, fontSize: 20)),
              ),
              Padding(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: CustomWidgetBuilder.buildFormInputText(
                    controller: _controller,
                      hint: accountLabel,
                      expression: accountRegex,
                      errorMessage: accountErr,
                      expErrorMessage: accountErr)
              ),
              Flexible(
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: _trxFields!=null? _trxFields.length: 0,
                      itemBuilder: (context, i) {
                        _controllers.add(new TextEditingController());
                        return Padding(
                          padding: EdgeInsets.symmetric(
                          vertical: 10, horizontal: 10),
                            child:
                            CustomWidgetBuilder.buildFormInputText(
                                hint: Localization.of(context).locale.languageCode == 'en' ? _trxFields[i].labelEn : _trxFields[i].label,
                                //expression: RegExp(_trxFields[i].regex),
                                inputType: _trxFields[i].type=='n'? TextInputType.number:TextInputType.text,
                                controller: _controllers[i],
                                errorMessage: Localization.of(context).locale.languageCode == 'en' ? _trxFields[i].errorEn : _trxFields[i].error,
                                expErrorMessage: Localization.of(context).locale.languageCode == 'en' ? _trxFields[i].errorEn : _trxFields[i].error
                            )
                        );
                  })
              ),
              Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 16.0, horizontal: 20),
                      child: CustomWidgetBuilder.buildRaisedButton(
                          context, validate, null,
                          getTranslated(context, 'purchase_btn_submit'),
                          size: MainAxisSize.min),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }
    );
  }
}
*/