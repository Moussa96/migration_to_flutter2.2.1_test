import 'dart:convert';

import 'package:cashless/apis/Services/MFSIntegrationService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/services/ServiceProvider.dart';
import 'package:cashless/models/services/ServicesResponse.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/pages/service/ServicesList.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ServiceProviderList extends KFDrawerContent{
  @override
  _ServiceProviderFormState createState() => _ServiceProviderFormState();
}
class _ServiceProviderFormState extends State<ServiceProviderList> {

  MFSIntegrationService mfsIntegrationService = MFSIntegrationService();
  late List<ServiceProvider> _servicesProvider , _filteredServicesProvider ;
  var _servicesCount ;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getServicesProvider();
  }
  Widget _buildEmptyCard() {
    return Card(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Text(
          getTranslated(context, 'serviceProvider_noServices'),
          style: TextStyle(fontSize: 28.0),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  _searchBar() {
    return Padding(
        padding: const EdgeInsets.all(5),
        child:
        TextField(
          decoration: InputDecoration(hintText: getTranslated(context, 'serviceProvider_search'),
            suffixIcon: Icon(Icons.search,color: Colors.blue[700]),
            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.blue[700]!,width: 2)),
            enabledBorder: OutlineInputBorder(borderSide: BorderSide(width: 0)),
          ),
          onChanged: (input) {
            setState(() {
              _filteredServicesProvider= [];
              if(input==""){
                _filteredServicesProvider = _servicesProvider;
              }else{
                for( int i=0;i<_servicesProvider.length;i++){
                  if(_servicesProvider[i].serviceProviderName.toLowerCase().contains(input.toLowerCase())){
                    _filteredServicesProvider.add(_servicesProvider[i]);
                  }
                }
                print(_filteredServicesProvider);
              }
              _servicesCount = _filteredServicesProvider.length;
            });
          },
        )
    );
  }

  _gridView() {
    return Expanded(child: GridView.count(
        crossAxisCount: 2,
        children: List.generate(_servicesCount, (index) {
          String path = 'lib/assets/${_servicesProvider[index].serviceProviderCode}.jpg';
          return Card(
              color: Colors.white,
              margin: EdgeInsets.all(5),
              child:  InkWell(
                  highlightColor: Colors.white60,
                  onTap: () {
                    Navigator.push(context,  MaterialPageRoute(builder: (context) => ServicesList(_servicesProvider[index])));
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [                      Container(
                  decoration: BoxDecoration(
                  shape: BoxShape.circle,
                    border: Border.all(color: Colors.white, width: 2),
                  ),
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  backgroundImage:
                  AssetImage(
                    path,
                  ),
                  radius: 40.0,
                ),
              ),
                      Center(
                          //padding: EdgeInsets.fromLTRB(0, 10, 0, 5),
                          child: Text(Localization.of(context).locale.languageCode=='en'?_servicesProvider[index].serviceProviderName:_servicesProvider[index].serviceProviderArabicName,style: TextStyle(fontWeight: FontWeight.w500))
                      ),
                    ],
                  ))
          );
        }))
    );
  }
  Widget _buildServicesGrid(BuildContext context){
    return _servicesCount == 0 ?
    Column(
        children: [
          _searchBar()
          ,_buildEmptyCard()]
    )
        :_servicesCount == null ?
    CustomWidgetBuilder.buildSpinner(context)
        :Column(
        children: [
          _searchBar()
          ,_gridView()]
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white70.withOpacity(0.9),
        appBar: ActionBar(context, titleText: getTranslated(context, 'serviceProvider_title'), icon: Icons.arrow_back_ios,pressFunc:  () => Navigator.pop(context)),
         body: _buildServicesGrid(context)

    );
  }
  void getServicesProvider(){
    mfsIntegrationService.getServices().then((value) =>
        setState(() {
          ServicesResponse servicesResponse = ServicesResponse.fromJson(jsonDecode(value));
          _servicesCount = servicesResponse.serviceProviders.length;
          _servicesProvider = servicesResponse.serviceProviders;
          _filteredServicesProvider = servicesResponse.serviceProviders;
        })
    );
  }
}
