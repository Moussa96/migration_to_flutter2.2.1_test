import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/ContactService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/widgets/custom_widgets/CustomAppBar.dart';
import 'package:cashless/widgets/custom_widgets/CustomButton.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:cashless/widgets/custom_widgets/CustomTextFormField.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ForgetPassword extends StatefulWidget {
  ForgetPassword({Key? key}) : super(key: key);

  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  ContactService contactService = ContactService();
  TextEditingController usernameController = TextEditingController();
  bool passwordChanged = false,error=false;
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: scaffoldKey,
        appBar: CustomAppBar(
          title: getTranslated(context, 'forget_password_title'),
        ),
        backgroundColor: BACKGROUND_COLOR,
        body: ListView(
          children: [
            Column(
              children: [
                Image.asset('lib/assets/forget_password_graphics_with_bg.png'),
                Padding(
                  padding: const EdgeInsets.all(10.3),
                  child: CustomWidgetBuilder.buildText(
                      getTranslated(context, 'forget_password_message'),
                      color: TEXT_PRIMARY_COLOR,
                      fontWeight: FontWeight.w400,
                      fontStyle: FontStyle.normal,
                      fontSize: 11.9),
                ),
                SizedBox(
                  height: 11,
                ),
                Form(
                  key: _formKey,

                  child: Column(children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.3),
                      child: CustomTextFormField(
                        controller: usernameController,
                        label: getTranslated(context, 'register_number'),
                        inputType: TextInputType.text,
                        errorMessage: getTranslated(context, 'login_username_required'),
                      ),

                    ),
                    passwordChanged?Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10.3,vertical: 4),
                      child: CustomWidgetBuilder.buildText(getTranslated(context,'forget_password_successful_message'),
                          color: SECONDARY_COLOR,fontSize: 11,textAlign: TextAlign.start),
                    ):Container()
                  ],)
                ),
                SizedBox(
                  height: 72.4,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.3),
                  child: CustomButton(
                    text: passwordChanged ? getTranslated(context, 'go_to_login'):getTranslated(context, 'submit_btn'),
                    color: SECONDARY_COLOR,
                    textColor: Colors.white,
                    onPressed: passwordChanged? goToLogin: recoverPassword,
                  ),
                ),
                SizedBox(
                  height: 11,
                ),
              ],
            ),
          ],
        ));
  }

  void recoverPassword() async{
    //print('recover password');
    if (_formKey.currentState != null) {
      if (_formKey.currentState!.validate()) {
        //print('valid');
      }
      _formKey.currentState!.save();
      await contactService.resetPassword(usernameController.text).then((value) => {
        if(value.statusCode == 200 && value.data != null){
          setState((){
            passwordChanged = true;
          })
        }else{
          CustomWidgetBuilder.buildSnackBar(scaffoldKey, context, getTranslated(context,'forget_password_error_message'))
        }
      });
    }
    else print("_formKey.currentState is null");
  }

    void goToLogin(){
    Navigator.pop(context);
  }
}
