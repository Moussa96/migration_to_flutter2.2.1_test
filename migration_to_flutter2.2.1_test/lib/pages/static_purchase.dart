import 'dart:async';
import 'dart:collection';
import 'dart:core';
import 'dart:io';
import 'package:cashless/apis/Services/ContactService.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/Configurations.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/OrderItems.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/QRResults.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/pages/confirm_payment.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/util/NFCUtil.dart';
import 'package:cashless/util/QRGenerator.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'fragments/action_bar.dart';

class Purchase extends KFDrawerContent {
  String purchaseType;
  bool isProductPurchase;
  double amount;
  OrderItems orderItems = OrderItems();
  Purchase({this.orderItems, this.purchaseType, this.isProductPurchase = false, this.amount, Key key}) : super();
  @override
  _PurchaseState createState() => _PurchaseState();
} 

class _PurchaseState extends State<Purchase> {
  Map<String, dynamic> purchaseInfo = new HashMap();
  final utility = QRGenerator();
  TextEditingController _mobileNumberController = TextEditingController();
  TextEditingController _amountController = TextEditingController();
  TextEditingController _descriptionController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  Payment payment = Payment();
  PaymentService paymentService = PaymentService();
  ContactService contactsService = ContactService();
  User user = User();
  bool _enableMobileNumber = true;
  String userName;
  String password;
  String userId;
  String onBehalfOfMemberId;
  Map<dynamic, dynamic> cashOutInfo = new HashMap();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    payment.transferTypeCode = PURCHASE_TRANSFER_TYPE_CODE;
    payment.paymentMethod = widget.purchaseType;
    setPINState();
  }

  void setPINState(){
    if(widget.purchaseType != 'pin') {
      setState(() {
        _enableMobileNumber = false;
      });
    }
  }

  void scanQR(BuildContext context) async {
    QRResults result ;
    QRGenerator.scan().then((value) async{
      result = value as QRResults;
      String userIdentifier = result.content;
      user = await contactsService.identifyUser(userIdentifier);
      if(user != null){
        setState(() {
          _mobileNumberController.text = user.username;
          _enableMobileNumber = false;
          //onBehalfOfMemberId = user.id;
        });
      }else{
        Scaffold.of(context).showSnackBar(SnackBar(content: Text(getTranslated(context,"USER_NOT_FOUND")), duration: Duration(seconds: 2)));
      }
    });
  }

  void readNFC(BuildContext context) async {
    if (Platform.isAndroid) {
      showDialog(context: context,
          builder: (context) => AlertDialog(
              title: Text(getTranslated(context, "SCAN_CARD")),
              actions: <Widget>[
                FlatButton(
                  child: Text(getTranslated(context, "OK")),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                )]
          )
      );
    }
    await NFCUtil.startScanning(context, getNFCData);
  }

  void getNFCData(String userIdentifier) async{
    user = await contactsService.identifyUser(userIdentifier);
    if(user != null){
      setState(() {
        payment.onbehalfOfMemberId = user.id;
        _mobileNumberController.text = user.username;
        Navigator.of(context, rootNavigator: true).pop('dialog');
      });
    }else{
      Scaffold.of(context).showSnackBar(SnackBar(content: Text(getTranslated(context,"USER_NOT_FOUND")), duration: Duration(seconds: 2)));
    }
  }

  showAlertDialog(BuildContext context){
    AlertDialog alert=AlertDialog(
        content: new Row(
            children: [
              CircularProgressIndicator(backgroundColor: Colors.blue),
              Container(margin: EdgeInsets.only(left: 5),child:Text(getTranslated(context, "loading"))),
            ])
    );
    showDialog(barrierDismissible: false, context:context, builder:(BuildContext context){
        return alert;
      },
    );
  }

  void validatePayment(BuildContext context){
    if(_formKey.currentState.validate()) {
      _formKey.currentState.save();
      Util.loadFromSharedPreferences(Util.USER_ID).then((result) {
        setState(() {
          payment.toMemberId = result;
        });
      });
      payment.amount = widget.isProductPurchase ? widget.amount:double.parse(_amountController.text);
      user.username = _mobileNumberController.text;
      if(payment.paymentMethod == Payment.QR_Payment_Method || payment.paymentMethod == Payment.PIN_Payment_Method) {

          Navigator.push(context, MaterialPageRoute(builder: (context) => ConfirmPayment(payment: payment,user:user, orderItems: widget.orderItems)));
      }
      else if(payment.paymentMethod == Payment.VIP_Payment_Method){
        Navigator.push(context, MaterialPageRoute(builder: (context) => ConfirmPayment(payment: payment,user:user, isVIP: true)));
      }
      else{
        showAlertDialog(context);
        paymentService.doPayment(context, payment, user, paymentStatus: paymentDone);
      }
    }
  }

  void paymentDone(){
    Timer(Duration(seconds: 1), () {
      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ActionBar(context, icon: Icons.arrow_back_ios,pressFunc:  () => Navigator.pop(context)),
      body: Builder(
        builder: (BuildContext context) {
          return Container(
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20.0),
                      child: Center(
                          child: Text(getTranslated(context, 'purchase_title')+" "+widget.purchaseType.toUpperCase(),
                            style: TextStyle(
                              fontSize: 30,
                              color: Colors.black54,
                            ),
                          )),
                    ),
                    widget.purchaseType == 'qr' || widget.purchaseType == 'vip'? Container(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 16.0, horizontal: 20),
                        child: CustomWidgetBuilder.buildRaisedButton(
                            context, scanQR, null, getTranslated(context, 'purchase_btn_scan_qr'),
                            size: MainAxisSize.max),
                      ),
                    )
                        :widget.purchaseType=='nfc'? Container(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 16.0, horizontal: 20),
                        child: CustomWidgetBuilder.buildRaisedButton(
                            context, readNFC, null, getTranslated(context, 'purchase_btn_scan_nfc'),
                            size: MainAxisSize.max),
                      ),
                    )
                        :Container(),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      child: CustomWidgetBuilder.buildFormInputText(controller: _mobileNumberController,
                          hint: getTranslated(context, 'purchase_mobile_num'),maxLength: 11,inputType: TextInputType.number,
                          errorMessage: getTranslated(context, 'purchase_mobile_num_required'),
                          isEnabled: _enableMobileNumber),
                    ),
                    !widget.isProductPurchase ? Padding(
                        padding:
                        EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                        child: CustomWidgetBuilder.buildFormInputText(
                          controller: _amountController,
                          hint: getTranslated(context, 'purchase_amount'),
                          errorMessage: getTranslated(context, 'purchase_amount_required'),
                          inputType: TextInputType.numberWithOptions(
                              decimal: true),
                        )
                    ):Container(),
                    !widget.isProductPurchase ? Padding(
                        padding:
                        EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                        child: CustomWidgetBuilder.buildFormInputText(
                          controller: _descriptionController,
                          hint: getTranslated(context, 'purchase_description'),
                        )
                    ):Container(),
                    // widget.purchaseType == 'pin' || widget.purchaseType == 'qr' ?
                    // Padding(
                    //     padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    //     child: CustomWidgetBuilder.buildFormInputText(
                    //         controller: _pinController, hint: getTranslated(context, "purchase_pin"), inputType: TextInputType.number,
                    //         errorMessage: getTranslated(context, "purchase_pin_required"), isPassword: true)
                    // ):Container(),

                    // Container(
                    //   child: Padding(
                    //     padding: const EdgeInsets.symmetric(
                    //         vertical: 16.0, horizontal: 20),
                    //     child: CustomWidgetBuilder.buildRaisedButton(
                    //         context, generateQR, null, getTranslated(context, 'cashout_btn_generateqr'),
                    //         size: MainAxisSize.max),
                    //   ),
                    // ),
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 16.0, horizontal: 20),
                        child: CustomWidgetBuilder.buildRaisedButton(
                            context, validatePayment, null, getTranslated(context, "purchase_btn_submit"), size: MainAxisSize.max),
                      ),
                    ),
                    // _isLoading ? Padding(
                    //   padding: const EdgeInsets.symmetric(horizontal: 25),
                    //   child: LinearProgressIndicator(backgroundColor: Colors.blue),
                    // ):Container()
                  ],
                ),
              ), // Child
            ),
          );
        },
      ),
    );
  }
}
