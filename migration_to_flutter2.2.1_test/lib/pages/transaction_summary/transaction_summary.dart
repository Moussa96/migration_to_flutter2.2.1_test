import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/AccountService.dart';
import 'package:cashless/models/Balance.dart';
import 'package:cashless/pages/home/home.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/custom_widgets/CashCallCustomWidget.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
class TransactionSummary extends StatefulWidget{
  Map<dynamic, dynamic> transcationResponse ;
  TransactionSummary({
    Key key,
    this.transcationResponse,
  }) : super();
  @override
  _TransactionSummaryState createState() => _TransactionSummaryState();
  
}
class _TransactionSummaryState extends State<TransactionSummary>{
  Size size;
  Balance balance = Balance();
  void gotoHome(BuildContext context){
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (c) => ConsumerHome()), (route) => false);
  }
  List<Widget> buildTransactionSummaryBody() {
    List<Widget> widgets = new List();
    for (var entry in widget.transcationResponse.entries) {
      widgets.add(Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: CashCallCustomWidget.buildDataConfirmCashCallServiceCard(label:entry.key ,
            data: entry.value.toString(),size: Size(size.width,size.height*0.075),icon: entry.key == getTranslated(context, 'confirm_service_reference')),
      )
      );
      widgets.add(SizedBox(height: 7,));
    }
    return widgets;
  }
  void initState() {
    super.initState();
    getBalance();
  }
  void getBalance(){
    AccountService _accountUseCase = AccountService();
    balance = Balance();
    _accountUseCase.listAccounts().then((value) => {
      setState(() {
        if (value.data != null) balance = Util.getBalanceInfo(value.data);
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: BACKGROUND_COLOR,
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          child:
            Column(
              children: <Widget>[
                Container(child: Image.asset('lib/assets/sucessful_transaction.png')),
                SizedBox(height: 16,),
                CustomWidgetBuilder.buildText(
                    getTranslated(context, "transaction_summary_status"),
                    color:  PRIMARY_COLOR,
                    fontWeight: FontWeight.w700,
                    fontStyle:  FontStyle.normal,
                    fontSize: 20.4
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 25,vertical: 10),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: CustomWidgetBuilder.buildText(getTranslated(context, 'transaction_summary_message'),color: CASHCALL_TITLE
                          ,fontSize: 14,fontWeight: FontWeight.w400,fontStyle: FontStyle.normal,
                        ),
                      ),
                      SizedBox(height: 20,),
                      Column(
                        children: buildTransactionSummaryBody(),
                      ),
                      SizedBox(height: 13,),
                      CustomWidgetBuilder.buildText('${getTranslated(context, 'transaction_summary_available_balance')} "${balance.availableBalance} ${balance.currency}"',
                          fontWeight: FontWeight.w400,
                          fontStyle:  FontStyle.normal,
                          fontSize: 13.6,
                          color: CASHCALL_TITLE
                      ),
                      SizedBox(height: 9.7,),
                      CustomWidgetBuilder.buildText(getTranslated(context, 'transaction_summary_tanks_message'),
                          fontWeight: FontWeight.w400,
                          fontStyle:  FontStyle.normal,
                          fontSize: 13.6,color: PRIMARY_COLOR
                      ),
                      SizedBox(height: 52,),
                      CustomWidgetBuilder.buildRaisedButton(context, gotoHome, null, getTranslated(context, "home_done_btn"), color: SECONDARY_COLOR,fontWeight: FontWeight.w400,
                          fontStyle:  FontStyle.normal,
                          fontSize: 13.6)
                    ],
                  ),
                ),
              ],
            ),

        )
      ),
    );

  }
}