import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/pages/DrawerMain.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
class TransactionSummary extends StatelessWidget {
  Map<dynamic, dynamic> transcationResponse = null;
  TransactionSummary(this.transcationResponse , {Key key,}) : super();

  void gotoHome(BuildContext context){
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (c) => DrawerMain()), (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> transactionSummary() {

      List<Widget> widget = new List();
      for (var entry in transcationResponse.entries) {
        widget.add(CustomWidgetBuilder.buildText(entry.key + ' : ' + entry.value.toString(),fontSize: 20, textColor: Colors.grey[500]));
      }
      return widget;
    }
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Colors.grey[300],
        resizeToAvoidBottomInset: false,
        appBar: ActionBar(context),
        // drawer: DrawerFragment(),
        body: SingleChildScrollView(
          child: Card(
            margin: EdgeInsets.all(20),
            elevation: 1,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top:15.0),
                  child: Center(child: Icon(Icons.check_circle, size: 150, color: Colors.green)),
                ),
                CustomWidgetBuilder.buildText(getTranslated(context, "transaction_summary_status"),fontSize: 26, textColor: Colors.green),
                Column(
                  children: transactionSummary(),
                ),
                Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: CustomWidgetBuilder.buildRaisedButton(context, gotoHome, null, getTranslated(context, "transaction_summary_btn"), size: MainAxisSize.max,icon: Icons.home),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

