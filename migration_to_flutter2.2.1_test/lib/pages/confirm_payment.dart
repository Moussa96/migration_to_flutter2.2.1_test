import 'dart:async';
import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/OrderItems.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/User.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';

class ConfirmPayment extends KFDrawerContent {
  Payment payment = Payment();
  User user = User();
  OrderItems orderItems = OrderItems();
  bool isVIP;

  ConfirmPayment(
      {Key key,
      @required this.payment,
      @required this.user,
      this.orderItems,
      this.isVIP = false})
      : super();

  @override
  _ConfirmPaymentState createState() => _ConfirmPaymentState();
}

class _ConfirmPaymentState extends State<ConfirmPayment> {
  PaymentService paymentService = PaymentService();
  TextEditingController _pinController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _pinEnabled = true;

  void showAlertDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(backgroundColor: Colors.blue),
          Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: Text(getTranslated(context, "loading"))),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void validatePayment(BuildContext context) {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      setState(() {
        _pinEnabled = false;
      });
      showAlertDialog(context);
      widget.user.password = widget.isVIP ? VIP_PASSWORD : _pinController.text;
      paymentService.doPayment(context, widget.payment, widget.user,
          paymentStatus: paymentDone, orderItems: widget.orderItems);
    }
  }

  void paymentDone() {
    Timer(Duration(seconds: 1), () {
      Navigator.pop(context);
      setState(() {
        _pinEnabled = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      // drawer: DrawerFragment(),
      resizeToAvoidBottomInset: false,
      appBar: ActionBar(context,
          icon: Icons.arrow_back_ios, pressFunc: () => Navigator.pop(context)),
      body: Builder(
        builder: (BuildContext context) {
          return Container(
            child: (Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20.0),
                      child: Text(
                          getTranslated(context, "confirm_payment_title"),
                          style:
                              TextStyle(fontSize: 22, color: Colors.black54))),
                  Text(widget.payment.amount.toString(),
                      style: TextStyle(fontSize: 65, color: Colors.green)),
                  !widget.isVIP
                      ? Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 10),
                          child: CustomWidgetBuilder.buildFormInputText(
                              controller: _pinController,
                              hint: getTranslated(context, "cashin_pin"),
                              inputType: TextInputType.number,
                              maxLength: 4,
                              errorMessage:
                                  getTranslated(context, "cashin_pin_required"),
                              isPassword: true),
                        )
                      : Container(),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 16.0, horizontal: 20),
                      child: CustomWidgetBuilder.buildRaisedButton(
                          context,
                          validatePayment,
                          null,
                          getTranslated(context, "confirm_payment_btn"),
                          size: MainAxisSize.max,
                          isEnabled: _pinEnabled),
                    ),
                  ),
                ],
              ),
            )), // Child
          );
        },
      ),
    );
  }
}
