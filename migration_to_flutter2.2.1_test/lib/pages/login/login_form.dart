import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/ContactService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Profile.dart';
import 'package:cashless/pages/home/home.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/custom_widgets/CustomButton.dart';
import 'package:cashless/widgets/custom_widgets/CustomCheckBox.dart';
import 'package:cashless/widgets/custom_widgets/CustomTextFormField.dart';
import 'package:cashless/pages/DrawerMain.dart';
import 'package:cashless/pages/Reset_Password/reset_password.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:cashless/pages/forget_password/forget_password.dart';
import 'package:cashless/pages/reset_password/old_resetpassword.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginForm extends  StatefulWidget{
  LoginForm({Key? key}) : super();

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  ContactService contactUseCase = ContactService();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  bool checked = false;

  @override
  initState() {
    super.initState();
  }

  loginAction(BuildContext context) async {
    if (_formKey.currentState!.validate()) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          duration: Duration(seconds: 1),
          content: Text(getTranslated(context, 'login_processing'))));
      _formKey.currentState!.save();
      RetrofitResponse<Profile>? response = await contactUseCase.login(
          _usernameController.text.trim(), _passwordController.text);
      if (response != null) {
        if (response.statusCode == 200 && response.data != null) {
          if (response.data!.permissions != null) {
            Util.saveUserSessionDate(response.data!,checked);
            if(response.data!.member!.groupCode! == GROUP_CONSUMER){
              if (response.data!.member!.forceChangePassword!) {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ChangePassword()));
              } else {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => ConsumerHome()));
              }
            } else {
              if (response.data!.member!.forceChangePassword!) {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ResetPassword()));
              } else {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => DrawerMain()));
              }
            }
          } else {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                duration: Duration(seconds: 2),
                content: Text(getTranslated(context, 'login_no_permissions'))));
          }
        } else {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text(getTranslated(context, response.getErrorCode))));
        }
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            duration: Duration(seconds: 2),
            content: Text(getTranslated(context, 'login_noConnection'))));
      }
    }
  }

  void rememberPassword(bool val){
    setState(() {
      checked = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Stack(
                children: [
                  Image.asset(
                    'lib/assets/login_top_area.png',
                  ),
                  Positioned(
                      bottom: 25,
                      left: 12,
                      child: Text(getTranslated(context, "login_title"),
                          style: GoogleFonts.lato(
                              color: TEXT_PRIMARY_COLOR, fontSize: 18)))
                ],
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: Column(
                  children: [
                    CustomTextFormField(
                      label: getTranslated(context, 'login_username'),
                      hint: getTranslated(context, 'login_username'),
                      controller: _usernameController,
                      errorMessage:
                          getTranslated(context, 'login_username_required'),
                    ),
                    SizedBox(height: 24),
                    CustomTextFormField(
                      label: getTranslated(context, 'login_password'),
                      hint: getTranslated(context, 'login_password'),
                      errorMessage:
                          getTranslated(context, 'login_password_required'),
                      isPassword: true,
                      addShowBtn: true,
                      controller: _passwordController,
                    ),
                    SizedBox(height: 22),
                    CustomWidgetBuilder.buildTextBtn(
                        context,
                        goToForgetPasswordPage,
                        getTranslated(context, 'forget_password'),
                        color: SECONDARY_COLOR,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        fontSize: 13.6),
                    SizedBox(height: 22),
                    CustomCheckBox(checked: checked,onChanged: rememberPassword,),
                    SizedBox(height: 91),
                    CustomButton(
                      text: getTranslated(context, 'login_btn'),
                      onPressed: () {
                        loginAction(context);
                      },
                      padding: 25,
                    ),
                    SizedBox(height: 40),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void goToForgetPasswordPage(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ForgetPassword()));
  }
}
