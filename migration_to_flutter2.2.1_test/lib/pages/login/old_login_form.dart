import 'dart:convert';

import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/ContactService.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Profile.dart';
import 'package:cashless/models/QRResults.dart';
import 'package:cashless/pages/DrawerMain.dart';
import 'package:cashless/pages/Reset_Password/old_resetpassword.dart';
import 'package:cashless/util/QRGenerator.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';

class OldLoginForm extends KFDrawerContent{
  OldLoginForm({Key? key}) : super();

  @override
  _OldLoginFormState createState() => _OldLoginFormState();
}

class _OldLoginFormState extends State<OldLoginForm> {
  final _formKey = GlobalKey<FormState>();
  late String _username;
  late String _password;
  ContactService contactUseCase = ContactService();

  @override
  initState() {
    super.initState();
  }

  Widget _buildUsername() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: getTranslated(context, 'login_username')),
      validator: (String? value) {
        if (value == null || value.isEmpty) {
          return getTranslated(context, 'login_username_required');
        }
        return null;
      },
      onSaved: (String? value) {
        _username = value!;
      },
    );
  }

  Widget _buildPassword() {
    return TextFormField(
      decoration:
          InputDecoration(labelText: getTranslated(context, 'login_password')),
      obscureText: true,
      validator: (String? value) {
        if (value == null || value.isEmpty) {
          return getTranslated(context, 'login_password_required');
        }
        return null;
      },
      onSaved: (String? value) {
        _password = value!;
      },
    );
  }

  void loginAction(BuildContext context) async {
    // Validate returns true if the form is valid, or false
    // otherwise.
    if (_formKey.currentState!.validate()) {
      // If the form is valid, display a Snackbar.
      ScaffoldMessenger.of(context)..showSnackBar(SnackBar(
          duration: Duration(seconds: 1),
          content: Text(getTranslated(context, 'login_processing'))));
      _formKey.currentState!.save();
      RetrofitResponse<Profile> response = await contactUseCase.login(_username.trim(), _password);
      if (response != null) {
        if (response.statusCode == 200 && response.data != null) {
          if(response.data!.permissions != null){
            Util.saveUserSessionDate(response.data!,false);
            if (response.data!.member!.forceChangePassword!) {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ResetPassword()));
            } else {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => DrawerMain()));
            }
          }else{
            Scaffold.of(context).showSnackBar(SnackBar(
                duration: Duration(seconds: 2),
                content: Text(getTranslated(context, 'login_no_permissions'))));
          }

        } else {
          Scaffold.of(context).showSnackBar(
              SnackBar(content: Text(getTranslated(context, response.getErrorCode))));
        }
      } else {
        Scaffold.of(context).showSnackBar(SnackBar(
            duration: Duration(seconds: 2),
            content: Text(getTranslated(context, 'login_noConnection'))));
      }
    }
  }

  void scanTenantConfigurations(BuildContext context) async {
    QRResults configuration = await QRGenerator.scan();
    if (configuration.status == 1) {
      try {
        dynamic body = json.decode(configuration.content);
        if (body['url'] != null) {
//          BASE_URL = body['url'];
          Scaffold.of(context)
              .showSnackBar(SnackBar(content: Text(body['url'])));
        }
      } catch (e) {
        Scaffold.of(context).showSnackBar(
            SnackBar(content: Text('Please scan the community QR code.')));
      }
    } else {
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text('Please scan the QR again.')));
    }
  }

  Widget _buildLoginForm() {
    return Form(
      key: _formKey,
      child: Container(
        margin: EdgeInsets.all(0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Padding(
                padding: const EdgeInsets.only(top: 40.0, bottom: 50.0),
                child: Image.asset('lib/assets/newlogo.png', height: 90),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: _buildUsername(),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: _buildPassword(),
            ),
            Padding(
              padding: const EdgeInsets.all(50.0),
              child: Center(
                child: CustomWidgetBuilder.buildRaisedButton(context,
                    loginAction, null, getTranslated(context, 'login_btn'),
                    color: Colors.lightBlueAccent[400]!, size: MainAxisSize.max),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(50.0),
              child: Center(
                  //child: CustomWidgetBuilder.buildRaisedButton(context, scanTenantConfigurations, null, 'Scan', size: MainAxisSize.max),
                  ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(15.0),
      child: _buildLoginForm(),
    );
  }
}
