import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/main.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:flutter/material.dart';

class ChangeLanguage extends KFDrawerContent {
  ChangeLanguage({Key key}) : super();

  @override
  _ChangeLanguageState createState() => _ChangeLanguageState();
}

class _ChangeLanguageState extends State<ChangeLanguage> {
  @override
  void initState() {
    super.initState();
  }

  void _changeLang(Language language) {
    Locale _temp;
    switch(language.languageCode){
      case 'en':
        _temp = Locale(language.languageCode, 'US');
        break;
      case 'ar':
        _temp = Locale(language.languageCode, 'EG');
        break;
      default:
        _temp = Locale(language.languageCode, 'US');
    }
    MyApp.setLocale(context, _temp);
  }

  Widget _buildLanguageCard(Language language) {
    return Card(
      elevation: 0,
      margin: EdgeInsets.only(top:1),
      child: InkWell(
        highlightColor: Colors.grey[200],
        onTap: () {
          _changeLang(language);
          Navigator.pop(context);
        },
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: [
              Text(language.flag, style: TextStyle(fontSize: 25)), Text(language.name, style: TextStyle(fontSize: 25))
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.grey[300],
      appBar: ActionBar(context, titleText: 'Language',icon: Icons.arrow_back_ios,pressFunc:  () => Navigator.pop(context)),
      // drawer: DrawerFragment(),
      body: ListView(
        children: Language.LanguageList().map((e) => _buildLanguageCard(e)).toList(),
      ),
    );
  }
}
