import 'package:flutter/material.dart';

import '../Configurations.dart';


class SettingsDropDown extends StatefulWidget {
  SettingsDropDown({Key key}) : super(key: key);

  @override
  _SettingsDropDown createState() => _SettingsDropDown();
}

class _SettingsDropDown extends State<SettingsDropDown> {
  Item dropdownValue ;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<Item>(
      isExpanded: true,
      icon: Icon(Icons.settings,color: Colors.white,),
      iconSize: 20,
      underline: Container(
        height: 0,
        color: MAJOR_COLOR,
      ),
      onChanged: (Item newValue) {
        Navigator.pushNamed(context, newValue.URL);
      },
      items: <Item>[Item('Setting','/profile'),Item('language','/profile')]
          .map<DropdownMenuItem<Item>>((Item value) {
        return DropdownMenuItem<Item>(
          value: value,
          child: Text(value.name,overflow:TextOverflow.fade),
        );
      }).toList(),
    );
  }
}
class Item{
  String _name;
  String _URL;

  Item(this._name, this._URL);

  String get name => _name;

  set name(String value) {
    _name = value;
  }

  String get URL => _URL;

  set URL(String value) {
    _URL = value;
  }
}
