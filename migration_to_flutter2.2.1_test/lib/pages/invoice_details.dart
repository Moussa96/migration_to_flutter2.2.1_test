import 'package:cashless/Configurations.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/pages/fragments/drawer_fragment.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';

class InvoiceDetails extends StatefulWidget {
  InvoiceDetails({Key key}) : super();

  @override
  _InvoiceDetailsState createState() => _InvoiceDetailsState();
}

class _InvoiceDetailsState extends State<InvoiceDetails> {
  Widget payInvoice(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: ActionBar(context, icon: Icons.arrow_back_ios,pressFunc:  () => Navigator.pop(context)),
        // drawer: DrawerFragment(),
        body: Builder(
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('AlertDialog Title'),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Text('This is a demo alert dialog.'),
                    Text('Would you like to approve of this message?'),
                  ],
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('Approve'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        ));
  }

  Future<void> dismissPopup(BuildContext context, BuildContext parentContext) {
    Navigator.of(context).pop();
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Success'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  'Payment done successfully',
                  style: TextStyle(fontSize: 18, color: Colors.green),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              color: MINOR_COLOR,
              onPressed: () async {
                Navigator.of(context).pop();
              },
              child: Container(
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.cancel),
                    Text(
                      'Cancel',
                      style: TextStyle(color: MAJOR_BUTTONS_TEXT_COLOR, fontSize: 20.0),
                    ),
                  ],
                ),
              ),
            )
          ],
        );
      },
    );
  }

  Future<void> showPaymentConfirmation(BuildContext context, double amount) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Confirm Payment'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('You will pay :'),
                Text(
                  amount.toString(),
                  style: MAJOR_TEXT_STYLE,
                ),
              ],
            ),
          ),
          actions: <Widget>[CustomWidgetBuilder.buildRaisedButton(context, dismissPopup, context, 'Pay', icon:Icons.payment, size:MainAxisSize.max)],
        );
      },
    );
  }

  Widget _buildInvoiceCard(BuildContext context, dynamic transaction) {
    var memberName = 'System';
    var date = transaction['date'];
    var amount = transaction['amount'];
    var description = transaction['description'];
    if (transaction['fromMember'] != null) {
      memberName = transaction['fromMember']['name'];
    }
    return Card(
      margin: EdgeInsets.all(5.0),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Text(memberName.toString(), style: MAJOR_TEXT_STYLE),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          date.toString(),
                          style: TextStyle(color: MAJOR_COLOR),
                        ),
                        Text(
                          amount.toString(),
                          style: TextStyle(
                            color: amount < 0.0 ? Colors.red : Colors.green,
                            fontSize: 18,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        description != null ? Text(description) : Container(),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          CustomWidgetBuilder.buildRaisedButton(context, showPaymentConfirmation, transaction['amount'], 'Pay', icon: Icons.payment, size: MainAxisSize.max),
                          CustomWidgetBuilder.buildRaisedButton(context, showPaymentConfirmation, double.parse(transaction['amount'].toString()) * 0.65, 'Pay Partial', icon: Icons.payment, size: MainAxisSize.max),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 5.0,
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: ActionBar(context),
      drawer: DrawerFragment(),
      body: Builder(builder: (BuildContext context) {
        return Container(
          child: _buildInvoiceCard(context, ModalRoute.of(context).settings.arguments),
        );
      }),
    );
  }
}
