import 'dart:convert';

import 'package:cashless/apis/RetrofitResponse.dart';
import 'package:cashless/apis/Services/ContactService.dart';
import 'package:cashless/apis/Services/PaymentService.dart';
import 'package:cashless/models/Member.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';

import 'package:cashless/apis/Services/TransferTypeService.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/response/TransferRes/TransferTypesResponse.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/material.dart';


class PaymentForm extends StatefulWidget {
  PaymentForm({Key key}) : super();

  @override
  _PaymentFormState createState() => _PaymentFormState();
}

class _PaymentFormState extends State<PaymentForm> {
  Payment payment = Payment();

  //String _amount;
  String _selectedTransferType;
  String _selectedContact;
  String _description;

  List<Map> _transferTypeList = [];
  List<Member> _contactsList = [];

  final _formKey = GlobalKey<FormState>();

  ContactService _contactUseCase = ContactService();
  PaymentService _paymentUseCase = PaymentService();
//  TransferTypeService transferTypeService = TransferTypeService();
  TransferTypeService transferTypeService = TransferTypeService();


  @override
  void initState() {
    super.initState();
    loadTransferTypes().then((result) {
      setState(() {});
    });
    loadContacts().then((result) {
      setState(() {});
    });
  }

  loadTransferTypes() async {

    TransferTypesResponse response;
    await transferTypeService
        .getTransferTypes()
        .then((value) => {response = value.data});

    List<dynamic> transferTypeLookup = response.transferTypeRes;
    _transferTypeList.clear();
    transferTypeLookup.forEach((element) {
      _transferTypeList.add(Map.from(element));
    });

  }

  loadContacts() async {
    RetrofitResponse response = await _contactUseCase.getContacts();
    List<Member> contactsLookup = response.data;

    _contactsList.clear();
    contactsLookup.forEach((element) {
      _contactsList.add(element);
    });
  }

  Widget _buildTransferTypes() {
    return DropdownButton<String>(
      hint: new Text("Select Transfer Type"),
      value: _selectedTransferType,
      onChanged: (String newValue) {
        setState(() {
          _selectedTransferType = newValue;
        });
      },
      items: _transferTypeList.map((Map map) {
        return new DropdownMenuItem<String>(
          value: map["id"].toString(),
          child: new Text(
            map["name"],
          ),
        );
      }).toList(),
    );
  }

  Widget _buildContacts() {
    return DropdownButton<String>(
      hint: new Text("Select Contact"),
      value: _selectedContact,
      onChanged: (String newValue) {
        setState(() {
          _selectedContact = newValue;
        });
      },
      items: _contactsList.map((Member member) {
        return new DropdownMenuItem<String>(
          value: member.id.toString(),
          child: new Text(
            member.name,
          ),
        );
      }).toList(),
    );
  }

  Widget _buildAmount() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Amount'),
      keyboardType: TextInputType.numberWithOptions(
        decimal: true),
      validator: (String value) {
        if (value.isEmpty) {
          return 'Amount is required';
        }
        return null;
      },
      onSaved: (String value) {
        payment.amount = double.parse(value);
      },
    );
  }

  Widget _buildDescription() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Description'),
      keyboardType: TextInputType.multiline,
      validator: (String value) {
        if (value.isEmpty) {
          return 'Description is required';
        }
        return null;
      },
      onSaved: (String value) {
        payment.description = value;
      },
    );
  }

  void payAction(BuildContext context) async {
    // Validate returns true if the form is valid, or false
    // otherwise.
    if (_selectedTransferType == null || _selectedContact == null) {
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text('All fields are required')));
    }
    if (_formKey.currentState.validate()) {
      // If the form is valid, display a Snackbar.
      _formKey.currentState.save();
      payment.toMemberId = _selectedContact;
      payment.transferTypeId = _selectedTransferType;
      RetrofitResponse response = await _paymentUseCase.makePayment(payment);
      if (response.statusCode == 200) {
        Scaffold.of(context).showSnackBar(
            SnackBar(content: Text('Payment done successfully.')));
      } else {
        Scaffold.of(context).showSnackBar(
            SnackBar(content: Text('Payment cannot be performed.')));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: ActionBar(context,
          icon: Icons.arrow_back_ios, pressFunc: () => Navigator.pop(context)),
      // drawer: DrawerFragment(),
      body: Builder(
        builder: (BuildContext context) {
          return Container(
            margin: EdgeInsets.all(15.0),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _buildContacts(),
                  _buildTransferTypes(),
                  _buildAmount(),
                  _buildDescription(),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: CustomWidgetBuilder.buildRaisedButton(
                        context, payAction, null, 'Pay',
                        icon: Icons.payment, size: MainAxisSize.max),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
