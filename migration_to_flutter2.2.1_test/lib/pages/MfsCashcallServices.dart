import 'dart:convert';

import 'package:cashless/Configurations.dart';
import 'package:cashless/apis/Services/MfsRestClientImpl.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Mfs/MfsCashcallServices.dart';
import 'package:cashless/models/Mfs/MfsCategories.dart';
import 'package:cashless/models/Mfs/MfsScenarioConfiguration.dart';
import 'package:cashless/models/Mfs/MfsServices.dart';
import 'package:cashless/models/Mfs/MfsSubCategory.dart';
import 'package:cashless/pages/OldMfsSelectedService.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:cashless/pages/fragments/action_bar.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class CashcallServices extends KFDrawerContent {
  CashcallServices({Key key}) : super();

  @override
  _CashcallServicesState createState() => _CashcallServicesState();
}

class _CashcallServicesState extends State<CashcallServices> {
  MfsRestClientImpl _mfsService = MfsRestClientImpl();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  MfsCashcallServices cashcall_services = MfsCashcallServices();
  MfsCategories categories = MfsCategories();
  List<MfsServices> services = List();
  List<MfsSubCategory> subCategories = List();
  Color selectedColor = Colors.blue[400];
  List<Color> cardColor = List<Color>();
  int lastIndex = 0;

  @override
  void initState() {
    super.initState();

    loadMfsCategories().then((result) {
      if(result.statusCode == 200 && result.data.cashcall_services != null) {
        setState(() {
          cashcall_services = result.data;
          services = cashcall_services.cashcall_services[0].services;
          subCategories = cashcall_services.cashcall_services[0].sub_category;
          cardColor = List.generate(cashcall_services.cashcall_services.length,
              (i) => i == 0 ? selectedColor : MINOR_COLOR);
        });
      }
      else{
        scaffoldKey.currentState.showSnackBar(
            SnackBar(
              content: Text(getTranslated(context, result.getErrorCode.toString()))
            )
        );
      }
    });
  }

  loadMfsCategories() async {
    return await _mfsService.getMfs();
  }

  selectCategory(int index) {
    setState(() {
      services = cashcall_services.cashcall_services[index].services;
      subCategories = cashcall_services.cashcall_services[index].sub_category;
      cardColor[lastIndex] = MINOR_COLOR;
      cardColor[index] = selectedColor;
      lastIndex = index;
    });
  }

  stringToList(String s) {
    if (s != null) {
      if (s.length != 0 && s != "[]") {
        return jsonDecode(s);
      }
      return [];
    }
  }

  void gotoSelectedSubcategoryService(int subCategoryIndex, int serviceIndex) {
    dynamic scenarioConfigString = subCategories[subCategoryIndex].services[serviceIndex].scenario_configuration;
    if (scenarioConfigString != null && jsonDecode(scenarioConfigString) != null) {
      MfsScenarioConfiguration scenarioConfiguration =
      MfsScenarioConfiguration.fromJson(jsonDecode(scenarioConfigString));
      Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SelectedService(
            service: subCategories[subCategoryIndex].services[serviceIndex],
            packages: stringToList(subCategories[subCategoryIndex].services[serviceIndex].Packages),
            scenarioConfiguration: scenarioConfiguration
          )
        )
      );
    }
    else{
      CustomWidgetBuilder.buildSnackBar(scaffoldKey, context, getTranslated(context, "service_unavailable"));
    }
  }

  void gotoSelectedService(int serviceIndex){
    dynamic scenarioConfigString = services[serviceIndex].scenario_configuration;
    if (scenarioConfigString != null && jsonDecode(scenarioConfigString) != null) {
      MfsScenarioConfiguration scenarioConfiguration =
      MfsScenarioConfiguration.fromJson(jsonDecode(scenarioConfigString));
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  SelectedService(
                      service: services[serviceIndex],
                      packages: stringToList(
                          services[serviceIndex].Packages),
                      scenarioConfiguration:scenarioConfiguration
                  )
          )
      );
    }
    else{
      CustomWidgetBuilder.buildSnackBar(scaffoldKey, context, getTranslated(context, "service_unavailable"));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.white.withOpacity(0.9),
      resizeToAvoidBottomInset: false,
      appBar: ActionBar(
          context,
          titleText: getTranslated(context, 'home_CashcallServices'),
          icon: Icons.arrow_back_ios,
          pressFunc: () => Navigator.pop(context)
      ),
      body: cashcall_services.cashcall_services != null
          ? SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                child:
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  CustomWidgetBuilder.buildCategoryCards(
                      cashcall_services.cashcall_services,
                      Localization.of(context).locale.languageCode == 'en'
                          ? 'category_name'
                          : 'category_arabic_name',
                      selectCategory,
                      //cardColor: cardColor
                  ),
                  subCategories.length > 0 && subCategories[0].sub_category_code != null ?
                  Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                              getTranslated(
                              context, "cashcall_services_sub_categories"),
                              style: TextStyle(fontSize: 18)),
                  )
                  : Container(),
                  subCategories.length > 0 &&
                          subCategories[0].sub_category_code != null ?
                      Column(
                          children:
                              List.generate(subCategories.length, (index) {
                            return Card(
                              color: Colors.white,
                              child: ExpansionTile(
                                leading:ClipRRect(
                                    borderRadius: BorderRadius.circular(5.0),
                                    child: Container(
                                      color: Colors.blue,
                                      child: FadeInImage.assetNetwork(
                                      height: 50,
                                          fit: BoxFit.fill,

                                          placeholder: 'lib/assets/services_default.jpeg',
                                      image: MFS_IMGS_BASE_URL + "categories/" + subCategories[index].sub_category_code + ".png"),
                                    ),
                                ),
                                  title: Text(
                                    Localization.of(context).locale.languageCode == 'en' ?
                                    subCategories[index].sub_category_name : subCategories[index].sub_category_arabic_name,
                                    style: TextStyle(color: MINOR_COLOR)
                                  ),
                                children: [
                                  Divider(),
                                  Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child:
                                        CustomWidgetBuilder.buildCategoryCards(
                                            subCategories[index].services,
                                            Localization.of(context).locale.languageCode == 'en' ?
                                            'service_name' : 'service_arabic_name',
                                            gotoSelectedSubcategoryService,
                                            subCategoryIndex: index,
                                            width: 125,
                                            height: 125),
                                  ),
                                ],
                              ),
                            );
                          }),
                        )
                      : Container(),
                  services != null && services.length > 0
                      ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                              getTranslated(
                                  context, "cashcall_services_services"),
                              style: TextStyle(fontSize: 18)),
                        )
                      : Container(),
                  services != null && services.length > 0 ?
                  Column(
                    children: List.generate(services.length, (index) {
                      return Container(
                        child: Card(
                            child: InkWell(
                              onTap: (){ gotoSelectedService(index);},
                              child: ListTile(
                                leading: ClipRRect(
                                  borderRadius: BorderRadius.circular(5.0),
                                  child: FadeInImage.assetNetwork(
                                      height: 40,
                                      placeholder: 'lib/assets/services_default.jpeg',
                                      image: MFS_IMGS_BASE_URL +
                                          "services/" +
                                          services[index].service_code +
                                          ".png"),
                                ),
                                title: Text(
                                    Localization.of(context).locale.languageCode == 'en' ? services[index].service_name : services[index].service_arabic_name,
                                    style: TextStyle(color: Colors.blue[700])
                                ),
                              ),
                            ),
                          ),
                        );
                      }),
                    )
                  : Container()
                ]),
              ),
            )
          : CustomWidgetBuilder.buildSpinner(context),
    );
  }
}
