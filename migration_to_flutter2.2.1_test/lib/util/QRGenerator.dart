import 'dart:convert';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:cashless/models/QRResults.dart';
import 'package:cashless/util/Util.dart';

import 'package:flutter/cupertino.dart';
import 'package:qr_flutter/qr_flutter.dart';


class QRGenerator {


  static Future<Widget> generate(Map<String, dynamic> params,{double size = 200.0}) async {
    dynamic encryptParam = await Util.encrypt(jsonEncode(params));
    return QrImage(
      data: encryptParam,
      version: QrVersions.auto,
      size: size,
    );
  }


  static dynamic scan({bool encrypt = true}) async {
    var result = await BarcodeScanner.scan();
    if (ResultType.Barcode == result.type) {
      dynamic encryptResult;
      if(encrypt) {
        encryptResult = await Util.encrypt(result.rawContent);
        return QRResults(content: encryptResult);
      }
      else {
        print('///////////////');
        print(result.rawContent);
        return QRResults(content: result.rawContent);
      }
    }else if(ResultType.Cancelled==result.type) {
      return QRResults();
    }
    else {
      return QRResults();
    }
  }
}


