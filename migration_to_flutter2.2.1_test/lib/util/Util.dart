import 'dart:collection';
import 'dart:convert';
import 'dart:math';
import 'package:cashless/Configurations.dart';
import 'package:cashless/models/Account.dart';
import 'package:cashless/models/AccountInfo.dart';
import 'package:cashless/models/Balance.dart';
import 'package:cashless/models/Payment.dart';
import 'package:cashless/models/Profile.dart';
import 'package:cashless/pages/notification/list_notification.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';
import 'package:flutter/material.dart';
import 'package:crypto/crypto.dart';

class Util {
  static final String ON_BOARD_VIEWS_CNT = "ON_BOARD_VIEWS_CNT";
  static final String USERNAME = 'USERNAME';
  static final String USER_ID = 'USER_ID';
  static final String ACCOUNT_ID = 'ACCOUNT_ID';
  static final String NAME = 'NAME';
  static final String EMAIL = 'EMAIL';
  static final String SP_Authorization_Token = 'authorizationToken';
  static final String BALANCE = 'balance';
  static final String GROUP = 'GROUP';
  static final String IMAGE_URL = 'IMAGE_URL';
  static final String PERMISSIONS = 'PERMISSIONS';
  static final String KEY = "B4A024AF77518A270378491E7E9B08AE";
  static final String LANGUAGE = 'LANGUAGE';
  static final String REMEMBER_ME = 'REMEMBER_ME';

  static void saveTOSharedPreferences(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
  }

  static bool isNumeric(String? s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  static void savePermissionsTOSharedPreferences(
      String key, List<String> value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setStringList(key, value);
  }

  static void saveCnt(String key, int value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt(key, value);
  }

  static Future<int?> loadCnt(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(key);
  }

  static Future<String?> loadFromSharedPreferences(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  static Future<List<String>?> loadPermissionsFromSharedPreferences(
      String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(key);
  }

  static Future<void> clear() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
  }

  static void saveUserSessionDate(Profile userData, bool rememberMe) async{
    /* get default account */
    String? accountId = getDefaultAccountId(userData.accounts!);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(ACCOUNT_ID, accountId!);
    prefs.setString(USER_ID,userData.member!.id.toString() );
    prefs.setString(USERNAME, userData.member!.username.toString() );
    prefs.setString(NAME, userData.member!.name.toString() );
    prefs.setString(EMAIL, userData.member!.email.toString() );
    prefs.setString(GROUP, userData.member!.groupCode.toString());
    prefs.setStringList(Util.PERMISSIONS, userData.permissions!);
    prefs.setBool(REMEMBER_ME, rememberMe);
    if (userData.member!.images != null)
      prefs.setString(Util.IMAGE_URL, userData.member!.images![0].fullUrl.toString());
    else
      prefs.setString(Util.IMAGE_URL, '');
  }
  static String? getDefaultAccountId(List<AccountInfo> accounts){
    for (int i = 0; i < accounts.length; i++)
      if (accounts[i].isDefault!)
        return accounts[i].id.toString();
  }

  static Future<Map<String, dynamic>> getUserSession() async {
    Map<String, dynamic> user = new HashMap();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    user['username'] = prefs.getString(Util.USERNAME);
    user['authorization'] = prefs.getString(Util.SP_Authorization_Token);
    user['onboarding'] = prefs.getInt(Util.ON_BOARD_VIEWS_CNT);
    user['group'] = prefs.getString(Util.GROUP);
    user['langCode'] = prefs.getString(Util.LANGUAGE);
    user['rememberMe'] = prefs.getBool(Util.REMEMBER_ME);
    return user;
  }

  static void updateOnboardingCnt(int oldCnt) {
    saveCnt(ON_BOARD_VIEWS_CNT, oldCnt != null ? ++oldCnt : 1);
  }

  static Future<bool> isValidPayment(Payment payment) async {
    String? userId = await Util.loadFromSharedPreferences(Util.USER_ID);
    return (userId == payment.fromMemberId &&
        payment.amount != null &&
        payment.amount != 0 &&
        payment.toMemberId != null &&
        payment.fromMemberId != null &&
        payment.transferTypeId != null);
  }

  static Balance getBalanceInfo(List<Account> accounts) {
    Balance _balance = Balance();
    for (int i = 0; i < accounts.length; i++) {
      _balance.currency = accounts[0].accountInfo!.accountType!.currency!.name!;
      if (accounts[i].accountInfo!.accountType!.id! == 5972) {
        _balance.balance += accounts[i].accountStatus!.availableBalance!;
      } else {
        _balance.balance += accounts[i].accountStatus!.availableBalance!;
        _balance.availableBalance += accounts[i].accountStatus!.availableBalance!;
      }
    }
    return _balance;
  }

  static void updateAuthorization(String userName, String password) {
    String authorization =
        'Basic ' + base64Encode(utf8.encode('$userName:$password'));
    Util.saveTOSharedPreferences(Util.SP_Authorization_Token, authorization);
  }

  static RegExp emailExp = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

  static String generateUUID() {
    var uuid = Uuid();
    String refrenceNumber = uuid.v4();
    print("in generate uuid");
    print(refrenceNumber);
    return refrenceNumber;
  }

  static String generateString(int size) {
    Random rnd = new Random();
    int min = 1, max = 9;
    String generated = "";
    for (int i = 0; i < size; i++) {
      int r = min + rnd.nextInt(max - min);
      generated += r.toString();
    }
    return generated;
  }

  static String makeSignature(
      double amount, String paymentMethod, String merchantOrderId) {
    // var key = utf8.encode('p@ssw0rd');
    String data = amount.toString() +
        paymentMethod +
        merchantOrderId +
        PAYMENT_GW_SECURE_HASH_KEY;
    var bytes = utf8.encode(data);

    // var hmacSha256 = new Hmac(sha256, key); // HMAC-SHA256
    var digest = sha256.convert(bytes);

    return digest.toString();
  }

  static String decrypt(String input) {
    print('////////////////////////');
    print(input);
    var decryptText = utf8.decode(base64.decode(input));
    return decryptText;
  }

  static String encrypt(String input) {
    var encryptText = base64.encode(utf8.encode(input));
    return encryptText;
  }

  static String encode64(String input) {
    return base64Encode(utf8.encode(input));
  }

  static void listNotifications(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => ListNotifications()));
  }

  static stringToList(String s) {
    if (s != null) {
      if (s.length != 0 && s != "[]") {
        return jsonDecode(s);
      }
      return [];
    }
  }

  static isConnectionError(String errorCode) {
    return errorCode == API_ERROR_DEFAULT ||
        errorCode == API_ERROR_RECEIVE_TIMEOUT ||
        errorCode == API_ERROR_CONNECT_TIMEOUT ||
        errorCode == API_ERROR_CANCEL;
  }

  static isServerError(String errorCode) {
    return errorCode == API_ERROR_RECEIVE_TIMEOUT ||
        errorCode == UNEXPECTED_ERROR ||
        errorCode == API_ERROR_SEND_TIMEOUT;
  }
}
