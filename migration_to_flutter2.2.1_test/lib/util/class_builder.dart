import 'package:cashless/pages/PendingPaymentRequests.dart';
import 'package:cashless/pages/balance_transfer.dart';
import 'package:cashless/pages/cash_out.dart';
import 'package:cashless/pages/contacts.dart';
import 'package:cashless/pages/home/old_home.dart';
import 'package:cashless/pages/invoice_details.dart';
import 'package:cashless/pages/invoice_page.dart';
import 'package:cashless/pages/marketplace/old_market_place.dart';
import 'package:cashless/pages/marketplace/old_market_place_products.dart';
import 'package:cashless/pages/order.dart';
import 'package:cashless/pages/pay_through_scan_QR.dart';
import 'package:cashless/pages/payment_form.dart';
import 'package:cashless/pages/profile/old_profile.dart';
import 'package:cashless/pages/registration.dart';
import 'package:cashless/pages/static_purchase.dart';
import 'package:cashless/pages/transaction_history/old_transaction_history.dart';
import 'package:cashless/pages/reset_password/old_resetpassword.dart';
import 'package:cashless/pages/load_wallet/old_load_wallet_form.dart';

typedef T Constructor<T>();

final Map<String, Constructor<Object>> _constructors = <String, Constructor<Object>>{};

void register<T>(Constructor<T> constructor) {
  _constructors[T.toString()] = constructor as Constructor<Object>;
}

class ClassBuilder {
  static void registerClasses() {
    register<Home>(() => Home());
    register<TransactionHistory>(() => TransactionHistory());
    register<PaymentForm>(() => PaymentForm());
    register<Contacts>(() => Contacts());
    register<InvoicePage>(() => InvoicePage());
    register<InvoiceDetails>(() => InvoiceDetails());
    register<LoadWallet>(() => LoadWallet());
    register<Profile>(() => Profile());
    register<Registration>(() => Registration());
    register<BalanceTransfer>(() => BalanceTransfer());
    register<CashOut>(() => CashOut());
    register<PayThroughScanQR>(() => PayThroughScanQR());
    register<MarketPlace>(() => MarketPlace());
    register<MarketPlaceProducts>(() => MarketPlaceProducts());


    register<PendingPaymentRequests>(() => PendingPaymentRequests());

    register<ResetPassword>(() => ResetPassword());
    register<Order>(() => Order());
    register<Registration>(() => Registration());
    register<Purchase>(() => Purchase());
  }

  static dynamic fromString(String type) {
    return _constructors[type]();
  }
}
