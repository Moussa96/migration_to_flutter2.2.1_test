import 'dart:async';
import 'dart:convert';

import 'package:cashless/util/Util.dart';
import 'package:nfc_in_flutter/nfc_in_flutter.dart';
import 'package:flutter/material.dart';

class NFCUtil{
  static void startScanning(BuildContext context, Function getData) {
    late StreamSubscription<NDEFMessage> _stream;
    _stream = NFC.readNDEF(alertMessage: "Custom message with readNDEF#alertMessage")
      .listen((NDEFMessage message) async{
        if (message.isEmpty || message == null) {
            print("Read empty NDEF message");
            return;
        }
        // print("Read NDEF message with ${message.records.length} records");
        for (NDEFRecord record in message.records) {
          print("Record '${record.id ?? "[NO ID]"}' with TNF '${record.tnf}', type '${record.type}', payload '${record.payload}' and data '${record.data}' and language code '${record.languageCode}'");
        }
        getData(message.records.last.data);

        // _stream = null;
    }, onError: (error) {
        _stream.cancel();
        if (error is NFCUserCanceledSessionException) {
          print("user canceled");
        } else if (error is NFCSessionTimeoutException) {
          print("session timed out");
        } else {
          print("error: $error");
        }
      }, onDone: (){
        _stream.cancel();
    });
  }
  static Future<Stream<NDEFTag>> writeNFC(BuildContext context,dynamic record,Function onDone, {Function? writeDone}) async {
    print('write nfc');
    print(record);
    String userEncryptData = await Util.encrypt(record);
    print(userEncryptData);
    List<NDEFRecord> records = [];
    NDEFRecord ndefRecord =
    new NDEFRecord.custom(payload: userEncryptData, type: "text/plain");
    records.add(ndefRecord);
    NDEFMessage.withRecords(records);
    Stream<NDEFTag> stream = await NFC.writeNDEF( NDEFMessage.withRecords(records));
    stream.listen((NDEFTag tag) async{
      if (writeDone != null) writeDone();
      print('write nfcqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq');
    },onDone: onDone(context),
    onError: (error){
      if (error is NFCUserCanceledSessionException) {
        print("user canceled");
      } else if (error is NFCSessionTimeoutException) {
        print("session timed out");
      } else {
        print("error: $error");
      }
    });
    return stream;
  }
}