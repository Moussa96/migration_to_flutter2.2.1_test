import 'package:cashless/Configurations.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomWidget {
  static Widget buildRaisedButton(BuildContext context,
      Function onPressedTarget, Object param, String buttonLabel,
      {double? width,
      double? height,
      bool isEnabled = true,
      bool addExtraParams = false,
      Object? extraParam1,
      Object? extraParam2,
      Object? extraParam3}) {
    return RaisedButton(
        onPressed: isEnabled
            ? () async {
                FocusScope.of(context).unfocus();
                if (param == null) {
                  onPressedTarget(context);
                } else {
                  if (addExtraParams) {
                    onPressedTarget(
                        context, param, extraParam1, extraParam2, extraParam3);
                  } else {
                    onPressedTarget(context, param);
                  }
                }
              }
            : null,
        child: Container(
          width: width, //size.width,
          height: height, //size.height*0.08,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(4)),
            boxShadow: [
              BoxShadow(
                  color: GRAY_COLOR,
                  offset: Offset(0, 1),
                  blurRadius: 2,
                  spreadRadius: 0)
            ],
            color: SECONDARY_COLOR,
          ),
          child: Center(
            child: Text(
              buttonLabel,
              style: GoogleFonts.lato(
                  color:  Colors.white,
                  fontWeight: FontWeight.w700,
                  fontStyle:  FontStyle.normal,
                  fontSize: 16.0
              ),

            ),
          ),
        ));
  }
}
