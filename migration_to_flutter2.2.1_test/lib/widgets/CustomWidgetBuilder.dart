import 'package:cashless/Configurations.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Product.dart';
import 'package:cashless/pages/fragments/KF_Drawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class CustomWidgetBuilder {
  static Widget buildRaisedButton(BuildContext context,
      Function onPressedTarget, Object? param, String buttonLabel,
      {IconData? icon,
      required MainAxisSize size,
      Color color = MINOR_COLOR,
      bool isEnabled = true,
      bool addExtraParams = false,
      Object? extraParam1,
      Object? extraParam2,
      Object? extraParam3}) {
    return ElevatedButton(
      child: Padding(
        padding: EdgeInsets.all(5),
        child: Row(
          mainAxisSize: size,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            icon != null
                ? Icon(
                    icon,
                    color: Colors.white,
                  )
                : Container(),
            Text(
              buttonLabel,
              style: TextStyle(color: MAJOR_BUTTONS_TEXT_COLOR, fontSize: 20.0),
            ),
          ],
        ),
      ),
      onPressed: isEnabled
          ? () async {
              FocusScope.of(context).unfocus();
              if (param == null) {
                onPressedTarget(context);
              } else {
                if (addExtraParams) {
                  onPressedTarget(
                      context, param, extraParam1, extraParam2, extraParam3);
                } else {
                  onPressedTarget(context, param);
                }
              }
            }
          : null,
      style: ElevatedButton.styleFrom(
        primary: color,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
            side: BorderSide(color: color, width: 0)),
      ),
    );
  }

  static Widget buildFloationActionButton(
      BuildContext context, IconData icon, Function onPressedTarget) {
    return FloatingActionButton(
      onPressed: () async {
        onPressedTarget(context);
      },
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Icon(icon, color: Colors.white),
      ),
      backgroundColor: MAJOR_COLOR,
    );
  }

  static Widget buildFormInputText(
      {TextEditingController? controller,
      String hint = '',
      String? errorMessage,
      RegExp? expression,
      String? valueResult,
      String? expErrorMessage,
      int? maxLength,
      double? maxValue,
      double? minValue,
      bool isPassword = false,
      TextInputType inputType = TextInputType.text,
      bool isEnabled = true}) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: TextFormField(
          controller: controller,
          enabled: isEnabled,
          maxLength: maxLength,
          obscureText: isPassword,
          style: MAJOR_TEXT_STYLE,
          keyboardType: inputType,
          decoration: InputDecoration(hintText: hint),
          validator: (String? value) {
            if (errorMessage != null) {
              if (value!.isEmpty) {
                return errorMessage;
              }
            }
            if (value!.isNotEmpty &&
                expression != null &&
                !expression.hasMatch(value)) {
              return expErrorMessage;
            }
            if (maxValue != null && minValue != null) {
              if (double.parse(value) > maxValue ||
                  double.parse(value) < minValue) {
                return errorMessage;
              }
            }
            return null;
          }),
    );
  }

  static Widget messageText(String title, TextStyle titleTextStyle,
      String message, TextStyle messageTextStyle) {
    return Column(children: <Widget>[
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
        child: Text(
          title,
          style: titleTextStyle,
        ),
      ),
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: Text(
          message,
          style: messageTextStyle,
        ),
      ),
    ]);
  }

  static Widget buildText(String text,
      {double fontSize = 18,
      FontWeight fontWeight = FontWeight.bold,
      TextAlign align = TextAlign.center,
      Color textColor = Colors.black}) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Text(
        text,
        textAlign: align,
        style: TextStyle(
            fontSize: fontSize, fontWeight: fontWeight, color: textColor),
      ),
    );
  }

  static Widget buildHomeMenu(BuildContext context, Function onTapTarget,
      {String? title,
      IconData? icon,
      required KFDrawerContent route,
      double? titleFontSize}) {
    return title != null
        ? Card(
            elevation: 1,
            color: Colors.white,
            child: InkWell(
              highlightColor: Colors.white60,
              onTap: () {
                Navigator.of(context).push(CupertinoPageRoute(
                  fullscreenDialog: true,
                  builder: (BuildContext context) {
                    return route;
                  },
                ));
              },
              child: Container(
                  child: Column(children: <Widget>[
                    Icon(icon, color: MINOR_COLOR, size: 35),
                    Text(title,
                        style: TextStyle(
                            fontSize: titleFontSize,
                            fontWeight: FontWeight.normal,
                            color: MINOR_COLOR),
                        textAlign: TextAlign.center),
                  ]),
                  // decoration: BoxDecoration(
                  //   borderRadius: BorderRadius.circular(1),
                  //   border: Border.all(color: Colors.white, width: 0.5)
                  // ),
                  height: 120,
                  padding: EdgeInsets.only(top: 25.0)),
            ))
        : Container();
  }

  static Widget buildAsteriskText(String text, {double fontSize = 14}) {
    return Text(text,
        style: TextStyle(
            fontSize: fontSize,
            fontWeight: FontWeight.bold,
            color: Colors.red));
  }

  static Widget buildScrollCards(
      BuildContext context, scrollType,  List<Product> items,
      {Function? decrement, Function? increment}) {
    return Expanded(
        child: ListView(
            scrollDirection: scrollType,
            children: List.generate(items.length, (int i) {
              return SizedBox(
                width: items.length == 1
                    ? MediaQuery.of(context).size.width * 0.97
                    : MediaQuery.of(context).size.width * 0.92,
                child: Card(
                  clipBehavior: Clip.antiAlias,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)),
                  child: Column(
                    children: <Widget>[
                      Padding(
                          padding: const EdgeInsets.fromLTRB(0, 10, 0, 3),
                          child: Text(items[i]!.title!,
                              style: TextStyle(fontSize: 18))),
                      items[i].categoryName != null
                          ? Padding(
                              padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                              child: Text(items[i]!.categoryName!,
                                  style: TextStyle(
                                      fontSize: 10,
                                      color: Colors.black.withOpacity(0.6))),
                            )
                          : Container(),
                      items[i].imageUrl != null
                          ? Image.network(items[i]!.imageUrl!,
                              height: MediaQuery.of(context).size.height * 0.48,
                              fit: BoxFit.cover)
                          : Container(),
                      items[i].description != null
                          ? Padding(
                              padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child: Html(
                                data: items[i].description,
                                  style: {
                                  "html": Style.fromTextStyle(TextStyle(
                                  color: Colors.black.withOpacity(0.6)))},
                              ),
                              )
                                  : Container(),
                              items[i].price != null
                              ? Container(
                              alignment: Alignment.centerLeft,
                              child: Padding(
                              padding:
                              const EdgeInsets.fromLTRB(10, 0, 0, 0),
                              child: Text(
                              items[i].price.toString() + " Units",
                                      style: TextStyle(color: Colors.green))))
                          : Container(),
                      decrement != null && increment != null
                          ? ButtonBar(
                              alignment: MainAxisAlignment.center,
                              children: [
                                IconButton(
                                  icon: Icon(Icons.remove),
                                  onPressed: () async {
                                    decrement(i);
                                  },
                                ),
                                Text(items[i].orderedAmount.toString()),
                                IconButton(
                                  icon: Icon(Icons.add),
                                  onPressed: () async {
                                    increment(i);
                                  },
                                )
                              ],
                            )
                          : Container(),
                    ],
                  ),
                ),
              );
            })));
  }

  static void buildAlertDialog(BuildContext context, Function onPressedTarget,
      Object param, String message, String buttonLabel,
      {bool barrierDismissible = false,
      bool cancelButton = false,
      required String labelCancelButton}) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(message),
        actions: <Widget>[
          FlatButton(
            child: Text(buttonLabel),
            onPressed: () {
              FocusScope.of(context).unfocus();
              if (param == null) {
                onPressedTarget(context);
              } else {
                onPressedTarget(context, param);
              }
            },
          ),
          cancelButton ?
          TextButton(
            child: Text(labelCancelButton),
            onPressed: () {
              FocusScope.of(context).unfocus();
              Navigator.pop(context);
            },
          )
              : Container(),
        ],
      ),
      barrierDismissible: barrierDismissible,
    );
  }

  static Widget buildSpinner(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      color: PALE_GRAY,
      height: 0.75 * size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SpinKitRing(
            color: PRIMARY_COLOR,
            size: 200.0,
          ),
          Padding(
            padding: const EdgeInsets.all(100.0),
            child: Text(getTranslated(context, "loading")),
          )
        ],
      ),
    );
  }

  static Widget buildCategoryCards(
      List<dynamic> categories, String labelName, Function selectTarget,
      {double width = 150,
      double height = 150,
      List<Color>? cardColor,
      int? subCategoryIndex,
      bool showImg = true}) {
    return SizedBox(
      height: height,
      child: ListView.builder(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: categories.length,
        itemBuilder: (BuildContext context, int index) => Container(
          width: width,
          child: Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            color: cardColor != null ? cardColor[index] : Colors.blue[700],
            child: InkWell(
              borderRadius: BorderRadius.circular(10),
              onTap: () {
                subCategoryIndex != null
                    ? selectTarget(subCategoryIndex, index)
                    : selectTarget(index);
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    showImg
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(5.0),
                            child: FadeInImage.assetNetwork(
                              height: height * 0.4,
                              placeholder: 'lib/assets/services_default.jpeg',
                              image: labelName == 'category_name' ||
                                      labelName == 'category_arabic_name'
                                  ? MFS_IMGS_BASE_URL +
                                      "categories/" +
                                      categories[index].category_code +
                                      ".png"
                                  : labelName == 'service_name' ||
                                          labelName == 'service_arabic_name'
                                      ? MFS_IMGS_BASE_URL +
                                          "services/" +
                                          categories[index].service_code +
                                          ".png"
                                      // :labelName =='package_name' || labelName =='package_arabic_name'?categories[index].service_code['package_code'] +".PNG"
                                      : "",
                            ),
                          )
                        : Container(),
                    Padding(
                      padding: const EdgeInsets.only(top: 2.0),
                      child: Text(
                          labelName == 'category_name'
                              ? categories[index].category_name
                              : labelName == 'category_arabic_name'
                                  ? categories[index].category_arabic_name
                                  : labelName == 'service_name'
                                      ? categories[index].service_name
                                      : labelName == 'service_arabic_name'
                                          ? categories[index]
                                              .service_arabic_name
                                          : categories[index][labelName],
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 10,
                              fontWeight: FontWeight.bold)),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  static Widget? buildSnackBar(
      GlobalKey<ScaffoldState> scaffoldKey, BuildContext context, String text,
      {int duration = 999999}) {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        duration: Duration(seconds: duration),
        content: Text(text),
        action: SnackBarAction(
            label: getTranslated(context, "dismiss"),
            onPressed: () {
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
            })));
  }
}
