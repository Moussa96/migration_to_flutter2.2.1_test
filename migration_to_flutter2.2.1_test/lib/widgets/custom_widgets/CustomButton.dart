import 'package:cashless/Configurations.dart';
import 'package:cashless/widgets/custom_widgets/CustomText.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final String? text;
  final Color color;
  final Color textColor;
  final Function? onPressed;
  final Alignment? alignment;
  final double padding;
  final double borderRadius;
  final bool isEnabled;

  const CustomButton(
      {Key? key,
      this.text,
      this.textColor = Colors.white,
      this.onPressed,
      this.alignment,
      this.color = PRIMARY_COLOR,
      this.isEnabled = true,
      this.padding = 20,
      this.borderRadius = 4
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      child:
          CustomText(text: text ?? '', alignment: Alignment.center, color: textColor),
      style: TextButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(borderRadius),
              side: BorderSide(color: color),
      ),
          padding: EdgeInsets.all(padding),
          primary: color,
          onSurface: Colors.grey,
      ),
      onPressed: isEnabled? () {
        onPressed!();
      }: null,
    );
  }
}
