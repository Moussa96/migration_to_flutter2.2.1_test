import 'package:cashless/Configurations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomAppBar extends PreferredSize {
  final double height;
  final String? title;

  CustomAppBar({Key? key, this.height = kToolbarHeight, this.title})
      : super(child: CustomAppBar(), preferredSize: Size.fromHeight(height));

  @override
  Size get preferredSize => Size.fromHeight(height);
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: PRIMARY_COLOR,
      iconTheme: IconThemeData(color: Colors.white),
      elevation: 2,
      centerTitle: true,
      title: Text(
          title ?? "",
          style: GoogleFonts.lato(
              color: Colors.white,
              fontWeight: FontWeight.normal
          )
      ),
    );
  }

}
