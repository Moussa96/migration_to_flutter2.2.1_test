import 'package:cashless/Configurations.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/widgets/custom_widgets/CustomSwitch.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Mfs/MfsScenarioConfiguration.dart';
import 'package:cashless/models/Mfs/MfsServices.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';

class CashCallCustomWidget {
  static Widget buildTopConfirmCashCallServiceCard(
      {Size? size,
      String? serviceCode,
      String? serviceName,
      String? messagePartOne,
      String? messagePartTwo}) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(3.408)),
          boxShadow: [
            BoxShadow(
                color: CASHCALL_BACKGROUND_COLOR,
                offset: Offset(0, 2),
                blurRadius: 3,
                spreadRadius: 0)
          ],
          color: Colors.white),
      child: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  width: 27.000000000000004,
                  height: 27,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5.112)),
                      boxShadow: [
                        BoxShadow(
                            color: const Color(0x33000000),
                            offset: Offset(0, 1),
                            blurRadius: 2,
                            spreadRadius: 0)
                      ],
                      color: Colors.white),
                  child: FadeInImage.assetNetwork(
                      placeholder: 'lib/assets/historyAccountIcWithBg.png',
                      image: MFS_IMGS_BASE_URL +
                          "services/" +
                          serviceCode! +
                          ".png"),
                ),
                SizedBox(
                  width: 5,
                ),
                Text(serviceName!,
                    style: GoogleFonts.lato(
                        color: PRIMARY_COLOR,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                        fontSize: 13.6))
              ],
            ),
            SizedBox(
              height: 6,
            ),
            Text('$messagePartOne $serviceName $messagePartTwo',
                style: GoogleFonts.lato(
                    color: CASHCALL_TITLE,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    fontSize: 11.9))
          ],
        ),
      ),
    );
  }

  static Widget buildDataConfirmCashCallServiceCard(
      {Size? size,
      required String label,
      required String data,
      Color color = Colors.white,
      Color labelFontColor = PRIMARY_COLOR,
      Color dataFontColor = Colors.black,
      double labelFontSize = 11.9,
      double dataFontSize = 11.9,
      bool icon = false}) {
    return Container(
      alignment: Alignment.center,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(3.408)),
          boxShadow: [
            BoxShadow(
                color: GRAY_COLOR,
                offset: Offset(0, 1),
                blurRadius: 2,
                spreadRadius: 0)
          ],
          color: color),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(label,
                overflow: TextOverflow.clip,
                style: GoogleFonts.lato(
                    color: labelFontColor,
                    fontWeight: FontWeight.w400,
                    fontStyle: FontStyle.normal,
                    fontSize: labelFontSize)),
            SizedBox(width: 10,),
            Flexible(
              flex: 12,
              child: Text(
                data,
                style: GoogleFonts.lato(
                    color: dataFontColor,
                    fontWeight: FontWeight.w700,
                    fontStyle: FontStyle.normal,
                    fontSize: dataFontSize),
                overflow: TextOverflow.clip,
              ),
            ),
          ],
        ),
      ),
    );
  }

  static Widget buildChangeDueAmount({required Size size}) {
    return Container(
      width: size.width,
      height: size.height,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(3.408)),
          boxShadow: [
            BoxShadow(
                color: const Color(0x3d000000),
                offset: Offset(0, 1),
                blurRadius: 2,
                spreadRadius: 0)
          ],
          color: Colors.white),
      child: Column(
        children: [
          Row(
            children: [],
          )
        ],
      ),
    );
  }

  static Widget buildSwitch(Function onSwitch, {required String title, required bool checked}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 8),
          child: Text(
            title,
            style: GoogleFonts.lato(
                color: PRIMARY_COLOR,
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                fontSize: 11.9),
          ),
        ),
        CustomSwitch(
            activeColor: SWITCH_COLOR,
            value: checked,
            onChanged: (val) {
              onSwitch(val);
            }),
      ],
    );
  }

  static Widget buildInputField(TextEditingController amountController,
      {String hint = '', required String error, required double max, required double min,TextInputType inputType = TextInputType.text}) {
    return TextFormField(
      decoration: new InputDecoration(
          contentPadding: EdgeInsets.all(10),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(3.408)),
            borderSide: BorderSide(
              color: BORDER_COLOR,
              width: 0.9,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(3.408)),
            borderSide: BorderSide(
              color: BORDER_COLOR,
              width: 0.9,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(3.408)),
            borderSide: BorderSide(
              color: BORDER_COLOR,
              width: 0.9,
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(3.408)),
            borderSide: BorderSide(
              color: Colors.red,
              width: 0.9,
            ),
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(3.408)),
            borderSide: BorderSide(
              color: BORDER_COLOR,
              width: 0.9,
            ),
          ),
          hintText: hint,
          hintStyle: GoogleFonts.lato(
              color: HINT_COLOR,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              fontSize: 11.9)),
      style: TextStyle(fontSize: 18),
      keyboardType: inputType,
      controller: amountController,
      validator: (value) {
        if (!Util.isNumeric(value!)) {
          return error; //Localization.of(context).locale.languageCode=='en'?trxParams[_amountIndex].err_en:trxParams[_amountIndex].err;
        }
        if (double.parse(value) > max || double.parse(value) < min) {
          return error; //Localization.of(context).locale.languageCode=='en'?trxParams[_amountIndex].err_en:trxParams[_amountIndex].err;
        }
        return null;
      },
    );
  }

  static Widget buildChangePackageCard(BuildContext context, Function onChange,
      {required String label, required String value, required String btnLabel}) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(3.408)),
          boxShadow: [
            BoxShadow(
                color: const Color(0x3d000000),
                offset: Offset(0, 1),
                blurRadius: 2,
                spreadRadius: 0)
          ],
          color: Colors.white),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(label,
                    style: GoogleFonts.lato(
                        color: PRIMARY_COLOR,
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        fontSize: 11.9)),
                SizedBox(
                  height: 3,
                ),
                Text(value,
                    style: GoogleFonts.lato(
                        color: BLACK_COLOR,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                        fontSize: 11.9),
                    textAlign: TextAlign.right),
              ],
            ),
            InkWell(
              onTap: () {
                onChange(context);
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 8.0, top: 8.0),
                child: Row(
                  children: [
                    Image.asset('lib/assets/services_change_ic.png'),
                    SizedBox(
                      width: 4,
                    ),
                    Text(btnLabel,
                        style: GoogleFonts.lato(
                            color: SECONDARY_COLOR,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            fontSize: 13.6),
                        textAlign: TextAlign.right)
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  static void buildPackagePopUp(BuildContext context, Function onPress,
      { required MfsServices service,
        MfsScenarioConfiguration? scenarioConfiguration,
        required List<dynamic> packages,
      height}) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        useRootNavigator: true,
        isDismissible: false,
        enableDrag: false,
        builder: (context) => Container(
            height: height, //size.height * 0.6,
            decoration: BoxDecoration(boxShadow: [
              BoxShadow(
                  color: const Color(0x33000000),
                  offset: Offset(0, 1),
                  blurRadius: 2,
                  spreadRadius: 0)
            ], color: BACKGROUND_COLOR),
            child: SingleChildScrollView(
              padding: const EdgeInsets.only(
                  bottom: 5, left: 10, right: 10, top: 20),
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        Localization.of(context).locale.languageCode == 'en'
                            ? service.service_name!
                            : service.service_arabic_name!,
                        style: GoogleFonts.lato(
                            color: PRIMARY_COLOR,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                            fontSize: 13.6),
                      ),
                      CustomWidgetBuilder.buildCancelbtn(context),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Column(
                    children: List.generate(packages.length, (index) {
                      return Padding(
                        padding: const EdgeInsets.only(bottom: 5),
                        child: InkWell(
                          onTap: () {
                            if (scenarioConfiguration == null)
                              onPress(context, service, packages[index]);
                            else
                              onPress(context, service, packages[index],
                                  scenarioConfiguration);
                          },
                          child: buildPackageCard(context,
                              Localization.of(context).locale.languageCode ==
                                      'en'
                                  ? packages[index]['package_name']
                                  : packages[index]['package_arabic_name']),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            )));
  }

  static Widget buildPackageCard(BuildContext context,String title) {
    return Container(
      padding: EdgeInsets.only(left: 10, top: 15, bottom: 16, right: 7),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(3.408)),
          boxShadow: [
            BoxShadow(
                color: const Color(0x1f000000),
                offset: Offset(0, 1),
                blurRadius: 3,
                spreadRadius: 0)
          ],
          color: Colors.white),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(title,
              style: GoogleFonts.lato(
                  color: PRIMARY_COLOR,
                  fontWeight: FontWeight.w700,
                  fontStyle: FontStyle.normal,
                  fontSize: 13.6)),
          Localization.of(context).locale.languageCode == 'en'?Image.asset("lib/assets/services_arrow.png"):
          RotatedBox(child:Image.asset("lib/assets/services_arrow.png") ,quarterTurns: 90,),
        ],
      ),
    );
  }

  static void buildServicePopUpForm(BuildContext context, buildFormView,
      {MfsScenarioConfiguration? scenarioConfiguration, MfsServices? service}) {
    final size = MediaQuery.of(context).size;
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return Container(
          padding: EdgeInsets.only( bottom: MediaQuery.of(context).viewInsets.bottom),
          width: size.width,
          //height: size.height * 0.85,
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
                color: BOXBORDER_COLOR,
                offset: Offset(0, 1),
                blurRadius: 2,
                spreadRadius: 0)
          ], color: Colors.white),
          child: SingleChildScrollView(
              child: buildFormView(context,
                  scenarioConfiguration: scenarioConfiguration,
                  service: service)),
        );
      },
    );
  }
}
