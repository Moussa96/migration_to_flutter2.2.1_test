import 'package:cashless/Configurations.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:contact_picker/contact_picker.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomTextFormField extends StatefulWidget {
  final String label;
  final String? hint;
  final String? errorMessage;
  final RegExp? expression;
  final String? expErrorMessage;
  final Function? validator;
  final TextInputType? inputType;
  int? maxLine;
  bool isPassword;
  bool addShowBtn;
  bool addContactButton;
  final bool? isEnabled;
  final int? maxLength;
  final double labelFontSize;
  final double? maxValue;
  final double? minValue;
  TextEditingController? controller;
  Color focusBorderColor;

  CustomTextFormField(
      {Key? key,
      required this.label,
      this.hint,
      this.validator,
      this.inputType,
      this.isPassword = false,
      this.addShowBtn = false,
      this.addContactButton = false,
      this.isEnabled,
      this.errorMessage,
      this.expression,
      this.expErrorMessage,
      this.maxLength,
      this.maxValue,
      this.minValue,
      this.controller,
      this.labelFontSize = 14,
      this.maxLine = 1,
      this.focusBorderColor = PRIMARY_COLOR})
      : super(key: key);

  @override
  _CustomTextFormFieldState createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends State<CustomTextFormField> {
  @override
  void initState() {
    super.initState();
  }

  void _toggle() {
    setState(() {
      widget.isPassword = !widget.isPassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          Text(widget.label,
          style: GoogleFonts.lato(
              color: TEXT_PRIMARY_COLOR,
              fontWeight: FontWeight.w700,
              fontStyle: FontStyle.normal,
            fontSize: widget.labelFontSize,
                )),
          SizedBox(height: 10),
          Container(
            color: Colors.white,
            child: TextFormField(
                maxLines: widget.maxLine,
                controller: widget.controller,
                obscureText: widget.isPassword,
                enabled: widget.isEnabled,
                maxLength: widget.maxLength,
                keyboardType: widget.inputType,
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: widget.focusBorderColor),
                  ),
                  border: OutlineInputBorder(
                    borderSide: BorderSide(color: BORDER_COLOR, width: 0.8),
                  ),
                  hintText: widget.hint,
                  hintStyle:
                      TextStyle(fontSize: 14, color: Colors.grey.shade500),
                  suffixIcon: widget.addShowBtn
                      ? GestureDetector(
                          onTap: _toggle,
                          child: Text(
                              widget.isPassword
                                  ? getTranslated(context, "show_btn")
                                  : getTranslated(context, "hide_btn"),
                              style: TextStyle(color: TEXT_PRIMARY_COLOR)))
                      : widget.addContactButton
                          ? IconButton(
                    iconSize: 28,
                              icon: Icon(
                                Icons.person_add,
                                color: SECONDARY_COLOR,
                              ),
                              onPressed: () async {
                                final PermissionStatus? permissionStatus =
                                    await _getPermission();
                                if (permissionStatus ==
                                    PermissionStatus.granted) {
                                  final ContactPicker contactPicker =
                                      new ContactPicker();
                                  Contact contact =
                                      await contactPicker.selectContact();
                                  setState(() {
                                    widget.controller!.text =
                                        contact.phoneNumber.number.replaceAll(new RegExp(r"\s+"), "");
                                  });
                                }
                              })
                          : null,
                ),
                validator: (String? value) {
                  if (widget.errorMessage != null) {
                    if (value!.isEmpty) {
                      return widget.errorMessage;
                    }
                  }
                  if (value!.isNotEmpty &&
                      widget.expression != null &&
                      !widget.expression!.hasMatch(value)) {
                    return widget.expErrorMessage;
                  }
                  if (widget.maxValue != null && widget.minValue != null) {
                    if (double.parse(value) > widget.maxValue! ||
                        double.parse(value) < widget.minValue!) {
                      return widget.errorMessage;
                    }
                  }
                  return null;
                }),
          ),
        ],
      ),
    );
  }

  //Check contacts permission
  Future<PermissionStatus?> _getPermission() async {
    final PermissionStatus permission = await Permission.contacts.status;
    if (permission != PermissionStatus.granted) {
      final Map<Permission, PermissionStatus> permissionStatus =
          await [Permission.contacts].request();
      return  permissionStatus[Permission.contacts];
    } else {
      return permission;
    }
  }
}
