import 'package:cashless/Configurations.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';

class CustomCheckBox extends StatefulWidget {
  bool checked ;
  final ValueChanged<bool> onChanged;
  @override
  _CustomCheckBoxState createState() => _CustomCheckBoxState();
  CustomCheckBox({Key? key,required this.checked,required this.onChanged}) : super();
}
class _CustomCheckBoxState extends State<CustomCheckBox>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      crossAxisAlignment:CrossAxisAlignment.center ,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InkWell(
          onTap: (){
            widget.onChanged(! widget.checked);
          },
          child:Image.asset(widget.checked?'lib/assets/checkbox_selected.png':'lib/assets/checkbox_empty.png'),
        ),
        SizedBox(width: 5,),
        CustomWidgetBuilder.buildText(getTranslated(context,'remember_my_password'),
                color:PRIMARY_COLOR,
                fontWeight: widget.checked ? FontWeight.w700:FontWeight.w400,
                fontStyle:  FontStyle.normal,
                fontSize: 11.9
            )

      ],
    );
  }

}