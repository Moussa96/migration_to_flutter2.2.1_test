import 'package:cashless/Configurations.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class CustomWidgetBuilder {
  static Widget buildSettingCategory(
      BuildContext context, Function onPress, Object object,
      {String? imagePath, String? title, required String iconPath}) {
    return InkWell(
      onTap: () {
        object != null ? onPress(context, object):onPress(context);
      },
      child: Container(
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
              color: const Color(0x1f000000),
              offset: Offset(0, -1),
              blurRadius: 0,
              spreadRadius: 0)
        ], color: Colors.white),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Image.asset(imagePath!),
                  SizedBox(
                    width: 10,
                  ),
                  Text(title ?? '',
                      style: GoogleFonts.lato(
                          color: PRIMARY_COLOR,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                          fontSize: 11.9))
                ],
              ),
              RotatedBox(child:Image.asset(iconPath) ,quarterTurns:Localization.of(context).locale.languageCode == 'ar'? 90:180,),
            ],
          ),
        ),
      ),
    );
  }

  static Widget buildSettingTitle(String label) {
    return Padding(
      padding: const EdgeInsets.only(left: 10,right: 10),
      child: Text(label,
          style: GoogleFonts.lato(
              color: CASHCALL_TITLE,
              fontWeight: FontWeight.w400,
              fontStyle: FontStyle.normal,
              fontSize: 10.2)),
    );
  }
  static void buildLoadingWidget(BuildContext context){
    final size = MediaQuery.of(context).size;
    showModalBottomSheet(
      context: context,
      useRootNavigator: true,
      isScrollControlled: true,
      isDismissible: false,
      enableDrag: false,
      builder: (context) => Container(
          height: size.height*0.08,
          decoration: BoxDecoration(
              boxShadow: [BoxShadow(
                  color: GRAY_BOXBORDER_COLOR,
                  offset: Offset(0,2),
                  blurRadius: 4,
                  spreadRadius: 0
              )] ,
              color: BACKGROUND_COLOR
          ),
          child: SingleChildScrollView(
            padding: const EdgeInsets.only(left: 10,right: 10,top: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Image.asset('lib/assets/loading_spinner.png'),
                    SizedBox(width: 5,),
                    Text(
                        getTranslated(context, 'loading_data'),
                        style: GoogleFonts.lato(
                            color:  PRIMARY_COLOR,
                            fontWeight: FontWeight.w700,
                            fontStyle:  FontStyle.normal,
                            fontSize: 13.6
                        ),
                        textAlign: TextAlign.center
                    )
                  ],
                ),
                buildCancelbtn(context),

              ],
            ),
          )

      ),
    );
  }
  static Widget? buildSnackBar(GlobalKey<ScaffoldState> scaffoldKey, BuildContext context, String text, {int duration = 999999}){
    ScaffoldMessenger.of(context).hideCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
            duration: Duration(seconds: duration),
            content: Text(text),
            action: SnackBarAction(
                label: getTranslated(context, "dismiss"),
                onPressed: () {
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                }
            )
        )
    );
  }
  static Widget buildCategoryCards(List<dynamic> categories, String labelName, Function selectTarget,{double width=85, double height=45, List<bool>? active, int? subCategoryIndex, bool showImg = true }){
    return Container(
      height: height,
      child: ListView.builder(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: categories.length,
        itemBuilder: (BuildContext context, int index) =>
            Container(
              //width: width,
              padding: EdgeInsets.only(left: 18,right: 18),
              decoration: active!=null && active[index]
                  ? BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 2.0, color: ACTIVE_CASHCALL_ITEMS),
                ),
              )
                  : null,
              child: InkWell(
                onTap : (){ subCategoryIndex!=null? selectTarget(subCategoryIndex, index):selectTarget(index);},
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child:Text(
                            labelName =='category_name'?categories[index].category_name: labelName =='category_arabic_name'?categories[index].category_arabic_name
                                :labelName =='service_name'?categories[index].service_name: labelName =='service_arabic_name'?categories[index].service_arabic_name
                                :categories[index][labelName],
                            style: TextStyle(
                                color: active!=null && active[index]?ACTIVE_CASHCALL_ITEMS:UNACTIVE_CASHCALL_ITEMS,
                                fontWeight: FontWeight.w400,
                                fontStyle:  FontStyle.normal,
                                fontSize: 14.0
                            ),
                            textAlign: TextAlign.center
                        )

                    ),
                  ],
                ),
              ),
            ),
      ),
    );
  }
  static Widget buildSpinner(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      color: PALE_GRAY,
      height: 0.75*size.height ,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SpinKitRing(
            color: PRIMARY_COLOR,
            size: 200.0,
          ),
          Padding(
            padding: const EdgeInsets.all(100.0),
            child: Text(getTranslated(context, "loading")),
          )
        ],
      ),
    );
  }
  static Widget buildCancelbtn(BuildContext context){
    return InkWell(
        onTap: () {
          Navigator.pop(context);
        },
        child: Row(
          children: [
            Image.asset(
                'lib/assets/services_list_cancel_ic.png'),
            SizedBox(
              width: 3,
            ),
            Text(
              getTranslated(context, 'home_cancel_btn'),
              style: GoogleFonts.lato(
                  color: CASHCALL_TITLE,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  fontSize: 11.9),
            ),
          ],
        ));
  }
  static showAlertDialog(BuildContext context,{Color color=SECONDARY_COLOR}) {
    AlertDialog alert = AlertDialog(
        content: Row(
          children: [
            CircularProgressIndicator(
                valueColor: new AlwaysStoppedAnimation<Color>(
                    color)),
            SizedBox(width: 10,),
            Text(
                getTranslated(context, 'loading_data'),
                style: GoogleFonts.lato(
                    color:  PRIMARY_COLOR,
                    fontWeight: FontWeight.w700,
                    fontStyle:  FontStyle.normal,
                    fontSize: 13.6
                ),
                textAlign: TextAlign.center
            )
          ],
        ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  static Widget buildText(String label,
      {Color color = Colors.white,
        double fontSize = 10,
        FontWeight fontWeight = FontWeight.w400,
        FontStyle fontStyle = FontStyle.normal,
        TextAlign textAlign = TextAlign.center}) {
    return Text(
      label,
      style: GoogleFonts.lato(
        color: color,
        fontSize: fontSize,
        fontWeight: fontWeight,
        fontStyle: fontStyle,
      ),
      textAlign: textAlign,
    );
  }

  static Widget buildRaisedButton(
      BuildContext context,
      Function onPressedTarget,
      Object? param,
      String buttonLabel, {
        IconData? icon,
        MainAxisSize? size,
        Color color = SECONDARY_COLOR,
        bool isEnabled = true,
        bool addExtraParams = false,
        Object? extraParam1,
        Object? extraParam2,
        Object? extraParam3,
        FontWeight fontWeight = FontWeight.w700,
        FontStyle fontStyle = FontStyle.normal,
        double fontSize = 13.6,
        Color fontColor = Colors.white,
      }) {
    return RaisedButton(
      color: color,
      onPressed: isEnabled
          ? () async {
        FocusScope.of(context).unfocus();
        if (param == null) {
          onPressedTarget(context);
        } else {
          if (addExtraParams) {
            onPressedTarget(
                context, param, extraParam1, extraParam2, extraParam3);
          } else {
            onPressedTarget(context, param);
          }
        }
      }
          : null,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            icon != null
                ? Icon(
              icon,
              color: Colors.white,
            )
                : Container(),
            Text(
              buttonLabel,
              style: GoogleFonts.lato(
                  color: fontColor,
                  fontWeight: fontWeight,
                  fontStyle: fontStyle,
                  fontSize: fontSize),
            ),
          ],
        ),
      ),
    );
  }
  static void buildAlertDialog(BuildContext context,Function onPressedTarget, Object param,String message,String buttonLabel,{bool barrierDismissible = false,bool cancelButton = false,String? labelCancelButton}){
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(message),
        actions: <Widget>[
          FlatButton(
            child: Text(buttonLabel),
            onPressed: () {
              FocusScope.of(context).unfocus();
              if (param == null) {
                onPressedTarget(context);
              } else {
                onPressedTarget(context, param);
              }
            },
          ),
          cancelButton ? TextButton(
            child: Text(labelCancelButton ?? ''),
            onPressed: () {
              FocusScope.of(context).unfocus();
              Navigator.pop(context);
            },
          )
          :Container(),
        ],
      ),
      barrierDismissible: barrierDismissible,
    );
  }
  static Widget buildTextBtn(BuildContext context,Function onPressedTarget,String label,{Color color = Colors.white,
    double fontSize = 10,
    FontWeight fontWeight = FontWeight.w400,
    FontStyle fontStyle = FontStyle.normal,
    TextAlign textAlign = TextAlign.center}){
    return InkWell(
      onTap: (){
        onPressedTarget(context);
      },
      child: buildText(label,color: color,fontSize: fontSize,fontStyle: fontStyle,fontWeight: fontWeight,textAlign: textAlign),
    );
  }
}
