import 'package:flutter/material.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomSwitch extends StatefulWidget {
  final bool value;
  final ValueChanged<bool> onChanged;
  final Color? activeColor;

  const CustomSwitch({Key? key, required this.value, required this.onChanged, this.activeColor})
      : super(key: key);

  @override
  _CustomSwitchState createState() => _CustomSwitchState();
}

class _CustomSwitchState extends State<CustomSwitch>
    with SingleTickerProviderStateMixin {
  late Animation _circleAnimation;
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 60));
    _circleAnimation = AlignmentTween(
        begin: widget.value ? Alignment.centerRight : Alignment.centerLeft,
        end: widget.value ? Alignment.centerLeft : Alignment.centerRight)
        .animate(CurvedAnimation(
        parent: _animationController, curve: Curves.linear));
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        return GestureDetector(
          onTap: () {
            if (_animationController.isCompleted) {
              _animationController.reverse();
            } else {
              _animationController.forward();
            }
            widget.value == false
                ? widget.onChanged(true)
                : widget.onChanged(false);
          },
          child: Container(
            height: 35.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: _circleAnimation.value == Alignment.centerLeft
                    ? Colors.grey
                    : widget.activeColor),
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 4.0, bottom: 4.0, right: 4.0, left: 4.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  _circleAnimation.value == Alignment.centerRight
                      ?
                  Padding(
                    padding: const EdgeInsets.only(right: 7),
                    child: Text(
                      getTranslated(context,'on'),
                      style: GoogleFonts.lato(
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                          fontSize: 14.2),
                    ),
                  )

                      : Container(),
                  Container(
                    width: 30.0,
                    height: 30.0,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.white),
                  ),
                  _circleAnimation.value == Alignment.centerLeft
                      ?
                  Padding(
                    padding: const EdgeInsets.only(left: 4),
                    child: Text(
                      getTranslated(context,'off'),
                      style: GoogleFonts.lato(
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                          fontSize: 14.2),
                    ),
                  )

                      : Container(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
