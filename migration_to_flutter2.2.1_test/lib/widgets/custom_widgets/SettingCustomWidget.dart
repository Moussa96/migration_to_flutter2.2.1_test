import 'package:cashless/widgets/custom_widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:cashless/Configurations.dart';
import 'package:cashless/localization/localization_utility.dart';
class SettingCustomWidget{
  static void buildLogoutAlert(BuildContext context,Function logout,double width){
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      useRootNavigator: true,
      builder: (context) => Container(
          width: width,
          decoration: BoxDecoration(
            boxShadow: [BoxShadow(
                color: GRAY_BOXBORDER_COLOR,
                offset: Offset(0,2),
                blurRadius: 4,
                spreadRadius: 0
            )] ,
            color: BACKGROUND_COLOR,
          ),
          child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset('lib/assets/more_logout_alert_ic.png'),
                SizedBox(height: 11,),
                Text(
                    getTranslated(context, 'logout_alert'),
                    style: GoogleFonts.lato(
                        color:  PRIMARY_COLOR,
                        fontWeight: FontWeight.w700,
                        fontStyle:  FontStyle.normal,
                        fontSize: 14
                    ),
                    textAlign: TextAlign.center
                ),
                SizedBox(height: 5,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 65),
                  child: Text(
                    getTranslated(context, 'logout_alert_message'),
                    style: GoogleFonts.lato(
                        color:  LOGOUT_COLOR,
                        fontWeight: FontWeight.w300,
                        fontStyle:  FontStyle.normal,
                        fontSize: 14
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 21,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 11),
                  child: CustomWidgetBuilder.buildRaisedButton(context,logout,null,getTranslated(context, 'confirm_logout_btn'),
                      color: Color(0xffcd1717),fontColor: Colors.white,fontWeight: FontWeight.w400,   fontStyle:  FontStyle.normal,
                      fontSize: 14),
                ),
                SizedBox(height: 16,),
                Container(
                    width: width*0.7,
                    height: 1,
                    decoration: BoxDecoration(
                        color: const Color(0xffd8d8d8)
                    )
                ),
                SizedBox(height: 16,),
                InkWell(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Text(
                      getTranslated(context, 'home_cancel_btn'),
                      style: GoogleFonts.lato(
                          color:  CASHCALL_TITLE,
                          fontWeight: FontWeight.w700,
                          fontStyle:  FontStyle.normal,
                          fontSize: 16.0
                      ),
                      textAlign: TextAlign.center
                  ),
                )
              ],),
          )
      ),
    );
  }

}