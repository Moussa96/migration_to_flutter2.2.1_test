import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/models/Balance.dart';
import 'package:cashless/pages/load_wallet/load_wallet.dart';
import 'package:cashless/pages/profile/profile.dart';
import 'package:cashless/pages/qr_payment/qr_payment.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../Configurations.dart';
import 'package:cashless/util/Util.dart';

class HomeCustomWidget {
  static void buildPayPopUp(BuildContext context) {
    final size = MediaQuery.of(context).size;
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
            width: size.width,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    color: GRAY_BOXBORDER_COLOR,
                    offset: Offset(0, 2),
                    blurRadius: 4,
                    spreadRadius: 0)
              ],
              color: BACKGROUND_COLOR,
            ),
            child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(vertical: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    getTranslated(context, 'home_pay_now'),
                    style: GoogleFonts.lato(
                        color: PRIMARY_COLOR,
                        fontWeight: FontWeight.w700,
                        fontStyle: FontStyle.normal,
                        fontSize: 20.0),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(getTranslated(context, 'home_select_method_message'),
                      style: GoogleFonts.lato(
                          color: const Color(0xff878787),
                          fontWeight: FontWeight.w300,
                          fontStyle: FontStyle.normal,
                          fontSize: 14.0),
                      textAlign: TextAlign.center),
                  SizedBox(
                    height: 18,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      //SizedBox(width: 12,),
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => QRPayment()));
                        },
                        child: Container(
                          width: size.width * 0.44,
                          height: size.height * 0.19,
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(6)),
                              boxShadow: [
                                BoxShadow(
                                    color: GRAY_COLOR,
                                    offset: Offset(0, 1),
                                    blurRadius: 2,
                                    spreadRadius: 0)
                              ],
                              gradient: LinearGradient(
                                  begin: Alignment(0.5, 0),
                                  end: Alignment(0.5, 1.907269636960704),
                                  colors: [PRIMARY_COLOR, SECOND_COLOR])),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'lib/assets/pay_now_qr_code.png',
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(getTranslated(context, 'home_scan_qr'),
                                  style: GoogleFonts.lato(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                      fontStyle: FontStyle.normal,
                                      fontSize: 16.0),
                                  textAlign: TextAlign.center),
                            ],
                          ),
                        ),
                      ),
                      /*InkWell(
                        onTap: (){

                        },
                        child: Container(
                          width: size.width*0.44,
                          height: size.height*0.19,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(6)
                              ),
                              boxShadow: [BoxShadow(
                                  color: GRAY_COLOR,
                                  offset: Offset(0,1),
                                  blurRadius: 2,
                                  spreadRadius: 0
                              )] ,
                              gradient: LinearGradient(
                                  begin: Alignment(0.5, 0),
                                  end: Alignment(0.5, 1.907269636960704),
                                  colors: [PRIMARY_COLOR, SECOND_COLOR])
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'lib/assets/pay_now_user_id.png',
                              ),
                              SizedBox(height: 10,),
                              Text(
                                  getTranslated(context, 'home_payNowUserId'),
                                  style: GoogleFonts.lato(
                                      color:  Colors.white,
                                      fontWeight: FontWeight.w700,
                                      fontStyle:  FontStyle.normal,
                                      fontSize: 16.0
                                  ),
                                  textAlign: TextAlign.center
                              ),

                            ],
                          ),
                        ),
                      ),*/
                      //SizedBox(width: 12,),
                    ],
                  ),
                  SizedBox(
                    height: 14,
                  ),
                  Container(
                      width: 235,
                      height: 1,
                      decoration: BoxDecoration(color: DIVIDER_COLOR)),
                  SizedBox(
                    height: 18,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Text(getTranslated(context, 'home_cancel_btn'),
                        style: GoogleFonts.lato(
                            color: CASHCALL_TITLE,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.normal,
                            fontSize: 16.0),
                        textAlign: TextAlign.center),
                  )
                ],
              ),
            ));
      },
    );
  }

  static void buildLoadWalletPopUp(BuildContext context, Balance balance) {
    final size = MediaQuery.of(context).size;
    showModalBottomSheet(
      context: context,
      builder: (context) => Container(
        width: size.width,
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
              color: GRAY_BOXBORDER_COLOR,
              offset: Offset(0, 2),
              blurRadius: 4,
              spreadRadius: 0)
        ], color: BACKGROUND_COLOR),
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Image.asset('lib/assets/wallet_details_ic.png'),
                  SizedBox(
                    width: 10,
                  ),
                  Text(getTranslated(context, 'home_wallet_details'),
                      style: GoogleFonts.lato(
                          color: PRIMARY_COLOR,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          fontSize: 15.3))
                ],
              ),
              SizedBox(
                height: 10,
              ),
              buildLoadWaletItem(context,
                  imageName: 'current_balance_ic_with_bg',
                  title: '${balance.balance.toString()}  ${balance.currency}',
                  descriptionkey: 'home_current_balance'),
              SizedBox(
                height: 10,
              ),
              buildLoadWaletItem(context,
                  imageName: 'available_balance_ic_with_bg',
                  title:
                      '${balance.availableBalance.toString()}  ${balance.currency}',
                  descriptionkey: 'home_available_balance'),
              SizedBox(
                height: 20,
              ),
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LoadWallet()));
                },
                child: Container(
                  height: size.height * 0.08,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(3.408)),
                      boxShadow: [
                        BoxShadow(
                            color: GRAY_COLOR,
                            offset: Offset(0, 1),
                            blurRadius: 2,
                            spreadRadius: 0)
                      ],
                      color: SECONDARY_COLOR),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset('lib/assets/my_wallet_load_wallet_ic.png'),
                      SizedBox(
                        width: 8,
                      ),
                      Text(
                        getTranslated(context, 'home_load_wallet'),
                        style: GoogleFonts.lato(
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontStyle: FontStyle.normal,
                            fontSize: 13.6),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Center(
                child: Container(
                    width: size.width * 0.5,
                    height: 1,
                    decoration: BoxDecoration(color: DIVIDER_COLOR)),
              ),
              SizedBox(
                height: 15,
              ),
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Center(
                  child: Text(getTranslated(context, 'home_done_btn'),
                      style: GoogleFonts.lato(
                          color: CASHCALL_TITLE,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.normal,
                          fontSize: 13.6),
                      textAlign: TextAlign.center),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  static Widget buildLoadWaletItem(BuildContext context,
      {String? imageName, String? title, String? descriptionkey}) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.08,
      width: size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5.112)),
          color: Colors.white),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 11, right: 11, top: 10),
            child: Image.asset('lib/assets/$imageName.png'),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 12,
              ),
              Text(title!,
                  style: GoogleFonts.lato(
                      color: CASHCALL_TITLE,
                      fontWeight: FontWeight.w700,
                      fontStyle: FontStyle.normal,
                      fontSize: 17.0)),
              SizedBox(
                height: 4,
              ),
              Text(getTranslated(context, descriptionkey!),
                  style: GoogleFonts.lato(
                      color: const Color(0xff98a3a5),
                      fontWeight: FontWeight.w300,
                      fontStyle: FontStyle.normal,
                      fontSize: 13.6))
            ],
          )
        ],
      ),
    );
  }

  static Widget buildHomeTopCard(BuildContext context, Balance balance) {
    final size = MediaQuery.of(context).size;
    return Container(
      color: PALE_GRAY,
      child: Container(
        width: size.width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(12)),
            gradient: LinearGradient(
                begin: Alignment(0.5, 0.5386387563848061),
                end: Alignment(0.5, 2.962704824565857),
                colors: [PRIMARY_COLOR, BLUE_COLOR])),
        child: Column(
          children: [
            Container(
                width: size.width,
                color: PRIMARY_COLOR,
                padding: EdgeInsets.only(left: 12, top: 28, right: 12),
                child: Row(
                  children: [
                    Image.asset('lib/assets/wallet_graphics.png'),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                              balance.availableBalance.toString() +
                                  " " +
                                  balance.currency,
                              style: GoogleFonts.lato(
                                  color: Colors.white, fontSize: 20)),
                          Text(
                            getTranslated(context, 'available_balance_cashout'),
                            style: GoogleFonts.lato(
                                color: LIGHT_GRAY_BLUE, fontSize: 13.6),
                          ),
                        ],
                      ),
                    )
                  ],
                )),
            Container(
                padding: EdgeInsets.only(
                    top: size.height * 0.055, bottom: size.height * 0.02),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        buildLoadWalletPopUp(context, balance);
                      },
                      child: Row(
                        children: [
                          SizedBox(
                            width: 31,
                          ),
                          Image.asset('lib/assets/load_wallet_ic.png'),
                          SizedBox(
                            width: 7,
                          ),
                          Text(getTranslated(context, 'home_load_wallet'),
                              style: GoogleFonts.lato(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  fontSize: 13.6)),
                        ],
                      ),
                    ),
                    Opacity(
                      opacity: 0.07,
                      child: Container(
                          width: 1,
                          height: 34,
                          decoration: BoxDecoration(color: Colors.white)),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Profile()));
                      },
                      child: Row(
                        children: [
                          Image.asset('lib/assets/my_qr_code_ic.png'),
                          SizedBox(
                            width: 7,
                          ),
                          Text(getTranslated(context, 'my_code'),
                              style: GoogleFonts.lato(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontStyle: FontStyle.normal,
                                  fontSize: 13.6)),
                          SizedBox(
                            width: 31,
                          ),
                        ],
                      ),
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }

  static Widget buildFormInputText(
      {TextEditingController? controller,
      String? hint = '',
      String? errorMessage,
      RegExp? expression,
      String? valueResult,
      String? expErrorMessage,
      int? maxLength,
      double? maxValue,
      double? minValue,
      bool? isPassword = false,
      TextInputType inputType = TextInputType.text,
      bool? isEnabled = true,
      Function? onFoucus}) {
    // return CustomTextFormField(
    //   addContactButton: true,
    // );
    return TextFormField(
      onTap: (){
        if (onFoucus != null )
          onFoucus();
      },
        decoration: new InputDecoration(
          contentPadding: EdgeInsets.all(15),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(3.408)),
            borderSide: BorderSide(
              color: BORDER_COLOR,
              width: 0.9,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(3.408)),
            borderSide: BorderSide(
              color: BORDER_COLOR,
              width:0.9,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(3.408)),
            borderSide: BorderSide(
              color: BORDER_COLOR,
              width: 0.9,
            ),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(3.408)),
            borderSide: BorderSide(
              color: Colors.red,
              width: 0.9,
            ),
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(3.408)),
            borderSide: BorderSide(
              color: BORDER_COLOR,
              width: 0.9,
            ),
          ),
          hintText: hint,
        ),
        controller: controller,
        enabled: isEnabled,
        maxLength: maxLength,
        obscureText: isPassword!,
        style: MAJOR_TEXT_STYLE,
        keyboardType: inputType,
        enableInteractiveSelection: true,
        keyboardAppearance: Brightness.light,
        validator: (String? value) {
          if (errorMessage != null) {
            if (value!.isEmpty) {
              return errorMessage;
            }
          }
          if (value!.isNotEmpty &&
              expression != null &&
              !expression.hasMatch(value)) {
            return expErrorMessage;
          }
          if(maxValue!=null && minValue !=null) {
            if(! Util.isNumeric(value))
              return errorMessage;
            if (Util.isNumeric(value) && (double.parse(value) > maxValue ||
                double.parse(value) < minValue)) {
              return errorMessage;
            }
          }
          return null;
        });
  }
}
