import 'package:cashless/models/services/Service.dart';
import 'package:cashless/widgets/CustomWidgetBuilder.dart';
import 'package:flutter/cupertino.dart';

class ServiceFormBuilder {
  static Widget? buildBillForm() {}

  static Widget buildChargeForm(Key formKey, Service service) {
    TextEditingController accountController = TextEditingController();
    String accountLabel = service.serviceScenario.scenarioAccount.getLabel();
    Widget form = Form(
      key: formKey,
      child: Container(
        margin: EdgeInsets.all(0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: CustomWidgetBuilder.buildFormInputText(
                    controller: accountController,
                    expression: RegExp(r'(^(?:[+0]9)?[0-9]{10,12}$)'),hint: accountLabel)),
          ],
        ),
      ),
    );
    return form;
  }

  static Widget? buildChargePackageForm() {}

  static Widget? buildVoucherForm() {}
}
