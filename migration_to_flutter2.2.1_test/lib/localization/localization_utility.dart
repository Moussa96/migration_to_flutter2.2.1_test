import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class Localization {
  final Locale locale;
  Localization(this.locale);

  static Localization of(BuildContext context) {
    return Localizations.of<Localization>(context, Localization)!;
  }




   static late Map<String, String> _localizedValues;

  Future load() async {
    String jsonStringValues = await rootBundle.loadString('lib/lang/${locale.languageCode}.json');
    Map <String, dynamic> mappedJson = json.decode(jsonStringValues);
    _localizedValues = mappedJson.map((key, value) => MapEntry(key, value.toString()));
  }

  String getTranslatedValue(String key){
    return _localizedValues[key]!;
  }
  static const LocalizationsDelegate<Localization> delegate =  _DemoLocalizationsDelegate();
}

class _DemoLocalizationsDelegate extends LocalizationsDelegate<Localization> {
  const _DemoLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'ar'].contains(locale.languageCode);

  @override
  Future<Localization> load(Locale locale) async {
    Localization localization = new Localization(locale);
    await localization.load();
    return localization;
  }

  @override
  bool shouldReload(_DemoLocalizationsDelegate old) => false;
}

class Language{
  final int id;
  final String name;
  final String flag;
  final String languageCode;

  Language(this.id, this.flag, this.name, this.languageCode);

  static List<Language> LanguageList(){
    return <Language>[
      Language(1, '🇺🇸', 'English', 'en'),
      Language(2, '🇪🇬', 'العربية', 'ar')
    ];
  }
}

String getTranslated(BuildContext context, String key){
  if(key == null){
    return Localization.of(context).getTranslatedValue("no_translation");
  }
  return Localization.of(context).getTranslatedValue(key);
}