import 'package:cashless/Configurations.dart';
import 'package:cashless/localization/localization_utility.dart';
import 'package:cashless/pages/ChangeLanguage.dart';
import 'package:cashless/pages/QRregistration.dart';
import 'package:cashless/pages/SelectedNotification.dart';
import 'package:cashless/pages/PendingPaymentRequests.dart';
import 'package:cashless/pages/balance_transfer.dart';
import 'package:cashless/pages/cash_out.dart';
import 'package:cashless/pages/contacts.dart';
import 'package:cashless/pages/home/home.dart';
import 'package:cashless/pages/home/old_home.dart';
import 'package:cashless/pages/invoice_details.dart';
import 'package:cashless/pages/invoice_page.dart';
import 'package:cashless/pages/login/login_form.dart';
import 'package:cashless/pages/marketplace/old_market_place.dart';
import 'package:cashless/pages/marketplace/old_market_place_products.dart';
import 'package:cashless/pages/marketplace_requests.dart';
import 'package:cashless/pages/on_boarding/on_boarding.dart';
import 'package:cashless/pages/order.dart';
import 'package:cashless/pages/pay_through_scan_QR.dart';
import 'package:cashless/pages/payment_form.dart';
import 'package:cashless/pages/profile/old_profile.dart';
import 'package:cashless/pages/registration.dart';
import 'package:cashless/pages/load_wallet/old_load_wallet_form.dart';
import 'package:cashless/pages/static_purchase.dart';
import 'package:cashless/pages/transaction_history/old_transaction_history.dart';
import 'package:cashless/util/Util.dart';
import 'package:cashless/util/class_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:cashless/pages/reset_password/old_resetpassword.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:cashless/pages/DrawerMain.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Map<String, dynamic> userSession = await Util.getUserSession();
  Util.updateOnboardingCnt(userSession['onboarding']);
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    ClassBuilder.registerClasses();
    runApp(new MyApp(userSession: userSession));
  });
  //runApp(MyApp());
}

class MyApp extends StatefulWidget {
  Map<String, dynamic> userSession;
  static void setLocale(BuildContext context, Locale locale) {
    _MyAppState state = context.findAncestorStateOfType<_MyAppState>();
    state.setLocaleState(locale);
  }

  MyApp({Key key, @required this.userSession}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
// This widget is the root of your application.
}

class _MyAppState extends State<MyApp> {
  Locale _locale;
  void setLocaleState(Locale locale) {
    setState(() {
      widget.userSession['langCode'] = locale.languageCode;
      _locale = locale;
    });
  }

  @override
  Widget build(BuildContext context) {
    return OverlaySupport(
        child: MaterialApp(
            title: 'Cashless',
            initialRoute: '/',
            routes: {
              '/home': (context) => Home(),
              '/transactionHistory': (context) => TransactionHistory(),
              '/payment': (context) => PaymentForm(),
              '/login': (context) => LoginForm(),
              '/contacts': (context) => Contacts(),
              '/invoices': (context) => InvoicePage(),
              '/invoices/details': (context) => InvoiceDetails(),
              '/load_wallet': (context) => LoadWallet(),
              '/profile': (context) => Profile(),
              '/register': (context) => Registration(),
              '/transferBalance': (context) => BalanceTransfer(),
              '/cashout': (context) => CashOut(),
              '/scan': (context) => PayThroughScanQR(),
              '/marketplace': (context) => MarketPlace(),
              '/marketplaceproducts': (context) => MarketPlaceProducts(),
              '/pendingpaymentrequests': (context) => PendingPaymentRequests(),
              '/marketplacerequests': (context) => MarketPlaceRequests(),
              '/resetpassword': (context) => ResetPassword(),
              '/order': (context) => Order(),
              '/purchase': (context) => Purchase(),
              '/selectednotification': (context) => SelectedNotification(),
              '/changelanguage': (context) => ChangeLanguage(),
              '/qrregisteration': (context) => QRRegistration(),
            },
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              primarySwatch: MINOR_COLOR,
              visualDensity: VisualDensity.adaptivePlatformDensity,
            ),
            locale: _locale,
            supportedLocales: [Locale('en', 'US'), Locale('ar', 'EG')],
            localizationsDelegates: [
              Localization.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate
            ],
            localeResolutionCallback: (deviceLocale, supportedLocales) {
              if (widget.userSession['langCode'] != null &&
                  widget.userSession['langCode'] == 'ar')
                return supportedLocales.last;
              if (widget.userSession['langCode'] != null &&
                  widget.userSession['langCode'] == 'en')
                return supportedLocales.first;
              for (var locale in supportedLocales) {
                if (locale.languageCode == deviceLocale.languageCode &&
                    locale.countryCode == deviceLocale.countryCode) {
                  return deviceLocale;
                }
              }
              return supportedLocales.first;
            },
            home: Scaffold(
                backgroundColor: Colors.white, body: buildHomeBody())));
  }

  Widget buildHomeBody() {
    if (widget.userSession['rememberMe'] != null &&
        widget.userSession['rememberMe']) {
      if (widget.userSession['username'] != null &&
          widget.userSession['authorization'] != null) {
        if (widget.userSession['group'] != null &&
            widget.userSession['group'] == GROUP_CONSUMER)
          return ConsumerHome();
        if (widget.userSession['group'] != null &&
            (widget.userSession['group'] == GROUP_AGENT ||
                widget.userSession['group'] == GROUP_MERCHANT))
          return DrawerMain();
      }
    }
    if (widget.userSession['onboarding'] != null &&
        widget.userSession['onboarding'] >= 3) return LoginForm();
    return OnBoarding();
  }
}
